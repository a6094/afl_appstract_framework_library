﻿using System.Text.RegularExpressions;

namespace AFLUmbraco.Validation
{
	public static class ValidationUtils
	{
		public const string emailRegex = @"^[a-zA-Z0-9_\-.]+@[a-zA-Z0-9_\-.]+\.[a-zA-Z]{2,4}$";
		public static bool isValidEmail(string email) { return new Regex(emailRegex).IsMatch(email ?? ""); }
	}
}