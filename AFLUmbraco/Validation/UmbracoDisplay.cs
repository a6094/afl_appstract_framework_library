﻿using System;
using System.ComponentModel;

namespace AFLUmbraco.Validation
{
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public sealed class UmbracoDisplay : DisplayNameAttribute
    {
        // This is a positional argument
        public UmbracoDisplay(string dictionaryKey) : base(UmbracoUtils.UmbracoHelper.GetDictionaryValue(dictionaryKey))
        {
            
        }
    }
}
