﻿using System;

namespace AFLUmbraco.Validation
{
	public static class UmbracoValidationHelper
    {
        static UmbracoValidationHelper()
        {
        }

        public static string GetDictionaryItem(string errorMessageDictionaryKey)
        {
            return UmbracoUtils.Translate(errorMessageDictionaryKey);
        }

        public static string FormatErrorMessage(string name, string errorMessageDictionaryKey)
        {
            //Get dictionary value for thge required error message
            var error = UmbracoUtils.Translate(errorMessageDictionaryKey);
			
            // String replacment the token with our localised propertyname
            return error.Replace("{{Field}}", name);
        }

        public static string FormatRangeErrorMessage(string name, string errorMessageDictionaryKey, object min, object max)
        {
            //Get dictionary value for thge required error message
            var error = UmbracoUtils.Translate(errorMessageDictionaryKey);

			//Convert object to int's
            var minVal = Convert.ToInt32(min);
            var maxVal = Convert.ToInt32(max);

            // String replacment the token wiht our localised propertyname
            // The field {{Field}} must be between {0} and {1}
            error = error.Replace("{{Field}}", name);
            error = string.Format(error, minVal, maxVal);

            //Return the value
            return error;
        }

        public static string FormatCompareErrorMessage(string name, string errorMessageDictionaryKey, string otherProperty)
        {
            //Get dictionary value for thge required error message
            var error = UmbracoUtils.Translate(errorMessageDictionaryKey);
			
            //TODO - Somehow figure out
            //Get other property display name, but from UmbracoDisplay as getting C# property name
            

            // String replacment the token with our localised propertyname
            //'{{Field}}' and '{0}' do not match.
            error = error.Replace("{{Field}}", name);
            error = string.Format(error, otherProperty);

            //Return the value
            return error;
        }

        public static string FormatLengthErrorMessage(string name, string errorMessageDictionaryKey, int maxLength, int minLength)
        {
            //Get dictionary value for thge required error message
            var error = UmbracoUtils.Translate(errorMessageDictionaryKey);
			
            // it's ok to pass in the minLength even for the error message without a {2} param since String.Format will just
            // ignore extra arguments

            // String replacment the token wiht our localised propertyname
            // The field {{Field}} must be less than {0} (MaxLength)
            // The field {{Field}} must be less than {0} (MaxLength) & greater than {1} (MinLength)
            error = error.Replace("{{Field}}", name);
            error = string.Format(error, maxLength, minLength);

            //Return the value
            return error;
        }
        
    }
}
