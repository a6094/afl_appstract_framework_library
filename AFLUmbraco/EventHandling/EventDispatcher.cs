﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AFL;
using AFL.EventHandling;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace AFLUmbraco.EventHandling
{


	//class EventDispatcher : ApplicationEventHandler
	//{
	//	protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
	//	{
	//		AttachEvents();
	//	}

	//	public void AttachEvents()
	//	{
	//		ContentService.Created += (sender, args) => OnChange(new[] { args.Entity }, new CMSEventArgs(CMSEvent.Created, args, args.CanCancel));
	//		ContentService.Saved += (sender, args) => OnChange(args.SavedEntities, new CMSEventArgs(CMSEvent.Saved, args, args.CanCancel));
	//		ContentService.Published += (sender, args) => OnChange(args.PublishedEntities, new CMSEventArgs(CMSEvent.Published, args, args.CanCancel));
	//		ContentService.UnPublished += (sender, args) => OnChange(args.PublishedEntities, new CMSEventArgs(CMSEvent.Unpublished, args, args.CanCancel));
	//		ContentService.Trashed += (sender, args) => OnChange(args.MoveInfoCollection.Select(e => e.Entity), new CMSEventArgs(CMSEvent.Trashed, args, args.CanCancel));
	//		ContentService.Deleted += (sender, args) => OnChange(args.DeletedEntities, new CMSEventArgs(CMSEvent.Deleted, args, args.CanCancel));
	//		ContentService.Moved += (sender, args) => OnChange(args.MoveInfoCollection.Select(e => e.Entity), new CMSEventArgs(CMSEvent.Moved, args, args.CanCancel));
	//		//ContentService.Creating += (sender, args) => OnChange(new[] { args.Entity }, new CMSEventArgs(CMSEvent.Creating, args, args.CanCancel)); //deprecated
	//		ContentService.Saving += (sender, args) => OnChange(args.SavedEntities, new CMSEventArgs(CMSEvent.Saving, args, args.CanCancel));
	//		ContentService.Publishing += (sender, args) => OnChange(args.PublishedEntities, new CMSEventArgs(CMSEvent.Publishing, args, args.CanCancel));
	//		ContentService.UnPublishing += (sender, args) => OnChange(args.PublishedEntities, new CMSEventArgs(CMSEvent.Unpublishing, args, args.CanCancel));
	//		ContentService.Trashing += (sender, args) => OnChange(args.MoveInfoCollection.Select(e => e.Entity), new CMSEventArgs(CMSEvent.Trashing, args, args.CanCancel));
	//		ContentService.Deleting += (sender, args) => OnChange(args.DeletedEntities, new CMSEventArgs(CMSEvent.Deleting, args, args.CanCancel));
	//		ContentService.Moving += (sender, args) => OnChange(args.MoveInfoCollection.Select(e => e.Entity), new CMSEventArgs(CMSEvent.Moving, args, args.CanCancel));
	//	}

	//	private void OnChange(IEnumerable<IContent> contents, CMSEventArgs arg)
	//	{
	//		if (contents == null) { return; }
	//		foreach(var content in contents.Where(c => c != null))
	//		{
	//			var model = Registry.Current.GetDBBackedModelFromNativeDBType(content);
	//			if(model != null)
	//			{
	//				var methods = model.GetType().GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
	//				foreach (var method in methods)
	//				{
	//					var attr = (CMSEventHandlerAttribute)method.GetCustomAttributes(typeof(CMSEventHandlerAttribute), false).FirstOrDefault();
	//					if(attr != null && attr.events.Contains(arg.EventType))
	//					{
	//						if(method.GetParameters().Count() == 0)
	//						{
	//							method.Invoke(model, new object[] {});
	//						}
	//						else if(method.GetParameters().Count() == 1 && method.GetParameters().First().ParameterType == typeof(CMSEventArgs))
	//						{
	//							method.Invoke(model, new object[] { arg });
	//						}
	//						else
	//						{
	//							throw new TargetParameterCountException("Event handler must have either 0 parameters, or one parameter of type CMSEventArgs. Content type: " + content.ContentType.Alias);
	//						}

	//						if(arg.NativeEvent is CancellableEventArgs && arg.Canceled)
	//						{
	//							var nativeArg = (CancellableEventArgs)arg.NativeEvent;
	//							nativeArg.Cancel = true;
	//							nativeArg.Messages.Add(new EventMessage("", arg.CancelMessage));
	//						}
	//					}
	//				}
	//			}
	//		}
	//	}
	//}
}
