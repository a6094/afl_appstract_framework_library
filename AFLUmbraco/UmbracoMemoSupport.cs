﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AFL;
using AFL.Core;
using AFLUmbraco.Models;
using Archetype.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors.ValueConverters;
using Umbraco.Web;

namespace AFLUmbraco
{
	class UmbracoMemoSupport : MemoSupportBase
	{
		const string umbDocumentPrefix = "umb://document/";
		const string umbMemberPrefix = "umb://member/";
		const string umbMediaPrefix = "umb://media/";

		public override object GetValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse, Injector injector)
		{
			if (injector != null)
			{
				return injector.GetInstance(elementType);
			}

			return GetPropertyValue<T>(model, propertyInfo, callerName, elementType, recurse);
		}

		private object GetPropertyValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse)
		{
			var node = ((AFLModelBase)model).Content;
			var type = typeof(T);
			object value;
			if (string.IsNullOrEmpty(propertyInfo.CMSName))
			{
				throw new InvalidOperationException(model.GetType().Name + "." + callerName + " has no corresponding CMS property. (If you just added it, remember to recycle the app pool).");
			}
			if (IsSimpleType<T>())
			{
				//value = node.HasValue(propertyInfo.CMSName, recurse) ? (object)node.GetPropertyValue<T>(propertyInfo.CMSName, recurse) : default(T);
			}
			else if (typeof(ArchetypeBase[]).IsAssignableFrom(type))
			{
				//value = EnumerableToTypedArray(elementType, node.GetPropertyValue<ArchetypeModel>(propertyInfo.CMSName, recurse, null)?.Select(f => Activator.CreateInstance(elementType, model, f)) ?? Enumerable.Empty<object>());
			}
			else if (typeof(ArchetypeBase).IsAssignableFrom(type))
			{
				//value = node.GetPropertyValue<ArchetypeModel>(propertyInfo.CMSName, recurse, null)?.Select(f => Activator.CreateInstance(type, model, f)).FirstOrDefault() ?? Enumerable.Empty<object>().FirstOrDefault();
			}
			else if (typeof(NestedContentBase[]).IsAssignableFrom(type))
			{
				//value = EnumerableToTypedArray(elementType, node.GetPropertyValue<List<IPublishedContent>>(propertyInfo.CMSName, recurse, null)?.Select(n => Activator.CreateInstance(elementType, model, n)) ?? Enumerable.Empty<object>());
			}
			else if (typeof(NestedContentBase).IsAssignableFrom(type))
			{
				//value = node.GetPropertyValue<List<IPublishedContent>>(propertyInfo.CMSName, recurse, null)?.Select(n => Activator.CreateInstance(type, model, n)).FirstOrDefault() ?? null;
			}
			else if (typeof(UmbracoDynamicXmlEntity[]).IsAssignableFrom(type)) //deprecated in umbraco?
			{
				//var dynObj = node.GetPropertyValue<DynamicXml>(propertyInfo.CMSName, recurse, null);
				//var dynList = new List<DynamicXml>();
				//if (dynObj != null)
				//{
				//	foreach (var elem in dynObj)
				//	{
				//		dynList.Add(elem);
				//	}
				//}
				//value = EnumerableToTypedArray(elementType, dynList.Select(f => Activator.CreateInstance(elementType, f)));
			}
			else if(typeof(Umbraco.Core.Models.Membership.IUser).IsAssignableFrom(type))
			{
				//var userid = node.GetPropertyValue<int>(propertyInfo.CMSName, recurse);
				//value = ApplicationContext.Current.Services.UserService.GetUserById(Convert.ToInt32(userid));
			}
			else if(typeof(Umbraco.Core.Models.Membership.IUser[]).IsAssignableFrom(type))
			{
				//what format?
				throw new NotImplementedException("TODO handle user list: " + type.FullName);
			}
			else if (typeof(IMember).IsAssignableFrom(type))
			{
				//var memberid = node.GetPropertyValue<string>(propertyInfo.CMSName, recurse);
				//value = ApplicationContext.Current.Services.MemberService.GetByKey(UmbMemberGuidToGuid(memberid));
			}
			else if (typeof(IMember[]).IsAssignableFrom(type))
			{
				//var strongValue = node.GetPropertyValue<IEnumerable<IPublishedContent>>(propertyInfo.CMSName, recurse);
				//if(strongValue == null)
				//{
				//	var memberidList = node.GetPropertyValue<string>(propertyInfo.CMSName, recurse);
				//	value = memberidList == null ? (new List<IMember>()).ToArray() : memberidList.Split(',').Select(umbguid => ApplicationContext.Current.Services.MemberService.GetByKey(UmbMemberGuidToGuid(umbguid))).Where(m => m != null).ToArray();
				//}
				//else
				//{
				//	value = strongValue == null || strongValue.Count() == 0 ? (new List<IMember>()).ToArray() : strongValue.Select(mm => ApplicationContext.Current.Services.MemberService.GetByKey(mm.GetKey())).Where(m => m != null).ToArray();
				//}
			}
			else if (typeof(UmbracoColor).IsAssignableFrom(type))
			{
				//var strongValue = node.GetPropertyValue<ColorPickerValueConverter.PickedColor>(propertyInfo.CMSName, recurse);
				//if (strongValue == null || string.IsNullOrEmpty(strongValue.Color))
				//{
				//	var str = node.GetPropertyValue<string>(propertyInfo.CMSName, recurse);
				//	value = UmbracoColor.FromString(str);
				//}
				//else
				//{
				//	value = new UmbracoColor { Label = strongValue.Label, Value = strongValue.Color };
				//}
			}
			else if (typeof(UmbracoGrid).IsAssignableFrom(type))
			{
				//var str = node.GetPropertyValue<string>(propertyInfo.CMSName, recurse);
				//value = UmbracoGrid.Deserialize(str, model);
			}
			else
			{
				object data;
				try
				{
					//data = node.GetPropertyValue(propertyInfo.CMSName, recurse);

				}
				catch (Exception)
				{
					//data = node.GetProperty(propertyInfo.CMSName, recurse)?.DataValue;

				}
				//if (!GetComplexValueFromObject(data, type, out value, node))
				//{
				//	throw new NotImplementedException("Unsupported return type: " + type.FullName);
				//}
			}

			return new object(); //TODO: FIX
		}

		public override bool GetComplexValueFromObject(object objectValue, Type resultType, out object value, object contextNode)
		{
			if (objectValue == null)
			{
				value = null;
				return true;
			}
			else if (resultType.IsArray)
			{
				var elementType = resultType.GetElementType();
				if (objectValue is int[] && typeof(AFLModelBase[]).IsAssignableFrom(resultType))
				{
					//var list = ((int[])objectValue).Where(v => v != 0).Select(v => v.ToString()).ToArray();
					//value = EnumerableToTypedArray(elementType, list.Select(id => GetModel(id, contextNode)).Where(n => n!=null));
					value = new object();
					return true;
				}
				else if (objectValue is string[] && typeof(AFLModelBase[]).IsAssignableFrom(resultType))
				{
					//var list = ((string[])objectValue).Where(v => !string.IsNullOrEmpty(v)).ToArray();
					//value = EnumerableToTypedArray(elementType, list.Select(id => GetModel(id, contextNode)).Where(n => n != null));
					value = new object();
					return true;
				}
				else if(objectValue is int[] || objectValue is string[])
				{
					if(elementType == typeof(int))
					{
						//value = ((object[])objectValue).Select(v => Convert.ToInt32(v)).ToArray();
					value = new object();
						return true;
					}
					else if(elementType == typeof(string))
					{
						//value = ((object[])objectValue).Select(v => v?.ToString() ?? "").ToArray();
					value = new object();
						return true;
					}
				}
				else if ((objectValue is string || objectValue is JArray) && typeof(UmbracoJArrayEntity[]).IsAssignableFrom(resultType))
				{
					//var jArray = objectValue is JArray ? (JArray)objectValue : Newtonsoft.Json.JsonConvert.DeserializeObject<JArray>((string)objectValue) ?? new JArray();
					//value = this.EnumerableToTypedArray(elementType, jArray.Select(f => Activator.CreateInstance(elementType, f)));
					value = new object();
					return true;
				}
				else if((objectValue is string || objectValue.GetType().FullName == "nuPickers.Picker") && typeof(AFLModelBase[]).IsAssignableFrom(resultType))
				{
					value = EnumerableToTypedArray(elementType, ParseUmbracoNodeList(objectValue.ToString(), contextNode));
					return true;
				}
				else if (objectValue is IPublishedContent)
				{
					if (typeof(AFLModelBase).IsAssignableFrom(elementType))
					{
						value = new[] { Registry.Current.GetModel(objectValue) };
						return true;
					}
					else
					{
						throw new Exception("todo");
					}
				}
				else if (objectValue is IEnumerable<IPublishedContent>) //nested content (when used with normal AFLModelBase types, as opposed to NestedContentBase, and Multiple media picker in new umbraco versions
				{
					var nodes = ((IEnumerable<IPublishedContent>)objectValue);
					if (typeof(AFLModelBase).IsAssignableFrom(elementType))
					{
						//value = EnumerableToTypedArray(elementType, nodes.Select(n => GetModel(n)).Where(v => v != null));
						value = new object();
						return true;
					}
					else
					{
						throw new Exception("Unsupported Memo return type for NestedContent: " + resultType.FullName);
					}
				}
				else if (objectValue.GetType().Name == "Udi[]")
				{
					//value = EnumerableToTypedArray(elementType, ParseUmbracoNodeList(string.Join(",", ((IEnumerable<object>)objectValue).Select(udi => udi.ToString())), contextNode));
					value = new object();
					return true;
				}
				else if (objectValue.GetType().Name == "RelatedLinks") // special case of single result from array of related links
				{
					//value = ((IEnumerable<object>)objectValue).Select(x => FixRelatedLink(x, contextNode));
					//value = EnumerableToTypedArray(elementType, ((IEnumerable<object>)objectValue).Select(x => FixRelatedLink(x, contextNode)));
					value = new object();
					return true;
				}
				else if (objectValue is IEnumerable<object>)
				{
					//if there are any values in the result list, and the return type does not match, then we now have to re-deserialze the objectValue... hopefully we'll find a better solution in the future...
					//this was needed for the case of RelatedLinks on the Aktindsigtshaandbog project.
					var sampleValue = ((IEnumerable<object>)objectValue).FirstOrDefault();
					if (sampleValue != null && !(elementType.IsAssignableFrom(sampleValue.GetType())))
					{
						objectValue = SpecialHandlingForRelatedLinksAndSimilar(JsonConvert.SerializeObject(objectValue), resultType, elementType, contextNode);
					}
					//end silly hack :)

					value = EnumerableToTypedArray(elementType, (IEnumerable<object>)objectValue);
					return true;
				}
				else if(objectValue is string)
				{
					if(typeof(AFLModelBase).IsAssignableFrom(elementType))
					{
						value = EnumerableToTypedArray(elementType, ParseUmbracoNodeList((string)objectValue, contextNode));
					}
					else //e.g. related links - noramlly umbraco would deserialize these internally, but in at least one case it's been observed that a we end up with a string here, and in that case the below code handles it correctly...
					{
						value = EnumerableToTypedArray(elementType, SpecialHandlingForRelatedLinksAndSimilar((string)objectValue, resultType, elementType, contextNode));
					}
					return true;
				}
				
				throw new Exception("Type not supported: " + resultType?.FullName + ", value: " + objectValue?.GetType().FullName);
			}
			else if ((objectValue is string || objectValue is JArray) && typeof(UmbracoJArrayEntity).IsAssignableFrom(resultType))
			{
				var objValue = objectValue is JArray ? ((JArray)objectValue).FirstOrDefault() : Newtonsoft.Json.JsonConvert.DeserializeObject<JArray>((string)objectValue)?.FirstOrDefault();
				value = Activator.CreateInstance(resultType, objValue);
				return true;
			}
			else if (objectValue is string)
			{
				value = ParseUmbracoNodeList((string)objectValue, contextNode).FirstOrDefault();
				return true;
			}
			else if (objectValue is int || objectValue.GetType().FullName == "nuPickers.Picker")
			{
				if (typeof(AFLModelBase).IsAssignableFrom(resultType))
				{
					value = IsEmptyValue(objectValue, true) ? null : GetModel(objectValue.ToString(), contextNode);
					return true;
				}
				else
				{
					throw new Exception("todo");
				}
			}
			else if (objectValue is IPublishedContent)
			{
				if (typeof(AFLModelBase).IsAssignableFrom(resultType))
				{
					value = Registry.Current.GetModel(objectValue);
					return true;
				}
				else
				{
					throw new Exception("todo");
				}
			}
			else if (objectValue is IEnumerable<IPublishedContent>)
			{
				objectValue = ((IEnumerable<IPublishedContent>)objectValue).FirstOrDefault();
				if(objectValue == null)
				{
					value = null;
					return true;
				}

				if (typeof(AFLModelBase).IsAssignableFrom(resultType))
				{
					value = Registry.Current.GetModel(objectValue);
					return true;
				}
				else
				{
					throw new Exception("todo");
				}
			}
			else if (objectValue.GetType().Name == "Udi")
			{
				value = UmbGuidToNode(objectValue.ToString(), contextNode);
				return true;
			}
			else if (objectValue.GetType().Name == "Udi[]")
			{
				value = UmbGuidToNode(((IEnumerable<object>)objectValue).FirstOrDefault()?.ToString(), contextNode);
				return true;
			}
			else if (objectValue is int[] && typeof(AFLModelBase).IsAssignableFrom(resultType))
			{
				var id = ((int[])objectValue).FirstOrDefault().ToString();
				value = GetModel(id, contextNode);
				return true;
			}
			else if (objectValue is string[] && typeof(AFLModelBase).IsAssignableFrom(resultType))
			{
				var id = ((string[])objectValue).FirstOrDefault();
				value = GetModel(id, contextNode);
				return true;
			}
			else if (objectValue.GetType().Name == "RelatedLinks") // special case of single result from array of related links
			{
				//value = ((IEnumerable<object>)objectValue).Select(x => FixRelatedLink(x, contextNode)).FirstOrDefault();
				value = new object();

				return true;
			}
			else
			{
				throw new Exception("todo");
			}
		}

		private IEnumerable<object> SpecialHandlingForRelatedLinksAndSimilar(string objectValue, Type resultType, Type elementType, object contextNode)
		{
			if (!objectValue.Trim().StartsWith("[")) // might only happen for IContent and not IPublishedContent: case of Umbraco Multi Textstring
			{
				return objectValue.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
			}

			var objectList = (IEnumerable<object>)JsonConvert.DeserializeObject((string)objectValue, resultType) ?? new string[0];
			//if (elementType.FullName == "Umbraco.Web.Models.RelatedLink" && objectList.FirstOrDefault()?.GetType().FullName == "Umbraco.Web.Models.RelatedLink")
			//{
			//	return objectList.Select(x => FixRelatedLink(x, contextNode));
			//}
			return objectList;
		}

		private dynamic FixRelatedLink(dynamic relatedLink, object contextNode)
		{
			var link = (string)relatedLink.Link?.ToString();
			if (link?.StartsWith(umbDocumentPrefix) ?? false) {
				relatedLink.Link = ((AFLModelBase)UmbGuidToNode(link, contextNode)).RelativeUrl;
			}
			else if (relatedLink.IsInternal && Regex.IsMatch(link ?? "", "^[0-9]+$"))
			{
				relatedLink.Link = ((AFLModelBase)GetModel(link, contextNode)).RelativeUrl;
			}
			return relatedLink;
		}

		//handles various kinds of string formats that resolve to node lists.
		private IEnumerable<IAFLModel> ParseUmbracoNodeList(string value, object contextNode)
		{
			if (value.StartsWith("[")) //relevant for IContent access to nested content...
			{
				var parsedList = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>[]>(value);
				//var nodes = parsedList.Select(v => new DBDetachedContent(v));
				//return nodes.Select(n => GetModel(n)).Where(v => v != null).ToArray();
			}
			else if (value.StartsWith(umbDocumentPrefix)) //relevant for the new umbraco ID replacement scheme...
			{
				//return value.Split(',').Select(str => UmbGuidToNode(str, contextNode)).Where(n => n != null).ToArray();
			}
			else if (value.StartsWith(umbMediaPrefix)) //relevant for the new umbraco ID replacement scheme...
			{
				//return value.Split(',').Select(str => UmbMediaGuidToNode(str, contextNode)).Where(n => n != null).ToArray();
			}
			else //relevant for normal comma seperated id list
			{
				var nodes = value.Trim().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
				//return nodes.Select(n => GetModel(n, contextNode)).Where(v => v != null).ToArray();
			}
			//return new IEnumerable<AFLModel>();
		return null;
		}

		private Guid ParseGuidStringOrThrow(string strGuid)
		{
			Guid guid;
			var ok = Guid.TryParseExact(strGuid, "N", out guid);
			if (!ok)
			{
				throw new Exception("invalid guid: " + strGuid);
			}
			return guid;
		}

		private Guid UmbDocumentGuidToGuid(string umbGuid)
		{
			if (umbGuid == null) { return Guid.Empty; }
			var strGuid = umbGuid.Replace(umbDocumentPrefix, "");
			return ParseGuidStringOrThrow(strGuid);
		}

		private Guid UmbMediaGuidToGuid(string umbGuid)
		{
			if (umbGuid == null) { return Guid.Empty; }
			var strGuid = umbGuid.Replace(umbMediaPrefix, "");
			return ParseGuidStringOrThrow(strGuid);
		}

		private Guid UmbMemberGuidToGuid(string umbGuid)
		{
			if (umbGuid == null) { return Guid.Empty; }
			var strGuid = umbGuid.Replace(umbMemberPrefix, "");
			return ParseGuidStringOrThrow(strGuid);
		}

		private IAFLModel UmbGuidToNode(string umbGuid, object contextNode)
		{
			var guid = UmbDocumentGuidToGuid(umbGuid);
			if (guid == Guid.Empty)
			{
				return null;
			}
			return GetModel(guid, contextNode);
		}

		private IAFLModel UmbMediaGuidToNode(string umbGuid, object contextNode)
		{
			var guid = UmbMediaGuidToGuid(umbGuid);
			if (guid == Guid.Empty)
			{
				return null;
			}
			return GetModel(guid, contextNode, isMedia: true);
		}

		public override IAFLModel GetModel(Guid guid, object contextNode, bool isMedia = false)
		{
			IPublishedContent content;
			if (!isMedia && (contextNode is DBContent || contextNode is DBDetachedContent))
			{
				//var icontent = ApplicationContext.Current.Services.ContentService.GetById(guid);
				//if (icontent == null) { return null; }
				//content = new DBContent(icontent);
			}
			else
			{
				//content = isMedia ? UmbracoUtils.UmbracoHelper.TypedMedia(guid) : UmbracoUtils.UmbracoHelper.TypedContent(guid);
			}
			return GetModel(null);
		}

		public override IAFLModel GetModel(string id, object contextNode)
		{
			return (contextNode is DBContent || contextNode is DBDetachedContent) ? Registry.Current.GetDBBackedModel(id) : Registry.Current.GetModel(id);
		}

		public override IAFLModel GetModel(object content)
		{
			if (content == null) { return null; }
			return (content is DBContent || content is DBDetachedContent) ? Registry.Current.GetDBBackedModelFromWrappedNativeDBType(content) : Registry.Current.GetModel(content);
		}
	}
}