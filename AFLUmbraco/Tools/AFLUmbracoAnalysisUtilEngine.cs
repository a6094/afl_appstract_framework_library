﻿using System.Collections.Generic;
using System.Linq;
using AFL.Core;
using AFL.Tools;
using Umbraco.Core;
using Umbraco.Core.Models;


namespace AFL.Utils
{
	/// <summary>
	/// 
	/// </summary>
	[Rest("DocumentTypeAnalyser")]
	public class UmbracoAnalysisToolEngine
	{
		public enum CellTypes { AllowedAndPresent, Allowed, Present, Neither }

		public UmbracoAnalysisToolEngine()
		{
			AllContentTypes = ApplicationContext.Current.Services.ContentTypeService.GetAllContentTypes().Select(x => (ContentType)x).ToList();
			//AllContentTypes = Umbraco.Core.Composing.Current.Services.ContentTypeService.GetAll().Select(x => (ContentType)x).ToList();
			RootContentTypes = AllContentTypes.Where(x => x.Level == 1).Select(act => new UmbracoAnalysisToolElement(act)).ToList();
		}

		public List<ContentType> AllContentTypes { get; set; }
		public List<UmbracoAnalysisToolElement> RootContentTypes { get; set; }

		[RestMethod(DebugOnly = true, Default = true, Description = "Gets the document type overview as an HTML table(in string format)")]
		public HTMLActionResult GetHTMLTable()
		{
			HTMLBuilder html = new HTMLBuilder(true);
			var head = AllContentTypes.Select(elm => new Elem("div", elm.Name));
			List<List<Elem>> rows = new List<List<Elem>>();/*RootContentTypes.Select(rootElement => RenderAnalysisLevel(rootElement));*/
			foreach (var rootElement in RootContentTypes)
			{
				var rootrows = RenderAnalysisLevel(rootElement);
				rows.AddRange(rootrows);
			}

			html.Body.AddTable(rows, head);

			return new HTMLActionResult(html.Result);
		}

		private List<List<Elem>> RenderAnalysisLevel(UmbracoAnalysisToolElement analysisToolElement)
		{
			var level = new List<List<Elem>>();
			var row = new List<Elem>();
			row.Add(new Elem("div", analysisToolElement.Name, "td-div level level-" + analysisToolElement.Level));

			foreach (var elm in AllContentTypes)
			{
				var isPresent = analysisToolElement.IsPresentOn.Any(x => x.Key == elm.Alias);
				var isAllowed = analysisToolElement.AllowedContentTypes.Any(x => x == elm.Alias);

				var classString = "td-div";
				if (isPresent && isAllowed)
				{
					classString += "is-both-allowed-and-present";
				}
				else if (isPresent)
				{
					classString += "is-present";
				}
				else if (isAllowed)
				{
					classString += "is-allowed";
				}
				else
				{
					classString += "not-available";
				}

				row.Add(new Elem("div", isPresent || isAllowed ? "x" : "", classString));
			}

			level.Add(row);

			if (analysisToolElement.Children != null && analysisToolElement.Children.Any())
			{
				foreach (var childElement in analysisToolElement.Children)
				{
					level.AddRange(RenderAnalysisLevel(childElement));
				}
			}

			return level;
		}
	}

	public class UmbracoAnalysisToolElement
	{
		public UmbracoAnalysisToolElement(ContentType contentType)
		{
			Name = contentType.Name;
			Alias = contentType.Alias;
			Level = contentType.Level.ToString();
			if (contentType.AllowedContentTypes != null && contentType.AllowedContentTypes.Any())
			{
				AllowedContentTypes = contentType.AllowedContentTypes.Select(x => x.Alias).ToList();
			}
			else
			{
				AllowedContentTypes = new List<string>();
			}

			Children = ApplicationContext.Current.Services.ContentTypeService.GetContentTypeChildren(contentType.Id).Select(child => new UmbracoAnalysisToolElement((ContentType)child)).ToList();

			IsPresentOn = ApplicationContext.Current.Services.ContentService
								.GetContentOfContentType(contentType.Id)
									.Where(c => c.Published)
										.Select(p => p.Parent() ?? p as Content)
											.DistinctBy(x => x?.ContentTypeId)
												.Where(x => x != null && x.ContentType != null)
													.Select(x => new KeyValuePair<string, string>(x.ContentType.Alias, x.ContentType.Name))
														.ToList();
		}

		public List<UmbracoAnalysisToolElement> Children { get; set; }
		public string Name { get; set; }
		public string Alias { get; set; }
		public string Level { get; set; }
		public List<string> AllowedContentTypes { get; set; } //Populated with aliases 
		public List<KeyValuePair<string, string>> IsPresentOn { get; set; } //Populated with alias,name

	}
}
