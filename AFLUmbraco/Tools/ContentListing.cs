﻿using System.Linq;
using AFL;
using AFL.Core;
using AFL.Tools;
using AFL.Utils;
using Umbraco.Core;

namespace AFLUmbraco.Tools
{
	[Rest]
	public class ContentListing
	{
		[RestMethod(DebugOnly = true, Default = true, Description = "Lists all content from the CMS.")]
		public object List(string expression)
		{
			var cs = ApplicationContext.Current.Services.ContentService;

			var getParameterizedData = CodeExecutor.GetExecutor(expression ?? "0", "IContent c", new[] { "Umbraco.Core", "Umbraco.Core.Models" }, new[] { CodeExecutor.AssemblyPathFromType(typeof(Umbraco.Core.Attempt)) });

			var html = new HTMLBuilder(true);
			html.Body.AddChild(new Elem("form").AddText("(IContent c) => (").AddChild(new Elem("input").AddAttr("name", "expression").AddAttr("value", "c.Template==null ? \"\" : c.Template.Name")).AddText(") ").AddChild(new Elem("button", "Evaluate")));
			//html.Body.AddTable(cs.GetRootContent().SelectMany(c => cs.GetDescendants(c.Id).Concat(c)).OrderBy(c => c.Path).Select(c => new object[] {
			//	UmbracoUtils.UmbracoHelper.TypedContent(c.Id)!=null ? (object)new Elem("a", string.Concat(Enumerable.Repeat("- ", c.Level)) + c.Name).AddAttr("href", UmbracoUtils.UmbracoHelper.TypedContent(c.Id)?.Url) : string.Concat(Enumerable.Repeat("- ", c.Level)) + c.Name,
			//	c.Id,
			//	c.Path,
			//	c.ContentType.Name,
			//	//c.Template?.Name,
			//	getParameterizedData.Invoke(null, new object[] { c })
			//}), new[] { "Name", "ID", "Path", "Content Type", "Computed" });
			return new HTMLActionResult(html.Result);
		}
	}
}
