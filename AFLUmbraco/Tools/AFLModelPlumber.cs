﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using AFL;
using AFL.Core;
using AFL.Tools;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Pdb;
using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace AFLUmbraco.Tools
{
	[Rest("modelplumber")]
	public class AFLModelPlumber
	{
		Dictionary<string, AssemblyDefinition> cecilAssemblies = new Dictionary<string, AssemblyDefinition>();
		Dictionary<int, IContentType> contentTypeMap;
		List<IContentType> umbracoContentTypes;

		public AFLModelPlumber()
		{
			var cts = Umbraco.Core.ApplicationContext.Current.Services.ContentTypeService;
			//contentTypeMap = umbracoContentTypes.ToDictionary(t => t.Id);
		}

		[RestProperty]
		public bool IgnoreExisting { get; set; }
		[RestProperty]
		public bool WithComments { get; set; }

		[RestMethod(DebugOnly = true, Default = true, Description = "Helps creating the models needed for mapping the CMS types.")]
		public HTMLActionResult WriteModels(string rawUrl)
		{
			var html = new HTMLBuilder(true);
			html.Body.AddChild(new Elem("h2", "AFL Model Plumber"));
			var commentSection = new Elem("section");
			html.Body.AddChild(commentSection);

			commentSection.AddParagraph("This is a utility to map all the document types that are defined in the current Umbraco database to C# code.");
			commentSection.AddChild(new Elem("h4", "Options:"));
			commentSection.AddChild(new Elem("p").AddChild(new Elem("ul")
				.AddChild(new Elem("li").AddChild(new Elem("a", $"{(IgnoreExisting ? "Don't" : "Always")} print all models.").AddAttr("href", RestUrl.Make(this, includeModelProps: true).Override("IgnoreExisting", !IgnoreExisting).ToString())))
				.AddChild(new Elem("li").AddChild(new Elem("a", $"{(WithComments ? "Disable" : "Enable")} comments with extra info from Umbraco.").AddAttr("href", RestUrl.Make(this, includeModelProps: true).Override("WithComments", !WithComments).ToString())))
			));

			commentSection.AddParagraph("Note about Page Content: remember that no special handing is neccesary for custom page content types, since AFL will instantiate the correct type, all custom PageContents should derive from the same base, and the backoffice will limit which ones can be used. Just make a simple 'PageContent PageContent => Memo(Nav.FirstChild<PageContent>())' in you base Page");

			foreach (var contentType in umbracoContentTypes)
			{
				html.Body.AddChild(new Elem("h3", $"{contentType.Name} ({contentType.Alias})"));
				var info = Registry.Current.TryGetInfo(contentType.Alias);
				bool wasDefined = false;
				if (info != null)
				{
					var cecilType = GetCecilType(info.ModelType);
					html.Body.AddParagraph($"Defined in {GetSourcePath(cecilType)}.");
					var diff = WriteModelClassDiff(contentType, info, cecilType, WithComments);
					if (!string.IsNullOrWhiteSpace(diff))
					{
						html.Body.AddChild(new Elem("p").AddText("The following properties do not match:").AddChild(new Elem("a", "(fix it!)").AddAttr("href", RestUrl.Make(this, "WriteDiffToFile", queryString: $"alias={contentType.Alias}&redirect={HttpUtility.UrlEncode(rawUrl)}").ToString())));
						html.Body.AddChild(new Elem("textarea", diff).AddAttr("rows", "6").AddAttr("cols", "100").AddAttr("readonly", "1"));
					}
					wasDefined = true;
				}
				else
				{
					html.Body.AddParagraph($"{GetModelNameForAlias(contentType.Alias)} is not defined in code.");
				}
				if (!wasDefined || IgnoreExisting)
				{
					html.Body.AddChild(new Elem("p").AddText("Suggested class definition:").AddChild(new Elem("a", "(write it!)").AddAttr("href", RestUrl.Make(this, "WriteClassToFile", queryString: $"alias={contentType.Alias}&redirect={HttpUtility.UrlEncode(rawUrl)}").ToString())));
					string code = WriteModelClassCode(contentType);
					html.Body.AddChild(new Elem("textarea", code).AddAttr("rows", "20").AddAttr("cols", "100").AddAttr("readonly", "1"));
				}
			}
			return new HTMLActionResult(html.Result);
		}

		void CalcModelClassDiff(IContentType contentType, AFLTypeInfo info, TypeDefinition cecilType, out List<PropertyType> missingProps, out List<PropertyDefinition> orphanedCecilProps)
		{
			var propMap = info.GetRegisteredCMSProperties();
			missingProps = contentType.PropertyTypes.Where(p => !propMap.ContainsKey(p.Alias)).ToList();
			orphanedCecilProps = propMap.Keys.Where(n => !contentType.PropertyTypes.Any(p => p.Alias == n)).Select(n => cecilType.Properties.FirstOrDefault(tp => tp.Name == propMap[n].Name)).Where(cp => cp != null && IsMemoProperty(cp)).ToList();
		}

		string WriteModelClassDiff(IContentType contentType, AFLTypeInfo info, TypeDefinition cecilType, bool withComments)
		{
			List<PropertyType> missingProps;
			List<PropertyDefinition> orphanedCecilProps;
			CalcModelClassDiff(contentType, info, cecilType, out missingProps, out orphanedCecilProps);
			var builder = new CodeBuilder();
			if (missingProps.Any())
			{
				builder.Append($"//Missing properties:");
				foreach (var prop in missingProps)
				{
					builder.Append(GetCMSPropertyMemo(prop, withComments));
				}
			}
			if (orphanedCecilProps.Any())
			{
				builder.Append($"//Orphaned properties:");
				foreach (var codeProp in orphanedCecilProps)
				{
					builder.Append($"No such CMS property: {codeProp.Name}");
				}
			}

			return builder.ToString();
		}

		string GetModelNameForAlias(string alias)
		{
			var typeName = Registry.Current.TryGetInfo(alias)?.ModelType.Name ?? alias;
			return char.ToUpperInvariant(typeName.First()) + typeName.Substring(1);
		}

		string WriteModelClassCode(IContentType contentType)
		{
			var builder = new CodeBuilder();

			builder.Append("using AFL;");
			builder.Append("using AFLUmbraco.Models;");
			builder.Append("using Umbraco.Core.Models;");
			builder.Append("");

			string baseType = "AFLModelBase";
			if (contentType.ParentId > 0 && contentTypeMap.ContainsKey(contentType.ParentId))
			{
				baseType = GetModelNameForAlias(contentTypeMap[contentType.ParentId].Alias);
			}

			//TODO: only works in umbraco 7.6 and up...
			//var cts = Umbraco.Core.ApplicationContext.Current.Services.ContentTypeService;
			//var namespaceList = cts.GetContentTypeContainers(contentType) ?? new EntityContainer[] { };
			//string namespaces = Registry.Current.BusinessLogicAssembly.GetName().Name + "." + string.Join(".", namespaceList.Select(ns => ns.Name));
			string namespaces = Registry.Current.BusinessLogicAssembly.GetName().Name;

			builder.Append($"namespace {namespaces}");
			builder.Append("{", 1);
			var comments = builder.AppendBuilder();
			builder.Append("");

			builder.Append($"public class {GetModelNameForAlias(contentType.Alias)} : {baseType}");
			builder.Append("{", 1);
			builder.Append($"public {GetModelNameForAlias(contentType.Alias)}(IPublishedContent node) : base(node)");
			builder.Append("{", 1);
			builder.Append("}", -1);
			builder.Append("");

			foreach (var prop in contentType.PropertyTypes.ToList())
			{
				builder.Append(GetCMSPropertyMemo(prop, WithComments));
			}
			if (contentType.AllowedContentTypes.Any())
			{
				builder.Append(GetChildrenMemo(contentType, WithComments));
			}

			builder.Append("}", -1);
			builder.Append("}", -1);
			builder.Append("");

			return builder.ToString();
		}

		[RestMethod(HideFromHandlerList = true)]
		public RedirectToUrlActionResult WriteClassToFile(string alias, string redirect)
		{
			IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);
			var path = Path.GetFullPath(HttpRuntime.AppDomainAppPath + $"../{Registry.Current.BusinessLogicAssembly.GetName().Name}/{GetModelNameForAlias(contentType.Alias)}.cs");
			System.IO.File.WriteAllText(path, WriteModelClassCode(contentType));

			var projFilePath = Path.GetFullPath(HttpRuntime.AppDomainAppPath + $"../{Registry.Current.BusinessLogicAssembly.GetName().Name}/{Registry.Current.BusinessLogicAssembly.GetName().Name}.csproj");
			var editor = new SimpleSourceCodeEditor(projFilePath);
			editor.InsertBefore("\\<Compile Include\\=\\\"[A-Za-z0-9_]+\\.cs\" \\/\\>", $"<Compile Include=\"{GetModelNameForAlias(contentType.Alias)}.cs\" />\n");
			editor.Save();

			return new RedirectToUrlActionResult(redirect);
		}

		[RestMethod(HideFromHandlerList = true)]
		public RedirectToUrlActionResult WriteDiffToFile(string alias, string redirect)
		{
			IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);

			var info = Registry.Current.TryGetInfo(alias);
			var cecilType = GetCecilType(info.ModelType);
			var diff = WriteModelClassDiff(contentType, info, cecilType, WithComments);

			List<PropertyType> missingProps;
			List<PropertyDefinition> orphanedCecilProps;
			CalcModelClassDiff(contentType, info, cecilType, out missingProps, out orphanedCecilProps);

			var filePath = GetSourcePath(cecilType);
			var editor = new SimpleSourceCodeEditor(filePath);
			foreach (var prop in missingProps)
			{
				var regex = "^\\s*(public|protected) .+ [A-Za-z0-9_]+ \\=\\> Memo(\\<.+\\>)?\\(.*\\);";
				editor.InsertBefore(regex, editor.GetIndentAtLine(regex) + GetCMSPropertyMemo(prop, false) + "\n");
			}
			foreach (var codeProp in orphanedCecilProps)
			{
				editor.Replace($"^\\s*(public|protected) .+ {codeProp.Name} \\=\\> Memo(\\<.+\\>)?\\(.*\\);\\s*$", "");
			}
			editor.Save();

			return new RedirectToUrlActionResult(redirect);
		}

		AssemblyDefinition GetCecilAssembly(Type type)
		{
			var name = type.Assembly.FullName;
			if (!cecilAssemblies.ContainsKey(name))
			{
				var readerParameters = new ReaderParameters { ReadSymbols = Registry.AFLDebugMode, SymbolReaderProvider = new PdbReaderProvider() };
				cecilAssemblies[name] = AssemblyDefinition.ReadAssembly(new Uri(type.Assembly.CodeBase).LocalPath, readerParameters);
			}
			return cecilAssemblies[name];
		}

		private TypeDefinition GetCecilType(Type type)
		{
			return GetCecilAssembly(type).MainModule.Types.FirstOrDefault(tt => tt.Name == type.Name);
		}

		private string GetCodePropertyName(PropertyType prop)
		{
			return new string(prop.Alias.Select((c, idx) => idx == 0 ? char.ToUpper(c) : c).ToArray());
		}

		private string GetClosestCommonBase(string model1, string model2)
		{
			return "";
		}

		private string GetCommonBaseTypeName(IEnumerable<string> aliases)
		{
			string closest = aliases.FirstOrDefault() ?? "AFLModelBase";

			foreach (var alias in aliases)
			{
				if (alias != closest)
				{
					//todo
				}
			}

			return GetModelNameForAlias(closest);
		}

		private string GetChildrenMemo(IContentType contentType, bool printComments)
		{
			var aliases = contentType.AllowedContentTypes.Select(d => d.Alias);
			var type = GetCommonBaseTypeName(aliases);
			return $"public {type} Children => Memo<{type}>();{(printComments ? "// " + JsonConvert.SerializeObject(aliases) : "")}";
		}

		private string GetCMSPropertyMemo(PropertyType prop, bool printComments)
		{
			var dts = Umbraco.Core.ApplicationContext.Current.Services.DataTypeService;
			//var dts = Umbraco.Core.Composing.Current.Services.DataTypeService;
			//var def = dts.GetDataTypeDefinitionById(prop.DataTypeDefinitionId);
			//var pv = dts.GetPreValuesCollectionByDataTypeId(def.Id).FormatAsDictionary();

			//var comment = printComments ? $" // {prop.PropertyEditorAlias}/{def.Name}, {def.DatabaseType.ToString()}, pv: {JsonConvert.SerializeObject(pv)}".Replace("\n", " ") : "";
			string retType;

			switch (prop.PropertyEditorAlias)
			{
				//case "Numeric": retType = "int"; break;
				//case "Umbraco.MediaPicker": retType = GetModelNameForAlias("File"); break;
				//case "Umbraco.MultipleMediaPicker": retType = GetModelNameForAlias("File") + "[]"; break;
				//case "Umbraco.TrueFalse": retType = "bool"; break;
				//case "Umbraco.Slider": retType = "int"; break;
				//case "Umbraco.MultiNodeTreePicker": retType = (pv.ContainsKey("filter") && !string.IsNullOrEmpty(pv["filter"].Value) ? GetModelNameForAlias(pv["filter"].Value) : "AFLModelBase") + "[]"; break;
				//case "Umbraco.ContentPickerAlias": retType = (pv.ContainsKey("filter") && !string.IsNullOrEmpty(pv["filter"].Value) ? GetModelNameForAlias(pv["filter"].Value) : "AFLModelBase"); break;
				//case "Umbraco.DropDown": retType = "string"; break;
				//case "Umbraco.RelatedLinks": retType = "RelatedLink[]"; break;
				//case "Our.Umbraco.NestedContent": retType = "AFLModelBase[]"; break;
				default: retType = null; break;
			}

			if (retType == null)
			{
				//...?

				if (retType == null)
				{
					//check the underlaying type...
					//switch (def.DatabaseType)
					//{
					//	case DataTypeDatabaseType.Ntext: retType = "string"; break;
					//	case DataTypeDatabaseType.Nvarchar: retType = "string"; break;
					//	case DataTypeDatabaseType.Integer: retType = "int"; break;
					//	case DataTypeDatabaseType.Date: retType = "DateTime"; break;
					//	//case DataTypeDatabaseType.Decimal: retType = "decimal"; break;
					//	default: retType = "string"; break;
					//}
				}
			}

			var templateTag = retType == "string" ? "" : "<" + retType + ">";

			return $"public {retType} {GetCodePropertyName(prop)} => Memo{templateTag}();";//{comment}";
		}

		private bool IsMemoProperty(PropertyDefinition prop)
		{
			int stackHeight = 0;
			if (prop.GetMethod == null) { return false; }
			var instructions = prop.GetMethod.Body.Instructions;
			for (int i = 0; i < instructions.Count; ++i)
			{
				var ins = instructions[i];
				int newStackHeight = stackHeight + GetStackDelta(ins, stackHeight);

				if (stackHeight > 0 && newStackHeight == 0)
				{
					var call = i == 0 ? null : instructions[i - 1].Operand as MethodReference;
					if (call == null || call.Name != "Memo")
					{
						return false; //not memo!
					}

					var isBodyPropertyEnd = i == instructions.Count - 4 && ins.OpCode == OpCodes.Stloc_0 && instructions[i + 1].OpCode == OpCodes.Br_S && instructions[i + 2].OpCode == OpCodes.Ldloc_0 && instructions[i + 3].OpCode == OpCodes.Ret;
					var isInlinePropertyEnd = i == instructions.Count - 1 || ins.OpCode == OpCodes.Ret;
					if (!isBodyPropertyEnd && !isInlinePropertyEnd)
					{
						throw new InvalidOperationException("More than one statement!");
					}

					//yay memo call. check type of call (hacky)
					return call.Parameters.Count == 3 && call.Parameters[1].ParameterType.IsPrimitive;
				}

				stackHeight = newStackHeight;
			}
			return false;
		}

		int GetStackDelta(Instruction instruction, int oldStackHeight)
		{
			return GetPushDelta(instruction) - GetPopDelta(oldStackHeight, instruction);
		}

		static int GetPushDelta(Instruction instruction)
		{
			OpCode code = instruction.OpCode;
			switch (code.StackBehaviourPush)
			{
				case StackBehaviour.Push0:
					return 0;

				case StackBehaviour.Push1:
				case StackBehaviour.Pushi:
				case StackBehaviour.Pushi8:
				case StackBehaviour.Pushr4:
				case StackBehaviour.Pushr8:
				case StackBehaviour.Pushref:
					return 1;

				case StackBehaviour.Push1_push1:
					return 2;

				case StackBehaviour.Varpush:
					if (code.FlowControl == FlowControl.Call)
					{
						MethodReference method = (MethodReference)instruction.Operand;
						return method.ReturnType.MetadataType == MetadataType.Void ? 0 : 1;
					}

					break;
			}
			throw new ArgumentException("unknown instruction " + instruction.ToString());
		}

		int GetPopDelta(int stackHeight, Instruction instruction)
		{
			OpCode code = instruction.OpCode;
			switch (code.StackBehaviourPop)
			{
				case StackBehaviour.Pop0:
					return 0;
				case StackBehaviour.Popi:
				case StackBehaviour.Popref:
				case StackBehaviour.Pop1:
					return 1;

				case StackBehaviour.Pop1_pop1:
				case StackBehaviour.Popi_pop1:
				case StackBehaviour.Popi_popi:
				case StackBehaviour.Popi_popi8:
				case StackBehaviour.Popi_popr4:
				case StackBehaviour.Popi_popr8:
				case StackBehaviour.Popref_pop1:
				case StackBehaviour.Popref_popi:
					return 2;

				case StackBehaviour.Popi_popi_popi:
				case StackBehaviour.Popref_popi_popi:
				case StackBehaviour.Popref_popi_popi8:
				case StackBehaviour.Popref_popi_popr4:
				case StackBehaviour.Popref_popi_popr8:
				case StackBehaviour.Popref_popi_popref:
					return 3;

				case StackBehaviour.PopAll:
					return stackHeight;

				case StackBehaviour.Varpop:
					if (code.FlowControl == FlowControl.Call)
					{
						MethodReference method = (MethodReference)instruction.Operand;
						int count = method.Parameters.Count;
						if (method.HasThis && OpCodes.Newobj.Value != code.Value)
							++count;
						return count;
					}

					if (code.Value == OpCodes.Ret.Value)
						return 1; //assume non-void, since we are only interested in property getters.
					break;
			}
			throw new ArgumentException(instruction.ToString());
		}

		string GetSourcePath(TypeDefinition typeDefinition)
		{
			foreach (var prop in typeDefinition.Properties.Where(x => x.GetMethod?.HasBody ?? false))
			{
				foreach (var instruction in prop.GetMethod.Body.Instructions)
				{
					if (instruction.SequencePoint != null)
					{
						return instruction.SequencePoint.Document.Url;
					}
				}
			}
			foreach (var method in typeDefinition.Methods.Where(x => x.HasBody))
			{
				foreach (var instruction in method.Body.Instructions)
				{
					if (instruction.SequencePoint != null)
					{
						return instruction.SequencePoint.Document.Url;
					}
				}
			}
			return null;
		}
	}

	public class CodeBuilder
	{
		private StringBuilder builder = new StringBuilder();
		private int indent = 0;
		private List<CodeBuilder> nestedBuilders = new List<CodeBuilder>();

		public void Append(string val, int indentChange = 0)
		{
			if (indentChange < 0) //negative before.
			{
				ChangeIndent(indentChange);
			}

			builder.Append('\t', indent);
			builder.AppendLine(val);

			if (indentChange > 0) //positive after.
			{
				ChangeIndent(indentChange);
			}
		}

		public void ChangeIndent(int indentChange)
		{
			indent += indentChange;
			if (indent < 0) { throw new InvalidOperationException("negative indent"); }
		}

		public CodeBuilder AppendBuilder()
		{
			var nested = new CodeBuilder();
			nested.indent = indent;
			builder.Append($"$$CB:{nestedBuilders.Count}$$");
			nestedBuilders.Add(nested);
			return nested;
		}

		public new string ToString()
		{
			var result = builder.ToString();
			result = Regex.Replace(result, "\\$\\$CB:([0-9]+)\\$\\$", s => nestedBuilders[Convert.ToInt32(s.Groups[1].Value)].ToString());
			return result;
		}
	}

	public class SimpleSourceCodeEditor
	{
		string filePath;
		string content;
		public SimpleSourceCodeEditor(string filePath)
		{
			this.filePath = filePath;
			content = System.IO.File.ReadAllText(filePath);
		}

		public bool InsertBefore(string regex, string value)
		{
			var match = Regex.Match(content, regex, RegexOptions.Multiline);
			if (match != null)
			{
				content = content.Insert(match.Index, value);
				return true;
			}
			return false;
		}

		public bool Replace(string regex, string value)
		{
			var match = Regex.Match(content, regex, RegexOptions.Multiline);
			if (match != null)
			{
				content = content.Remove(match.Index, match.Length);
				content = content.Insert(match.Index, value);
				return true;
			}
			return false;
		}

		public string GetIndentAtLine(string regex)
		{
			var match = Regex.Match(content, regex, RegexOptions.Multiline);
			if (match != null)
			{
				return new string(content.Substring(content.LastIndexOf('\n', match.Index) + 1).TakeWhile(c => c == '\t' || c == ' ').ToArray());
			}
			return "";
		}

		public void Save()
		{
			System.IO.File.WriteAllText(filePath, content);
		}
	}
}
