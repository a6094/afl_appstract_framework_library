﻿using AFL;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models.Identity;
using Umbraco.Core.Models.Membership;
using Umbraco.Core.Security;
using Umbraco.Web.Mvc;

namespace AFLUmbraco.UmbracoAdLogin.ADFix
{
	public class UmbracoADLoginSurfaceController : SurfaceController
	{
		public const int loginValidityMinutes = 1;
		public async Task LoginAsync(string email, string guid)
		{

			// Check the validity of the request
			var repo = new InMemoryOneTimeToken();
			if (!repo.ConsumeOneTimeTokenAndValidate(guid, email))
			{
				Logger.Info(typeof(UmbracoADLoginSurfaceController), $"\nAdfsController Returned:\nToken validation failed: {guid}");
				return;
			}

			//var owin = Request.GetOwinContext();
			////var backOfficeManager = owin.GetBackOfficeSignInManager();
			////var userManager = owin.GetBackOfficeUserManager();

			////get the user
			//BackOfficeIdentityUser user = null;
			//user = userManager.FindByEmailAsync(email).Result;
			//if (user == null)
			//{
			//	// we have a new user. Create it.
			//	UserHelper.CreateUser(email);
			//	user = userManager.FindByEmailAsync(email).Result;
			//}
			//await backOfficeManager.SignInAsync(user, false, false);
			//HttpContext.Response.Redirect("/umbraco");
		}
	}
	class UserHelper
	{
		public static void CreateUser(string email)
		{
			//var us = ApplicationContext.Current.Services.UserService;
			//var user = us.CreateWithIdentity(email, email, "xxx", "Writers", true);
			//user.AddGroup(us.GetUserGroupByAlias("editor") as IReadOnlyUserGroup);
			//us.Save(user);
		}
	}
}
