﻿using AFL;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Logging;

namespace AFLUmbraco.UmbracoAdLogin.ADFix
{
	public static class UmbracoADFix
	{
		public static void LoginRedirect(string email, string userId)
		{
			var repo = new InMemoryOneTimeToken();
			var guid = repo.GenerateOneTimeToken(email, 5, true);
			int umbracoExternalLoginContainsUser = 0;
			if (!string.IsNullOrWhiteSpace(userId))
			{
				//var db = ApplicationContext.Current.DatabaseContext.Database;
				//var db = Umbraco.Core.Composing.d
				//umbracoExternalLoginContainsUser = db.Query<int>($"select count(*) from umbracoExternalLogin where userId = {userId}").FirstOrDefault();
			}
			
			if (umbracoExternalLoginContainsUser > 0)
			{
				// user will be null for a new login, so just create without linking, user will be linked on next login 
				// or when it presses the link button
				HttpContext.Current.Response.Redirect($"umbraco/surface/umbracoADLoginsurface/loginAsync?email={email}&guid={guid}");
			}
			else
			{
				//Serilog.Core.Logger.Info(typeof(UmbracoADFix), string.Format("\nEmail {0} Was not redireted\nUser ID: {1}\nEnd", email, userId));
			}
		}
	}
}