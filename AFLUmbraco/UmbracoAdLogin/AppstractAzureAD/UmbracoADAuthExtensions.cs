using AFLUmbraco.UmbracoAdLogin.ADFix;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using System;
using System.Globalization;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models.Membership;

namespace AFLUmbraco.UmbracoAdLogin.AppstractAzureAD
{
	public static class UmbracoADAuthExtensions
	{

		///  <summary>
		///  Configure ActiveDirectory sign-in
		///  </summary>
		///  <param name="app"></param>
		///  <param name="tenant"></param>
		///  <param name="clientId"></param>
		///  <param name="postLoginRedirectUri">
		///  The URL that will be redirected to after login is successful, example: http://mydomain.com/umbraco/;
		///  </param>
		///  <param name="issuerId">
		/// 
		///  This is the "Issuer Id" for you Azure AD application. This a GUID value and can be found
		///  in the Azure portal when viewing your configured application and clicking on 'View endpoints'
		///  which will list all of the API endpoints. Each endpoint will contain a GUID value, this is
		///  the Issuer Id which must be used for this value.        
		/// 
		///  If this value is not set correctly then accounts won't be able to be detected 
		///  for un-linking in the back office. 
		/// 
		///  </param>
		/// <param name="caption"></param>
		/// <param name="style"></param>
		/// <param name="icon"></param>
		/// <remarks>
		///  ActiveDirectory account documentation for ASP.Net Identity can be found:
		///  https://github.com/AzureADSamples/WebApp-WebAPI-OpenIDConnect-DotNet
		///  </remarks>
		//public static void ConfigureBackOfficeAzureActiveDirectoryAuth(this IAppBuilder app, 
		//	string tenant, string clientId, string postLoginRedirectUri, Guid issuerId,
		//	string caption = "appstract Azure Active Directory", string style = "btn-microsoft", string icon = "fa-appstract")
		//{
		//	var authority = string.Format(
		//		CultureInfo.InvariantCulture, 
		//		"https://login.windows.net/{0}", 
		//		tenant);

		//	var adOptions = new OpenIdConnectAuthenticationOptions
		//	{
		//		SignInAsAuthenticationType = Constants.Security.BackOfficeExternalAuthenticationType,
		//		ClientId = clientId,
		//		Authority = authority,
		//		RedirectUri = postLoginRedirectUri,
		//		AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive,
		//		Notifications = new OpenIdConnectAuthenticationNotifications
		//		{
		//			AuthorizationCodeReceived = async context =>
		//			{
		//				var userService = Services.UserService;

		//				var email = context.JwtSecurityToken.Claims.First(x => x.Type == "upn").Value;
		//				var issuer = context.JwtSecurityToken.Claims.First(x => x.Type == "iss").Value;
		//				var providerKey = context.JwtSecurityToken.Claims.First(x => x.Type == "sub").Value;
		//				var name = context.JwtSecurityToken.Claims.First(x => x.Type == "name").Value;
		//				var isAdmin = AFL.Registry.AFLDebugMode || context.JwtSecurityToken.Claims.Any(x => x.Type == "groups" && x.Value== "f115777c-a3a2-4d4f-ba23-7c2ee5f889fc");
		//				var userManager = context.OwinContext.GetUserManager<BackOfficeUserManager>();

		//				var user = userService.GetByEmail(email);

		//				if (user == null)
		//				{
		//					user = userService.CreateUserWithIdentity(email, email);
		//					user.Language = "en-US";
		//				}
		//				if (isAdmin)
		//				{
		//					user.AddGroup(userService.GetAllUserGroups().FirstOrDefault(g => g.Alias == "admin") as IReadOnlyUserGroup);
		//				}
		//				else
		//				{
		//					user.AddGroup(userService.GetAllUserGroups().FirstOrDefault(g => g.Alias == "writer") as IReadOnlyUserGroup);
		//				}
		//				userService.Save(user, false);

		//				var identity = await userManager.FindByEmailAsync(email);
		//				if (identity.Logins.All(x => x.ProviderKey != providerKey))
		//				{
		//					identity.Logins.Add(new Umbraco.Core.Models.Identity.IdentityUserLogin(issuer, providerKey, user.Id));
		//					identity.Name = name;
		//					await userManager.UpdateAsync(identity);
		//				}
		//				UmbracoADFix.LoginRedirect(email, user?.Id.ToString());
		//			}
		//		}
		//	};


		//	adOptions.ForUmbracoBackOffice(style, icon);
		//	adOptions.Caption = caption;
		//	//Need to set the auth tyep as the issuer path
		//	adOptions.AuthenticationType = string.Format(
		//		CultureInfo.InvariantCulture,
		//		"https://sts.windows.net/{0}/",
		//		issuerId);
		//	app.UseOpenIdConnectAuthentication(adOptions); 
		//}
	}
}