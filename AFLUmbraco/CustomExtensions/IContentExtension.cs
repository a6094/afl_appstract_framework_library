﻿using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace AFLUmbraco.CustomExtensions
{
	// ReSharper disable once InconsistentNaming
	public static class IContentExtension
	{
		/// <summary>
		/// Determines whether this instance has changed.
		/// </summary>
		/// <param name="node">The node.</param>
		/// <returns>
		///   <c>true</c> if the specified node has changed; otherwise, <c>false</c>.
		/// </returns>
		public static bool HasChanged(this IContent node)
		{
			ContentService cs = (ContentService)ApplicationContext.Current.Services.ContentService;

			IContent previewsVersion = cs.GetVersions(node.Id).Skip(1).FirstOrDefault();

			if (previewsVersion == null)
			{
				return true;
			}

			foreach (Property item in node.Properties)
			{
				var valueNodeCurrentProperty = node.GetValue(item.Alias)?.ToString();
				var valuePreviewsVersionCurrentProperty = previewsVersion.GetValue(item.Alias)?.ToString();

				if ((valueNodeCurrentProperty.IsNullOrWhiteSpace() && valuePreviewsVersionCurrentProperty.IsNullOrWhiteSpace())
					|| valueNodeCurrentProperty == valuePreviewsVersionCurrentProperty)
				{
					continue;
				}

				return true;
			}

			return false;
		}
	}
}