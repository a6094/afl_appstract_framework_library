﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using AFL;
using Archetype.Models;
using Umbraco.Web;

namespace AFLUmbraco.Models
{
	public abstract class ArchetypeBase : IAFLModel
	{
		protected readonly ArchetypeFieldsetModel fieldset;
		protected readonly AFLModelBase container;

		protected UmbracoHelper UmbracoHelper => UmbracoUtils.UmbracoHelper;

		public ArchetypeBase(AFLModelBase container, ArchetypeFieldsetModel fieldset)
		{
			this.container = container;
			this.fieldset = fieldset;
		}

		//for cases like: string AddressLine1 => Memo();
		protected string Memo(string fallback = null, [CallerMemberName] string callerName = "")
		{
			return MemoImplemenation<string, string>(null, null, fallback, callerName);
		}

		protected T Memo<T>(Func<T, T> postProcess = null, Func<T> customFunc = null, T fallback = default(T), [CallerMemberName] string callerName = "")
		{
			return MemoImplemenation(customFunc, postProcess, fallback, callerName);
		}

		protected TPost Memo<TPre, TPost>(Func<TPre, TPost> postProcess = null, Func<TPre> customFunc = null, TPost fallback = default(TPost), [CallerMemberName] string callerName = "")
		{
			return MemoImplemenation<TPre, TPost>(customFunc, postProcess, fallback, callerName);
		}

		private TPost MemoImplemenation<TPre, TPost>(Func<TPre> valueGetterFunc = null, Func<TPre, TPost> postProcessFunc = null, TPost fallback = default(TPost), string callerName = "")
		{
			var propertyInfo = Registry.Current.GetInfo(GetType()).GetProperty(callerName);

			if (valueGetterFunc == null)
			{
				valueGetterFunc = () =>
				{
					object value = null;
					var valueType = typeof(TPre);
					if (Registry.Current.MemoSupport.IsSimpleType<TPre>())
					{
						value = fieldset.GetValue<TPre>(propertyInfo.CMSName);
					}
					else if (typeof(ArchetypeBase[]).IsAssignableFrom(valueType))
					{
						var elementType = valueType.IsArray ? valueType.GetElementType() : valueType;
						//value = Registry.Current.MemoSupport.EnumerableToTypedArray(elementType, fieldset.GetValue<ArchetypeModel>(propertyInfo.CMSName)?.Select(f => Activator.CreateInstance(elementType, container, f)) ?? Enumerable.Empty<object>());
					}
					else if (typeof(ArchetypeBase).IsAssignableFrom(valueType)) // case to support single archetype element - this might be better handled by supporting pretype/posttype, like AFLModelBase.
					{
						//value = fieldset.GetValue<ArchetypeModel>(propertyInfo.CMSName)?.Select(f => Activator.CreateInstance(valueType, container, f)).FirstOrDefault() ?? Enumerable.Empty<object>().FirstOrDefault();
					}
					else if (!Registry.Current.MemoSupport.GetComplexValueFromObject(fieldset.GetValue<object>(propertyInfo.CMSName), valueType, out value, container.Content))
					{
						throw new NotImplementedException("Unsupported return type: " + typeof(TPre).FullName);
					}
					return (TPre)value;
				};
			}

			return Registry.Current.MemoSupport.MemoImplementation(valueGetterFunc, postProcessFunc, fallback, UID, callerName, propertyInfo);
		}

		public string UID => fieldset.Id.ToString();

		public string ViewTypeName
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public string Translate(string key)
		{
			return UmbracoUtils.Translate(key);
		}
	}
}
