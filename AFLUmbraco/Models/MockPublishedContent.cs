﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace AFLUmbraco.Models
{
	public class MockPublishedContent : IPublishedContent
	{
		private static Dictionary<string, object> globalFallbackProperties = new Dictionary<string, object> { { "_", "Lorem Ipsum" } };
		private static Dictionary<int, Dictionary<string, object>> globalContentCache = new Dictionary<int, Dictionary<string, object>>();

		private string url;
		private Dictionary<string, object> properties;
		public MockPublishedContent(object props = null, string url = "http://localhost")
		{
			this.url = url;
			MockRequest = new HttpRequest(null, url, null);
			properties = MakeDictionary(props ?? new { });
			if (properties.ContainsKey("Id") && (int)properties["Id"] == 119)
			{

			}

			MergeIntoCache(properties);
			if (!properties.ContainsKey("_")) { properties["_"] = globalFallbackProperties["_"]; }
		}

		public HttpRequest MockRequest { get; protected set; }

		public static MockPublishedContent TypedContent(int id)
		{
			return new MockPublishedContent(new { Id = id });
		}

		public static void SetGlobalFallbacks(object fb)
		{
			globalFallbackProperties = MakeDictionary(fb);
			MergeIntoCache(globalFallbackProperties);
			if (!globalFallbackProperties.ContainsKey("_")) { globalFallbackProperties["_"] = "Lorem Ipsum"; }
		}

		private static void MergeIntoCache(Dictionary<string, object> properties)
		{
			foreach (var key in properties.Keys)
			{
				if (Regex.IsMatch(key, "^node[0-9]+$"))
				{
					var dict = MakeDictionary(properties[key]);
					dict["Id"] = int.Parse(key.Replace("node", ""));
					MergeIntoCache(dict);
				}
			}

			if (properties.ContainsKey("Id"))
			{
				int id = (int)properties["Id"];
				if (!globalContentCache.ContainsKey(id))
				{
					globalContentCache[id] = new Dictionary<string, object>();
				}

				foreach (var key in properties.Keys)
				{
					globalContentCache[id][key] = properties[key];
				}

				//now copy back, to ensure that the current properties has everything from any old version in the cache...
				foreach (var key in globalContentCache[id].Keys)
				{
					properties[key] = globalContentCache[id][key];
				}
			}
		}

		private static Dictionary<String, object> MakeDictionary(object dynObj)
		{
			if (dynObj is Dictionary<string, object>)
			{
				return dynObj as Dictionary<string, object>;
			}

			var dictionary = new Dictionary<string, object>();
			foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(dynObj))
			{
				object obj = propertyDescriptor.GetValue(dynObj);
				dictionary.Add(propertyDescriptor.Name, obj);
			}
			return dictionary;
		}


		public IEnumerable<IPublishedContent> Children
		{
			get
			{
				var children = properties.ContainsKey("Children") ? (IEnumerable<object>)properties["Children"] : Enumerable.Empty<object>();
				return children.Select(c => new MockPublishedContent(props: c, url: url));
			}
		}

		public IEnumerable<IPublishedContent> ContentSet
		{
			get { throw new NotImplementedException(); }
		}

		public global::Umbraco.Core.Models.PublishedContent.PublishedContentType ContentType
		{
			get { throw new NotImplementedException(); }
		}

		public DateTime CreateDate
		{
			get { return properties.ContainsKey("CreateDate") ? (DateTime)properties["CreateDate"] : DateTime.Now; }
		}

		public int CreatorId
		{
			get { throw new NotImplementedException(); }
		}

		public string CreatorName
		{
			get { throw new NotImplementedException(); }
		}

		public string DocumentTypeAlias
		{
			get { throw new NotImplementedException(); }
		}

		public int DocumentTypeId
		{
			get { throw new NotImplementedException(); }
		}

		public int GetIndex()
		{
			throw new NotImplementedException();
		}

		public IPublishedProperty GetProperty(string alias, bool recurse)
		{
			return new MockPublishedProperty(properties.ContainsKey(alias) ? properties[alias] : (globalFallbackProperties.ContainsKey(alias) ? globalFallbackProperties[alias] : properties["_"]));
		}

		public IPublishedProperty GetProperty(string alias)
		{
			return new MockPublishedProperty(properties.ContainsKey(alias) ? properties[alias] : (globalFallbackProperties.ContainsKey(alias) ? globalFallbackProperties[alias] : properties["_"]));
		}

		public int Id
		{
			get { return 12345678; }
		}

		public bool IsDraft
		{
			get { throw new NotImplementedException(); }
		}

		public PublishedItemType ItemType
		{
			get { throw new NotImplementedException(); }
		}

		public int Level
		{
			get { return properties.ContainsKey("Level") ? (int)properties["Level"] : 4; }
		}

		public string Name
		{
			get { return "Mock Entity"; }
		}

		public IPublishedContent Parent
		{
			get
			{
				if (Level == 1)
				{
					return null;
				}
				else
				{
					var dict = properties.ContainsKey("Parent") ? MakeDictionary(properties["Parent"]) : new Dictionary<string, object>();
					dict["Level"] = Level - 1;
					return new MockPublishedContent(props: dict, url: url);
				}
			}
		}

		public string Path
		{
			get { throw new NotImplementedException(); }
		}

		public ICollection<IPublishedProperty> Properties
		{
			get { throw new NotImplementedException(); }
		}

		public int SortOrder
		{
			get { throw new NotImplementedException(); }
		}

		public int TemplateId
		{
			get { throw new NotImplementedException(); }
		}

		public DateTime UpdateDate
		{
			get { throw new NotImplementedException(); }
		}

		public string Url
		{
			get { return MockRequest.RawUrl; }
		}

		public string UrlName
		{
			get { throw new NotImplementedException(); }
		}

		public Guid Version
		{
			get { throw new NotImplementedException(); }
		}

		public int WriterId
		{
			get { throw new NotImplementedException(); }
		}

		public string WriterName
		{
			get { throw new NotImplementedException(); }
		}

		public string UrlSegment => throw new NotImplementedException();

		//int? IPublishedContent.TemplateId => throw new NotImplementedException();

		//public IReadOnlyDictionary<string, PublishedCultureInfo> Cultures => throw new NotImplementedException();

		//public Guid Key => throw new NotImplementedException();

		//IEnumerable<IPublishedProperty> IPublishedElement.Properties => throw new NotImplementedException();

		public object this[string alias]
		{
			get { throw new NotImplementedException(); }
		}

		public string MockTranslate(string key)
		{
			//if (GetProperty(key).Value != GetProperty("_").Value)
			//{
			//	return GetProperty(key).Value.ToString();
			//}
			//if (GetProperty("tr").Value != GetProperty("_").Value)
			//{
			//	var dict = MakeDictionary(GetProperty("tr").Value);
			//	if (dict.ContainsKey(key))
			//	{
			//		return dict[key].ToString();
			//	}
			//}
			return key;
		}

		public string GetUrl(string culture = null)
		{
			throw new NotImplementedException();
		}

		//public PublishedCultureInfo GetCulture(string culture = null)
		//{
		//	throw new NotImplementedException();
		//}

		//bool IPublishedContent.IsDraft(string culture)
		//{
		//	throw new NotImplementedException();
		//}

		public bool IsPublished(string culture = null)
		{
			throw new NotImplementedException();
		}
	}

	public class MockPublishedProperty : IPublishedProperty
	{
		private object value;

		public MockPublishedProperty(object value)
		{
			this.value = value;
		}

		public object DataValue
		{
			get { return value; }
		}

		public bool HasValue
		{
			get { return value != null; }
		}

		public string PropertyTypeAlias
		{
			get { throw new NotImplementedException(); }
		}

		public object Value
		{
			get { return value; }
		}

		public object XPathValue
		{
			get { throw new NotImplementedException(); }
		}

		public PublishedPropertyType PropertyType => throw new NotImplementedException();

		public string Alias => throw new NotImplementedException();

		public object GetSourceValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		public object GetValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		public object GetXPathValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		//bool IPublishedProperty.HasValue(string culture, string segment)
		//{
		//	throw new NotImplementedException();
		//}
	}
}
