﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace AFLUmbraco.Models
{
	//used by AFL in preference to the PublishedContentExtensions.Siblings method, so that we an get around a problem for the case of root elements.
	public interface IContentWithSiblings
	{
		IEnumerable<IPublishedContent> Siblings();
	}

	public interface ISettableContent
	{
		void SetProperty(string property, object value);
		T GetRawPropertyValue<T>(string property);
		void SetName(string value);
		void Save(bool raiseEvents);
		void Publish();
	}

	//public class DBContent : IPublishedContentWithKey, IContentWithSiblings, ISettableContent
	public class DBContent :  IContentWithSiblings, ISettableContent
	{
		private IContent content;
		public DBContent(IContent c)
		{
			if (c == null) { throw new ArgumentException("DBContent instantiated with null backing-content"); }
			content = c;
		}

		public DBContent(int id)
		{
			//content = ApplicationContext.Current.Services.ContentService.GetById(id);
		}

		public IContent BackingContent => content;

		//public IEnumerable<IPublishedContent> Children
		//{
		//	get
		//	{
		//		return content.Children().Select(c => new DBContent(c)).ToArray();
		//	}
		//}

		public IEnumerable<IPublishedContent> Siblings()
		{
			var parent = Parent;
			//var siblings = parent == null ? ApplicationContext.Current.Services.ContentService.GetRootContent().Select(c => new DBContent(c)) : parent.Children;
			var siblings = parent.Children;

			// make sure we run it once
			return siblings.ToArray();
		}

		public IEnumerable<IPublishedContent> ContentSet
		{
			get { throw new NotImplementedException(); }
		}

		public global::Umbraco.Core.Models.PublishedContent.PublishedContentType ContentType
		{
			get { throw new NotImplementedException(); }
		}

		public DateTime CreateDate => content.CreateDate;

		public int CreatorId => content.CreatorId;

		public string CreatorName
		{
			get { throw new NotImplementedException(); }
		}

		public string DocumentTypeAlias => content.ContentType.Alias;

		public int DocumentTypeId => content.ContentType.Id;

		public int GetIndex()
		{
			return 0;
		}

		public IPublishedProperty GetProperty(string alias, bool recurse)
		{
			try
			{
				var c = content;

				var prop = c.Properties[alias];
				//var value = TransmogrifyValue(prop.Value, prop.PropertyType);

				//while (value == null && recurse)
				//{
				//	c = c.Parent();
				//	if (c == null) { break; }
				//	prop = c.Properties[alias];
				//	value = TransmogrifyValue(prop.Value, prop.PropertyType);
				//}

				//return new DBProperty(alias, value);
				return new DBProperty(alias, null);
			}
			catch (Exception e)
			{
				return null;
			}
		}

		public static object TransmogrifyValue(object value, PropertyType type)
		{
			if (type?.PropertyEditorAlias == "Umbraco.DropDown")
			{
				//var preValues = ApplicationContext.Current.Services.DataTypeService.GetPreValuesCollectionByDataTypeId(type.Id);
				//var preValue = preValues.IsDictionaryBased ? (preValues.PreValuesAsDictionary.ContainsKey(value.ToString()) ? preValues.PreValuesAsDictionary[value.ToString()] : null) : preValues.PreValuesAsArray.FirstOrDefault(a => a.Id.ToString() == value.ToString());
				//return preValue.Value;

				//if the above fails, then use: ApplicationContext.Current.Services.DataTypeService.GetPreValueAsString(Convert.ToInt32(value));
			}
			return value;
		}

		public IPublishedProperty GetProperty(string alias)
		{
			try
			{
				return new DBProperty(alias, content.GetValue(alias));
			}
			catch (Exception e)
			{
				return null;
			}
		}

		public void SetProperty(string property, object value)
		{
			BackingContent.SetValue(property, value);
		}

		public void SetName(string value)
		{
			BackingContent.Name = value;
		}

		public void Save(bool raiseEvents)
		{
			//ApplicationContext.Current.Services.ContentService.Save(BackingContent, raiseEvents: raiseEvents);
		}

		public void Publish()
		{
			//ApplicationContext.Current.Services.ContentService.Publish(BackingContent);
		}

		public T GetRawPropertyValue<T>(string property)
		{
			return BackingContent.GetValue<T>(property);
		}

		public int Id => content.Id;

		public bool IsDraft => false;

		public PublishedItemType ItemType => PublishedItemType.Content;

		public int Level => content.Level;

		public string Name => content.Name;

		public IPublishedContent Parent
		{
			get
			{
				//var parent = content.Parent();
				var parent = content;
				return null;//parent == null ? null : new DBContent(parent);
			}
		}

		public string Path
		{
			get { throw new NotImplementedException(); }
		}

		//public ICollection<IPublishedProperty> Properties
		//{
		//	get { return content.Properties.Select(p => new DBProperty(p.Alias, p.Value)).ToArray(); }
		//}

		public int SortOrder => content.SortOrder;

		//public int TemplateId => content.Template?.Id ?? -1;

		public DateTime UpdateDate => content.UpdateDate;

		public string Url => "";

		public string UrlName
		{
			get { throw new NotImplementedException(); }
		}

		//public Guid Version => content.Version;

		public int WriterId => content.WriterId;

		public string WriterName
		{
			get { throw new NotImplementedException(); }
		}

		public Guid Key => BackingContent.Key;

		public object this[string alias]
		{
			get { throw new NotImplementedException(); }
		}
	}

	public class DBDetachedContent : IPublishedContent, ISettableContent
	{
		public const string ncContentTypeAlias = "ncContentTypeAlias";
		private Dictionary<string, object> data;
		public DBDetachedContent(Dictionary<string, object> data)
		{
			this.data = data;
		}

		public Dictionary<string, object> GetData() { return data; }

		public object this[string alias]
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public IEnumerable<IPublishedContent> Children => Enumerable.Empty<IPublishedContent>();

		public IEnumerable<IPublishedContent> ContentSet
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public PublishedContentType ContentType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public DateTime CreateDate
		{
			get { return DateTime.MinValue; }
		}

		public int CreatorId
		{
			get
			{
				return 0;
			}
		}

		public string CreatorName
		{
			get
			{
				return null;
			}
		}

		public string DocumentTypeAlias => data.ContainsKey(ncContentTypeAlias) ? (data[ncContentTypeAlias] ?? "").ToString() : null;

		public int DocumentTypeId
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public int Id
		{
			get
			{
				return 0;
			}
		}

		public bool IsDraft
		{
			get
			{
				return false;
			}
		}

		public PublishedItemType ItemType => PublishedItemType.Content;

		public int Level
		{
			get
			{
				return 0;
			}
		}

		public string Name
		{
			get
			{
				return "TODO";//GetProperty("name").Value?.ToString();
			}
		}

		public IPublishedContent Parent
		{
			get { return null; }
		}

		public string Path
		{
			get { return null; }
		}

		public ICollection<IPublishedProperty> Properties
		{
			get { return null; }//data.Select(p => new DBProperty(p.Key, p.Value)).ToArray(); }
		}

		public int SortOrder
		{
			get
			{
				return 0;
			}
		}

		public int TemplateId
		{
			get
			{
				return 0;
			}
		}

		public DateTime UpdateDate
		{
			get
			{
				return DateTime.MinValue;
			}
		}

		public string Url
		{
			get
			{
				return "";
			}
		}

		public string UrlName
		{
			get
			{
				return "";
			}
		}

		public Guid Version
		{
			get
			{
				return Guid.Empty;
			}
		}

		public int WriterId
		{
			get
			{
				return 0;
			}
		}

		public string WriterName
		{
			get
			{
				return null;
			}
		}

		public string UrlSegment => throw new NotImplementedException();

		//int? IPublishedContent.TemplateId => throw new NotImplementedException();

		//public IReadOnlyDictionary<string, PublishedCultureInfo> Cultures => throw new NotImplementedException();

		//public Guid Key => throw new NotImplementedException();

		//IEnumerable<IPublishedProperty> IPublishedElement.Properties => throw new NotImplementedException();

		public int GetIndex()
		{
			throw new NotImplementedException();
		}

		public IPublishedProperty GetProperty(string alias)
		{
			//todo - caching...

			object value = data.ContainsKey(alias) ? data[alias] : null;
			if(!string.IsNullOrEmpty(DocumentTypeAlias))
			{
				//var doctype = ApplicationContext.Current.Services.ContentTypeService.GetContentType(DocumentTypeAlias);
				var proptype = new object();//doctype.PropertyTypes.FirstOrDefault(p => p.Alias == alias);
				//value = DBContent.TransmogrifyValue(value, proptype);
				value = DBContent.TransmogrifyValue(value, null);
			}
			return new DBProperty(alias, value);
		}

		public IPublishedProperty GetProperty(string alias, bool recurse)
		{
			return GetProperty(alias);
		}

		public void Save(bool raiseEvents)
		{
			throw new InvalidOperationException("Detached content can not be saved directly.");
		}

		public void Publish()
		{
			throw new InvalidOperationException("Detached content can not be published directly.");
		}

		public void SetName(string value)
		{
			data["name"] = value;
		}

		public void SetProperty(string property, object value)
		{
			data[property] = value;
		}

		public T GetRawPropertyValue<T>(string property)
		{
			return data.ContainsKey(property) ? ((T)data[property]) : default(T);
		}

		public string GetUrl(string culture = null)
		{
			throw new NotImplementedException();
		}

		//public PublishedCultureInfo GetCulture(string culture = null)
		//{
		//	throw new NotImplementedException();
		//}

		//bool IPublishedContent.IsDraft(string culture)
		//{
		//	throw new NotImplementedException();
		

		public bool IsPublished(string culture = null)
		{
			throw new NotImplementedException();
		}
	}

	public class DBProperty : IPublishedProperty
	{
		string alias;
		private object value;

		public DBProperty(string alias, object value)
		{
			this.alias = alias;
			this.value = value;
		}

		public object DataValue
		{
			get { return value; }
		}

		public bool HasValue
		{
			get { return value != null; }
		}

		public string PropertyTypeAlias => alias;

		public object Value => value;

		public object XPathValue
		{
			get { throw new NotImplementedException(); }
		}

		public PublishedPropertyType PropertyType => throw new NotImplementedException();

		public string Alias => throw new NotImplementedException();

		public object GetSourceValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		public object GetValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		public object GetXPathValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		//bool IPublishedProperty.HasValue(string culture, string segment)
		//{
		//	throw new NotImplementedException();
		//}
	}
}
