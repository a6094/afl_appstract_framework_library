﻿using AFL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace AFLUmbraco.Models
{
	/// <summary>
	/// 
	/// Usage:
	/// 
	/// 1) Put an umbraco grid as a property on one of your document types.
	/// 2) Put a corresponding Memo property on your page model, with the data type UmbracoGrid:
	/// 
	/// public UmbracoGrid Grid => Memo<UmbracoGrid>();
	/// 
	/// 3) Implement models for your grid components like in the below two examples. Notice that RTEControl has a string Value type, and HelloWorld has a custom class (in this case called Data and placed inside HelloWorldControl itself)
	/// 
	/// [Alias("rte")]
	/// public class RTEControl : UmbracoGridControl<string>
	/// {
	/// 
	/// }
	/// 
	/// public class HelloWorld : UmbracoGridControl<HelloWorld.Data>
	/// {
	/// 	public class Data
	/// 	{
	/// 		public string hello;
	/// 		public string world;
	/// 	}
	/// }
	/// 
	/// optionally add a property like this:
	/// 
	///   public MyPageType Container { get; private set; }
	///   
	/// to your control, replacing MyPageType with your model type. It will be automatically set to the containing page instance. This will allow the control-view to reference page-properties as needed.
	/// 
	/// 4) If the umbraco control was given a weird name, you can use the Alias property to give it a different class name as seen in one of the examples above.
	/// 5) Put code like this in your page view:
	/// 
	/// @foreach (var section in Model.ComponentList.Sections)
	/// {
	/// 	foreach (var row in section.Rows)
	/// 	{
	/// 		foreach (var area in row.Areas)
	/// 		{
	/// 			foreach (var control in area.Controls)
	/// 			{
	/// 				@Html.Partial("GridControls/" + control.Editor.Alias, control)
	/// 			}
	/// 		}
	/// 	}
	/// }
	/// 
	/// 6) Your component views should look something like this:
	/// 
	///	@model HelloWorldControl
	/// @Model.Value.hello and @Model.Value.world
	/// 
	/// 7) That's it!
	/// 
	/// </summary>
	public class UmbracoGrid
	{
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("sections")]
		public UmbracoGridSection[] Sections { get; set; }

		public static UmbracoGrid Deserialize(string json, IAFLModel container)
		{
			if (string.IsNullOrEmpty(json) || !json.StartsWith("{"))
			{
				return new UmbracoGrid { Sections = new UmbracoGridSection[0], Name = "" };
			}
			var settings = new JsonSerializerSettings();
			settings.Converters.Add(new UmbracoGridJsonConverter(container));
			settings.Converters.Add(new UmbracoGridArrayJsonConverter());
			settings.Converters.Add(new UmbracoGridImageJsonConverter());
			settings.Converters.Add(new UmbracoGridContentJsonConverter());
			settings.Converters.Add(new UmbracoGridBooleanJsonConverter());
			return JsonConvert.DeserializeObject<UmbracoGrid>(json, settings);
		}

		/// <summary>
		/// Retrieve the first component which matches <typeparamref name="T"/> if no component is found <c>null</c> is return
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public T GetComponent<T>()
			where T : UmbracoGridControl
		{
			foreach (var section in this.Sections)
			{
				foreach (var row in section.Rows)
				{
					foreach (var area in row.Areas)
					{
						foreach (var control in area.Controls)
						{
							if (control is T component)
							{
								return component;
							}
						}
					}
				}
			}

			return null;
		}
	}

	public class UmbracoGridSection
	{
		[JsonProperty("grid")]
		public int Grid { get; set; }
		[JsonProperty("rows")]
		public UmbracoGridRow[] Rows { get; set; }
	}

	public class UmbracoGridRow
	{
		[JsonProperty("label")]
		public string Label { get; set; }
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("hasConfig")]
		public bool HasConfig { get; set; }
		[JsonProperty("id")]
		public string ID { get; set; }
		[JsonProperty("hasActiveChild")]
		public bool HasActiveChild { get; set; }
		[JsonProperty("active")]
		public bool Active { get; set; }
		[JsonProperty("areas")]
		public UmbracoGridArea[] Areas { get; set; }
	}

	public class UmbracoGridArea
	{
		[JsonProperty("grid")]
		public int Grid { get; set; }
		[JsonProperty("allowAll")]
		public bool AllowAll { get; set; }
		[JsonProperty("hasConfig")]
		public bool HasConfig { get; set; }
		[JsonProperty("allowed")]
		public string[] Allowed { get; set; }
		[JsonProperty("hasActiveChild")]
		public bool HasActiveChild { get; set; }
		[JsonProperty("active")]
		public bool Active { get; set; }
		[JsonProperty("controls")]
		public UmbracoGridControl[] Controls { get; set; }
	}

	public abstract class UmbracoGridControl
	{
		[JsonProperty("active")]
		public bool Active { get; set; }
		//[JsonProperty("selection")]
		//public object[] Selection { get; set; }
		[JsonProperty("editor")]
		public UmbracoGridControlEditor Editor { get; set; }
	}

	public class UmbracoGridControl<TValue> : UmbracoGridControl
	{
		[JsonProperty("value")]
		public TValue Value { get; set; }
	}

	/// <summary>
	/// Allows you to be explicit about what Container type the control is meant for. You can choose to leave the TContainer as generic, and only resolve it on the memo model property return type.
	/// if you choose to not use this base, but inherit from UmbracoGridControl<TValue> directly, you can add your own Container property of the required type, and it will still be set upon instantiation (see the guide in the comment for UmbracoGrid).
	/// </summary>
	public class UmbracoGridControl<TValue, TContainer> : UmbracoGridControl<TValue> where TContainer : AFLModelBase
	{
		public TContainer Container { get; private set; }
	}

	public class UmbracoGridControlEditor
	{
		[JsonProperty("name")]
		public string Name { get; set; }
		[JsonProperty("alias")]
		public string Alias { get; set; }
		[JsonProperty("view")]
		public string View { get; set; }
		[JsonProperty("render")]
		//public string Render { get; set; }
		//[JsonProperty("icon")]
		public string Icon { get; set; }
		//[JsonProperty("config")]
		//public string Config { get; set; }
	}

	public class UmbracoGridRelatedLink
	{
		[JsonProperty("caption")]
		public string Caption { get; private set; }
		[JsonProperty("link")]
		public string Link { get; private set; }
		[JsonProperty("newWindow")]
		public bool NewWindow { get; private set; }
		[JsonProperty("edit")]
		public string Edit { get; private set; }
		[JsonProperty("isInternal")]
		public bool IsInternal { get; private set; }
		[JsonProperty("type")]
		public string Type { get; private set; }
		[JsonProperty("title")]
		public string Title { get; private set; }

		/// <summary>
		/// A safe way to get the url.
		/// If the RelatedLink his an internal link then the nodeId will be set by the grid.
		/// This method will fetch the node and return the url.
		/// </summary>
		/// <returns></returns>
		public string GetUrl()
		{
			if (int.TryParse(this.Link, out var _))
			{
				var node = Registry.Current.GetModel<AFLModelBase>(this.Link);

				//return node?.AbsoluteUrl;
			}

			return this.Link;
		}
	}

	public class UmbracoGridImageJsonConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType) => 
			typeof(UmbracoImage).IsAssignableFrom(objectType);

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
			=> Registry.Current.GetModel<UmbracoImage>(reader.Value.ToString());

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}
	}

	public class UmbracoGridArrayJsonConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType) =>
			objectType.IsArray
			&& (typeof(UmbracoImage[]).IsAssignableFrom(objectType) || typeof(MediaItem[]).IsAssignableFrom(objectType));

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (string.IsNullOrEmpty(reader.Value?.ToString()))
			{
				return Array.CreateInstance(objectType.GetElementType(), 0);
			}

			var ids = reader.Value.ToString()
				.Split(',');
			var array = Array.CreateInstance(objectType.GetElementType(), ids.Length);

			for (int i = 0; i < ids.Length; i++)
			{
				array.SetValue(Registry.Current.GetModel(ids[i]), i);
			}

			return array;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}
	}

	public class UmbracoGridBooleanJsonConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType) => typeof(bool).IsAssignableFrom(objectType);

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var value = reader.Value.ToString();
			switch (value)
			{
				case "0":
					return false;
				case "1":
					return true;
				default:
					bool.TryParse(value, out var res);

					return res;
			}
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}
	}

	public class UmbracoGridContentJsonConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return typeof(IAFLModel).IsAssignableFrom(objectType);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var dictionary = serializer.Deserialize<Dictionary<string, object>>(reader);
			var content = new DBDetachedContent(dictionary);
			var model = Registry.Current.GetDBBackedModelFromWrappedNativeDBType(content);
			return model;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}
	}

	public class UmbracoGridJsonConverter : JsonConverter
	{
		private static Dictionary<string, Type> availableControlTypes;
		private IAFLModel containerModel;
		public UmbracoGridJsonConverter(IAFLModel containerModel)
		{
			this.containerModel = containerModel;

			if (availableControlTypes == null)
			{
				var list = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes().Where(t => !t.IsAbstract && typeof(UmbracoGridControl).IsAssignableFrom(t))).ToArray();
				availableControlTypes = list.ToDictionary(t => t.GetCustomAttribute<AliasAttribute>(true)?.Alias ?? t.Name, t => t);
			}
		}

		public override bool CanConvert(Type objectType)
		{
			return typeof(UmbracoGridControl).IsAssignableFrom(objectType);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var jsonObject = JObject.Load(reader);

			var alias = jsonObject.SelectToken("editor.alias")?.ToString();

			Type controlType;
			if (alias != null && availableControlTypes.ContainsKey(alias))
			{
				controlType = availableControlTypes[alias];

				if (controlType.IsGenericTypeDefinition)
				{
					controlType = controlType.MakeGenericType(containerModel.GetType());
				}
			}
			else
			{
				controlType = typeof(UmbracoGridControl<object>);
			}
			UmbracoGridControl control = (UmbracoGridControl)Activator.CreateInstance(controlType);

			var containerProp = controlType.GetProperty("Container");
			if (containerProp != null && containerProp.PropertyType.IsAssignableFrom(containerModel.GetType()))
			{
				containerProp.SetValue(control, containerModel);
			}
			serializer.Populate(jsonObject.CreateReader(), control);
			return control;
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}
	}
}
