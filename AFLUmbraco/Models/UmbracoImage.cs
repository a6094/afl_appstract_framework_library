﻿using System;
using AFL;
using Newtonsoft.Json;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace AFLUmbraco.Models
{
	[Alias("Image")]
	public class UmbracoImage : UmbracoFile
	{
		/// <summary>
		/// Optionally extend from this, in a project specific class, and create strongly typed crops like this:
		/// public string NewsImageUrl => Memo(() => node.GetCropUrl("NewsMedium"));
		/// </summary>
		/// <param name="node"></param>
		public UmbracoImage(IPublishedContent node) : base(node)
		{
			//Media m = node.GetCropUrl(;
		}
		[PropertyAlias("umbracoWidth")]
		public string Width => Memo();
		[PropertyAlias("umbracoHeight")]
		public string Height => Memo();

		public override string FilePath => Memo(() => {
			object cropperImage = "";//node.GetPropertyValue("umbracoFile", "");
		    if (cropperImage is string)
		    {
                if ("".Equals(cropperImage))
                {
                    return "";
                }

                try
                {
                    dynamic cropperJson = JsonConvert.DeserializeObject((string)cropperImage);
                    return cropperJson.src;
                }
                catch (Exception)
                {
                    return cropperImage;
                }
            }

            if (cropperImage is ImageCropDataSet)
		    {
		        var image = (ImageCropDataSet)cropperImage;
		        return image.Src;
		    }
            return "";
			
		});
	}
}
