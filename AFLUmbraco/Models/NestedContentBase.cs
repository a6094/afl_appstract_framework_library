﻿using System;
using System.Runtime.CompilerServices;
using AFL;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace AFLUmbraco.Models
{
	public abstract class NestedContentBase : IAFLModel
	{
		protected readonly IPublishedContent node;
		protected readonly AFLModelBase container;
		protected readonly string cachedGuid;

		protected UmbracoHelper UmbracoHelper => UmbracoUtils.UmbracoHelper;

		public NestedContentBase(AFLModelBase container, IPublishedContent node)
		{
			this.container = container;
			this.node = node;
			cachedGuid = container.UID + "-" + (GetNestedContentUniqueKey() ?? Guid.NewGuid().ToString()); //GetNestedContentUniqueKey() might not be globally unique, so prepend container uid.
		}

		protected T Memo<T>(Func<T> func, T fallback = default(T), [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, func, fallback, false, callerName);
		}

		//for cases like: int ZipCode => Memo<int>();
		protected T Memo<T>(T fallback = default(T), bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, null, fallback, recurse, callerName);
		}
		//for cases like: string AddressLine1 => Memo();
		protected string Memo(string fallback = null, bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo<string, string>(null, null, fallback, recurse, callerName);
		}

		//for cases like: string SearchPageUrl => Memo<IPublishedContent[], string>(n => n.FirstOrDefault()?.Url);
		protected TPost Memo<TPre, TPost>(Func<TPre, TPost> postProcessFunc, TPost fallback = default(TPost), bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo(postProcessFunc, null, fallback, recurse, callerName);
		}
		//for cases like: EventPage[] RawEventPages => Memo<EventPage[]>(list => list.OrderByDescending(n => n.PublicationDate).ToArray());
		protected T Memo<T>(Func<T, T> postProcessFunc, T fallback = default(T), bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(postProcessFunc, null, fallback, recurse, callerName);
		}

		private TPost Memo<TPre, TPost>(Func<TPre, TPost> postProcessFunc, Func<TPre> valueGetterFunc, TPost fallback, bool recurse, string callerName)
		{
			var propertyInfo = Registry.Current.GetInfo(this.GetType())?.GetProperty(callerName);

			if (valueGetterFunc == null)
			{
				var preType = typeof(TPre);
				var elementType = preType.IsArray ? preType.GetElementType() : preType;
				object value = null;
				valueGetterFunc = () =>
				{
					value = Registry.Current.MemoSupport.GetValue<TPre>(this, propertyInfo, callerName, elementType, recurse, null);
					return (TPre)value;
				};
			}

			return Registry.Current.MemoSupport.MemoImplementation(valueGetterFunc, postProcessFunc, fallback, Registry.Current.GetID(node), callerName, propertyInfo);
		}

		//public string ViewTypeName { get { return node.GetTemplateAlias(); } }

		/// <summary>
		/// override this method and return the string value of a property that is guaranteed to be unique for this element.
		/// The value does not have to be unique across different container content nodes.
		/// </summary>
		public virtual string GetNestedContentUniqueKey() { return null; }

		public string UID => cachedGuid;

		public string ViewTypeName => throw new NotImplementedException();

		public string Translate(string key)
		{
			var val = UmbracoHelper.GetDictionaryValue(key);
			return string.IsNullOrEmpty(val) ? key : val;
		}

		public string Translate(string key, string defaultValue)
		{
			var result = Translate(key);
			return result == key ? defaultValue : result;
		}
	}
}
