﻿using System.Collections.Generic;
using System.Linq;
using AFL;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace AFLUmbraco.Models
{
	public class Breadcrumb
	{
		public Breadcrumb(IPublishedContent node, bool includeSelf = false)
		{
			//var list = node.Ancestors()
			//	.Select(n => new { n = n, info = Registry.Current.GetInfo(Registry.Current.GetAlias(n)) })
			//	.Where(v => v.info != null && v.info.Renderable && !v.info.HasAttribute<DisableBreadcrumbAttribute>())
			//	.Select(v => Registry.Current.GetModel(v.n) as AFLModelBase).Reverse().ToList();
			var list = new List<AFLModelBase>();

			if(includeSelf)
			{
				list.Add(Registry.Current.GetModel(node) as AFLModelBase);
			}

			Ancestors = list.ToArray();
		}

		public AFLModelBase[] Ancestors { get; protected set; }
	}
}
