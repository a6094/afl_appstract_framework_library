﻿using AFL;
using Umbraco.Core.Models;

namespace AFLUmbraco.Models
{
	[Alias("File")]
	public class UmbracoFile : MediaItem
	{
		public UmbracoFile(IPublishedContent node) : base(node)
		{
		}
	}
}
