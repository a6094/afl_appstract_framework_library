﻿using AFL;
using AFL.Search;
using Examine;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Umbraco.Web;

namespace AFLUmbraco.Search
{
	public class Searcher<ResultT>
	{
		/// <summary>
		/// Specify pageSize only if pagination is done here, else specify -1 to return all results
		/// </summary>
		/// <param name="searchIndexID"></param>
		/// <param name="query"></param>
		/// <param name="page">zero indexed!</param>
		/// <param name="pageSize"></param>
		/// <param name="newestFirst"></param>
		/// <param name="forceResultType">specify if searcher should use Activator.CreateInstance() instead of the Registry.CreateModelInstance() For compatibility with older code</param>
		/// <param name="noResultsOnEmptyQuery"></param>
		/// <param name="maxResults">max results considered before pagination. default is set to a high number, since any sorting is only applied after this cutoff, which could yield unexpected results, if the resultset was limited by this variable.</param>
		/// <param name="setHighlight"></param>
		/// <param name="extraRules"></param>
		//public Searcher(int searchIndexID, string query, int page = 0, int pageSize = -1, bool newestFirst = false, bool useResultAdaptor = false, bool noResultsOnEmptyQuery = false, int maxResults = 10000, Action<ResultT, Lazy<string>> setHighlight = null, params ExtraRule[] extraRules)
		//{
		//	if(Registry.Current.SearchDefinitions is NoSearch)
		//	{
		//		throw new Exception("Indexing has not been configured. Search not possible.");
		//	}
			
		//	//var safeUserQuery = Regex.Replace(query ?? "", IndexingHandler.NotAllowedChars, " ").ToLower().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Where(w => w.Length > 0).ToArray();
		//	var safeUserQuery = "";
		//	if (noResultsOnEmptyQuery && safeUserQuery.Length == 0)
		//	{
		//		RawResults = new SearchResult[0];
		//		Results = new ResultT[0];
		//		return;
		//	}
		//	var searchDefinition = Registry.Current.SearchDefinitions.SearchIndexes.FirstOrDefault(d => d.ID == searchIndexID);
		//	if(searchDefinition == null)
		//	{
		//		throw new Exception("Search Definition not registered!");
		//	}

		//	var normalSearchFieldDefs = SearchDefinitions.Boosts.Where(kv => kv.Key != SearchBoost.Special).Select(kv => kv.Value).ToArray();
		//	var searchProvider = ExamineManager.Instance.SearchProviderCollection[searchDefinition.Searcher];
		//	if (searchProvider == null)
		//	{
		//		throw new Exception("Search provider not found!");
		//	}

		//	string luceneQuery = "";
		//	if (safeUserQuery.Length > 0)
		//	{
		//		luceneQuery = string.Join(" ",
		//			normalSearchFieldDefs.Select(fieldDef =>
		//			{
		//				var boostVal = "*^" + fieldDef.Boost.ToString(CultureInfo.InvariantCulture);
		//				var field = fieldDef.Field + ":";
		//				return string.Join(" ", safeUserQuery.Select(s => field + s + boostVal));
		//			})
		//		);
		//	}
		//	if (extraRules != null && extraRules.Length > 0)
		//	{
		//		var extraRulesString = string.Join(" AND ", extraRules.Select(r =>
		//		{
		//			if (r is ExtraRuleContains)
		//			{
		//				var rC = (ExtraRuleContains)r;
		//				if (!string.IsNullOrEmpty(rC.strings))
		//				{
		//					var ids = rC.strings.Split(',');
		//					if(ids.Length == 1)
		//					{
		//						return " ( " + r.field + ":\\-1*" + ids[0] + "*) ";
		//					} else
		//					{
		//						return " ( "+ string.Join(" OR ", ids.Select(id => r.field + ":\\-1*" + id + "*" ))+ ") ";
		//					}
							
		//				}
		//				return null;
		//			}
		//			else
		//			{
		//				//careful with the empty-string-case: it will cause lucene syntax errors.
		//				if (!string.IsNullOrEmpty(r.equals))
		//				{
		//					return " ( " + r.field + ":" + r.equals + " ) ";
		//				}
		//				else if (!string.IsNullOrEmpty(r.equalsAnySpaceSeparated))
		//				{
		//					return " ( " + string.Join(" ", r.equalsAnySpaceSeparated.Split(' ').Select(w => r.field + ":" + w)) + " ) ";
		//				}
		//				else
		//				{
		//					return null;
		//				}
		//			}
		//		}).Where(str => str != null));
		//		if (!string.IsNullOrEmpty(extraRulesString))
		//		{
		//			luceneQuery = string.IsNullOrEmpty(luceneQuery) ? extraRulesString : "( " + luceneQuery + " ) AND " + extraRulesString;
		//		}
		//	}

		//	//lucene does not allow queries staring with *, but *:* is a special syntax that returns all documents.
		//	if (string.IsNullOrEmpty(luceneQuery)) { luceneQuery = "*:*"; }

		//	//perform search...
		//	var res = searchProvider.Search(searchProvider.CreateSearchCriteria().RawQuery(luceneQuery), maxResults).AsQueryable();
		//	if (newestFirst)
		//	{
		//		var oldKeyForPublicationDate = "publicationDate";
		//		if (res.Any(r => r.Fields.Any(f => f.Key == oldKeyForPublicationDate)))
		//		{
		//			res = res.OrderByDescending(r => r.Fields[oldKeyForPublicationDate]);
		//		}
		//		else
		//		{
		//			res = res.OrderByDescending(r => UmbracoUtils.UmbracoHelper.TypedContent(r.Id).CreateDate);
		//		}
				
		//	}
		//	TotalResults = res.Count();
		//	if (pageSize > -1)
		//	{
		//		res = res.Skip(page * pageSize).Take(pageSize);
		//	}

		//	RawResults = res.ToArray();
		//	Results = RawResults.Select(r =>
		//	{
		//		var node = Registry.Current.GetNode(r.Id.ToString());
		//		ResultT entity = default(ResultT);
		//		if (node != null)
		//		{
		//			if (useResultAdaptor)
		//			{
		//				entity = (ResultT)Activator.CreateInstance(typeof(ResultT), Registry.Current.GetModel(node));
		//			}
		//			else if(typeof(ResultT).IsAssignableFrom(Registry.Current.TryGetInfo(node)?.ModelType))
		//			{
		//				entity = (ResultT)Registry.Current.GetModel(node);
		//			}
		//		}
		//		if (entity != null && setHighlight != null)
		//		{
		//			var pieces = normalSearchFieldDefs
		//				.SelectMany(f => (r.Fields[f.Field] ?? "").Split('|'))
		//				.Where(s => safeUserQuery.Any(word => CultureInfo.CurrentCulture.CompareInfo.IndexOf(s, word, CompareOptions.IgnoreCase) >= 0));
		//			setHighlight(entity, new Lazy<string>(() => MakeHighlights(pieces, safeUserQuery)));
		//		}
		//		return entity;
		//	}).Where(p => p != null).ToArray();
		//}

		public ResultT[] Results { get; private set; }

		public SearchResult[] RawResults { get; private set; }

		//public IEnumerable<Tuple<ResultT, SearchResult>> ResultsAndRawResults => Results.Select((r, idx) => new Tuple<ResultT, SearchResult>(r, RawResults[idx]));

		/// <summary>
		/// Total results, before any pagination
		/// </summary>
		public int TotalResults { get; private set; }

		//private string MakeHighlights(IEnumerable<string> pieces, string[] words)
		//{
		//	const int maxPieces = 3;
		//	const int approxMaxTotalChars = 300;
		//	const int minCharsBeforeFirstAndAfterLastHighlight = 60;

		//	//no more than maxPieces...
		//	var shownPieces = pieces.Take(maxPieces).Select(p => new Piece { text = p, snipBefore = false, snipAfter = false }).ToList();

		//	//cut away beginning and end of each piece, if it doesn't contain highlights, and cut of the end if a piece is too long...
		//	for (int i = 0; i < shownPieces.Count; ++i)
		//	{
		//		var piece = shownPieces[i];
		//		int firstIndex = words.Select(w => piece.text.IndexOf(w, StringComparison.OrdinalIgnoreCase)).Where(idx => idx != -1).Min();
		//		int lastIndex = words.Select(w => piece.text.LastIndexOf(w, StringComparison.OrdinalIgnoreCase)).Where(idx => idx != -1).Max();
		//		if (firstIndex > minCharsBeforeFirstAndAfterLastHighlight * 2)
		//		{
		//			piece.snipBefore = DiscardBeginningAtWordBoundary(ref piece.text, firstIndex - minCharsBeforeFirstAndAfterLastHighlight);
		//		}
		//		if (piece.text.Length > approxMaxTotalChars || piece.text.Length - lastIndex > minCharsBeforeFirstAndAfterLastHighlight * 2)
		//		{
		//			piece.snipAfter = DiscardEndAtWordBoundary(ref piece.text, Math.Min(approxMaxTotalChars, lastIndex + minCharsBeforeFirstAndAfterLastHighlight));
		//		}
		//	}

		//	//try to limit to no more than ~ approxMaxTotalChars chars, by showing less pieces if neccesary...
		//	int totalChars = 0;
		//	for (int i = 0; i < shownPieces.Count; ++i)
		//	{
		//		totalChars += shownPieces[i].text.Length;
		//		if (totalChars > approxMaxTotalChars) { shownPieces = shownPieces.Take(i + 1).ToList(); break; }
		//	}

		//	//merge into one string (while removing multiple spaces and newlines etc.)...
		//	var result = Regex.Replace(string.Join(" ... ", shownPieces.Select(p => p.text)), "\\s{2,}", " ");

		//	//append and prepend elipsis if needed...
		//	if (shownPieces.FirstOrDefault()?.snipBefore ?? false) { result = "... " + result; }
		//	if (shownPieces.LastOrDefault()?.snipAfter ?? false) { result = result + " ..."; }

		//	//actual highlighting...
		//	foreach (var word in words)
		//	{
		//		result = Regex.Replace(result, word, "<strong><em>$&</strong></em>", RegexOptions.IgnoreCase); //$& substitutes the entire match.
		//	}

		//	return result;
		//}

		private bool DiscardBeginningAtWordBoundary(ref string str, int cutPoint)
		{
			while (cutPoint > 0 && str[cutPoint] != ' ')
			{
				--cutPoint;
			}
			str = str.Substring(cutPoint);
			return cutPoint > 0;
		}

		private bool DiscardEndAtWordBoundary(ref string str, int cutPoint)
		{
			while (cutPoint < str.Length - 1 && str[cutPoint] != ' ')
			{
				--cutPoint;
			}
			bool hasCut = cutPoint < str.Length - 1;
			str = str.Substring(0, cutPoint);
			return hasCut;
		}

		//used by MakeHighlights()
		class Piece
		{
			public bool snipBefore;
			public bool snipAfter;
			public string text;
		}
	}

	/// <summary>
	/// if all possible rules are null, then the whole rule is skipped.
	/// </summary>
	public class ExtraRule
	{
		public readonly string field;
		public readonly string equals;
		public readonly string equalsAnySpaceSeparated;

		public ExtraRule(string field, string equals = null, string equalsAnySpaceSeparated = null)
		{
			if (equals != null & equalsAnySpaceSeparated != null) { throw new InvalidOperationException("can't have equals and equalsAnySpaceSeparated at the same time."); }
			this.field = field;
			this.equals = equals;
			this.equalsAnySpaceSeparated = equalsAnySpaceSeparated;
		}
	}

	public class ExtraRuleContains : ExtraRule
	{
		public readonly string strings;

		public ExtraRuleContains(string field, string strings = null) : base(field, null, null)
		{
			this.strings = strings;
		}
	}

	public class Autocompleter
	{
		//public Autocompleter(int searchIndexID, string query, int count = 10)
		//{
		//	var searchDefinition = Registry.Current.SearchDefinitions.SearchIndexes.FirstOrDefault(d => d.ID == searchIndexID);
		//	var searchProvider = ExamineManager.Instance.SearchProviderCollection[searchDefinition.Searcher];

		//	var safeUserQuery = Regex.Replace(query, IndexingHandler.NotAllowedChars, " ").ToLower();
		//	safeUserQuery = safeUserQuery.TrimEnd();
		//	IEnumerable<SearchResult> res;
		//	if(!string.IsNullOrEmpty(safeUserQuery))
		//	{
		//		res = searchProvider.Search(searchProvider.CreateSearchCriteria().RawQuery(safeUserQuery + "*")).AsEnumerable();
		//	}
		//	else
		//	{
		//		res = new SearchResult[0];
		//	}
			

		//	Results = res.SelectMany(r => r.Fields.Values).SelectMany(v => v.ToLower().Split(' ').Where(word => word.StartsWith(safeUserQuery))).Distinct().Take(count).ToArray();
		//}

		public string[] Results { get; private set; }
	}
}
