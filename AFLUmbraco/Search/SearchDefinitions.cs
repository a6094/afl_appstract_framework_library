﻿using System;
using System.Collections.Generic;
using AFL;
using AFL.Search;

namespace AFLUmbraco.Search
{
	public abstract class SearchDefinitions : ISearchDefinitions
	{
		public const int AllIndexes = -1;

		public const string nodeType = "__NodeTypeAlias";
		public const string path = "__Path";

		public static Dictionary<SearchBoost, SearchBoostValue> Boosts => new Dictionary<SearchBoost, SearchBoostValue> {
			{ SearchBoost.Low, new SearchBoostValue("appstract_boost_low", 0.9) },
			{ SearchBoost.Normal, new SearchBoostValue("appstract_boost_normal", 1) },
			{ SearchBoost.High, new SearchBoostValue("appstract_boost_high", 1.1) },
			{ SearchBoost.VeryHigh, new SearchBoostValue("appstract_boost_veryhigh", 1.2) },
			{ SearchBoost.Special, new SearchBoostValue("appstract_boost_special", 0) }
		};

		public abstract IEnumerable<SearchIndex>  SearchIndexes { get; }

		public virtual void AddCustomFields(SearchIndex index, IAFLModel entity, Dictionary<string, string> fields)
		{
		}
		[Obsolete("Use PreventIndexing(IAFLModel, ...) instead")]
		public virtual bool PreventIndexing(object node, SearchIndex index) { return false; }
		public virtual bool PreventIndexing(IAFLModel model, SearchIndex index) { return false; }
	}
}
