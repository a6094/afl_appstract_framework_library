﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using AFL;
using AFL.Core;
using AFL.Search;
using AFL.Utils;
using AFLUmbraco.Models;
using Examine;
using Lucene.Net.Analysis.Standard;
using Umbraco.Core;
using Umbraco.Web;

namespace AFLUmbraco.Search
{
	class IndexingHandler
	{
		public const string NotAllowedChars = "[^a-zA-Z0-9_æøåÆØÅ]+";

		public void OnAFLStarted()
		{
			//foreach (var provider in ExamineManager.Instance.IndexProviderCollection.ToList())
			//{
			//	var searchIndex = Registry.Current.SearchDefinitions.SearchIndexes.FirstOrDefault(index => index.Indexer == provider.Name);
			//	if (searchIndex != null)
			//	{
			//		provider.GatheringNodeData += (sender, e) => AddPageToIndex(sender, e, searchIndex);
			//		provider.NodeIndexing += (sender, e) => ShouldNodeBeIndexed(sender, e, searchIndex); // Cancel
			//	}
			//}
		}

		//public void ShouldNodeBeIndexed(object sender, IndexingNodeEventArgs e, SearchIndex index)
		//{
		//	var pageEntity = Registry.Current.GetDBBackedModel<AFLModelBase>(e.NodeId.ToString());
		//	bool shouldBeIndexed = ShouldBeIndexed(pageEntity, index);
		//	e.Cancel = !shouldBeIndexed;
		//}

		private bool IndexAsRenderablePage(AFLTypeInfo info)
		{
			return info.Renderable || info.HasAttribute<IndexedAsRenderablePage>();
		}

		private bool ShouldBeIndexed(AFLModelBase model, SearchIndex index)
		{
			var info = model == null ? null : Registry.Current.GetInfo(model);
			if (info == null || Registry.Current.SearchDefinitions.PreventIndexing(model, index) || Registry.Current.SearchDefinitions.PreventIndexing(model.Content, index) || !IndexAsRenderablePage(info) || info.HasAttribute<DisableIndexingAttribute>())
			{
				return false;
			}
			var onlyInIndexes = info.GetAttribute<LimitIndexingAttribute>()?.SearchIndexIDs;
			return onlyInIndexes == null || onlyInIndexes.Contains(index.ID);
		}

		//public void AddPageToIndex(object sender, IndexingNodeDataEventArgs e, SearchIndex index)
		//{
		//	var pageEntity = Registry.Current.GetDBBackedModel<AFLModelBase>(e.NodeId.ToString());

		//	bool shouldBeIndexed = ShouldBeIndexed(pageEntity, index);
		//	if (!shouldBeIndexed)
		//	{
		//		return;
		//	}

		//	var builders = SearchDefinitions.Boosts.Keys.ToDictionary(k => k, k => new StringBuilder());

		//	FetchSearchableEntityPropertyText(pageEntity, e.Fields, builders, index);

		//	foreach (var boost in SearchDefinitions.Boosts)
		//	{
		//		e.Fields.Add(boost.Value.Field, builders[boost.Key].ToString());
		//	}

		//	Registry.Current.SearchDefinitions.AddCustomFields(index, pageEntity, e.Fields);
		//}

		public void FetchSearchableEntityPropertyText(AFLModelBase entity, Dictionary<string, string> fields, Dictionary<SearchBoost, StringBuilder> builders, SearchIndex index)
		{
			try
			{
				var info = Registry.Current.GetInfo(entity);

				//entity.GetType().GetProperties()
				//.Select(p => new { prop = p, propInfo = info.GetProperty(p.Name) })
				//.ForEach(g =>
				//{
				//	var searchable = g.propInfo.GetAttributes<SearchableAttribute>().FirstOrDefault(s => s.SearchIndexID == SearchDefinitions.AllIndexes || s.SearchIndexID == index.ID);
				//	if (searchable != null && searchable.Boost != SearchBoost.NotIndexed && (searchable.Boost == SearchBoost.CustomField || builders.ContainsKey(searchable.Boost)))
				//	{
				//		string val;
				//		if (searchable.RawCMSProperty || !g.prop.PropertyType.IsSimpleType())
				//		{
				//			val = StringUtils.StripHtml(entity.Content.GetPropertyValue<string>(g.propInfo.CMSName)).ToLower();
				//		}
				//		//else if(typeof(AFLModelBase).IsAssignableFrom(g.prop.PropertyType))
				//		//{
				//		//	FetchSearchableEntityPropertyText((AFLModelBase)g.prop.GetValue(entity), fields, builders, index);
				//		//}
				//		//else if (typeof(AFLModelBase[]).IsAssignableFrom(g.prop.PropertyType))
				//		//{
				//		//	foreach(...)
				//		//	FetchSearchableEntityPropertyText((AFLModelBase)g.prop.GetValue(entity), fields, builders, index);
				//		//}
				//		else
				//		{
				//			val = StringUtils.StripHtml(Convert.ToString(g.prop.GetValue(entity))).ToLower();
				//		}
				//		if (!string.IsNullOrEmpty(val))
				//		{
				//			//replacing, with space is needed when looking for comma separated numbers, otherwise lucene doesn't find anything.
				//			val = Regex.Replace(val, NotAllowedChars, " ");

				//			if (searchable.Boost == SearchBoost.CustomField) //custom fields (special use case)
				//			{
				//				if (fields.ContainsKey(searchable.CustomFieldName))
				//				{
				//					fields[searchable.CustomFieldName] = fields[searchable.CustomFieldName] + " | " + val; //seperating the actual fields with '|', so we can split them in the highlighter.
				//				}
				//				else
				//				{
				//					fields.Add(searchable.CustomFieldName, val);
				//				}
				//			}
				//			else // normal stuff goes into the builders...
				//			{
				//				builders[searchable.Boost].Append(val + " | "); //seperating the actual fields with '|', so we can split them in the highlighter.
				//			}
				//		}
				//	}
				//});
				//entity.Content.Children.Where(c => Registry.Current.GetInfo(c) != null && !IndexAsRenderablePage(Registry.Current.GetInfo(c))).Select(c => Registry.Current.GetModel(c)).ForEach(m => FetchSearchableEntityPropertyText((AFLModelBase)m, fields, builders, index));
			}
			catch (Exception e) when (!System.Diagnostics.Debugger.IsAttached)
			{
				//LogHelper.Error(typeof(IndexingHandler), $"Exception caught while rebuilding index '{index.Indexer}'", e);
			}
		}
	}

	//public class CustomAnalyzer : StandardAnalyzer
	//{
	//	//public CustomAnalyzer() : base(Lucene.Net.Util.Version.LUCENE_29, new Hashtable()) { }
	//}
}
