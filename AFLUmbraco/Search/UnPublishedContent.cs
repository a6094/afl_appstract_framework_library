﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace AFLUmbraco.Search
{
	public class UnPublishedContent : IPublishedContent
	{
		IContent content;
		public UnPublishedContent(IContent content)
		{
			this.content = content;
		}

		public object this[string alias] => GetProperty(alias);

		//public IEnumerable<IPublishedContent> Children => content.Children().Select(c => new UnPublishedContent(c));

		//public IEnumerable<IPublishedContent> ContentSet => Children;

		public PublishedContentType ContentType { get { throw new NotImplementedException(); } }

		public DateTime CreateDate => content.CreateDate;

		public int CreatorId => content.CreatorId;

		public string CreatorName => content.CreatorId.ToString();

		public string DocumentTypeAlias => content.ContentType.Alias;

		public int DocumentTypeId => content.ContentTypeId;

		public int Id => content.Id;

		public bool IsDraft => false;

		public PublishedItemType ItemType => PublishedItemType.Content;

		public int Level => content.Level;

		public string Name => content.Name;

		//public IPublishedContent Parent => new UnPublishedContent(content.Parent());

		public string Path => content.Path;

		public ICollection<IPublishedProperty> Properties
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public int SortOrder => content.SortOrder;

		//public int TemplateId => content.Template.Id;

		public DateTime UpdateDate => content.UpdateDate;

		public string Url => "/"; //not available.

		public string UrlName => "";

		//public Guid Version => content.Version;

		public int WriterId => content.WriterId;

		public string WriterName => content.WriterId.ToString();

		public string UrlSegment => throw new NotImplementedException();

		//public IReadOnlyDictionary<string, PublishedCultureInfo> Cultures => throw new NotImplementedException();

		public Guid Key => throw new NotImplementedException();

		public IPublishedContent Parent => throw new NotImplementedException();

		public IEnumerable<IPublishedContent> Children => throw new NotImplementedException();

		public IEnumerable<IPublishedContent> ContentSet => throw new NotImplementedException();

		public int TemplateId => throw new NotImplementedException();

		public Guid Version => throw new NotImplementedException();

		//int? IPublishedContent.TemplateId => throw new NotImplementedException();

		//IEnumerable<IPublishedProperty> IPublishedElement.Properties => throw new NotImplementedException();

		//public PublishedCultureInfo GetCulture(string culture = null)
		//{
		//	throw new NotImplementedException();
		//}

		public int GetIndex()
		{
			return 0;
		}

		public IPublishedProperty GetProperty(string alias)
		{
			return new UnPublishedProperty(alias, content.GetValue(alias));
		}

		public IPublishedProperty GetProperty(string alias, bool recurse)
		{
			return GetProperty(alias);
		}

		public string GetUrl(string culture = null)
		{
			throw new NotImplementedException();
		}

		public bool IsPublished(string culture = null)
		{
			throw new NotImplementedException();
		}

		//bool IPublishedContent.IsDraft(string culture)
		//{
		//	throw new NotImplementedException();
		//}

		class UnPublishedProperty : IPublishedProperty
		{
			string alias;
			object value;
			public UnPublishedProperty(string alias, object value)
			{
				this.alias = alias;
				this.value = value;
			}
			public object DataValue => value;

			public bool HasValue => value != null;

			public string PropertyTypeAlias => alias;

			public object Value => value;

			public object XPathValue => value;

			public PublishedPropertyType PropertyType => throw new NotImplementedException();

			public string Alias => throw new NotImplementedException();

			public object GetSourceValue(string culture = null, string segment = null)
			{
				throw new NotImplementedException();
			}

			public object GetValue(string culture = null, string segment = null)
			{
				throw new NotImplementedException();
			}

			public object GetXPathValue(string culture = null, string segment = null)
			{
				throw new NotImplementedException();
			}

			//bool IPublishedProperty.HasValue(string culture, string segment)
			//{
			//	throw new NotImplementedException();
			//}
		}
	}
}
