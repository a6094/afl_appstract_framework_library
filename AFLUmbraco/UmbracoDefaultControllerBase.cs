﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AFL;
using AFL.Core;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.Routing;

namespace AFLUmbraco
{
	public abstract class UmbracoDefaultControllerBase : RenderMvcController, ICMSDefaultController
	{
		[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
		public override ActionResult Index(RenderModel model)
		{
			UmbracoUtils.EnsureCultureStored();

			return new DefaultControllerImplementation(this).IndexHelper(new object());//model.Content);
		}

		[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
		public ActionResult HandleWSCall()
		{
			if(Registry.Current.RestBasePath == null) { return null; }
			//UmbracoUtils.FixCulture();
			return new DefaultControllerImplementation(this).WebserviceHelper();
		}

		public ActionResult Rss(RenderModel model)
		{
			return new DefaultControllerImplementation(this).RssHelper(new object());//model.Content);
		}

		public ActionResult RedirectToNode(string toNodeID, string queryString)
		{
			return new RedirectResult("/");//RedirectToUmbracoPageResult(Convert.ToInt32(toNodeID), queryString);
		}

		public ActionResult RedirectToUrl(string url)
		{
			return new RedirectResult(url);
		}

		public ActionResult View(IAFLModel model)
		{
			return base.View(model);
		}

		public ActionResult Partial(string viewName, IAFLModel model)
		{
			return base.PartialView(viewName, model);
		}

		public ActionResult View(string viewName, IAFLModel model = null)
		{
			return base.View(viewName, model);
		}

		[NonAction]
		public virtual ActionResult MakeActionResult(IAFLModel model) { return View(model); }
	}

	/// <summary>
	/// todo move to own file :)
	/// </summary>
	public class RestWithUmbracoRoutingContentFinder : IContentFinder
	{
		//public bool TryFindContent(PublishedContentRequest request)
		//{
		//	if (request != null && UmbracoContext.Current!=null && UmbracoContext.Current.ContentCache.GetByRoute(request.Uri.AbsolutePath) == null)
		//	{
		//		var lastPart = request.Uri.Segments.LastOrDefault();
		//		if (!string.IsNullOrEmpty(lastPart))
		//		{
		//			var parentPath = string.Join("/", request.Uri.Segments.Take(request.Uri.Segments.Length - 1));
		//			var content = UmbracoContext.Current.ContentCache.GetByRoute(parentPath);

		//			if (content != null)
		//			{
		//				var info = Registry.Current.GetInfo(content);
		//				var handler = info.GetRestHandlerByUrlName(lastPart);
		//				if (handler != null && handler.Attribute.CMSRoutable)
		//				{
		//					request.PublishedContent = content;
		//					HttpContext.Current.Items["afl-rest-route-handler"] = handler;
		//					return true;
		//				}
		//			}
		//		}
		//	}

		//	return false;
		//}
		public bool TryFindContent(PublishedContentRequest contentRequest)
		{
			throw new NotImplementedException();
		}
	}
}
