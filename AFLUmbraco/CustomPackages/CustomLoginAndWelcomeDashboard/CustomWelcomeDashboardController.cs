﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Editors;
using Umbraco.Web.Mvc;

//namespace AFLUmbraco.CustomPackages.CustomLoginAndWelcomeDashboard
//{
//	[PluginController("CustomLoginAndWelcomeDashboard")]
//	public class CustomWelcomeDashboardController : UmbracoAuthorizedJsonController
//	{
//		[HttpPost]
//		public Result UploadClientsLogo()
//		{

//			if (HttpContext.Current.Request.Files.Count != 1)
//			{
//				return new Result { ok = false, errorMessage = "No file has been sent." };
//			}
//			var attachment = HttpContext.Current.Request.Files[0];

//			var serverLocation = HttpContext.Current.Server.MapPath("~/App_Plugins/CustomLoginAndWelcomeDashboard/img");
//			var fileName = Path.Combine(serverLocation, attachment.FileName);
//			attachment.SaveAs(fileName);
//			return new Result { ok = true };
//		}

//		[HttpPost]
//		public Result SaveSettings(Settings settings)
//		{

//			var serverLocation = HttpContext.Current.Server.MapPath("~/App_Plugins/CustomLoginAndWelcomeDashboard/storage");
//			var fileName = Path.Combine(serverLocation, "settings.json");
//			try
//			{
//				System.IO.File.WriteAllText(fileName, JsonConvert.SerializeObject(settings));
//				return new Result { ok = true };
//			}
//			catch (Exception ex)
//			{
//				return new Result { ok = false, errorMessage = ex.Message };
//			}
//		}

//		public class Result
//		{
//			public bool ok;
//			public string errorMessage;
//		}

//		public class Settings
//		{
//			public bool logoIsEnabled;
//			public PluginObject[] selectedPluginsList;
//		}
//		public class PluginObject
//		{
//			public string name;
//			public string settings;
//		}
//	}
//}
