﻿using System;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using umbraco.interfaces;

namespace AFLUmbraco.CustomPackages.CustomLoginAndWelcomeDashboard
{

	public class addDashboardSectionToTheTop : IPackageAction
	{
		/// <example>
		/// <code>
		/// <Action runat="install" [undo="false"] alias="addDashboardSectionToTheTop" dashboardAlias="MyDashboardSection">
		///     <section>
		///         <areas>
		///         <area>default</area>
		///         <area>content</area>
		///         </areas>
		///	        <tab caption="Welcome">
		///             <control>/App_plugins/MyDashboardSection/index.html</control>
		///         </tab>
		///     </section>
		/// </Action>
		/// </code>
		/// </example>
		public bool Execute(string packageName, XmlNode xmlData)
		{

			//if (xmlData.HasChildNodes)
			//{
			//	string sectionAlias = xmlData.Attributes["dashboardAlias"].Value;
			//	string dbConfig = "";//Umbraco.Core.IO.SystemFiles.DashboardConfig;

			//	XmlNode section = xmlData.SelectSingleNode("./section");
			//	//XmlDocument dashboardFile = XmlHelper.OpenAsXmlDocument(dbConfig);

			//	var found = dashboardFile.SelectNodes("//section[@alias='" + sectionAlias + "']");
			//	if (found == null || found.Count <= 0)
			//	{
			//		XmlNode importedSection = dashboardFile.ImportNode(section, true);

			//		XmlAttribute alias = XmlHelper.AddAttribute(dashboardFile, "alias", sectionAlias);
			//		importedSection.Attributes.Append(alias);

			//		dashboardFile.DocumentElement.PrependChild(importedSection);

			//		dashboardFile.Save(Umbraco.Core.IO.IOHelper.MapPath(dbConfig));
			//	}

			//	return true;
			//}

			return false;
		}


		public string Alias()
		{
			return "addDashboardSectionToTheTop";
		}

		public bool Undo(string packageName, XmlNode xmlData)
		{

			//string sectionAlias = xmlData.Attributes["dashboardAlias"].Value;
			//string dbConfig = Umbraco.Core.IO.SystemFiles.DashboardConfig;
			//XmlDocument dashboardFile = XmlHelper.OpenAsXmlDocument(dbConfig);

			//XmlNode section = dashboardFile.SelectSingleNode("//section [@alias = '" + sectionAlias + "']");

			//if (section != null)
			//{

			//	dashboardFile.SelectSingleNode("/dashBoard").RemoveChild(section);
			//	dashboardFile.Save(Umbraco.Core.IO.IOHelper.MapPath(dbConfig));
			//}

			return true;
		}

		public XmlNode SampleXml()
		{
			return null;
		}

		public bool Execute(string packageName, XElement xmlData)
		{
			throw new NotImplementedException();
		}

		public bool Undo(string packageName, XElement xmlData)
		{
			throw new NotImplementedException();
		}
	}

	/// <example>
	/// <code>
	/// <Action runat="install" [undo="false"] alias="ReplaceXmlFragment" file="~/config/umbracosettings.config" xpath="//settings/content">
	///     <loginBackgroundImage>../App_Plugins/MyBackgroundImage/background-image.png</loginBackgroundImage>
	/// </Action>
	/// </code>
	/// </example>
	public class ReplaceXmlFragment : IPackageAction
	{
		public string Alias()
		{
			return "ReplaceXmlFragment";
		}

		public bool Execute(string packageName, XmlNode xmlData)
		{
			bool result = false;

			string configFileName = VirtualPathUtility.ToAbsolute(GetAttributeValueFromNode(xmlData, "file"));

			string xPath = GetAttributeValueFromNode(xmlData, "xpath");

			//XmlDocument configDocument = XmlHelper.OpenAsXmlDocument(configFileName);

			XmlNode xmlFragment = xmlData.SelectSingleNode("./*");

			//XmlNode rootNode = configDocument.SelectSingleNode(xPath);

			//var oldNode = rootNode.SelectSingleNode($"//{xmlFragment.Name}");
			//if (oldNode != null)
			//{
			//	rootNode.ReplaceChild(configDocument.ImportNode(xmlFragment, true), oldNode);
			//}
			//else
			//{
			//	rootNode.AppendChild(configDocument.ImportNode(xmlFragment, true));
			//}

			//configDocument.Save(HttpContext.Current.Server.MapPath(configFileName));

			result = true;

			return result;
		}

		public bool Undo(string packageName, System.Xml.XmlNode xmlData)
		{
			bool result = false;

			string configFileName = VirtualPathUtility.ToAbsolute(GetAttributeValueFromNode(xmlData, "file"));

			string xPath = GetAttributeValueFromNode(xmlData, "xpath");

			//XmlDocument configDocument = XmlHelper.OpenAsXmlDocument(configFileName);

			XmlNode xmlFragment = xmlData.SelectSingleNode("./*");

			//XmlNode rootNode = configDocument.SelectSingleNode(xPath);

			//var nodeToBeRemoved = rootNode.SelectSingleNode($"//{xmlFragment.Name}");
			//if (nodeToBeRemoved != null)
			//{
			//	rootNode.RemoveChild(nodeToBeRemoved);
			//}

			//configDocument.Save(HttpContext.Current.Server.MapPath(configFileName));

			result = true;

			return result;
		}

		public static string GetAttributeValueFromNode(XmlNode node, string attributeName)
		{
			return GetAttributeValueFromNode<string>(node, attributeName, string.Empty);
		}

		public static T GetAttributeValueFromNode<T>(XmlNode node, string attributeName, T defaultValue)
		{
			if (node.Attributes[attributeName] != null)
			{
				string result = node.Attributes[attributeName].InnerText;
				if (string.IsNullOrEmpty(result))
					return defaultValue;

				return (T)Convert.ChangeType(result, typeof(T));
			}
			return defaultValue;
		}

		public XmlNode SampleXml()
		{
			return null;
		}

		public bool Execute(string packageName, XElement xmlData)
		{
			throw new NotImplementedException();
		}

		public bool Undo(string packageName, XElement xmlData)
		{
			throw new NotImplementedException();
		}
	}

}
