﻿using AFL.GoogleRecaptcha;
using System;
using System.Collections.Generic;
using System.Web;
using Umbraco.Forms.Core;

namespace AFLUmbraco.CustomPackages.UmbracoFormsFieldTypes.GoogleRecaptcha
{
	public class GoogleRecaptchaAnswerType : Umbraco.Forms.Core.FieldType
	{
		public GoogleRecaptchaAnswerType()
		{
			this.Id = new Guid("bff137d5-10ea-4d4c-8efe-c83c6c8ee68e"); // Replace this!
			this.Name = "Google recaptcha";
			this.Description = "Avoid fake form submit";
			this.Icon = "icon-re-post";
			this.DataType = FieldDataType.String;
			this.SortOrder = 10;
			this.SupportsRegex = true;
		}

		// You can do custom validation in here which will occur when the form is submitted.
		// Any strings returned will cause the submit to be invalid!
		// Where as returning an empty ienumerable of strings will say that it's okay.
		//public override IEnumerable<string> ValidateField(Umbraco.Forms.Core.Form form, Field field, IEnumerable<object> postedValues, HttpContextBase context)
		//{
		//	var returnStrings = new List<string>();

		//	var res = HttpContext.Current.Request["g-recaptcha-response"];
		//	if (!ReCaptcha.IsValid(res))
		//	{
		//		returnStrings.Add("ReCaptcha failed");
		//	}

		//	// Also validate it against the original default method.
		//	returnStrings.AddRange(base.ValidateField(form, field, postedValues, context));

		//	return returnStrings;
		//}
	}

}
