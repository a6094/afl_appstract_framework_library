﻿using System;
using System.Collections.Generic;
using System.Linq;
using AFL;
using AFL.Core;
using AFLUmbraco.Models;
using AFLUmbraco.Search;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace AFLUmbraco
{

	public class AFLUmbraco : IAFLImplementation
	{
		public void OnStartup()
		{
			var handler = new IndexingHandler();
			handler.OnAFLStarted();
		}

		public string GetAlias(object node)
		{
			//return ((IPublishedContent)node)?.DocumentTypeAlias;
			return "TODO";
		}

		public string GetID(object node)
		{
			int id = ((IPublishedContent)node).Id;
			if (id == 0 && (node.GetType().Name == "DetachedPublishedContent" || node.GetType().Name == "DBDetachedContent")) // todo - more robust identification. the latter one is for the AFL IContent support.
			{
				return Guid.NewGuid().ToString(); // give it a unique id, to prevent caching collisions. as of now, there is no way to distinguish nested content nodes, so this is the best we can do.
			}
			return id.ToString();
		}

		public object GetNode(string nodeID)
		{
			//return UmbracoUtils.UmbracoHelper.TypedContent(nodeID) ?? UmbracoUtils.UmbracoHelper.TypedMedia(nodeID); //todo: bad idea? (media case used in GetNodeAttachmentPaths)
			return null;
		}

		public IEnumerable<object> GetTopLevelNodes(AFLTypeInfo nodeType = null)
		{
			//UmbracoUtils.UmbracoHelper.TypedContentAtRoot();
			//return UmbracoUtils.UmbracoHelper.TypedContentAtRoot().Where(n => nodeType == null || n.DocumentTypeAlias == nodeType.Alias);
			return new List<Object>();
		}

		public object GetDBNode(string nodeID)
		{
			//var content =  ApplicationContext.Current.Services.ContentService.GetById(Convert.ToInt32(nodeID));
			object content = null;
			if (content == null) { return null; }
			//return new DBContent(content); 
			return null;
		}
		public object NativeDBTypeToDBNode(object dbType)
		{
			if (dbType == null) { return null; }
			return new DBContent((IContent)dbType);
		}

		public object CreateMemoryBackedNode(IAFLModel copyFrom = null)
		{
			var node = new DBDetachedContent(new Dictionary<string, object>());

			var copyFromModel = copyFrom as AFLModelBase;
			if (copyFromModel != null)
			{
				var aflType = Registry.Current.GetInfo(copyFromModel.GetType());
				node.SetName(copyFromModel.Name);
				node.SetProperty("ncContentTypeAlias", aflType.Alias);
				var properties = aflType.AllPropertyNames;
				foreach (var property in properties)
				{
					var cmsName = aflType.GetProperty(property).CMSName;
					if (!string.IsNullOrEmpty(cmsName))
					{
						//node.SetProperty(cmsName, copyFromModel.Content.GetProperty(cmsName).DataValue);
					}
				}
			}

			return node;
		}

		public object GetParentNode(object node)
		{
			return ((IPublishedContent)node).Parent;
		}

		public IEnumerable<PropertyTypeData> GetPropertiesForAlias(string alias)
		{
			if (alias == "File" || alias == "Image") //these don't exist in ContentTypeService, but should still be mapped.
			{
				return new [] { "umbracoFile", "umbracoBytes", "umbracoExtension", "umbracoWidth", "umbracoHeight" }.Select(s => new PropertyTypeData(s));
			}

			//return ApplicationContext.Current.Services.ContentTypeService.GetContentType(alias)?.CompositionPropertyTypes.Select(cpt => new PropertyTypeData(cpt.Alias, cpt.Name));
			return new List<PropertyTypeData>();
		}

		public string InferAliasFromTypeName(string name)
		{
			//var type = ApplicationContext.Current.Services.ContentTypeService.GetContentType(name); //case insensitive lookup.
			//if(type != null)
			//{
			//	return type.Alias; //return correctly capitalized version.
			//}
			return null;
		}

		public string InferViewName(IAFLModel model)
		{
			if(model is AFLModelBase)
			{
				//return ((AFLModelBase)model).Content.GetTemplateAlias();
			}
			return null;
		}

		public IMemoSupport MakeMemoSupport()
		{
			return new UmbracoMemoSupport();
		}

		public Type NodeType => typeof(IPublishedContent);
	}
}
