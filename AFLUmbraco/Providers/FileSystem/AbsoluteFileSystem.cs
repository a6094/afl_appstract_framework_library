﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Umbraco.Core.IO;

namespace AFLUmbraco.Providers.FileSystem
{
	public class AbsoluteFileSystem : IFileSystem
	{
		public static string urlBase = "media";
		private string basePath;

		public AbsoluteFileSystem(string virtualRoot)
		{
			this.basePath = virtualRoot;
		}

		public bool CanAddPhysical => throw new NotImplementedException();

		public void AddFile(string path, Stream stream, bool overrideIfExists)
		{
			string fullPath = GetFullPath(path);
			Directory.CreateDirectory(Path.GetDirectoryName(fullPath));

			FileStream fs;
			var mode = overrideIfExists ? FileMode.Create : FileMode.CreateNew;
			fs = new FileStream(fullPath, mode);
			stream.CopyTo(fs);
			fs.Close();
		}

		public void AddFile(string path, System.IO.Stream stream)
		{
			AddFile(GetFullPath(path), stream, false);
		}

		public void AddFile(string path, string physicalPath, bool overrideIfExists = true, bool copy = false)
		{
			throw new NotImplementedException();
		}

		public void DeleteDirectory(string path, bool recursive)
		{
			Directory.Delete(GetFullPath(path), recursive);
		}

		public void DeleteDirectory(string path)
		{
			Directory.Delete(GetFullPath(path), false);
		}

		public void DeleteFile(string path)
		{
			File.Delete(GetFullPath(path));
		}

		public bool DirectoryExists(string path)
		{
			return Directory.Exists(GetFullPath(path));
		}

		public bool FileExists(string path)
		{
			return File.Exists(GetFullPath(path));
		}

		public DateTimeOffset GetCreated(string path)
		{
			return File.GetCreationTime(GetFullPath(path));
		}

		public IEnumerable<string> GetDirectories(string path)
		{
			return Directory.GetDirectories(GetFullPath(path));
		}

		public IEnumerable<string> GetFiles(string path, string filter)
		{
			return Directory.GetFiles(GetFullPath(path), filter);
		}

		public IEnumerable<string> GetFiles(string path)
		{
			return Directory.GetFiles(GetFullPath(path));
		}

		public string GetFullPath(string path)
		{
			return Path.Combine(basePath, path);
		}

		public DateTimeOffset GetLastModified(string path)
		{
			return File.GetLastWriteTime(GetFullPath(path));
		}

		public string GetRelativePath(string fullPathOrUrl)
		{
			if (fullPathOrUrl.StartsWith(basePath))
			{
				return fullPathOrUrl.Substring(basePath.Length);
			}
			else if (fullPathOrUrl.StartsWith("http://") || fullPathOrUrl.StartsWith("/" + urlBase))
			{
				var parts = fullPathOrUrl.Split(new string[] { "/" + urlBase + "/" }, StringSplitOptions.None);
				if (parts.Length == 2)
				{
					return HttpUtility.UrlDecode(parts[1]);
				}
			}
			throw new IOException("Path has unexpected base part.");
		}

		public long GetSize(string path)
		{
			throw new NotImplementedException();
		}

		public string GetUrl(string path)
		{
			return "/" + urlBase + "/" + path.Replace('\\', '/').TrimStart('/');
		}

		public System.IO.Stream OpenFile(string path)
		{
			return File.Open(GetFullPath(path), FileMode.Open);
		}
	}
}
