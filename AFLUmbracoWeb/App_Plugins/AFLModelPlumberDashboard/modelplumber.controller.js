﻿angular.module("umbraco").controller("AFLModelPlumberController", ($scope, $http, notificationsService) => {
	let modelPlumberApi = "/umbraco/BackOffice/Api/AFLModelPlumber/";
	let debugModeApi = "/umbraco/BackOffice/Api/AFLDebugMode/";

	$scope.showAllModels = false;
	$scope.showCodePreview = false;
	$scope.codeText = "";
	$scope.isDebug = false;

	$scope.saveModel = function saveModel(contentType) {
		"use strict";
		let url = modelPlumberApi + "WriteClassToFile";
		$http.get(url, { params: { alias: contentType.alias } })
			.then((response) => {
				if (response.status === 200) {
					contentType.defined = true;
					contentType.diff = false;
					notificationsService.success("Success", "Model saved");
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to save model");
			});
	}

	$scope.saveDiff = function saveDiff(contentType) {
		"use strict";
		let url = modelPlumberApi + "WriteDiffToFile";
		$http.get(url, { params: { alias: contentType.alias } })
			.then((response) => {
				if (response.status === 200) {
					contentType.defined = true;
					contentType.diff = false;
					notificationsService.success("Success", "Diff saved");
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to save diff");
			});
	}

	$scope.getClassDiffCode = function getClassDiffCode(contentType) {
		"use strict";
		let url = modelPlumberApi + "GetClassDiffCode";
		$http.get(url, { params: { alias: contentType.alias } })
			.then(response => {
				if (response.status === 200) {
					$scope.presentCodePreview(response.data);
				}
			}
			).catch(err => {
				notificationsService.error("Error", "Failed to load diff");
			});
	}

	$scope.getClassCode = function getClassCode(contentType) {
		"use strict";
		let url = modelPlumberApi + "GetClassCode";
		$http.get(url, { params: { alias: contentType.alias } })
			.then(response => {
				if (response.status === 200) {
					$scope.presentCodePreview(response.data);
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to load class code");
			});
	}

	$scope.checkMode = function checkMode() {
		"use strict";
		let url = debugModeApi + "IsDebug"
		$http.get(url).then((response) => {
			if (response.status === 200) {
				$scope.isDebug = response.data;
				$scope.getModels();
			}
		});
	}

	$scope.getModels = function getModels() {
		"use strict";
		let url = modelPlumberApi + "GetModels";
		$http.get(url)
			.then(response => {
				if (response.status === 200) {
					$scope.contentTypes = response.data
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to check debug mode status");
			});
	}

	$scope.toggleAllModels = function toggleAllModels() {
		"use strict";
		$scope.showAllModels = !$scope.showAllModels;
	}

	$scope.showInList = function showInList(contentType) {
		"use strict";
		return !contentType.defined || contentType.diff || $scope.showAllModels;
	}

	$scope.presentCodePreview = function presentCodePreview(code) {
		"use strict";
		$scope.showCodePreview = true;
		$scope.codeText = code;
	}

	$scope.hideCodePreview = function hideCodePreview() {
		"use strict";
		$scope.showCodePreview = false;
		$scope.codeText = "";
	}

	$scope.checkMode();
});