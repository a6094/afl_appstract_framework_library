﻿angular.module("umbraco").controller("UmbracoRedirectHelperController", function ($scope, assetsService, $routeParams, $http, localizationService) {
	if (!$scope.model) {
		$scope.model = {};
	}

	$scope.loadInProgress = true;
	$scope.displayValue = "";

	$scope.model.redirects = {};

	$scope.onAdd = function () {
		if (($scope.model.value || "") != "") {
			$scope.loadInProgress = true;
			$http.get("backoffice/URH/UmbracoRedirectHelper/AddAlternativeRedirect" +
				"?nodeId=" + $routeParams.id +
				"&base64Url=" + btoa($scope.model.value) +
				"&languageISOCode=" + ($routeParams.cculture || $routeParams.mculture)
			).then(function (response) {
				$loadRedirects();
				$scope.loadInProgress = false;
				delete $scope.model.value;
			}, function (err) {
				$scope.loadInProgress = false;
			});
		}
	}

	$scope.onDelete = function (id) {
		if ((id || "") != "") {
			$scope.loadInProgress = true;
			$http.get("backoffice/URH/UmbracoRedirectHelper/DeleteRedirect" +
				"?id=" + id
			).then(function (response) {
				$loadRedirects();
				$scope.loadInProgress = false;
			}, function (err) {
				$scope.loadInProgress = false;
			});
		}
	}

	$loadRedirects = function () {
		$http.get("backoffice/URH/UmbracoRedirectHelper/GetExistingRedirects" +
			"?nodeId=" + $routeParams.id
		).then(function (response) {
			$scope.model.redirects = response.data;
			$scope.loadInProgress = false;
		}, function (err) {
				$scope.loadInProgress = false;

				$scope.info = "Please check if your redirect helper controller is installed and configured correcrly (https://our.umbraco.com/packages/backoffice-extensions/umbraco-redirect-helper/)";
				notificationsService.remove(0);
				notificationsService.error($scope.info);
		});
	}

	$loadRedirects();

	$scope.$on("formSubmitting", function (ev, args) {
		if (args.action === "save" || args.action === "publish") {
			delete $scope.model.value;
		}
	});
	assetsService.loadCss("~/App_Plugins/UmbracoRedirectHelper/style.css");
});
