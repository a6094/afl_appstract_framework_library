﻿angular.module("umbraco").controller("AFLDictionaryController", ($scope, $http, notificationsService) => {
	let toolApi = "/umbraco/BackOffice/Api/AFLDictionaryTool/";

	$scope.setupMissingNoTrans = function setupMissingNoTrans() {
		"use strict";
		let url = toolApi + "SetupMissingDictionaryItemsNoTrans";
		$http.get(url)
			.then(response => {
				if (response.status === 200) {
					notificationsService.success("Success", "Set up missing items with no translations");

				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to set up missing items with no translations");
			});
	}

	$scope.setupMissing = function setupMissing() {
		"use strict";
		let url = toolApi + "SetupMissingDictionaryItems";
		$http.get(url)
			.then(response => {
				if (response.status === 200) {
					notificationsService.success("Success", "Set up missing dictionary items");

				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to set up missing dictionary items");
			});
	}

	$scope.fillOutEmpty = function fillOutEmpty() {
		"use strict";
		let url = toolApi + "FillOutEmptyDictionaryItems";
		$http.get(url)
			.then(response => {
				if (response.status === 200) {
					notificationsService.success("Success", "Filled out empty dictionary items");
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to fill out empty dictionary items");
			});
	}

	$scope.makeAll = function makeAll() {
		"use strict";
		let url = toolApi + "MakeAllContentTypesIntoDictionaryItems";
		$http.get(url)
			.then(response => {
				if (response.status === 200) {
					notificationsService.success("Success", "Made all content types into dictionary items");
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to make all content types into dictionary items");
			});
	}

});