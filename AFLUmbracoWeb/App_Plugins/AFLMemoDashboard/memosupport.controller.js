﻿angular.module("umbraco").controller("AFLMemoController", ($scope, $http, notificationsService) => {
	let memoSupportApi = "/umbraco/BackOffice/Api/AFLMemoSupport/";

	$scope.getMissingProperties = function getMissingProperties() {
		"use strict";
		let url = memoSupportApi + "GetMissingProperties";
		$http.get(url)
			.then(response => {
				if (response.status === 200) {
					$scope.missingProperties = response.data;
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to load missing properties");
			});
	}

	$scope.reregisterTypes = function reregisterTypes() {
		"use strict";
		let url = memoSupportApi + "ReregisterTypes";
		$http.get(url)
			.then(response => {
				if (response.status === 200) {
					notificationsService.success("Success", "Registry has been reinitialized");
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to load missing properties");
			});
	}

	$scope.clearPropertyEntry = function clearPropertyEntry(property) {
		"use strict";
		let url = memoSupportApi + "ClearPropertyEntry";
		$http.get(url, { params: { alias: property.alias, propertyName: property.name } })
			.then(response => {
				if (response.status === 200) {
					$scope.missingProperties.splice($scope.missingProperties.indexOf(property), 1);
					notificationsService.success("Success", "Removed property entry");
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to remove property entry");
			});
	}

	$scope.clearAllProperties = function clearAllProperties() {
		"use strict";
		let url = memoSupportApi + "ClearAllProperties";
		$http.get(url)
			.then(response => {
				if (response.status === 200) {
					$scope.missingProperties = [];
					notificationsService.success("Success", "Removed all property entries");
				}
			})
			.catch(err => {
				notificationsService.error("Error", "Failed to clear properties");
			});
	}

	$scope.getMissingProperties();

});