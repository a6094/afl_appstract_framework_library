﻿//using AFL.Tools;
//using NUnit.Framework;

//namespace AFLTests.Tools
//{
//	[TestFixture]
//	public class YoutubeVideoTests
//	{
//		private const string VideoId = "kxqxZEhtSlQ";
//		private readonly string _duration = "30";

//		[Test]
//		public void YoutubeVideoTest()
//		{
//			var video = new YoutubeVideo(VideoId);

//			Assert.AreEqual("//img.youtube.com/vi/kxqxZEhtSlQ/0.jpg", video.ImageURL);
//			Assert.AreEqual("//img.youtube.com/vi/kxqxZEhtSlQ/maxresdefault.jpg", video.ImageMaxResURL);
//			Assert.AreEqual("kxqxZEhtSlQ", video.Id);
//		}

//		[Test]
//		public void YoutubeVideoTest1()
//		{
//			var video = new YoutubeVideo(VideoId, _duration);

//			Assert.AreEqual("30", video.DefaultDuration);
//			// TODO: There is an error with loading the JSON library. Should probably be fixed
//			Assert.AreEqual("0", video.Duration);
//		}
//	}
//}