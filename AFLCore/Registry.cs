using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AFLCore.Attributes;
using AFLCore.Caching;
using AFLCore.Core;
using AFLCore.Exceptions;
using AFLCore.Options;
using AFLCore.Rest.Attributes;
using AFLCore.Search;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace AFLCore
{
	public class Registry
	{
		public event EventHandler<RegistryEventArgs> Started;
		private IAFLImplementation implementation;
		private readonly IOptions<AFLOptions> aflOptions;
		private readonly IAFLCache aflCache;
		private readonly Dictionary<string, AFLTypeInfo> modelsByAlias = new Dictionary<string, AFLTypeInfo>();
		private readonly Dictionary<string, AFLTypeInfo> modelsByName = new Dictionary<string, AFLTypeInfo>();
		private Dictionary<string, RestHandlerInfo> restHandlerDescriptors = null;
		private IEnumerable<Assembly> registeredAssemblies;


		public Registry(IAFLImplementation implementation, IOptions<AFLOptions> AFLOptions, ISearchDefinitions SearchDefinitions, IHost host, IAFLCache aflCache) 
		{
			this.implementation = implementation;
			aflOptions = AFLOptions;
			this.SearchDefinitions = SearchDefinitions;
			Host = host;
			this.aflCache = aflCache;
		}

		public ISearchDefinitions SearchDefinitions { get; private set; }
		public IHost Host { get; private set; }

		public IReadOnlyDictionary<string, RestHandlerInfo> RestHandlerDescriptors => restHandlerDescriptors;

		public string RestBasePath { get; private set; }

		public void Initialize(IEnumerable<Assembly> assemblies, string restBasePath = null)
		{
			this.registeredAssemblies = assemblies.Concat(new Assembly[] { GetType().Assembly, implementation.GetType().Assembly });

			if(!string.IsNullOrEmpty(restBasePath))
			{
				if(!restBasePath.StartsWith("/")) { restBasePath = "/"+restBasePath; };
				if(!restBasePath.EndsWith("/")) { restBasePath = restBasePath+"/"; };
				this.RestBasePath = restBasePath;
				this.restHandlerDescriptors = new Dictionary<string, RestHandlerInfo>();
			}

			this.RegisterTypes();
			this.Started?.Invoke(this, new RegistryEventArgs());
		}
		public void Configure()
		{

		}

		//public void Reinitialize()
		//{
		//	Initialize(instance.implementation, instance.SearchDefinitions, instance.assemblies, instance.restBasePath);
		//}

		public bool AFLDebugMode => aflOptions.Value.DebugMode;

		/// <summary>
		/// Assumes that the first registeded assembly is the main business logic assembly, and returns that.
		/// </summary>
		public Assembly BusinessLogicAssembly => registeredAssemblies.FirstOrDefault(x => x.GetName().Name.EndsWith("Core")) ?? registeredAssemblies.First();

		/// <summary>
		/// For debugging and testing purposes.
		/// </summary>
		public IReadOnlyDictionary<string, AFLTypeInfo> AllModelsByName => modelsByName;

		void RegisterTypes()
		{
			var allTypes = registeredAssemblies.SelectMany(a =>
				a.GetTypes().Where(t => (!t.IsAbstract && (typeof(IAFLModel).IsAssignableFrom(t) || t.GetCustomAttribute(typeof(RestAttribute), false)!=null) ||
					t.GetCustomAttribute(typeof(CompositionAttribute), false) != null))
			);
			foreach(var t in allTypes)
			{
				var info = new AFLTypeInfo(t, this);
				modelsByName[t.AssemblyQualifiedName] = info;
				if (!string.IsNullOrEmpty(info.Alias))
				{
					if (modelsByAlias.ContainsKey(info.Alias))
					{
						if (modelsByAlias[info.Alias].OverriddenType == info.ModelType)
						{
							//ok. we already registered a model type for this alias, and it specifies that it should override one we have here. do nothing.
							continue;
						}
						else if (info.OverriddenType != modelsByAlias[info.Alias].ModelType)
						{
							//doh. we already registered a model type for this alias, and neither knows about the other.
							throw new InvalidOperationException("Duplicate model for alias: " + info.Alias + ". If you need to have both, then specify which one should be selected by using the Override property of AliasAttribute.");
						}
					}
					modelsByAlias[info.Alias] = info;
				}
				if(restHandlerDescriptors != null)
				{
					info.RegisterRestHandlers(restHandlerDescriptors);
				}
			}
			var aliasList = modelsByAlias.Values.Where(a => !string.IsNullOrEmpty(a.Alias)).ToArray();
			foreach(var info in modelsByName.Values)
			{
				info.CompatibleAliases = aliasList.Where(a => a.ModelIs(info.ModelType)).Select(a => a.Alias).ToArray();
			}
		}

		public IAFLModel GetModel(object node)
		{
			if (node == null)
			{
				return null;
			}

			var key = "afl-model-" + GetID(node);
			object model;
			aflCache.Load(key, out model);
			if(model is null)
			{
				model = GetInfo(node).MakeInstance(node, Host.Services);
				aflCache.Store(key, model);
			}

			return (IAFLModel)model;
		}

		//in the case of umbraco: this takes a fake IPublishedContent
		public IAFLModel GetDBBackedModelFromWrappedNativeDBType(object node)
		{
			if (node is null)
			{
				return null;
			}

			//TODO: Fix for Umbraco8
			var key = "afl-db-model-" + GetID(node);
			object model;
			aflCache.Load(key, out model);
			if (model == null)
			{
				model = TryGetInfo(node)?.MakeInstance(node, Host.Services);
				if (model != null)
				{
					aflCache.Store(key, model);
				}
			}

			return (IAFLModel)model;
		}


		public T GetModel<T>(object node) where T : IAFLModel
		{
			return (T)GetModel(node);
		}

		public IAFLModel GetModel(string nodeID)
		{
			return GetModel(implementation.GetNode(nodeID));
		}

		public T GetModel<T>(string nodeID) where T : IAFLModel
		{
			return (T)GetModel(nodeID);
		}

		public IAFLModel GetDBBackedModel(string nodeID)
		{
			var node = implementation.GetDBNode(nodeID);
			return GetDBBackedModelFromWrappedNativeDBType(node);
		}

		public T GetDBBackedModel<T>(string nodeID) where T : IAFLModel
		{
			return (T)GetDBBackedModel(nodeID);
		}

		//in the case of umbraco: this takes an IContent
		public object GetDBBackedModelFromNativeDBType(object dbType)
		{
			if (dbType == null) { return null; }
			var node = implementation.NativeDBTypeToDBNode(dbType);
			return GetDBBackedModelFromWrappedNativeDBType(node);
		}
		public T GetDBBackedModelFromNativeDBType<T>(object dbType) where T : IAFLModel
		{
			return (T)GetDBBackedModelFromNativeDBType(dbType);
		}

		public IEnumerable<T> GetTopLevelModels<T>() where T : IAFLModel
		{
			var info = TryGetInfo(typeof(T));
			return implementation.GetTopLevelNodes(info).Select(n => GetModel<T>(n)).ToArray();
		}

		public AFLTypeInfo GetInfo(string alias)
		{
			var info = TryGetInfo(alias);
			if(info == null) { throw new Exception("Unregistered alias: " + alias); }
			return info;
		}

		public AFLTypeInfo TryGetInfo(string alias)
		{
			if (modelsByAlias.ContainsKey(alias))
			{
				return modelsByAlias[alias];
			}
			return null;
		}

		public AFLTypeInfo GetInfo(Type modelT)
		{
			if (modelsByName.ContainsKey(modelT.AssemblyQualifiedName))
			{
				return modelsByName[modelT.AssemblyQualifiedName];
			}
			return null;
		}

		public AFLTypeInfo GetInfo(object obj)
		{
			if(obj is IAFLModel)
			{
				return GetInfo(obj.GetType());
			}

			var alias = GetAlias(obj);
			if (alias == null) { return null; }
			return GetInfo(alias); //assume IPublishedContent.
		}

		public AFLTypeInfo TryGetInfo(object node)
		{
			var alias = GetAlias(node);
			if (alias == null) { return null; }
			return TryGetInfo(alias);
		}

		public string GetAlias(object node)
		{
			return implementation.GetAlias(node);
		}

		public string GetID(object node)
		{
			return implementation.GetID(node);
		}

		public object GetNode(Guid nodeID)
		{
			return implementation.GetNode(nodeID);
		}

		public object GetNode(string nodeID)
		{
			return implementation.GetNode(nodeID);
		}

		public object GetDBBackedNode(string nodeID)
		{
			return implementation.GetDBNode(nodeID);
		}

		public T GetNode<T>(Guid nodeID)
		{
			return (T)implementation.GetNode(nodeID);
		}
		public T GetNode<T>(string nodeID)
		{
			return (T)implementation.GetNode(nodeID);
		}

		public object GetParentNode(object node)
		{
			return implementation.GetParentNode(node);
		}

		public IEnumerable<PropertyTypeData> GetCMSPropertiesForAlias(string alias)
		{
			return implementation.GetPropertiesForAlias(alias);
		}

		public string InferAliasFromTypeName(string name)
		{
			return implementation.InferAliasFromTypeName(name);
		}

		public string InferViewName(IAFLModel model)
		{
			return implementation.InferViewName(model);
		}

		public Type NodeType => implementation.NodeType;
	}

	public class RegistryEventArgs {}
}