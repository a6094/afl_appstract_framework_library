﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFLCore.Utils
{
    /// <summary>
    /// Extensions for Partial view
    /// </summary>
    public static class PartialViewExtensions
    {
        /// <summary>
        /// Check if a partial view exists, better than try catch...
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="viewName"></param>
        /// <returns></returns>
        public static bool PartialExists(this HtmlHelper helper, string viewName)
        {
			throw new NotImplementedException("RE IMPLEMENT ME");
            if (string.IsNullOrEmpty(viewName))
            {
                throw new ArgumentNullException(viewName, "View name cannot be empty");
            }
            //var view = ViewEngines.Engines.FindPartialView(helper.ViewContext, viewName);
            //return view.View != null;
        }

    }
}
