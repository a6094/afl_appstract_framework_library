﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace AFLCore.Utils
{
	public static class StringUtils
	{

		public static string StripHtml(string str)
		{
			if (string.IsNullOrWhiteSpace(str)) { return string.Empty; }
			HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
			doc.LoadHtml(str);
			StringBuilder sb = new StringBuilder();
			foreach (var node in doc.DocumentNode.ChildNodes)
			{
				sb.Append(node.InnerText);
			}
			return sb.ToString();
		}

		public static string CombineUri(params string[] uriParts)
		{
			string uri = string.Empty;
			if (uriParts != null && uriParts.Count() > 0)
			{
				char[] trims = new char[] { '\\', '/' };
				uri = (uriParts[0] ?? string.Empty).TrimEnd(trims);
				for (int i = 1; i < uriParts.Count(); i++)
				{
					uri = string.Format("{0}/{1}", uri.TrimEnd(trims), (uriParts[i] ?? string.Empty).TrimStart(trims));
				}
			}
			return uri;
		}
	}
}
