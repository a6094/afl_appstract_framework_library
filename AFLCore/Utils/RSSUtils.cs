﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace AFLCore.Utils
{
	/*
	 * RSS feed generator
	 *  
	 * produces valid RSS according to http://validator.w3.org/feed/check.cgi
	 * 
	 * Example usage:
	 * 
	 * new RSSChannel(
	 *	 title: "Nyheder", 
	 *	 description: "", 
	 *	 link: "http://bl.dk", 
	 *	 items: newsItems.Select(a => new RSSItem(
	 *		 title: "tit", 
	 *		 description: "desc", 
	 *		 link: "http://bl.dk" + a.Url, 
	 *		 source: a.NewsSource, 
	 *		 sourceURL: "http://bl.dk", 
	 *		 pubDate: a.CreateDate,
	 *		 guid: "http://bl.dk" + a.Url
	 *	 )
	 * ).ToList())
	 */

	public interface IRSSFeed
	{
		RSSChannel RSSChannel { get; }
		bool ShowRSSFeedLink { get; }
	}

	public class XmlActionResult : ActionResult
	{
		private XDocument document;

		public XmlActionResult(XDocument document)
		{
			this.document = document;
		}

		public override void ExecuteResult(ActionContext context)
		{
			context.HttpContext.Response.Clear();
			context.HttpContext.Response.ContentType = "text/xml";
			var writer = new XmlTextWriter(context.HttpContext.Response.Body, Encoding.UTF8);
			document.WriteTo(writer);
			writer.Close();
			writer.Dispose();
		}
	}

	public class RSSItem
	{
		public string Title { get; private set; }
		public string Description { get; private set; }
		public string Link { get; private set; }
		public string Author { get; private set; }
		public string Category { get; private set; }
		public string Comments { get; private set; }
		//public string Enclosure { get; private set; }
		public string GUID { get; private set; }
		public DateTime PubDate { get; private set; }
		public string Source { get; private set; }
		public string SourceURL { get; private set; }
		
		public RSSItem(string title, string description, string link, string author = "", string category = "", string comments = "", string guid = "", DateTime pubDate = new DateTime(), string source = "", string sourceURL = null)
		{
			Title = title;
			Description = description;
			Link = link;
			Author = author;
			Category = category;
			Comments = comments;
			GUID = guid;
			PubDate = pubDate;
			Source = source;
			SourceURL = sourceURL ?? ""; //sourceURL must be specified if source is specified, but if the data contains an empty value it is still better not to crash.
			if (!string.IsNullOrEmpty(source))
			{
				if (sourceURL == null) //check that the user did provide the sourceURL argument - but allow it to be "".
				{
					throw new ArgumentException("If source is specified, then sourceURL must also be specified.");
				}
			}
		}

		public XElement GetXMLNode()
		{
			var item = new XElement("item",
				new XElement("title", Title),
				new XElement("description", Description),
				new XElement("link", Link)
			);
			if (!string.IsNullOrEmpty(Author)) item.Add(new XElement("author", Author));
			if (!string.IsNullOrEmpty(Category)) item.Add(new XElement("category", Category));
			if (!string.IsNullOrEmpty(Comments)) item.Add(new XElement("comments", Comments));
			if (!string.IsNullOrEmpty(GUID)) item.Add(new XElement("guid", GUID));
			if (PubDate != DateTime.MinValue) item.Add(new XElement("pubDate", PubDate.ToString("r"))); //almost rfc822
			if (!string.IsNullOrEmpty(Source)) item.Add(new XElement("source", Source, new XAttribute("url", SourceURL)));
			return item;
		}
	}

	public class RSSChannel
	{
		public string Title { get; private set; }
		public string Description { get; private set; }
		public string Link { get; private set; }
		public DateTime LastBuildDate { get; private set; }
		public DateTime PubDate { get; private set; }
		public int TTL { get; private set; }

		private List<RSSItem> items;
		public List<RSSItem> Items { get {
			if (items == null) { items = new List<RSSItem>(); }
			return items;
		}}

		public RSSChannel(string title, string description, string link, DateTime lastBuildDate = new DateTime(), DateTime pubDate = new DateTime(), int ttl = 43200, IEnumerable<RSSItem> items = null)
		{
			Title = title;
			Description = description;
			Link = link;
			LastBuildDate = lastBuildDate;
			PubDate = pubDate;
			TTL = ttl;
			this.items = items.ToList();
			this.items.Sort((x, y) => -x.PubDate.CompareTo(y.PubDate));
		}

		public void AddItem(RSSItem item)
		{
			items.Add(item);
			items.Sort((x, y) => x.PubDate.CompareTo(y.PubDate)); //todo: not optimal to sort after each add - use the constructor parameter instead.
		}

		public XDocument GetXMLDocument()
		{
			var channel = new XElement("channel",
				new XElement("title", Title),
				new XElement("description", Description),
				new XElement("link", Link),
				new XElement("ttl", TTL) //although it's not mandatory, we default initialize it to 12 hours, so let's just include it no matter what.
			);
			if (LastBuildDate != DateTime.MinValue) channel.Add(new XElement("lastBuildDate", LastBuildDate.ToString("r"))); //almost rfc822
			if (PubDate != DateTime.MinValue) channel.Add(new XElement("pubDate", PubDate.ToString("r"))); //almost rfc822

			foreach(var item in items)
			{
				channel.Add(item.GetXMLNode());
			}

			return new XDocument(new XElement("rss", new XAttribute("version", "2.0"), channel));
		}

		public XmlActionResult GetXMLActionResult()
		{
			return new XmlActionResult(GetXMLDocument());
		}
	}
}
