﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AFLCore.Utils
{
	public static class LinqExtensions
	{
		// TODO: DistinctBy Has been replaced in .Net6
		//public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		//{
		//	HashSet<TKey> seenKeys = new HashSet<TKey>();
		//	foreach (TSource element in source)
		//	{
		//		if (seenKeys.Add(keySelector(element)))
		//		{
		//			yield return element;
		//		}
		//	}
		//}

		/// <summary>
		/// Returns a sequence consisting of the head element and the given tail elements.
		/// </summary>
		/// <typeparam name="T">Type of sequence</typeparam>
		/// <param name="head">Head element of the new sequence.</param>
		/// <param name="tail">All elements of the tail. Must not be null.</param>
		/// <returns>A sequence consisting of the head elements and the given tail elements.</returns>
		/// <remarks>This operator uses deferred execution and streams its results.</remarks>
		public static IEnumerable<T> Concat<T>(this T head, IEnumerable<T> tail)
		{
			if (tail == null) throw new ArgumentNullException("tail");
			return tail.Prepend(head);
		}

		/// <summary>
		/// Returns a sequence consisting of the head elements and the given tail element.
		/// </summary>
		/// <typeparam name="T">Type of sequence</typeparam>
		/// <param name="head">All elements of the head. Must not be null.</param>
		/// <param name="tail">Tail element of the new sequence.</param>
		/// <returns>A sequence consisting of the head elements and the given tail element.</returns>
		/// <remarks>This operator uses deferred execution and streams its results.</remarks>
		public static IEnumerable<T> Concat<T>(this IEnumerable<T> head, T tail)
		{
			if (head == null) throw new ArgumentNullException("head");
			return Enumerable.Concat(head, Enumerable.Repeat(tail, 1));
		}

		/// <summary>
		/// Prepends a single value to a sequence.
		/// </summary>
		/// <typeparam name="TSource">The type of the elements of <paramref name="source"/>.</typeparam>
		/// <param name="source">The sequence to prepend to.</param>
		/// <param name="value">The value to prepend.</param>
		/// <returns>
		/// Returns a sequence where a value is prepended to it.
		/// </returns>
		/// <remarks>
		/// This operator uses deferred execution and streams its results.
		/// </remarks>
		/// <code>
		/// int[] numbers = { 1, 2, 3 };
		/// IEnumerable&lt;int&gt; result = numbers.Prepend(0);
		/// </code>
		/// The <c>result</c> variable, when iterated over, will yield 
		/// 0, 1, 2 and 3, in turn.

		public static IEnumerable<TSource> Prepend<TSource>(this IEnumerable<TSource> source, TSource value)
		{
			if (source == null) throw new ArgumentNullException("source");
			return Enumerable.Concat(Enumerable.Repeat(value, 1), source);
		}

		public static T FirstOrFallback<T>(this IEnumerable<T> me, T fallback) where T: class
		{
			if(me == null) { return fallback; } //extra userfriendly: now works on a null IEnumerable!
			return me.FirstOrDefault() ?? fallback;
		}

		/// <summary>
		/// Fisher–Yates shuffle: statistically correct way of shuffling a list.
		/// </summary>
		public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> list, Random rnd)
		{
			var newList = list.ToArray();
			for(var i=0; i < newList.Length; i++)
			{
				newList.Swap(i, rnd.Next(i, newList.Length));
			}
			return newList;
		}

		public static void Swap<T>(this IList<T> list, int i, int j)
		{
			var temp = list[i];
			list[i] = list[j];
			list[j] = temp;
		}

		///<summary>Finds the index of the first item matching an expression in an enumerable.</summary>
		///<param name="items">The enumerable to search.</param>
		///<param name="predicate">The expression to test the items against.</param>
		///<returns>The index of the first matching item, or -1 if no items match.</returns>
		public static int FindIndex<T>(this IEnumerable<T> items, Func<T, bool> predicate) {
			if (items == null) throw new ArgumentNullException("items");
			if (predicate == null) throw new ArgumentNullException("predicate");

			int retVal = 0;
			foreach (var item in items) {
				if (predicate(item)) return retVal;
				retVal++;
			}
			return -1;
		}
		///<summary>Finds the index of the first occurence of an item in an enumerable.</summary>
		///<param name="items">The enumerable to search.</param>
		///<param name="item">The item to find.</param>
		///<returns>The index of the first matching item, or -1 if the item was not found.</returns>
		public static int IndexOf<T>(this IEnumerable<T> items, T item) { return items.FindIndex(i => EqualityComparer<T>.Default.Equals(item, i)); }
	}
}
