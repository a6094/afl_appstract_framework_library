﻿using System.Text;

namespace AFLCore.Extensions
{
	public static class StringExtensions
	{
		public static string Capitalize(this string str)
		{
			if (str == null)
			{
				return null;
			}
			if (str.Length > 1)
			{
				return char.ToUpper(str[0]) + str.Substring(1);
			}
			return str.ToUpper();
		}	
		public static bool IsNullOrWhitespace(this string str)
		{
			return string.IsNullOrWhiteSpace(str);
		}

		public static bool IsNullOrEmpty(this string str)
		{
			return string.IsNullOrEmpty(str);
		}
		public static string ReplaceLastOccurence(this string str, string find, string replace)
		{
			var index = str.LastIndexOf(find);
			if (index > -1)
			{
				str = str.Substring(0, index) + str.Substring(index).Replace(find, replace);
			}
			return str;
		}

		/// <summary>
		/// Flattens UTF-8 to ASCII so we can search it out again. æ becomes a, ø becomes o, å becomes a
		/// </summary>
		/// <param name="q"></param>
		/// <returns></returns>
		public static string SearchFriendlyString(this string q)
		{
			byte[] tempBytes;
			tempBytes = Encoding.GetEncoding("ISO-8859-8").GetBytes(q);
			return Encoding.UTF8.GetString(tempBytes);
		}
	}
}