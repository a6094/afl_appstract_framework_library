﻿using System;

namespace AFLCore.EventHandling
{
	public enum CMSEvent { 
		Published, 
		Unpublished, 
		Saved,
		Saving,
		Deleted,
		Deleting,
		Publishing,
		Unpublishing,
	};

	/// <summary>
	/// Usage:
	/// Specify the event type(s) that should trigger the method.
	/// The method must take excactly one parameter of type CMSEventArgs.
	/// Method will be invoked on a DB model instance.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class CMSEventHandlerAttribute : Attribute
	{
		public readonly CMSEvent[] events;
		public CMSEventHandlerAttribute(params CMSEvent[] events)
		{
			this.events = events;
			if (events == null || events.Length == 0)
			{
				throw new Exception("CMSEventHandlerAttribute requires at least one event type parameter.");
			}
		}
	}

	public class CMSEventArgs<T> where T : class
	{
		public CMSEventArgs(CMSEvent eventType, T nativeEvent)
		{
			EventType = eventType;
			NativeEvent = nativeEvent;
		}
		public CMSEvent EventType { get; protected set; }
		public T NativeEvent { get; protected set; }
	}
}
