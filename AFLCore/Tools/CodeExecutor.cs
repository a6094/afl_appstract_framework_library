﻿//using System;
//using System.CodeDom.Compiler;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Text.RegularExpressions;
//using AFL.Core;
//using Microsoft.CSharp;

//namespace AFL.Tools
//{
//	[Rest]
//	public class CodeExecutor
//	{
//		[RestMethod(Default = true, DebugOnly = true, Description = "Execute C# code in context of current application")]
//		public object Index(string code, string httpMethod)
//		{
//			if (httpMethod == "POST")
//			{
//				try
//				{
//					return ExecProgramMain(code)?.ToString() ?? "<No return value>";
//				}
//				catch (Exception e)
//				{
//					return e.Message;
//				}
//			}

//			var html = new HTMLBuilder(true);
//			html.Head.AddChild(new Elem("script").AddAttr("src", "http://code.jquery.com/jquery-3.1.0.min.js"));
//			html.Head.AddChild(new Elem("script", html: "$(() => { $('.submit').on('click', e => { e.preventDefault(); $.post(window.location, { code: $('.code').val() }).done(res => $('.output').val(res)) }) });"));
//			html.Body.AddChild(new Elem("form")
//				.AddChild(new Elem("textarea", Regex.Replace(@"
//					using System.Linq;
//					using AFL;
//					class Program {
//					  public static object Main() {
					
//return string.Join(""\n"", Registry.Current.AllModelsByName.Values.Select(info => info.Alias));
//					  }
//					}", "\n\t+", "\n"), classes: "code").AddAttr("rows", "6").AddAttr("cols", "100")
//				)
//				.AddChild(new Elem("button", "Run!", classes: "submit"))
//			);
//			html.Body.AddChild(new Elem("textarea", classes: "output").AddAttr("cols", "100").AddAttr("name", "code"));
//			return new HTMLActionResult(html.Result);
//		}

//		public static MethodInfo GetProgramMainExecutor(string code, IEnumerable<string> assemblies = null)
//		{
//			CompilerParameters parameters = new CompilerParameters
//			{
//				GenerateInMemory = true,
//				TreatWarningsAsErrors = false,
//				GenerateExecutable = false,
//				CompilerOptions = "/optimize"
//			};
//			parameters.ReferencedAssemblies.AddRange(new[] { "mscorlib.dll", "System.Core.dll", "System.dll", "Microsoft.CSharp.dll", new Uri(typeof(Registry).Assembly.CodeBase).LocalPath });
//			if (assemblies != null) { parameters.ReferencedAssemblies.AddRange(assemblies.ToArray()); }
//			var results = new CSharpCodeProvider(new Dictionary<String, String> { { "CompilerVersion", "v4.0" } }).CompileAssemblyFromSource(parameters, code);

//			if (results.Errors.Count > 0)
//			{
//				throw new Exception(string.Join("\n", results.Errors.Cast<CompilerError>().Select(error => error.ErrorText)));
//			}
//			else
//			{
//				var module = results.CompiledAssembly.GetModules()[0];
//				return module.GetType("Program")?.GetMethod("Main");
//			}
//		}

//		public static object ExecProgramMain(string code, object arg = null, IEnumerable<string> assemblies = null)
//		{
//			var method = GetProgramMainExecutor(code, assemblies);
//			return method?.Invoke(null, method.GetParameters().Count() == 0 ? new object[] { } : new object[] { arg });
//		}

//		public static MethodInfo GetExecutor(string returnExpression, string argList, IEnumerable<string> usings = null, IEnumerable<string> assemblies = null)
//		{
//			return GetProgramMainExecutor(string.Join("\n", (usings ?? Enumerable.Empty<string>()).Select(s => "using " + s + ";")) + @"
//			using System.Linq;
//			using AFL;
//			class Program {
//				public static object Main(" + argList + @") {
//				return " + returnExpression + @";
//				}
//			}", assemblies);
//		}

//		public static string AssemblyPathFromType(Type type)
//		{
//			return new Uri(type.Assembly.CodeBase).LocalPath;
//		}
//	}
//}
