﻿using AFLCore.Core.ActionResults;
using AFLCore.Rest.Attributes;
using System;
using System.Linq;

namespace AFLCore.Tools
{
	[Rest]
	public class ModelListing
	{
		[RestMethod(DebugOnly = true, Default = true, Description = "Lists all models registered by the system, with extra info.")]
		public object ModelList()
		{
			var html = new HTMLBuilder(true);
			throw new NotImplementedException();
			//html.Body.AddTable(Registry.Current.AllModelsByName.Values.Select(m => new object[] {
			//	m.ModelType.Name,
			//	m.Alias,
			//	new Elem().AddTable(m.GetRegisteredCMSProperties().Values.Select(p => new object[] { p.Name, p.PropertyType.Name }), new[] { "Name", "Type" }),
			//	m.Renderable
			//}), new[] { "Name", "Alias", "Properties", "Renderable" });
			return new HTMLActionResult(html.Result);
		}
	}
}
