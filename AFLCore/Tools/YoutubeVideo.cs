﻿using System;


namespace AFLCore.Tools
{

	public class YoutubeVideo
	{
		private string duration, defaultDuration;
		private string youtubeImageUrl = "//img.youtube.com/vi/{videoid}/0.jpg";
		private string youtubeImageMaxResUrl = "//img.youtube.com/vi/{videoid}/maxresdefault.jpg";
		private string youtubeVideoInfoUrl = "http://gdata.youtube.com/feeds/api/videos/{videoid}?alt=json";
		private string videoId = "";
		public YoutubeVideo(string videoId)
		{
			this.videoId = videoId;
		}

		public YoutubeVideo(string videoId, string defaultDuration) : this(videoId)
		{
			this.defaultDuration = defaultDuration;
		}

		public string ImageURL { get { return youtubeImageUrl.Replace("{videoid}", videoId); } }

		public string ImageMaxResURL { get { return youtubeImageMaxResUrl.Replace("{videoid}", videoId); } }
		public string Id { get { return videoId; } }
		/// <summary>
		/// Youtube does not support unauthenticated API calls anymore. Hence getting the exact duration of a video is currently not supported and 0 will be returned.
		/// </summary>
		// This post explains how to fix it: https://stackoverflow.com/questions/15596753/youtube-api-v3-how-to-get-video-durations
		public string Duration => "0";

		public string DefaultDuration { get { return defaultDuration; } }

		private string SecondsToMinutes(string seconds)
		{
			int secondsInt;
			if (int.TryParse(seconds, out secondsInt))
			{
				TimeSpan ts = TimeSpan.FromSeconds(secondsInt);
				return string.Format("{0:D2}:{1:D2}", ts.Minutes, ts.Seconds);
			}
			return "";
		}
	}
}