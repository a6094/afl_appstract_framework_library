﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AFLCore.Tools
{
	public class HTMLBuilder
	{
		public HTMLBuilder(bool withBootstrap)
		{
			if (withBootstrap) { Head.AddChild(new Elem("link").AddAttr("rel", "stylesheet").AddAttr("href", "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css")); }
		}
		public Elem Head { get; private set; } = new Elem("head");
		public Elem Body { get; private set; } = new Elem("body");

		public string Result => "<!DOCTYPE html>\n" + new Elem("html", children: new[] { Head, Body });
	}

	public class Elem
	{
		private TagBuilder builder;
		private List<Elem> children;
		public Elem(string tagName = "div", string text = null, string classes = null, IEnumerable<KeyValuePair<string, string>> attributes = null, IEnumerable<Elem> children = null, string html = null)
		{
			builder = new TagBuilder(tagName);
			if (classes != null) { builder.AddCssClass(classes); }
			if (text != null) { builder.InnerHtml.SetContent(text); }
			if (html != null) { builder.InnerHtml.AppendHtml(html); }
			if (attributes != null)
			{
				foreach (var attr in attributes)
				{
					builder.Attributes.Add(attr.Key, attr.Value);
				}
			}
			if (children != null)
			{
				Children.AddRange(children);
			}
		}

		public string Classes
		{
			set { builder.AddCssClass(value); }
		}

		public IDictionary<string, string> Attributes
		{
			get { return builder.Attributes; }
		}

		public Elem AddAttr(string key, string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				if (builder.Attributes.ContainsKey(key))
				{
					builder.Attributes.Remove(key);
				}
				builder.Attributes.Add(key, value);
			}
			return this;
		}
		public Elem AddAttrIf(bool condition, string key, string value)
		{
			if (condition) { AddAttr(key, value); }
			return this;
		}

		public Elem AddAttributes(Dictionary<string, string> attributes)
		{
			if (attributes != null)
			{
				foreach (KeyValuePair<string, string> item in attributes)
				{
					AddAttr(item.Key, item.Value);
				}
			}
			return this;
		}

		public Elem SetText(string value)
		{
			builder.InnerHtml.SetContent(value);
			return this;
		}

		public Elem AddChild(Elem child)
		{
			Children.Add(child);
			return this;
		}

		public Elem AddParagraph(string text)
		{
			Children.Add(new Elem("p", text));
			return this;
		}

		/// <summary>
		/// splits text into lines, and adds paragraphs.
		/// </summary>
		public Elem AddParagraphs(string text)
		{
			return AddParagraphs(text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries));
		}

		public Elem AddParagraphs(IEnumerable<string> text)
		{
			Children.AddRange(text.Select(line => new Elem("p", line)));
			return this;
		}

		public Elem AddTable(IEnumerable<IEnumerable<object>> data, IEnumerable<object> head = null, bool withBorder = true)
		{
			var table = new Elem("table", classes: withBorder ? "table-bordered table-striped" : "");
			if (head != null) { table.AddChild(new Elem("tr").AddChildren(head.Select(d => new Elem("th").AddChild(d is Elem ? (Elem)d : new TextTag(d?.ToString()))))); }
			table.AddChildren(data.Select(row => new Elem("tr").AddChildren(row.Select(d => new Elem("td").AddChild(d is Elem ? (Elem)d : new TextTag(d?.ToString()))))));
			AddChild(table);
			return this;
		}

		public Elem AddText(string text)
		{
			Children.Add(new TextTag(text));
			return this;
		}

		public Elem AddGlyph(string name, string color = null)
		{
			AddChild(new Elem("span", classes: "glyphicon glyphicon-" + name).AddAttrIf(color != null, "style", "color: " + color + ";"));
			return this;
		}

		public Elem AddChildIf(bool condition, Func<Elem> child)
		{
			if (condition) { Children.Add(child()); }
			return this;
		}

		public Elem AddChildren(IEnumerable<Elem> children)
		{
			Children.AddRange(children);
			return this;
		}

		public List<Elem> Children
		{
			get
			{
				if (children == null) { children = new List<Elem>(); }
				return children;
			}
		}

		public override string ToString()
		{
			if (children != null)
			{
				builder.InnerHtml.SetContent("");
				builder.InnerHtml.SetContent(string.Join("", children.Select(c => c.ToString())));
			}
			return builder.ToString();
		}

		class TextTag : Elem
		{
			string txt;
			public TextTag(string t) { txt = t; }
			public override string ToString() { return txt; }
		}
	}
}
