﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AFLCore.Tools
{
	public class StringEncryption
	{
		private readonly Random random;
		private readonly byte[] key;
		private readonly RijndaelManaged rm;
		private readonly UTF8Encoding encoder;
		public static StringEncryption Current { get; } = new StringEncryption();

		private StringEncryption()
		{
			this.random = new Random();
			this.rm = new RijndaelManaged();
			this.encoder = new UTF8Encoding();
			this.key = this.encoder.GetBytes("simple short enc");
		}

		public string Encrypt(string unencrypted)
		{
			var vector = new byte[16];
			this.random.NextBytes(vector);
			var cryptogram = vector.Concat(this.Encrypt(this.encoder.GetBytes(unencrypted), vector));
			return ToBase64(cryptogram.ToArray());
		}

		public string Decrypt(string encrypted)
		{
			var cryptogram = FromBase64(encrypted.Trim());
			if (cryptogram == null) { return ""; }

			var vector = cryptogram.Take(16).ToArray();
			var buffer = cryptogram.Skip(16).ToArray();
			return this.encoder.GetString(this.Decrypt(buffer, vector));
		}

		private byte[] Encrypt(byte[] buffer, byte[] vector)
		{
			var encryptor = this.rm.CreateEncryptor(this.key, vector);
			return this.Transform(buffer, encryptor);
		}

		private byte[] Decrypt(byte[] buffer, byte[] vector)
		{
			var decryptor = this.rm.CreateDecryptor(this.key, vector);
			return this.Transform(buffer, decryptor);
		}

		private byte[] Transform(byte[] buffer, ICryptoTransform transform)
		{
			var stream = new MemoryStream();
			using (var cs = new CryptoStream(stream, transform, CryptoStreamMode.Write))
			{
				cs.Write(buffer, 0, buffer.Length);
			}

			return stream.ToArray();
		}

		private byte[] FromBase64(string msg)
		{
			if (string.IsNullOrEmpty(msg)) { return null; }
			msg = msg.Replace('_', '/').Replace('-', '+');
			switch (msg.Length % 4)
			{
				case 2: msg += "=="; break;
				case 3: msg += "="; break;
			}
			return Convert.FromBase64String(msg);
		}

		private string ToBase64(byte[] msg)
		{
			if (msg == null || msg.Length == 0) { return ""; }
			return Convert.ToBase64String(msg).TrimEnd('=').Replace('+', '-').Replace('/', '_');
		}
	}
}
