﻿namespace AFLCore.Options
{
	public class AFLOptions
	{
		public bool DebugMode { get; set; } = false;
		public string RestBasePath { get; set; }
	}
}
