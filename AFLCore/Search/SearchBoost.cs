﻿namespace AFLCore.Search
{
	public enum SearchBoost { NotIndexed, Low, Normal, High, VeryHigh, Special, CustomField };
}
