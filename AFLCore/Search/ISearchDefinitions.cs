﻿using AFLCore;
using System.Collections.Generic;

namespace AFLCore.Search
{
	public interface ISearchDefinitions
	{
		IEnumerable<SearchIndex> SearchIndexes { get; }

		void AddCustomFields(SearchIndex index, IAFLModel entity, IDictionary<string, List<object>> fields);

		bool PreventIndexing(IAFLModel model, SearchIndex index);
	}
}
