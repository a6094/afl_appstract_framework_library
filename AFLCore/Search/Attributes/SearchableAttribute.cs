﻿using AFLCore.Search;
using System;

namespace AFLCore.Search.Attributes
{
	[AttributeUsage(AttributeTargets.Property, Inherited = true)]
	public class SearchableAttribute : Attribute
	{
		public SearchableAttribute(int indexID = -1, SearchBoost boost = SearchBoost.Normal, string customFieldName = null, bool rawCMSProperty = false)
		{
			if (customFieldName != null != (boost == SearchBoost.CustomField)) { throw new InvalidOperationException("custom field name should (and should only) be set if boost is set to CustomField"); }
			SearchIndexID = indexID;
			Boost = boost;
			RawCMSProperty = rawCMSProperty;
			CustomFieldName = customFieldName;
		}

		public SearchableAttribute(SearchBoost boost = SearchBoost.Normal, string customFieldName = null, bool rawCMSProperty = false) : this(-1, boost, customFieldName, rawCMSProperty)
		{
		}

		public SearchableAttribute() : this(-1)
		{
		}

		public int SearchIndexID { get; private set; }

		public SearchBoost Boost { get; private set; }

		public string CustomFieldName { get; private set; }

		/// <summary>
		/// If this is set to true, or the return-type is not a simple type (TypeUtils.IsSimpleType),
		/// then the umbraco property will be evaluated directly by the search indexer,
		/// without invoking the model property.
		/// </summary>
		public bool RawCMSProperty { get; set; }
	}
}
