﻿using System;

namespace AFLCore.Search.Attributes
{
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public class NodeNameSearchableInherited : NodeNameSearchableAttribute
	{
		public NodeNameSearchableInherited(SearchBoost boost = SearchBoost.Normal) : base(boost)
		{

		}
	}
}
