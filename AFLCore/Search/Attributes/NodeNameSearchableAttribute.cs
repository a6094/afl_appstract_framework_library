﻿using System;

namespace AFLCore.Search.Attributes
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class NodeNameSearchableAttribute : Attribute
	{
		public NodeNameSearchableAttribute(SearchBoost boost = SearchBoost.Normal)
		{
			Boost = boost;
		}
		public SearchBoost Boost { get; set; }
	}
}
