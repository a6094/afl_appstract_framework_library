﻿using System;

namespace AFLCore.Search.Attributes
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class LimitIndexingAttribute : Attribute
	{
		public LimitIndexingAttribute(params int[] searchIndexIDs)
		{
			SearchIndexIDs = searchIndexIDs;
		}

		public int[] SearchIndexIDs { get; private set; }
	}
}
