﻿using System;

namespace AFLCore.Search.Attributes
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class IndexedAsRenderablePage : Attribute
	{
	}
}
