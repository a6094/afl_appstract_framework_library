﻿using System;
using System.Linq;

namespace AFLCore.Search
{
	/// <summary>
	/// initialize some of these in a project-specific static class, into readonly static properties with useful names,
	/// one for each index, so the different indexes can be conveniently accessed.
	/// </summary>
	public class SearchIndex
	{
		public SearchIndex(int id, string indexName)
		{
			ID = id;
			IndexName = indexName;
			PublishedValuesOnly = true;
			ParentID = null;
			IncludeItemTypes = Array.Empty<string>();
			ExcludeItemTypes = Array.Empty<string>();
		}
		public SearchIndex(int id, string indexName, bool publishedValuesOnly, int? parentID) : this(id, indexName)
		{
			PublishedValuesOnly = publishedValuesOnly;
			ParentID = parentID;
		}
		public SearchIndex(int id, string indexName, bool publishedValuesOnly = true, int? parentID = null, string[] includeItemTypes = null, string[] excludeItemTypes = null) : this(id, indexName, publishedValuesOnly, parentID)
		{
			IncludeItemTypes = includeItemTypes ?? Array.Empty<string>();
			ExcludeItemTypes = excludeItemTypes ?? Array.Empty<string>();
		}
		public int ID { get; protected set; }
		public string Searcher { get; protected set; }
		public string IndexName { get; protected set; }

		public bool PublishedValuesOnly { get; protected set; }
		public int? ParentID { get; protected set; }
		public string[] IncludeItemTypes { get; protected set; }
		public string[] ExcludeItemTypes { get; protected set; }
	}
}
