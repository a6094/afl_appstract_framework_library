﻿using System;

namespace AFLCore.Rest.Attributes
{
	/// <summary>
	/// This attribute must be present, for a property to be used by the rest system.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class RestPropertyAttribute : Attribute
	{
		public RestPropertyAttribute(string shortName)
		{
			ShortName = shortName;
		}

		public RestPropertyAttribute() : this(null)
		{
		}

		/// <summary>
		/// Shorter alias to use in query arguments.
		/// Note that this is not used for forms.
		/// </summary>
		public string ShortName { get; private set; }

		/// <summary>
		/// Note that this is not respected for forms.
		/// </summary>
		public bool Encrypted { get; set; }
	}
}
