﻿using System;
using System.Text.RegularExpressions;

namespace AFLCore.Rest.Attributes
{
	/// <summary>
	/// Specifies that the decorated method can be used with Rest.
	/// The optional name will be used as path for the rest url. falls back to the method name.
	/// Note that the name can contain slashes, so that the resulting path can be any arbitrary depth.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class RestMethodAttribute : Attribute
	{
		public RestMethodAttribute(string name = null)
		{
			if (name != null) //allow null, since it will fall back to method name, but otherwise, check the format for illegal chars.
			{
				name = name.Trim('/');
				if (!Regex.IsMatch(name, "^[a-zA-Z0-9_\\-\\./]+$"))
				{
					throw new InvalidOperationException("Invalid rest path name: " + name);
				}
			}
			Name = name;
		}

		public string Name { get; protected set; }

		/// <summary>
		/// Indicates that the handler path will not include the class part.
		/// </summary>
		public bool Global { get; set; }

		/// <summary>
		/// Default handler in class. This means its path does not include the method name.
		/// </summary>
		public bool Default { get; set; }

		public bool AllowPost { get; set; } = true;
		public bool AllowGet { get; set; } = true;

		/// <summary>
		/// This should mainly be used for backoffice handlers, where the node might be dynamically obtained clientside.
		/// </summary>
		public bool UnencryptedRoute { get; set; }

		/// <summary>
		/// The model is not created through CreateModelInstance by default, since it should normally instantiate the class containing this method.
		/// This can be inconvenient when, eg. using inheritance, in which case CreateModelDynamically should be set.
		/// This property is ignored if the handler method is static.
		/// </summary>
		public bool CreateModelDynamically { get; set; }

		/// <summary>
		/// If true, the return value of this method will be used for rendering a razor view, as specified by the route argument.
		/// Make sure the return value is void or derived from IAFLModel, or this will have no effect.
		/// </summary>
		public bool UseRouteView { get; set; }

		/// <summary>
		/// ...
		/// </summary>
		public bool CMSRoutable { get; set; }

		/// <summary>
		/// Marks this rest handler method as a model form submit handler.
		/// If the return value is void or derived from IAFLModel, this will render a view, just like if using UseRouteView - alternatively, returning e.g. a redirect will also work.
		/// </summary>
		public bool ModelFormSubmitHandler { get; set; }

		public bool DebugOnly { get; set; }
		public bool HideFromHandlerList { get; set; }

		/// <summary>
		/// Descriptions are shown on the rest root url, in debug mode.
		/// </summary>
		public string Description { get; set; }
	}
}
