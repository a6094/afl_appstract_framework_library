﻿using System;
using System.Text.RegularExpressions;

namespace AFLCore.Rest.Attributes
{
	/// <summary>
	/// Specifies that the decorated class can be used with Rest.
	/// The optional name will be used as path for the rest url. falls back to the class name.
	/// Note that the name can contain slashes, so that the resulting path can be any arbitrary depth.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public class RestAttribute : Attribute
	{
		public RestAttribute(string name = null)
		{
			if (name != null) //allow null, since it will fall back to method name, but otherwise, check the format for illegal chars.
			{
				name = name.Trim('/');
				if (!Regex.IsMatch(name, "^[a-zA-Z0-9_\\-\\./]+$"))
				{
					throw new InvalidOperationException("Invalid rest path name: " + name);
				}
			}
			Name = name;
		}

		public string Name { get; protected set; }

		/// <summary>
		/// If true, this class can be instantiated and used for rendering with a view, as specified in the route parameter of the rest request.
		/// For this use-case, no RestMethod is needed.
		/// </summary>
		public bool RestRenderable { get; set; }

		/// <summary>
		/// Mark this model as submittable.
		/// This can be used together with a rest handler method which is marked as ModelFormSubmitHandler, or it can be used
		/// without a method. If using without a handler method, you should normally combine with RestRenderable, except if it's a GET submit and
		/// the model is just some component on the rendered page. In that case you don't want a redirect, and you just want the regular rendering to proceed
		/// after the component model has been filled with request data, and in that case ModelFormSubmittable makes sense without a handler method and without RestRenderable.
		/// </summary>
		public bool ModelFormSubmittable { get; set; }
	}
}
