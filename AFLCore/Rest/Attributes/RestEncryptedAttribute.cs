﻿using System;

namespace AFLCore.Rest.Attributes
{
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
	public class RestEncryptedAttribute : Attribute
	{

	}
}
