﻿using System;

namespace AFLCore.Exceptions
{

	[Serializable]
	public class AFLRegistryNotInitializedException : Exception
	{
		private const string DEFAULT_ERROR_MESSAGE = "The globa AFL registry hasn't been initialized. " +
			"In order to set it up call the method Registry.Initialize(...) with the required parameters into your IComponent.Initialize method";

		public AFLRegistryNotInitializedException() : this(DEFAULT_ERROR_MESSAGE) { }

		public AFLRegistryNotInitializedException(string message) : base(message) { }

		public AFLRegistryNotInitializedException(string message, Exception inner) : base(message, inner) { }

		protected AFLRegistryNotInitializedException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}
