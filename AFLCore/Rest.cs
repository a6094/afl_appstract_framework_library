﻿//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace AFL
//{
//	public static class Rest
//	{
//		/// <summary>
//		/// Use the attribute [Rest(ModelFormSubmittable = true)] on your model, and use this kind of stuff in the view:
//		/// 
//		/// @using(Html.BeginModelForm(Model))
//		/// {
//		/// 	@Html.ValidationMessageFor(m => m.TestField, "", new { @class = "text-blah" })
//		/// 	@Html.TextBoxFor(x => x.TestField)
//		/// 	<button>submit</button>
//		/// }
//		/// </summary>
//		public static CshtmlViewPartDisposable BeginModelForm(this HtmlHelper helper, IAFLModel model, string method = null, string viewName = null, string httpMethod = "post", string htmlClasses = "", IEnumerable<KeyValuePair<string, string>> attributes = null)
//		{
//			var attributeString = "";
//			if(attributes != null)
//			{
//				attributeString = string.Join(" ", attributes.Select(kv => kv.Key + "=\""+kv.Value+"\""));
//			}

//			helper.ViewContext.Writer.Write("<form method=\""+httpMethod+"\" "+(string.IsNullOrEmpty(htmlClasses) ? "" : " class=\""+ htmlClasses + "\" ")+ attributeString + ">");

//			var global = false;
//			var path = (model.GetType().Name + (method!=null ? "/"+method : "")).ToLower();
//			if (!Registry.Current.RestHandlerDescriptors.ContainsKey(path))
//			{
//				path = (method ?? "").ToLower();
//				global = true;
//			}
//			if (!Registry.Current.RestHandlerDescriptors.ContainsKey(path))
//			{
//				throw new KeyNotFoundException("No matching path found in rest route table");
//			}
//			if (Registry.Current.RestHandlerDescriptors[path].Attribute.Global != global)
//			{
//				throw new KeyNotFoundException("No matching path found in rest route table: expected global: " + global);
//			}

//			var route = new RestUrl(path, model.UID, viewName, internalRoute: true).GetEncryptedRouteString();
//			helper.ViewContext.Writer.Write("<input type=\"hidden\" name=\""+RestUrl.RouteParam+"\" value=\""+route+"\">");

//			return new CshtmlViewPartDisposable(helper, "</form>");
//		}

//		/// <summary>
//		/// This generates a simple GET form, which will submit fields back to the specified path (CMS route) or current page, the model of which should support "Rest",
//		/// and have rest-settable properties. The properties will be set from the request, and these will be available when the view renders.
//		/// This method is the same as just implementing a simple <form></form>, and is partially just provided as a reminder that this is possible :)
//		/// </summary>
//		public static CshtmlViewPartDisposable BeginSimpleForm(this HtmlHelper helper, string url = null, string htmlClasses = "", IEnumerable<KeyValuePair<string, string>> attributes = null)
//		{
//			var attributeString = "";
//			if (attributes != null)
//			{
//				attributeString = string.Join(" ", attributes.Select(kv => kv.Key + "=\"" + kv.Value + "\""));
//			}
//			var action = url==null ? "" : " action=\""+url+"\"";
//			helper.ViewContext.Writer.Write("<form method=\"get\" "+action+(string.IsNullOrEmpty(htmlClasses) ? "" : " class=\"" + htmlClasses + "\" ") + attributeString + ">");
//			return new CshtmlViewPartDisposable(helper, "</form>");
//		}

//		public static void FillModelFromQuery(object model, bool useShortName, bool respectEncryptedAttribute, NameValueCollection requestParams, string encryptedParams = null)
//		{
//			var encryptedMap = encryptedParams==null ? null : HttpUtility.ParseQueryString(encryptedParams);
//			var info = Registry.Current.GetInfo(model.GetType());
//			foreach(var settable in info.RestSettableProperties)
//			{
//				var name = ((useShortName ? settable.Attribute.ShortName : null) ?? settable.Property.Name).ToLower();
//				var useEncrypted = (respectEncryptedAttribute && (settable.Attribute?.Encrypted ?? false));
//				var map = useEncrypted ? encryptedMap : requestParams;
//				string value = map[name];


//                if (value != null && settable.Property.PropertyType.IsEquivalentTo(typeof(bool)))
//				{
//					value = value.ToLower();
//					settable.Property.SetValue(model, value == "true" || value == "yes" || value == "y" || value == "1" || value == "on");
//				}
//                else if (value != null && settable.Property.PropertyType.IsEnum)
//                {
//                    var typedValue = Enum.Parse(settable.Property.PropertyType, value, true);
//                    settable.Property.SetValue(model, typedValue);
//                }
//				else if(value != null)
//				{
//					var typedValue = Convert.ChangeType(value, settable.Property.PropertyType);
//					settable.Property.SetValue(model, typedValue);
//				}
//				else
//				{
//					var head = name + ".";
//					var matches = map.Keys.Cast<string>().Where(key => key?.ToLower().StartsWith(head) ?? false);
//					if(matches.Any())
//					{
//						var children = new NameValueCollection();
//						foreach(var match in matches)
//						{
//							children.Add(match.Substring(head.Length).ToLower(), map[match]);
//						}
//						FillModelFromQuery(settable.Property.GetValue(model), useShortName, false, children);
//					}
//				}
//			}
//		}

//		/// <summary>
//		/// Call this from a rest handler, to figure out which validation constraints failed.
//		/// </summary>
//		public static List<ValidationResult> GetValidationErrors(object model)
//		{
//			var context = new ValidationContext(model, serviceProvider: null, items: null);
//			var results = new List<ValidationResult>();
//			Validator.TryValidateObject(model, context, results, true);
//			return results;
//		}
		
//		/// <summary>
//		/// Call this from a rest handler, to figure out if any validation constraints failed.
//		/// </summary>
//		public static bool IsModelValid(object model)
//		{
//			return !GetValidationErrors(model).Any();
//		}

//		public static string SetQueryArg(string key, object value)
//		{
//			var query = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
//			query.Set(key, value?.ToString() ?? "");
//			return HttpContext.Current.Request.Path + "?" + query.ToString();
//		}

//		public class CshtmlViewPartDisposable : IDisposable
//		{
//			private HtmlHelper helper;
//			private string output;
//			public CshtmlViewPartDisposable(HtmlHelper helper, string output)
//			{
//				this.helper = helper;
//				this.output = output;
//			}

//			public void Dispose()
//			{
//				this.helper.ViewContext.Writer.Write(output);
//			}
//		}
//	}
//}
