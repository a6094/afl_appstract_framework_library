﻿using System;

namespace AFLCore.Attributes
{
	[AttributeUsage(AttributeTargets.Property, Inherited = false)]
	public class PropertyAliasAttribute : Attribute
	{
		private string alias;

		public PropertyAliasAttribute(string alias = null)
		{
			this.alias = alias;
		}

		public string Alias => alias;
	}
}
