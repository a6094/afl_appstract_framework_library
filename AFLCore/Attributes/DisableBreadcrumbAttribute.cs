﻿using System;

namespace AFLCore.Attributes
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class DisableBreadcrumbAttribute : Attribute
	{
	}
}
