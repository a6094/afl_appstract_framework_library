﻿using System;

namespace AFLCore.Attributes
{
	/// <summary>
	/// Attribute to signify that the interface is an umbraco composition
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface, AllowMultiple = false)]
	public class CompositionAttribute : Attribute
	{
		//TODO: THIS HAS NOWHERE HOME IN AFLCORE nor REGISTRY. RETHINK!
	}
}
