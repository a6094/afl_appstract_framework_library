﻿using System;

namespace AFLCore.Attributes
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class DisableIndexingAttribute : Attribute
	{
	}
}
