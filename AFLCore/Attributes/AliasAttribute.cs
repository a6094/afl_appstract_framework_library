﻿using System;

namespace AFLCore.Attributes
{
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public class AliasAttribute : Attribute
	{
		private string alias;

		public AliasAttribute(string alias = null)
		{
			this.alias = alias;
		}

		public string Alias => alias;

		/// <summary>
		/// Use this to indicate that the class is meant to override another (specified class) in the alias -> entity type map.
		/// </summary>
		public Type Override { get; set; }
	}
}
