﻿using Microsoft.AspNetCore.Mvc;

namespace AFLCore.Core.ActionResults
{
	public class RedirectToNodeActionResult : IAFLActionResult
	{
		public RedirectToNodeActionResult(string nodeID, string queryString = null)
		{
			RedirectToNodeID = nodeID;
			QueryString = queryString;
		}

		public string RedirectToNodeID { get; protected set; }
		public string QueryString { get; protected set; }

		public IActionResult GetMVCAction(ICMSDefaultController controller)
		{
			return controller.RedirectToNode(RedirectToNodeID, QueryString);
		}
	}
}
