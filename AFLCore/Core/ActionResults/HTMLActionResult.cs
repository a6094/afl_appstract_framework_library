﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFLCore.Core.ActionResults
{
	public class HTMLActionResult : IAFLActionResult
	{
		public HTMLActionResult(string value = "")
		{
			Buffer.Append(value);
		}

		public StringBuilder Buffer { get; private set; } = new StringBuilder();

		public IActionResult GetMVCAction(ICMSDefaultController controller)
		{
			var result = new ContentResult();
			result.Content = Buffer.ToString();
			result.ContentType = "text/html";
			return result;
		}
	}
}
