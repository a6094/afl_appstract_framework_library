﻿using Microsoft.AspNetCore.Mvc;

namespace AFLCore.Core.ActionResults
{
	public class PartialActionResult : IAFLActionResult
	{
		public PartialActionResult(string name, IAFLModel model)
		{
			ViewName = name;
			Model = model;
		}
		/// <summary>
		/// View can be null, in which case the CMS decides.
		/// </summary>
		public string ViewName { get; protected set; }
		public IAFLModel Model { get; protected set; }

		public IActionResult GetMVCAction(ICMSDefaultController controller)
		{
			return controller.Partial(ViewName, Model);
		}
	}
}
