﻿using Microsoft.AspNetCore.Mvc;

namespace AFLCore.Core.ActionResults
{
	public interface IAFLActionResult
	{
		IActionResult GetMVCAction(ICMSDefaultController controller);
	}
}
