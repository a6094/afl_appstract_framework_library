﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace AFLCore.Core.ActionResults
{
	public class RedirectToUrlActionResult : IAFLActionResult
	{
		public RedirectToUrlActionResult(HttpContext context, string url = null)
		{
			RedirectToUrl = url ?? context.Request.GetDisplayUrl();
		}

		public string RedirectToUrl { get; protected set; }

		public IActionResult GetMVCAction(ICMSDefaultController controller)
		{
			return controller.RedirectToUrl(RedirectToUrl);
		}
	}
}
