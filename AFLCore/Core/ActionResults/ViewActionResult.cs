﻿using Microsoft.AspNetCore.Mvc;

namespace AFLCore.Core.ActionResults
{
	public class ViewActionResult : IAFLActionResult
	{
		public ViewActionResult(string name, IAFLModel model)
		{
			ViewName = name;
			Model = model;
		}
		/// <summary>
		/// View can be null, in which case the CMS decides.
		/// </summary>
		public string ViewName { get; protected set; }
		public IAFLModel Model { get; protected set; }

		public IActionResult GetMVCAction(ICMSDefaultController controller)
		{
			return ViewName == null ? controller.View(Model) : controller.View("~/Views/" + ViewName + ".cshtml", Model); // hardcoded view path, because otherwise only /views/shared/ etc are searched!?!
		}
	}
}
