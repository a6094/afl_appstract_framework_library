﻿using AFLCore.Rest.Attributes;
using System.Reflection;

namespace AFLCore.Core
{
	public class RestPropertyInfo
	{
		public RestPropertyInfo(PropertyInfo prop, RestPropertyAttribute attr)
		{
			Property = prop;
			Attribute = attr;
		}

		public PropertyInfo Property { get; private set; }
		public RestPropertyAttribute Attribute { get; private set; }
	}
}