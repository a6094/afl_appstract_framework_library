﻿using AFLCore.Caching;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AFLCore.Core
{
	public abstract class MemoSupportBase : IMemoSupport
	{
		private readonly IAFLCache aflCache;
		public MemoSupportBase(IAFLCache aflCache)
		{
			this.aflCache = aflCache;
		}
		public Injector Injector => Injector.FromPool();

		public TPost MemoImplementation<TPre, TPost>(Func<TPre> func, Func<TPre, TPost> postProcessFunc, TPost fallback, string uid, string callerName, AFLTypePropertyInfo propertyInfo = null)
		{
			var cacheKey = uid + callerName;
			object value = null;

			if (aflCache.Load(cacheKey, out value))
			{
				return (TPost)value;
			}

			value = func();

			if (postProcessFunc != null)
			{
				if (IsEmptyValue(value, false) && typeof(TPre).IsArray)
				{
					value = Array.CreateInstance(typeof(TPre).GetElementType(), 0);
				}
				value = postProcessFunc((TPre)value);
				if (IsEmptyValue(value, false))
				{
					value = fallback;
				}
			}
			else if (IsEmptyValue(value, false))
			{
				//if there is no specified fallback and TPost is an array type, then use an empty array of TPosts element type as fallabck, so we don't have a special case of null returns.
				value = fallback;
				if (typeof(TPost).IsArray && fallback == null)
				{
					value = Array.CreateInstance(typeof(TPost).GetElementType(), 0);
				}
			}

			aflCache.Store(cacheKey, value, propertyInfo.GetAttribute<CacheAttribute>());
			return (TPost)value;
		}


		public bool IsSimpleType<T>()
		{
			return typeof(T) == typeof(string) || typeof(T) == typeof(int) || typeof(T) == typeof(double) || typeof(T) == typeof(bool) || typeof(T) == typeof(DateTime);
		}

		//todo: and then move the DateTime check out to where it needs to be treated as a simple type?
		bool IsSimpleType2<T>()
		{
			var type = typeof(T);
			return type.IsPrimitive
				|| type.IsEnum
				|| type.Equals(typeof(string))
				|| type.Equals(typeof(decimal));
		}

		public bool IsEmptyValue(object v, bool considerZeroInt)
		{
			if (v == null) { return true; }
			if (v is string) { return (string)v == ""; }
			if (v is DateTime) { return (DateTime)v == DateTime.MinValue; }
			if (considerZeroInt && v is int) { return (int)v == 0; }
			if (considerZeroInt && v is double) { return (double)v == 0; }
			return false;
		}

		public abstract bool GetComplexValueFromObject(object objectValue, Type resultType, out object value, object contextNode);
		public abstract object GetValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse, Injector injector);

		public object EnumerableToTypedArray(Type elementType, IEnumerable<object> list)
		{
			var arr = list.ToArray();
			var typedArray = Array.CreateInstance(elementType, arr.Length);
			if (arr.Any(e => e == null || !elementType.IsAssignableFrom(e.GetType()))) //sanity check, for better error output.
			{
				throw new InvalidOperationException("Error creating list: " + arr.First(e => e == null || !elementType.IsAssignableFrom(e.GetType()))?.GetType().FullName + " is not of type " + elementType.FullName);
			}
			Array.Copy(arr, typedArray, arr.Length);
			return typedArray;
		}

		public abstract IAFLModel GetModel(string id, object contextNode);
		public abstract IAFLModel GetModel(Guid guid, object contextNode, bool isMedia = false);

		public abstract IAFLModel GetModel(object content);
	}
}
