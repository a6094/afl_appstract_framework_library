﻿using System;
using System.Collections.Generic;

namespace AFLCore.Core
{
	public interface IMemoSupport
	{
		TPost MemoImplementation<TPre, TPost>(Func<TPre> func, Func<TPre, TPost> postProcessFunc, TPost fallback, string uid, string callerName, AFLTypePropertyInfo propertyInfo = null);
		bool IsSimpleType<T>();
		bool IsEmptyValue(object v, bool considerZeroInt);
		object GetValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse, Injector injector);
		//object GetPropertyValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse);
		bool GetComplexValueFromObject(object objectValue, Type resultType, out object value, object contextNode);
		object EnumerableToTypedArray(Type elementType, IEnumerable<object> list);

		//helper method for making DB models / regular models easier
		IAFLModel GetModel(string id, object contextNode);
		IAFLModel GetModel(Guid guid, object contextNode, bool isMedia = false);
		//helper method for making DB models / regular models easier
		IAFLModel GetModel(object content);
		Injector Injector { get; }
	}
}
