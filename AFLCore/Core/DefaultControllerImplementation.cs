﻿using System;
using AFLCore.Core.ActionResults;
using AFLCore.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AFLCore.Core
{
	public class DefaultControllerImplementation
	{
		private ICMSDefaultController _controller;
		private readonly HttpContext _httpContext;
		private readonly Registry _registry;

		public DefaultControllerImplementation(ICMSDefaultController c, HttpContext httpContext, Registry registry)
		{
			_controller = c;
			_httpContext = httpContext;
			_registry = registry;
		}

		public IActionResult IndexHelper(object node)
		{
			//if ((httpContext.Request.Method == "POST" || httpContext.Request.Method == "GET") && httpContext.Request.Query[RestUrl.RouteParam] != default(StringValues))
			//{
			//	var action = HandleModelFormSubmit();
			//	if (action != null) { return action; }
			//}

			IAFLModel typedModel;
			var redirect = HandleRedirectsOrCreateModel(node, out typedModel);

			//if (httpContext.Items["afl-rest-route-handler"] != null) //todo! test stuff...
			//{
			//	var result = InvokeRestHandler(new RestUrl(httpContext.Request.GetUrl, nodeID: typedModel.UID, viewName: null), httpContext.Request.HttpMethod, formSubmit: false, handlerInfo: (RestHandlerInfo)httpContext.Items["afl-rest-route-handler"]);
			//	return ObjectToResult(result);
			//}

			if (redirect != null)
			{
				return redirect;
			}

			//defer to the ApplySpecialRules() method. normally this will return null...
			return _controller.MakeActionResult(typedModel);
		}

		private ActionResult Content(string content, string contentType = null)
		{
			var c = new ContentResult();
			c.Content = content;
			if (contentType != null) { c.ContentType = contentType; }
			return c;
		}

		//protected ActionResult HandleModelFormSubmit()
		//{
		//	var parms = httpContext.Request.Params;
		//	if (httpContext.Request.Method == "POST")
		//	{ //if the url of the origin page of the post contained a 'route' query arg, then we might have a problem here, since the Params will then contain that route along with the actual current FORM route (hidden form value). We fix that here by throwing away the wrong route arg.
		//		parms = new NameValueCollection(httpContext.Request.Query);
		//		parms.Set(RestUrl.RouteParam, httpContext.Request.Form[RestUrl.RouteParam]);
		//	}

		//	var route = new RestUrl(null, parms);
		//	var result = InvokeRestHandler(route, httpContext.Request.Method, true);
		//	return ObjectToResult(result);
		//}

		public IActionResult RssHelper(object node)
		{
			IAFLModel typedModel;
			var redirect = HandleRedirectsOrCreateModel(node, out typedModel);
			if (!(typedModel is IRSSFeed))
			{
				return Content("no feed here.");
			}
			return (typedModel as IRSSFeed).RSSChannel.GetXMLActionResult();
		}

		protected IActionResult HandleRedirectsOrCreateModel(object node, out IAFLModel typedModel)
		{
			typedModel = null;
			var info = _registry.TryGetInfo(node);
			if (info == null) { return null; } //could be alias not registered. needed to support sites which only use afl partially.
			if (!info.Renderable)
			{
				while ((node = _registry.GetParentNode(node)) != null)
				{
					if (_registry.GetInfo(node)?.Renderable ?? false)
					{
						return _controller.RedirectToNode(_registry.GetID(node));
					}
				}
				throw new Exception("No renderable root?!");
			}

			typedModel = _registry.GetModel(node);
			if (typedModel == null)
			{
				throw new InvalidOperationException("Typed model could not be created for node type: " + _registry.GetAlias(node));
			}

			//TODO: Consider if rest is something we want
			////if this class supports it, fill in the settable properties...
			//if (info?.GetAttribute<RestAttribute>() != null)
			//{
			//	Rest.FillModelFromQuery(typedModel, true, true, httpContext.Request.QueryString);
			//}

			return null;
		}

		private IActionResult ObjectToResult(object result)
		{
			if (result is ActionResult)
			{
				return result as ActionResult;
			}
			else if (result is IAFLActionResult)
			{
				var action = (IAFLActionResult)result;
				return action.GetMVCAction(_controller);
			}
			else if (result is string)
			{
				return Content(result as string, "text/plain");
			}
			else if (result == null) // needed for form submittal, where null means continue normal rendering.
			{
				return null;
			}
			else
			{
				return Content(System.Text.Json.JsonSerializer.Serialize(result), "application/json");
			}
		}

		//private object[] FillRestParameters(MethodInfo method, RestUrl route)
		//{
		//	var encryptedMap = route.EncryptedQueryCollection;
		//	var parms = method.GetParameters();
		//	List<object> result = new List<object>();
		//	foreach (var parm in parms)
		//	{
		//		object arg = parm.HasDefaultValue ? parm.DefaultValue : null;
		//		if (parm.ParameterType == typeof(ModelStateDictionary)) //beware: there is also a ModelStateDictionary in System.Web.Http.blarh...
		//		{
		//			arg = controller.ModelState;
		//		}
		//		else if (parm.ParameterType == typeof(TempDataDictionary))
		//		{
		//			arg = (controller as ControllerBase)?.TempData;
		//		}
		//		else if (parm.ParameterType == typeof(httpContext))
		//		{
		//			arg = httpContext;
		//		}
		//		else if(parm.Name == "requestBody")
		//		{
		//			var bodyStream = new StreamReader(httpContext.Request.InputStream);
		//			bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
		//			var body = bodyStream.ReadToEnd();
		//			if(parm.ParameterType == typeof(string))
		//			{
		//				arg = body;
		//			}
		//			else
		//			{
		//				arg = JsonConvert.DeserializeObject(body, parm.ParameterType);
		//			}
		//		}
		//		else if (parm.ParameterType == typeof(HttpPostedFileBase)) //fileupload is supported. methods must have a HttpPostedFileBase type argument, and the name maps to the html name.
		//		{
		//			arg = new HttpPostedFileWrapper(httpContext.Request.Files[parm.Name]);
		//		}
		//		else
		//		{
		//			var encrypted = parm.GetCustomAttribute<RestEncryptedAttribute>()!=null;
		//			string value = encrypted ? encryptedMap?[parm.Name] : httpContext.Request.Params[parm.Name];
		//			if(value != null)
		//			{
		//				try {
		//				arg = Convert.ChangeType(value, parm.ParameterType);
		//				} catch {} //ignore.
		//			}
		//		}
		//		result.Add(arg);
		//	}
		//	return result.ToArray();
		//}

		//private object InvokeRestHandler(RestUrl route, string httpMethod, bool formSubmit, RestHandlerInfo handlerInfo = null)
		//{
		//	if(handlerInfo == null)
		//	{
		//		if (string.IsNullOrEmpty(route.Path) || !Registry.Current.RestHandlerDescriptors.ContainsKey(route.Path))
		//		{
		//			if (Registry.AFLDebugMode)
		//			{
		//				var html = new HTMLBuilder(true);
		//				html.Body.AddChild(new Elem("h2", "List of available handlers:"));
		//				html.Body.AddTable(Registry.Current.RestHandlerDescriptors.Values.Where(h => !h.Attribute.HideFromHandlerList).Select(h => new object[] {
		//				new Elem("a", h.Path).AddAttr("href", Registry.Current.RestBasePath + h.Path),
		//				h.Attribute.Description
		//			}), new[] { "URL", "Description" });
		//				return new HTMLActionResult(html.Result);
		//			}
		//			return "";
		//		}
		//		handlerInfo = Registry.Current.RestHandlerDescriptors[route.Path];
		//	}
		//	if ((httpMethod == "GET" && !handlerInfo.Attribute.AllowGet) || (httpMethod == "POST" && !handlerInfo.Attribute.AllowPost) || (!route.RouteIsEmpty && route.EncryptedRoute != !handlerInfo.Attribute.UnencryptedRoute))
		//	{
		//		return "http method not allowed.";
		//	}
		//	object obj = null;
		//	if (handlerInfo.Method == null || !handlerInfo.Method.IsStatic)
		//	{
		//		var node = string.IsNullOrEmpty(route.NodeID) ? null : Registry.Current.GetNode(route.NodeID);
		//		if (node != null)
		//		{
		//			if (handlerInfo.Attribute.CreateModelDynamically)
		//			{
		//				obj = Registry.Current.GetModel(node);
		//			}
		//			else
		//			{
		//				obj = Activator.CreateInstance(handlerInfo.Type, node);
		//			}
		//		}
		//		else if (handlerInfo.Type.GetConstructor(new Type[0]) != null)
		//		{
		//			obj = Activator.CreateInstance(handlerInfo.Type);
		//		}
		//		else
		//		{
		//			return Registry.AFLDebugMode ? "Incorrect route. Check that the model implements IAFLModel, and that a node id is provided, or alternatively that the model has a default constructor." : "incorrect route.";
		//		}

		//		if(formSubmit)
		//		{
		//			var results = Rest.GetValidationErrors(obj);
		//			results
		//				.SelectMany(r => r.MemberNames.Select(n => new { name = n, error = r.ErrorMessage }))
		//				.ToList()
		//				.ForEach(e => controller.ModelState.AddModelError(e.name, e.error));
		//		}

		//		//fill properties.
		//		Rest.FillModelFromQuery(obj, true, true, httpContext.Request.Params, route.EncryptedQueryArgs);
		//	}

		//	if (formSubmit && (obj==null || !(Registry.Current.GetInfo(obj).GetAttribute<RestAttribute>()?.ModelFormSubmittable ?? false)))
		//	{
		//		return Content("model is null or not submittable.");
		//	}

		//	object result = null;
		//	if (handlerInfo.Method != null)
		//	{
		//		if(formSubmit && !handlerInfo.Attribute.ModelFormSubmitHandler) { return "method is not marked as submit handler."; }
		//		result = handlerInfo.Method.Invoke(obj, FillRestParameters(handlerInfo.Method, route)) ?? obj; //allow null result, and fall back to the handler object - useful for void returns from ModelFormSubmitHandler's

		//		if (result is IAFLModel && (handlerInfo.Attribute.UseRouteView || handlerInfo.Attribute.ModelFormSubmitHandler))
		//		{
		//			result = HandleRenderableRestResult(result, route, handlerInfo, httpMethod, formSubmit);
		//		}
		//	}
		//	else if (handlerInfo.ClassAttribute.RestRenderable)
		//	{
		//		result = HandleRenderableRestResult(obj, route, handlerInfo, httpMethod, formSubmit);
		//	}
		//	return result;
		//}

		//private object HandleRenderableRestResult(object result, RestUrl route, RestHandlerInfo handlerInfo, string httpMethod, bool formSubmit)
		//{
		//	if (string.IsNullOrEmpty(route.ViewName))
		//	{
		//		//if this is a modelform situation, and no view is specified, then the default is to show the same view again if error, or redirect if success...
		//		if (handlerInfo.ClassAttribute.ModelFormSubmittable && formSubmit)
		//		{
		//			if (controller.ModelState.IsValid && httpMethod == "POST")
		//			{
		//				return new RedirectToNodeActionResult(((IAFLModel)result).UID, httpContext.Request.QueryString.ToString());
		//			}
		//			else
		//			{
		//				return new ViewActionResult(null, (IAFLModel)result);
		//			}
		//		}
		//		return "Missing view in route.";
		//	}
		//	return (route.ViewName.StartsWith("@") ? (object)new PartialActionResult(route.ViewName.Substring(1), (IAFLModel)result) : (object)new ViewActionResult(route.ViewName, (IAFLModel)result));
		//}
	}
}
