﻿using System;
using System.Collections.Generic;

namespace AFLCore.Core
{
	public interface IAFLImplementation
	{
		//IServiceProvider ServiceProvider { get; }
		void OnStartup();
		string GetAlias(object node);
		string GetID(object node);
		object GetNode(string nodeID);
		object GetNode(Guid nodeID);

		IEnumerable<object> GetTopLevelNodes(AFLTypeInfo nodeType = null);

		/// <summary>
		/// Same as GetNode, but instead the returned node will be backed by (slow!) direct database content (in umbarco: IContent).
		/// </summary>
		object GetDBNode(string nodeID);

		/// <summary>
		/// This method should wrap the native DB type (in umbraco it's called IContent) in a facade to make it compatible with the native cms node type (in umbraco: IPublishedContent)
		/// </summary>
		object NativeDBTypeToDBNode(object dbType);

		object GetParentNode(object node);
		string InferViewName(IAFLModel model);

		/// <summary>
		/// Should return null if the alias does not map to a type. otherwise return a list of property all aliases that can be accessed for this type (including inherited ones).
		/// </summary>
		IEnumerable<PropertyTypeData> GetPropertiesForAlias(string alias);
		string InferAliasFromTypeName(string name);

		Type NodeType { get; }

	}
}