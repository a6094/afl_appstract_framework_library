﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace AFLCore.Core
{
	public interface ICMSDefaultController
	{
		IActionResult RedirectToNode(string toNodeID, string queryString = null);
		IActionResult RedirectToUrl(string url);
		IActionResult View(IAFLModel model);
		IActionResult Partial(string viewName, IAFLModel model);
		IActionResult View(string viewName, IAFLModel model = null);

		IActionResult MakeActionResult(IAFLModel model);

		ModelStateDictionary ModelState { get; }
	}
}
