﻿using AFLCore.Attributes;
using AFLCore.Rest.Attributes;
using AFLCore.Utils;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AFLCore.Core
{
	// This class allows to query over a type in a way that is useful for Umbraco. It makes custom attributes more accessible.
	public class AFLTypeInfo
	{
		private readonly Dictionary<string, AFLTypePropertyInfo> propertyInfo = new Dictionary<string, AFLTypePropertyInfo>();

		private Attribute[] attributes;
		private string[] compatibleAliases;
		private RestHandlerInfo[] restHandlers = new RestHandlerInfo[0];
		private FieldInfo nodeField;
		private IEnumerable<RestPropertyInfo> restSettableProperties;
		private Registry registry;

		public AFLTypeInfo(Type type, Registry registry)
		{
			ModelType = type;
			this.registry = registry;

			//NOTE: if you want a specific attribute to not be inherited then use [AttributeUsage (AttributeTargets.Class, Inherited = false)] in its definition.
			attributes = type.GetCustomAttributes<Attribute>(true).ToArray();

			HasDefaultConstructor = type.GetConstructor(new Type[] { }) != null;
			HasNodeConstructor = type.GetConstructor(new[] { registry.NodeType }) != null;
			var multiParamConstructor = type.GetConstructors().FirstOrDefault(x => x.GetParameters().Count() > 1);
			this.InjectionConstrutorParameters = multiParamConstructor?.GetParameters();
			HasInjectionContructor = multiParamConstructor != null;
			nodeField = ModelType.GetField("node", BindingFlags.NonPublic | BindingFlags.Instance);

			var aliasAttr = attributes.FirstOrDefault(a => a is AliasAttribute) as AliasAttribute;
			Alias = aliasAttr?.Alias ?? registry.InferAliasFromTypeName(type.Name);
			OverriddenType = aliasAttr?.Override;
			var cmsProperties = registry.GetCMSPropertiesForAlias(Alias)?.ToArray();
			if (cmsProperties == null)
			{
				Alias = null; //no such CMS type.
			}

			Renderable = attributes.FirstOrDefault(a => a is RenderableAttribute) != null;

			foreach (var prop in ModelType.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
			{
				propertyInfo[prop.Name] = new AFLTypePropertyInfo(prop, prop.DeclaringType == ModelType, cmsProperties);
			}
		}

		public string Alias { get; private set; }
		public Type ModelType { get; private set; }
		public bool Renderable { get; private set; }

		public bool HasNodeConstructor { get; private set; }
		public bool HasDefaultConstructor { get; private set; }
		/// <summary>
		/// Identifies whether services should be injected on construction
		/// </summary>
		public bool HasInjectionContructor { get; private set; }
		private ParameterInfo[] InjectionConstrutorParameters { get; set; }

		public Type OverriddenType { get; private set; }

		//all aliases that are compatible with this type, through inheritance. (only settable once, so Registry can initialize).
		public IEnumerable<string> CompatibleAliases { get { return compatibleAliases; } set { if (compatibleAliases != null) { throw new Exception("not allowed"); } compatibleAliases = value.ToArray(); } }

		public IEnumerable<RestPropertyInfo> RestSettableProperties
		{
			get
			{
				return restSettableProperties ?? (restSettableProperties =
					ModelType.GetProperties(BindingFlags.Instance | BindingFlags.Public)
					.Where(p => GetProperty(p.Name).GetAttribute<RestPropertyAttribute>() != null)
					.Select(p => new RestPropertyInfo(p, GetProperty(p.Name).GetAttribute<RestPropertyAttribute>()))
					.ToArray()
				);
			}
		}

		public IEnumerable<string> AllPropertyNames => propertyInfo.Keys;
		public IReadOnlyDictionary<string, AFLTypePropertyInfo> AllProperties => propertyInfo;

		public AFLTypePropertyInfo GetProperty(string name)
		{
			if (!propertyInfo.ContainsKey(name))
			{
				throw new InvalidOperationException(name + " is not a property on " + ModelType.Name + " (maybe it's defined private in a base class?)");
			}
			var prop = propertyInfo[name];
			if (prop.IsPrivate)
			{
				throw new InvalidOperationException(ModelType.Name + ": Using private properties with Memo is a bad idea, since they will break completely in derived classes. Use protected instead! Property: " + ModelType.Name + "." + name);
			}
			return prop;
		}

		public void RegisterRestHandlers(Dictionary<string, RestHandlerInfo> descriptors)
		{
			RestAttribute classAttr = GetAttribute<RestAttribute>();
			if (classAttr == null)
			{
				restHandlers = new RestHandlerInfo[0];
				return;
			}
			List<RestHandlerInfo> handlers = new List<RestHandlerInfo>();
			foreach (var m in ModelType.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance))
			{
				var attr = (RestMethodAttribute)m.GetCustomAttributes(typeof(RestMethodAttribute), false).FirstOrDefault();

				if (attr == null || !attr.CreateModelDynamically && m.DeclaringType != ModelType)
				{
					//if attribute is not marked as CreateModelDynamically, only register declared methods
					continue;
				}

				if (!attr.DebugOnly || registry.AFLDebugMode)
				{
					if (attr.Global && attr.Default) { throw new InvalidOperationException("Rest handler can not be both global and default"); }
					var path = ((attr.Global ? "" : classAttr.Name ?? ModelType.Name) + "/" + (attr.Default ? "" : attr.Name ?? m.Name)).Trim('/').ToLower();
					if (descriptors.ContainsKey(path)) { throw new InvalidOperationException("Duplicate rest handler path: '" + path + "'"); }
					handlers.Add(descriptors[path] = new RestHandlerInfo(ModelType, m, classAttr, attr, path));
				}
			}
			if (classAttr.RestRenderable || classAttr.ModelFormSubmittable) // RestRenderable means work-without-method and ModelFormSubmittable means submittable with-or-without-method :)
			{
				if (!typeof(IAFLModel).IsAssignableFrom(ModelType)) { throw new InvalidOperationException("Rest renderable model must be of IAFLModel type"); }
				var path = (classAttr.Name ?? ModelType.Name).ToLower();
				if (descriptors.ContainsKey(path)) { throw new InvalidOperationException("Duplicate rest handler path: '" + path + "'"); }
				handlers.Add(descriptors[path] = new RestHandlerInfo(ModelType, null, classAttr, null, path));
			}
			restHandlers = handlers.ToArray();
		}

		//returns true, if this entity type is, or derives from, the specified type.
		public bool EntityIs<T>()
		{
			return typeof(T).IsAssignableFrom(ModelType);
		}

		//returns true, if this model type is, or derives from, the specified type.
		public bool ModelIs(Type t)
		{
			return t.IsAssignableFrom(ModelType);
		}

		public IEnumerable<AttribT> GetAttributes<AttribT>() where AttribT : Attribute
		{
			return attributes.Where(a => typeof(AttribT).IsAssignableFrom(a.GetType())).Select(a => (AttribT)a);
		}

		public bool HasAttribute<AttribT>() where AttribT : Attribute
		{
			return GetAttributes<AttribT>().Any();
		}

		public AttribT GetAttribute<AttribT>() where AttribT : Attribute
		{
			return GetAttributes<AttribT>().FirstOrDefault();
		}

		/// <summary>
		/// Gets a dictionary of all mapped CMS properties declared in this type (not inherited) -> C# property.
		/// This method is slow and un-cached.
		/// </summary>
		public Dictionary<string, PropertyInfo> GetRegisteredCMSProperties()
		{
			//todo: if there are duplicate CMSName's in the values list, then the class has a weird definition. not sure what a nice response would be from here.
			return propertyInfo.Values.Where(v => v.DeclaredAtThisLevel && !string.IsNullOrEmpty(v.CMSName)).DistinctBy(v => v.CMSName).ToDictionary(v => v.CMSName, v => v.PropertyInfo);
		}

		/// <summary>
		/// Should not be called directly. Use the Registry.Current.GetModel method instead.
		/// </summary>
		public IAFLModel MakeInstance(object node, IServiceProvider serviceProvider)
		{
			IAFLModel entity = null;
			using var serviceScope = serviceProvider.CreateScope();
			if (HasNodeConstructor)
			{
				entity = (IAFLModel)Activator.CreateInstance(ModelType, new[] { node });
			}
			else if (HasDefaultConstructor)
			{
				entity = (IAFLModel)Activator.CreateInstance(ModelType);
				nodeField.SetValue(entity, node);
			}else if (HasInjectionContructor)
			{
				//TODO: TESTY TESTY
				var services = new List<object>() { node };
				foreach (var parameter in InjectionConstrutorParameters.Skip(1))
				{
					var service = serviceScope.ServiceProvider.GetRequiredService(parameter.ParameterType);
					services.Add(service);
				}
				entity = (IAFLModel)Activator.CreateInstance(ModelType, services.ToArray());
			}
			if (entity == null) { throw new InvalidOperationException("Failed to create instance for: " + Alias + ". node: " + node); }
			entity.Registry = registry;
			entity.MemoSupport = serviceScope.ServiceProvider.GetRequiredService<IMemoSupport>();
			return entity;
		}

		/// <summary>
		/// Returns a handler for the specified method. If methodName is null, it will return the class default handler, if any exists.
		/// </summary>
		public RestHandlerInfo GetRestHandler(string methodName)
		{
			var handler = restHandlers.FirstOrDefault(h => h.Method?.Name == methodName || methodName == null && h.Attribute.Default);

			if (handler == null && ModelType.BaseType != null)
			{
				handler = registry.GetInfo(ModelType.BaseType)?.GetRestHandler(methodName);
			}
			return handler;
		}

		/// <summary>
		/// only used by experimental umbraco routing rest thingie...
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public RestHandlerInfo GetRestHandlerByUrlName(string name)
		{
			return string.IsNullOrEmpty(name) ? null : restHandlers.FirstOrDefault(h => name.Equals(h.Attribute?.Name ?? h.Method?.Name, StringComparison.OrdinalIgnoreCase));
		}
	}
}