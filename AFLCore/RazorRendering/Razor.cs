﻿//using Microsoft.CodeDom.Providers.DotNetCompilerPlatform;
//using System;
//using System.CodeDom.Compiler;
//#if !DEBUG
//using System.Collections.Concurrent;
//#endif
//using System.IO;
//using System.Linq;
//using System.Web.Razor;

//namespace AFL.RazorRendering
//{
//	public class Razor
//	{
//		public static readonly Razor Instance = new Razor();

//#if !DEBUG
//		private readonly ConcurrentDictionary<string, Type> cache = new ConcurrentDictionary<string, Type>();
//#endif

//		private Razor()
//		{
//			// do nothing
//		}

//		public string Evaluate<TPage>(string viewFileName, dynamic model) where TPage : class, IPage
//		{
//			if (!File.Exists(viewFileName))
//			{
//				viewFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, viewFileName);
//			}

//			var key = viewFileName + "|" + typeof(TPage).AssemblyQualifiedName;

//			Type type;
//#if !DEBUG
//			if (!this.cache.TryGetValue(key, out type))
//#endif
//			{
//				type = this.GenerateType<TPage>(viewFileName, model);
//#if !DEBUG
//				this.cache.TryAdd(key, type);
//#endif
//			}

//			var template = Activator.CreateInstance(type) as TPage;
//			template.Model = model;
//			var result = template.ExecuteAndGetResult();
//			return result;
//		}

//		private Type GenerateType<TPage>(string viewFileName, dynamic model)
//		{
//			GeneratorResults generatorResults;
//			CompilerResults compilerResults;

//			var sourceLines = File
//				.ReadAllLines(viewFileName)
//				.Where(l => !l.StartsWith("@model ")) // remove lines starting with "@model " as the razor engine does not support it
//				;
//			var source = string.Join(Environment.NewLine, sourceLines);

//			// generate syntax tree from view
//			using (var reader = new StringReader(source))
//			{
//				var codeLanguage = new CSharpRazorCodeLanguage();
//				var engineHost = new RazorEngineHost(codeLanguage)
//				{
//					DefaultBaseClass = typeof(TPage).FullName,
//				};
//				engineHost.NamespaceImports.Add("System");

//				var templateEngine = new RazorTemplateEngine(engineHost);
//				generatorResults = templateEngine.GenerateCode(reader);
//			}

//			// generate assembly from syntax tree
//			using (var provider = new CSharpCodeProvider())
//			{
//				using (var writer = new StringWriter())
//				{
//					var options = new CodeGeneratorOptions();
//					provider.GenerateCodeFromCompileUnit(generatorResults.GeneratedCode, writer, options);

//					var code = writer.ToString();
//					var assemblyNames = AppDomain.CurrentDomain
//						.GetAssemblies()
//						.Where(a => !a.IsDynamic) // dynamic assemblies does not have a location
//						.Select(a => a.Location)
//						.Distinct()
//						.ToArray()
//						;
//					var parameters = new CompilerParameters(assemblyNames)
//					{
//						GenerateInMemory = true,
//					};
//					compilerResults = provider.CompileAssemblyFromDom(parameters, generatorResults.GeneratedCode);
//				}
//			}

//			if (compilerResults.Errors.HasErrors)
//			{
//				throw new CompileException(compilerResults);
//			}

//			var assembly = compilerResults.CompiledAssembly;
//			var type = assembly.GetType("Razor.__CompiledTemplate");
//			return type;
//		}
//	}
//}
