﻿using AFLCore.Utils;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AFLCore.GoogleRecaptcha
{
	public class ReCaptcha
	{
		public ReCaptcha(IHttpContextAccessor httpContextAccessor)
		{
			this.httpContextAccessor = httpContextAccessor;
		}
		public string RecaptchaPrivateKey => throw new NotImplementedException(); //TODO: AFL Options!?

		public bool IsValid(string EncodedResponse)
		{
			throw new NotImplementedException("NOT TESTED WITH CORE");
			if (string.IsNullOrWhiteSpace(RecaptchaPrivateKey))
			{
				throw new ArgumentException("Your recaptcha key is null, check your web config, please follow the instructions at our.umbraco.com/packages/backoffice-extensions/recaptcha", "original");
			}

			using (var client = new System.Net.WebClient())
			{
				string PrivateKey = RecaptchaPrivateKey;
				var GoogleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}&remoteip={2}", PrivateKey, EncodedResponse, SharedUtils.GetUserIP(httpContextAccessor.HttpContext)));
				var captchaResponse = JsonConvert.DeserializeObject<ReCaptcha>(GoogleReply);
				return captchaResponse.Success == "true";
			}
		}

		[JsonProperty("success")]
		public string Success
		{
			get { return m_Success; }
			set { m_Success = value; }
		}
		private string m_Success;

		[JsonProperty("error-codes")]
		public List<string> ErrorCodes
		{
			get { return m_ErrorCodes; }
			set { m_ErrorCodes = value; }
		}
		private List<string> m_ErrorCodes;
		private readonly IHttpContextAccessor httpContextAccessor;
	}

}
