﻿//using System;
//using System.Collections.Generic;
//using System.Collections.Specialized;
//using System.Linq;
//using System.Reflection;
//using System.Web;
//using AFL.Tools;

//namespace AFL
//{
//	// Is this supposed to be subclassed by consumers?
//	// What gets returned by the calls to GetProperties and GetCustomAttributes?
//	/// <summary>
//	/// Describes a single rest request.
//	/// This is used both to construct the rest url, and also to parse the url when the server receives a request.
//	/// </summary>
//	[Rest] //as a special case this is only included here so that we can use the RestProperty to serialize the special properties in this class.
//	public class RestUrl
//	{
//		public const string InternalRoutePath = "route/";
//		public const string RouteParam = "route";

//		private static List<Tuple<PropertyInfo, string>> RouteProperties = typeof(RestUrl).GetProperties()
//			.Where(p => p.GetCustomAttributes(typeof(RestPropertyAttribute), false).Any())
//			.Select(p => new Tuple<PropertyInfo, string>(p, ((RestPropertyAttribute)p.GetCustomAttributes(typeof(RestPropertyAttribute), false).First()).ShortName))
//			.ToList();

//		public RestUrl(string path, NameValueCollection query)
//		{
//			query = new NameValueCollection(query); //clone, so we don't modify original.
//			if (path != null && path.Length < Registry.Current.RestBasePath.Length) { path = path + "/"; } //avoid special case, where user types only the base-path without terminating slash. the code below doesn't like that :)
//			Path = path == null ? null : path.Substring(Registry.Current.RestBasePath.Length);
//			//internal route means that the path is stored inside the route. it also normally means that the actual url will contain the route ala: /route/[encrypted-route], but not in the case of BeginModelForm.
//			InternalRoute = Path == null || Path.StartsWith(InternalRoutePath) && Path.Split('/').Length == 2;
//			var route = InternalRoute && Path != null ? Path.Split('/')[1] : query[RouteParam]; //even though BeginModelForm uses internal route, we don't use a path-based route (/ws/route/xyz), so path will be null, and the route should be read from the route argument.
//			if (InternalRoute && Path != null) { Path = Path.Split('/')[0]; }
//			var restRouteQuery = query;
//			if (!string.IsNullOrEmpty(route)) //if route is present, assume it's our encrypted arguments, and use those instead
//			{
//				restRouteQuery = ToCollection(StringEncryption.Current.Decrypt(route));
//				query.Remove(RouteParam);
//				EncryptedRoute = true;
//			}
//			UnencryptedQueryArgs = query.ToString();
//			Rest.FillModelFromQuery(this, true, false, restRouteQuery);
//			if(!EncryptedRoute) { EncryptedQueryArgs = null; } //don't allow these to be passed without encryption.
//			Path = Path.ToLower(); //ensure path is lowercase. at this point we should have a non-null path, either from the actual url, or from the (maybe encrypted) route.
//		}

//		public RestUrl(string path, string nodeID, string viewName, bool encrypted = true, bool internalRoute = false, string encryptedQueryArgs = null, string unencryptedQueryParams = null)
//		{
//			Path = path?.ToLower();
//			NodeID = nodeID;
//			ViewName = viewName;
//			EncryptedRoute = encrypted;
//			InternalRoute = internalRoute;
//			EncryptedQueryArgs = encryptedQueryArgs == null ? null : HttpUtility.UrlEncode(encryptedQueryArgs);
//			if (!encrypted && EncryptedQueryArgs != null) { throw new InvalidOperationException("Invalid combination"); }
//			UnencryptedQueryArgs = unencryptedQueryParams;
//		}
		
//		/// <summary>
//		/// 
//		/// </summary>
//		/// <param name="model">Either an instance of a model, or the type-object for a model class</param>
//		/// <param name="method"></param>
//		/// <param name="nodeID"></param>
//		/// <param name="viewName"></param>
//		/// <param name="internalRoute"></param>
//		/// <param name="includeModelProps"></param>
//		/// <param name="queryString"></param>
//		/// <param name="encryptedQueryString"></param>
//		/// <returns></returns>
//		public static RestUrl Make(object model, string method = null, string nodeID = null, string viewName = null, bool internalRoute = false, bool includeModelProps = true, string queryString = null, string encryptedQueryString = null)
//		{
//			Type modelType;
//			if(model is Type)
//			{
//				modelType = model as Type;
//				model = null;
//			}
//			else
//			{
//				modelType = model.GetType();
//				if (nodeID == null && model is IAFLModel)
//				{
//					nodeID = ((IAFLModel)model).UID;
//				}
//			}

//			if(includeModelProps)
//			{
//				if(model == null) { throw new InvalidOperationException("Need model instance in order to include model properties."); }

//				var enc = HttpUtility.ParseQueryString(encryptedQueryString ?? "");
//				var unenc = HttpUtility.ParseQueryString(queryString ?? "");
//				foreach(var settable in Registry.Current.GetInfo(modelType).RestSettableProperties)
//				{
//					(settable.Attribute.Encrypted ? enc : unenc).Add(settable.Attribute.ShortName ?? settable.Property.Name, settable.Property.GetValue(model)?.ToString() ?? "");
//				}
//				encryptedQueryString = enc.ToString();
//				queryString = unenc.ToString();
//			}
			
//			var handler = Registry.Current.GetInfo(modelType)?.GetRestHandler(method);
//			if(handler == null) { throw new InvalidOperationException("No handler for specified model/method"); }
//			return new RestUrl(handler.Path, nodeID, viewName, encrypted: true, internalRoute: internalRoute, encryptedQueryArgs: encryptedQueryString, unencryptedQueryParams: queryString);
//		}

//		public static RestUrl Make(IAFLModel model, string viewName, bool isPartial, bool includeModelProps, string queryString = null, string encryptedQueryString = null, bool internalRoute = true)
//		{
//			return Make(model, null, model.UID, viewName: (isPartial ? "@" : "") + viewName, includeModelProps: includeModelProps, queryString: queryString, encryptedQueryString: encryptedQueryString, internalRoute: internalRoute);
//		}

//		public static RestUrl Make(IAFLModel model, bool includeModelProps = true, string queryString = null, string encryptedQueryString = null, bool internalRoute = true)
//		{
//			return Make(model, null, model.UID, viewName: Registry.Current.InferViewName(model), includeModelProps: includeModelProps, queryString: queryString, encryptedQueryString: encryptedQueryString, internalRoute: internalRoute);
//		}

//		/// <summary>
//		/// Override or add a query param, either encrypted or unencrypted. if it's not present already (e.g. as a model property), unencrypted is assumed.
//		/// Method call can be chained.
//		/// </summary>
//		public RestUrl Override(string key, object value)
//		{
//			var enc = EncryptedQueryCollection;
//			var unenc = UnencryptedQueryCollection;
//			if(enc[key] != null)
//			{
//				enc.Set(key, value.ToString());
//			}
//			else
//			{
//				unenc.Set(key, value.ToString());
//			}
//			EncryptedQueryArgs = enc.ToString();
//			UnencryptedQueryArgs = unenc.ToString();
//			return this;
//		}

//		public string GetEncryptedRouteString()
//		{
//			return StringEncryption.Current.Encrypt(StringifyRoute());
//		}

//		public override string ToString()
//		{
//			var route = StringifyRoute();
//			if(EncryptedRoute && route.Length>0)
//			{
//				route = StringEncryption.Current.Encrypt(route);
//			}
//			string url;
//			if(route.Length == 0)
//			{
//				url = Path + (string.IsNullOrEmpty(UnencryptedQueryArgs) ? "" : "?" + UnencryptedQueryArgs);
//			}
//			else if(InternalRoute)
//			{
//				url = InternalRoutePath + route + (string.IsNullOrEmpty(UnencryptedQueryArgs) ? "" : "?" + UnencryptedQueryArgs);
//			}
//			else
//			{
//				url = Path + "?" + RouteParam + "=" + route + (string.IsNullOrEmpty(UnencryptedQueryArgs) ? "" : "&" + UnencryptedQueryArgs);
//			}
//			return Registry.Current.RestBasePath + url;
//		}

//		private NameValueCollection ToCollection(string queryString)
//		{
//			return queryString.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Split('='))
//				.Aggregate(new NameValueCollection(), (seed, current) => { seed.Add(current[0], current.Length>1?current[1]:null); return seed; });
//		}

//		private string StringifyRoute()
//		{
//			return string.Join("&", RouteProperties.Where(p => p.Item1.GetValue(this)!=null && (p.Item1.GetValue(this) as string)!="" && (InternalRoute || p.Item1.Name != "Path")).Select(p => p.Item2 + "=" + p.Item1.GetValue(this)));
//		}

//		public bool InternalRoute { get; private set; }
//		public bool EncryptedRoute { get; private set; }
//		public bool RouteIsEmpty => StringifyRoute().Length == 0;
//		[RestProperty("_p")] //with underscore, to minimize conflict with user-queryargs
//		public string Path { get; private set; }
//		[RestProperty("_id")] //with underscore, to minimize conflict with user-queryargs
//		public string NodeID { get; private set; }
//		[RestProperty("_v")] //with underscore, to minimize conflict with user-queryargs
//		public string ViewName { get; private set; }
//		[RestProperty("_e")] //with underscore, to minimize conflict with user-queryargs
//		public string EncryptedQueryArgs { get; private set; }

//		public NameValueCollection EncryptedQueryCollection => HttpUtility.ParseQueryString(HttpUtility.UrlDecode(EncryptedQueryArgs ?? ""));

//		//regular query args. these are added to the generated url, and when deserializing, it's constructed from the query after removing the route.
//		public string UnencryptedQueryArgs { get; private set; }

//		public NameValueCollection UnencryptedQueryCollection => HttpUtility.ParseQueryString(HttpUtility.UrlDecode(UnencryptedQueryArgs ?? ""));
//	}
//}
