﻿using AFLCore.Core;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Caching;
using System.Runtime.Serialization.Formatters.Binary;

namespace AFLCore.Caching
{
	public class AFLCache : IAFLCache
	{
		protected const string cachePrefix = "attr-cache"; // TODO: MOVE ME
		private readonly IHttpContextAccessor httpContextAccessor;
		protected static readonly object nullPlaceholder = new object();
		private IDictionary<object, object> fallbackCache = null;
		public void NewRequest()
		{
			fallbackCache = new ConcurrentDictionary<object, object>();
		}

		public AFLCache(IHttpContextAccessor httpContextAccessor)
		{
			this.httpContextAccessor = httpContextAccessor;
		}
		public bool Load(string cacheKey, out object value)
		{
			value = (httpContextAccessor.HttpContext?.Items ?? fallbackCache)?[cacheKey] ?? FromByteArray<object>(httpContextAccessor.HttpContext?.Session?.Get(cacheKey)) ?? MemoryCache.Default[cachePrefix + cacheKey];
			if (value == nullPlaceholder)
			{
				value = null;
				return true;
			}

			return value != null;
		}
		T FromByteArray<T>(byte[] data)
		{
			//TODO: Move me!
			if (data == null)
				return default;
			BinaryFormatter bf = new BinaryFormatter();
			using (MemoryStream ms = new MemoryStream(data))
			{
				object obj = bf.Deserialize(ms);
				return (T)obj;
			}
		}
		byte[] ToByteArray<T>(T obj)
		{
			//TODO: MOVE ME
			if (obj == null)
				return null;
			BinaryFormatter bf = new BinaryFormatter();
			using (MemoryStream ms = new MemoryStream())
			{
				bf.Serialize(ms, obj);
				return ms.ToArray();
			}
		}


		public void Store(string cacheKey, object value, CacheAttribute attr = null)
		{
			cacheKey = cachePrefix + cacheKey; // what a waste.
			try
			{
				switch (attr?.Caching ?? CacheScope.Request)
				{
					case CacheScope.Request:
						if ((attr?.CacheDurationMinutes ?? 0) != 0) { throw new InvalidOperationException("cache duration not supported for request caching"); }
						(httpContextAccessor.HttpContext?.Items ?? fallbackCache)?.Add(cacheKey, value ?? nullPlaceholder);
						break;
					case CacheScope.Session:
						if (attr.CacheDurationMinutes != 0) { throw new InvalidOperationException("cache duration not supported for session caching"); }
						if (httpContextAccessor != null)
						{
							//HttpContext.Current.Session[cacheKey] = value ?? nullPlaceholder;
							httpContextAccessor.HttpContext.Session.Set(cacheKey, ToByteArray(value ?? nullPlaceholder));
						}
						else
						{
							fallbackCache?.TryAdd(cacheKey, value ?? nullPlaceholder);
						}
						break;
					case CacheScope.Global:
						var duration = attr.CacheDurationMinutes == 0 ? ObjectCache.InfiniteAbsoluteExpiration : DateTime.UtcNow + TimeSpan.FromMinutes(attr.CacheDurationMinutes);
						//HttpRuntime.Cache.Insert(cacheKey, value ?? nullPlaceholder, null, duration, System.Web.Caching.Cache.NoSlidingExpiration);
						MemoryCache.Default.Set(cacheKey, value ?? nullPlaceholder, new CacheItemPolicy() { AbsoluteExpiration = duration });
						break;
					case CacheScope.NotCached:
						if (attr.CacheDurationMinutes != 0) { throw new InvalidOperationException("cache duration not supported when not caching"); }
						break;
				}
			}
			catch (ArgumentException) { /* will happen if the key has already been added. this can happen in reentrant cases when two models use the same node, which is uncool but should probably be allowed */ }
		}
		public void Flush(string uid, string callerName)
		{
			var cacheKey = cachePrefix + uid + callerName;

			httpContextAccessor.HttpContext.Items.Remove(cacheKey);
			httpContextAccessor.HttpContext.Session?.Remove(cacheKey);
			MemoryCache.Default.Remove(cacheKey); //HttpRuntime.Cache?.Remove(cacheKey);
		}
	}
}
