﻿using AFLCore.Core;
using System;

namespace AFLCore.Caching
{
	[AttributeUsage(AttributeTargets.Property, Inherited = true)]
	public class CacheAttribute : Attribute
	{
		public CacheScope Caching { get; set; }

		public int CacheDurationMinutes { get; set; }
	}
}
