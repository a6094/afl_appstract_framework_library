﻿namespace AFLCore.Core
{
	public enum CacheScope { Request, Session, Global, NotCached };
}
