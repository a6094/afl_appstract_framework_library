﻿namespace AFLCore.Caching
{
	public interface IAFLCache
	{
		void NewRequest();
		void Flush(string uid, string callerName);
		bool Load(string cacheKey, out object value);
		void Store(string cacheKey, object value, CacheAttribute attr = null);
	}
}
