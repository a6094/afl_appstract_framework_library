﻿using AFLCore.Core;

namespace AFLCore
{
	/// <summary>
	/// This is the base interface for all model classes, which wrap CMS nodes/content/items etc.
	/// </summary>
	public interface IAFLModel
	{
		/// <summary>
		/// e.g. for Umbraco node entities this would be the Umbraco ID.
		/// </summary>
		string UID { get; }

		/// <summary>
		/// Allows the framework to render the model instance with the proper view - when relevant.
		/// </summary>
		string ViewTypeName { get; }
		string Translate(string key);
		Registry Registry { get; set; }
		IMemoSupport MemoSupport { get; set; }
	}
}
