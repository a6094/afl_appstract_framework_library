﻿using System;

namespace AFLCore
{
	public class Nav
	{
		public enum Take { Children, FirstChild, Descendants, FirstSibling, NextSibling, PreviousSibling, AllSiblings, AllAncestors, ClosestAncestorIncludingSelf, ClosestAncestorExcludingSelf, Parent, RootOrSelf };
		[Flags] public enum VisibilityFlags { All = 0, OnlyVisible = 1, OnlyRenderable = 2 };

		protected Nav()
		{
		}

		public Type[] types;
		public Take take;
		public VisibilityFlags Visibility { get; protected set; }

		public Nav OnlyVisible()
		{
			Visibility |= VisibilityFlags.OnlyVisible;
			return this;
		}

		public Nav OnlyRenderable()
		{
			Visibility |= VisibilityFlags.OnlyRenderable;
			return this;
		}

		public static Nav Children(params Type[] types)
		{
			return new Nav { take = Take.Children, types = types };
		}

		public static Nav<T[]> Children<T>()
		{
			return new Nav<T[]> { take = Take.Children, types = new[] { typeof(T) } };
		}

		public static Nav FirstChild(params Type[] types)
		{
			return new Nav { take = Take.FirstChild, types = types };
		}

		public static Nav<T> FirstChild<T>()
		{
			return new Nav<T> { take = Take.FirstChild, types = new[] { typeof(T) } };
		}

		public static Nav Siblings(params Type[] types)
		{
			return new Nav { take = Take.AllSiblings, types = types };
		}

		public static Nav<T[]> Siblings<T>()
		{
			return new Nav<T[]> { take = Take.AllSiblings, types = new[] { typeof(T) } };
		}

		public static Nav FirstSibling(params Type[] types)
		{
			return new Nav { take = Take.FirstSibling, types = types };
		}

		public static Nav<T> FirstSibling<T>()
		{
			return new Nav<T> { take = Take.FirstSibling, types = new[] { typeof(T) } };
		}

		public static Nav NextSibling(params Type[] types)
		{
			return new Nav { take = Take.NextSibling, types = types };
		}

		public static Nav<T> NextSibling<T>()
		{
			return new Nav<T> { take = Take.NextSibling, types = new[] { typeof(T) } };
		}

		public static Nav PreviousSibling(params Type[] types)
		{
			return new Nav { take = Take.PreviousSibling, types = types };
		}

		public static Nav<T> PreviousSibling<T>()
		{
			return new Nav<T> { take = Take.PreviousSibling, types = new[] { typeof(T) } };
		}

		public static Nav Descendants(params Type[] types)
		{
			return new Nav { take = Take.Descendants, types = types };
		}

		public static Nav<T[]> Descendants<T>()
		{
			return new Nav<T[]> { take = Take.Descendants, types = new[] { typeof(T) } };
		}

		public static Nav Ancestors(params Type[] types)
		{
			return new Nav { take = Take.AllAncestors, types = types };
		}

		public static Nav<T[]> Ancestors<T>()
		{
			return new Nav<T[]> { take = Take.AllAncestors, types = new[] { typeof(T) } };
		}

		public static Nav Parent(params Type[] types)
		{
			return new Nav { take = Take.Parent, types = types };
		}

		public static Nav<T> Parent<T>()
		{
			return new Nav<T> { take = Take.Parent, types = new[] { typeof(T) } };
		}

		public static Nav ClosestAncestor(params Type[] types)
		{
			return new Nav { take = Take.ClosestAncestorExcludingSelf, types = types };
		}

		public static Nav<T> ClosestAncestor<T>()
		{
			return new Nav<T> { take = Take.ClosestAncestorExcludingSelf, types = new[] { typeof(T) } };
		}

		public static Nav ClosestAncestorOrSelf(params Type[] types)
		{
			return new Nav { take = Take.ClosestAncestorIncludingSelf, types = types };
		}

		public static Nav<T> ClosestAncestorOrSelf<T>()
		{
			return new Nav<T> { take = Take.ClosestAncestorIncludingSelf, types = new[] { typeof(T) } };
		}

		public static Nav Root(params Type[] types)
		{
			return new Nav { take = Take.RootOrSelf, types = types };
		}

		public static Nav<T> Root<T>()
		{
			return new Nav<T> { take = Take.RootOrSelf, types = new[] { typeof(T) } };
		}
	}

	public class Nav<T> : Nav
	{
		public new Nav<T> OnlyVisible()
		{
			Visibility |= VisibilityFlags.OnlyVisible;
			return this;
		}

		public new Nav<T> OnlyRenderable()
		{
			Visibility |= VisibilityFlags.OnlyRenderable;
			return this;
		}
	}
}
