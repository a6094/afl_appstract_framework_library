﻿namespace AFLCore
{
	public class PropertyTypeData
	{
		public PropertyTypeData(string alias)
		{
			this.Alias = alias;
			this.Name = string.Empty;
		}

		public PropertyTypeData(string alias, string name)
		{
			this.Alias = alias;
			this.Name = name;
		}

		/// <summary>
		/// 
		/// </summary>
		public string Alias { get; }

		/// <summary>
		/// The name of the umbraco property.
		/// if the value is <value>string.Empty</value> then no name has been found or it's a native Property Value
		/// </summary>
		public string Name { get; }
	}
}
