﻿using AFLCore;
using AFLCore.Core;
using AFLCoreUmbraco.Models;
using AFLCoreUmbraco.Models.DatabaseWrappers;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Services;
using Umbraco.Extensions;

namespace AFLCoreUmbraco
{
	public class AFLCoreUmbraco : IAFLImplementation
	{
		private IContentService contentService;
		private readonly UmbracoUtils umbracoUtils;
		//private UmbracoHelper umbracoHelper; 
		private IContentTypeService contentTypeService;
		private IMediaTypeService mediaTypeService;

		public AFLCoreUmbraco(IMediaTypeService mediaTypeService, IContentTypeService contentTypeService, IContentService contentService, UmbracoUtils umbracoUtils)
		{
			this.mediaTypeService = mediaTypeService;
			this.contentTypeService = contentTypeService;
			this.contentService = contentService;
			this.umbracoUtils = umbracoUtils;
		}
		public void OnStartup()
		{
		}

		public string GetAlias(object node)
		{
			//TODO: Check up
			//return Current.Services.ContentService.GetById(((IPublishedContent)node).Id).ContentType.Alias;
			return (node as IPublishedContent)?.ContentType?.Alias;
		}

		public string GetID(object node)
		{
			//CHECK NESTED CONTENT
			//todo: FIX THIS
			int id = (node as IPublishedContent).Id;
			if (id == 0 && (node.GetType().Name.Equals("DetachedPublishedContent", StringComparison.Ordinal) || node.GetType().Name.Equals("DBDetachedContent", StringComparison.Ordinal))) // todo - more robust identification. the latter one is for the AFL IContent support.
			{
				return Guid.NewGuid().ToString(); // give it a unique id, to prevent caching collisions. as of now, there is no way to distinguish nested content nodes, so this is the best we can do.
			}
			return id.ToString();
		}

		public object GetNode(string nodeID)
		{
			return umbracoUtils.UmbracoHelper.Content(nodeID);
		}
		public object GetNode(Guid nodeID)
		{
			return umbracoUtils.UmbracoHelper.Content(nodeID);
		}

		public IEnumerable<object> GetTopLevelNodes(AFLTypeInfo nodeType = null)
		{
			return umbracoUtils.UmbracoHelper.ContentAtRoot().Where(n => nodeType == null || n.ContentType.Alias.Equals(nodeType.Alias, StringComparison.Ordinal));
		}

		public object GetDBNode(string nodeID)
		{
			var id = int.Parse(nodeID);
			IContent content = contentService.GetById(id);
			if (content == null) { return null; }
			return new DBContent(content, contentService); 
		}
		public object NativeDBTypeToDBNode(object dbType)
		{
			if (dbType == null) { return null; }
			return new DBContent((IContent)dbType, contentService);
		}

		public object GetParentNode(object node)
		{
			return ((IPublishedContent)node).Parent;
		}

		public IEnumerable<PropertyTypeData> GetPropertiesForAlias(string alias)
		{
			if (string.Equals(alias, "File", StringComparison.Ordinal) || string.Equals(alias, "Image", StringComparison.Ordinal))//these don't exist in ContentTypeService, but should still be mapped.
			{
				return mediaTypeService.Get(alias)?.CompositionPropertyTypes.Select(cpt => new PropertyTypeData(cpt.Alias, cpt.Name));
			}

			return contentTypeService.Get(alias)?.CompositionPropertyTypes.Select(cpt => new PropertyTypeData(cpt.Alias, cpt.Name));
		}

		public string InferAliasFromTypeName(string name)
		{
			var type = contentTypeService.Get(name);
			if (type != null)
			{
				return type.Alias; //return correctly capitalized version.
			}
			return null;
		}

		public string InferViewName(IAFLModel model)
		{
			if (model is AFLModelBase)
			{
				return ((AFLModelBase)model).Content.GetTemplateAlias();
			}
			return null;
		}


		public Type NodeType => typeof(IPublishedContent);
	}
}
