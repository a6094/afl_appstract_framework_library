﻿using AFLCore;
using AFLCore.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Umbraco.Cms.Web.BackOffice.Controllers;

namespace AFLCoreUmbraco.Tools
{
	public class AFLDebugModeController : UmbracoAuthorizedJsonController
	{
		private readonly IOptions<AFLOptions> options;

		public AFLDebugModeController(IOptions<AFLOptions> options)
		{
			this.options = options;
		}
		[HttpGet]
		public bool IsDebug()
		{
			return options.Value.DebugMode;
		}
	}
}
