﻿using System;
using System.Collections.Generic;
using System.Linq;
using AFLCore;
using Microsoft.AspNetCore.Mvc;
using Umbraco.Cms.Web.BackOffice.Controllers;

namespace AFLCoreUmbraco.Tools
{
	public class AFLMemoSupportController : UmbracoAuthorizedJsonController
	{

		[HttpGet]
		public IEnumerable<AFLMemoSupportProperty> GetMissingProperties()
		{

			var json = InMemoryMissingDocumentTypeProperties.GetInstance().Select(typeProperty =>
			{
				return new AFLMemoSupportProperty(typeProperty.TypeAlias, typeProperty.ModelName, typeProperty.PropertyName, typeProperty.PropertyType.ToString());
			});
			return json;


		}

		[HttpGet]
		public bool ReregisterTypes()
		{
			throw new NotImplementedException();
			//Registry.Current.Reinitialize();
			return true;
		}

		[HttpGet]
		public bool ClearPropertyEntry(string alias, string propertyName)
		{
			InMemoryMissingDocumentTypeProperties.GetInstance().RemoveAll(item => item.TypeAlias == alias && item.PropertyName == propertyName);
			return true;
		}

		[HttpGet]
		public bool ClearAllProperties()
		{
			InMemoryMissingDocumentTypeProperties.GetInstance().Clear();
			return true;
		}

	}

	public class AFLMemoSupportProperty
	{
		public string alias { get; }
		public string modelName { get; }
		public string name { get; }
		public string type { get; }
		public AFLMemoSupportProperty(string alias, string modelName, string name, string type)
		{
			this.alias = alias;
			this.modelName = modelName;
			this.name = name;
			this.type = type;
		}
	}
}
