﻿using AFLCore;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Web.BackOffice.Controllers;

namespace AFLCoreUmbraco.Tools.ModelPlumber
{
	public class AFLModelPlumberController : UmbracoAuthorizedJsonController
	{
		List<IContentType> umbracoContentTypes;
		AFLModelPlumber plumber;
		private readonly Registry registry;

		public AFLModelPlumberController(AFLModelPlumber plumber, IContentTypeService contentTypeService, Registry registry)
		{
			this.plumber = plumber;
			this.registry = registry;
			var cts = contentTypeService;
			umbracoContentTypes = cts.GetAll().ToList();//GetAllContentTypes().ToList();
		}

		[HttpGet]
		public IEnumerable<AFLModelDescriptor> GetModels()
		{
			var data = umbracoContentTypes.Select(contentType =>
			{
				var info = registry.TryGetInfo(contentType.Alias);
				var cecilType = info != null ? plumber.GetCecilType(info.ModelType) : null;
				var wasDefined = cecilType != null ? registry.TryGetInfo(contentType.Alias) != null : false;
				return new AFLModelDescriptor(contentType.Name, contentType.Alias, wasDefined, wasDefined 
					? !string.IsNullOrWhiteSpace(plumber.WriteModelClassDiff(contentType, info, cecilType, false)) 
					: false);
			});
			return data;
		}

		[HttpGet]
		public string GetClassDiffCode(string alias, bool withComments = false)
		{
			IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);
			var info = registry.TryGetInfo(alias);
			var cecilType = plumber.GetCecilType(info.ModelType);
			var text = plumber.WriteModelClassDiff(contentType, info, cecilType, withComments);
			return text;
		}

		[HttpGet]
		public string GetClassCode(string alias, bool withComments = false)
		{
			IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);
			var text = plumber.WriteModelClassCode(contentType, withComments);
			return text;
		}

		[HttpGet]
		public bool WriteClassToFile(string alias, bool withComments = false)
		{
			plumber.SaveClassToFile(alias, withComments);
			return true;
		}

		[HttpGet]
		public bool WriteDiffToFile(string alias, bool withComments = false)
		{

			plumber.SaveDiffToFile(alias, withComments);
			return true;
		}
	}

	public class AFLModelDescriptor
	{
		public string name { get; }
		public string alias { get; }
		public bool defined { get; }
		public bool diff { get; }

		public AFLModelDescriptor(string name, string alias, bool defined, bool diff)
		{
			this.name = name;
			this.alias = alias;
			this.defined = defined;
			this.diff = diff;
		}
	}
}
