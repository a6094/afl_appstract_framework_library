﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AFLCore;
using AFLCore.Core;
using AFLCore.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Pdb;
using Newtonsoft.Json;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.PropertyEditors;
using Umbraco.Cms.Core.Services;

namespace AFLCoreUmbraco.Tools.ModelPlumber
{
	public class AFLModelPlumber
	{
		Dictionary<string, AssemblyDefinition> cecilAssemblies = new Dictionary<string, AssemblyDefinition>();
		Dictionary<int, IContentType> contentTypeMap;
		List<IContentType> umbracoContentTypes;
		IContentTypeService contentTypeService;
		private readonly IContentService contentService;
		private readonly IWebHostEnvironment hostingEnvironment;
		private readonly Registry registry;
		private readonly IDataTypeService dataTypeService;
		private readonly ILogger<AFLModelPlumber> logger;

		public AFLModelPlumber(IContentTypeService contentTypeService, 
			IContentService contentService,
			IWebHostEnvironment hostingEnvironment, 
			Registry registry, 
			IDataTypeService dataTypeService, 
			ILogger<AFLModelPlumber> logger)
		{
			this.contentTypeService = contentTypeService;
			this.contentService = contentService;
			this.hostingEnvironment = hostingEnvironment;
			this.registry = registry;
			this.dataTypeService = dataTypeService;
			this.logger = logger;
			umbracoContentTypes = contentTypeService.GetAll().ToList();//GetAllContentTypes().ToList();
			contentTypeMap = umbracoContentTypes.ToDictionary(t => t.Id);
		}


		public string GetSourcePath(TypeDefinition typeDefinition)
		{
			foreach (var prop in typeDefinition.Properties.Where(x => x.GetMethod?.HasBody ?? false))
			{
				var method = prop.GetMethod;
				foreach (var instruction in method.Body.Instructions)
				{
					var sequencePoint = method.DebugInformation.GetSequencePoint(instruction);
					if (sequencePoint != null)
					{
						return sequencePoint.Document.Url;
					}
				}
			}
			foreach (var method in typeDefinition.Methods.Where(x => x.HasBody))
			{
				foreach (var instruction in method.Body.Instructions)
				{
					var sequencePoint = method.DebugInformation.GetSequencePoint(instruction);
					if (sequencePoint != null)
					{
						return sequencePoint.Document.Url;
					}
				}
			}
			return null;
		}

		public void SaveDiffToFile(string alias, bool withComments)
		{
			IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);

			var isComposition = IsComposition(contentType);
			var info = registry.TryGetInfo(alias);
			var cecilType = GetCecilType(info.ModelType);
			var diff = WriteModelClassDiff(contentType, info, cecilType, withComments);

			CalcModelClassDiff(contentType, info, cecilType, out var missingProps, out var orphanedCecilProps, out var missingInterfaces);

			var filePath = GetSourcePath(cecilType);
			var editor = new SimpleSourceCodeEditor(filePath);
			foreach (var item in missingInterfaces)
			{
				string class_definition_pattern = @"\b(public|private|internal|protected)\s*" + @"\b(class)?\s*[a-zA-Z]*(?<class>\s[a-zA-Z]+\s*)" + @": [a-zA-Z\<\>,\s]*\s*[a-zA-Z]*\s*[,]*?\s*]*";
				editor.AppendToLine(class_definition_pattern, $", {item}\n");
			}
			foreach (var prop in missingProps)
			{
				var regex = "^\\s*(public|protected) .+ [A-Za-z0-9_]+ \\=\\> Memo(\\<.+\\>)?\\(.*\\);";
				editor.InsertBefore(regex, editor.GetIndentAtLine(regex) + GetCMSPropertyMemo(prop, false, isComposition) + "\n");
			}
			foreach (var codeProp in orphanedCecilProps)
			{
				editor.Replace($"^\\s*(public|protected) .+ {codeProp.Name} \\=\\> Memo(\\<.+\\>)?\\(.*\\);\\s*$", "");
			}
			editor.Save();
		}

		private bool IsComposition(IContentType contentType)
		{
			//Compositions are parents and compositions themselves.
			//therefore if we are composition and not regular inheritance
			//we can find all nodes where the type is parent, and the children of current node. And if these two lists are not identical, we are composition.
			var nodesWhereContentTypeParent = umbracoContentTypes.Where(x => x.ContentTypeComposition.Contains(contentType));
			var children = contentTypeService.GetChildren(contentType.Id);

			return !nodesWhereContentTypeParent.All(x => children.Contains(x));
		}

		public void SaveClassToFile(string alias, bool withComments)
		{
			IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);
			var rootFolder = hostingEnvironment.ContentRootPath.Substring(0, hostingEnvironment.ContentRootPath.IndexOf(hostingEnvironment.ApplicationName));
			var path = Path.GetFullPath($"{rootFolder}/{registry.BusinessLogicAssembly.GetName().Name}/{GetModelNameForAlias(contentType.Alias)}.cs");
			System.IO.File.WriteAllText(path, WriteModelClassCode(contentType, withComments));

			var projFilePath = Path.GetFullPath($"{rootFolder}/{registry.BusinessLogicAssembly.GetName().Name}/{registry.BusinessLogicAssembly.GetName().Name}.csproj");
			var editor = new SimpleSourceCodeEditor(projFilePath);
			editor.InsertBefore("\\<Compile Include\\=\\\"[A-Za-z0-9_]+\\.cs\" \\/\\>", $"<Compile Include=\"{GetModelNameForAlias(contentType.Alias)}.cs\" />\n");
			editor.Save();

		}

		public string WriteModelClassCode(IContentType contentType, bool withComments)
		{
			var builder = new CodeBuilder();

			builder.Append("using AFLCore;");
			builder.Append("using AFLCoreUmbraco9.Models;");
			builder.Append("using Umbraco.Cms.Core.Models.PublishedContent;");
			builder.Append("using AFLCoreUmbraco9.Models.MediaTypes;"); //TODO IN a future version we could only append this if we actually add a type that uses it. for now we stay dumb

			builder.Append("");

			string baseType = "AFLModelBase";
			if (contentType.ParentId > 0 && contentTypeMap.ContainsKey(contentType.ParentId))
			{
				baseType = GetModelNameForAlias(contentTypeMap[contentType.ParentId].Alias);
			}
			var compositions = contentType.ContentTypeComposition.Where(x => !x.Alias.Equals(baseType, StringComparison.OrdinalIgnoreCase));
			if (compositions.Any())
			{
				baseType = baseType + ", " + string.Join(", ", compositions.Select(x => $"I{GetModelNameForAlias(x.Alias)}"));
			}
			var isComposition = IsComposition(contentType);

			//TODO: only works in umbraco 7.6 and up...
			var namespaceList = contentTypeService.GetContainers(contentType) ?? new EntityContainer[] { };
			//GetContentTypeContainers(contentType) ?? new EntityContainer[] { };
			//string namespaces = Registry.Current.BusinessLogicAssembly.GetName().Name + "." + string.Join(".", namespaceList.Select(ns => ns.Name));
			string namespaces = registry.BusinessLogicAssembly.GetName().Name;

			builder.Append($"namespace {namespaces}");
			builder.Append("{", 1);
			var comments = builder.AppendBuilder();
			builder.Append("");

			if (isComposition)
			{
				builder.Append("[Composition]");
			}
			builder.Append($"public {(isComposition ? "interface" : "class")} {GetModelNameForAlias(contentType.Alias)} {(!isComposition ? $": {baseType}" : "")}");
			builder.Append("{", 1);
			if (!isComposition)
			{
				builder.Append($"public {GetModelNameForAlias(contentType.Alias)}(IPublishedContent node) : base(node)");
				builder.Append("{", 1);
				builder.Append("}", -1);
			}
			builder.Append("");

			foreach (var prop in contentType.PropertyTypes.ToList())
			{
				builder.Append(GetCMSPropertyMemo(prop, withComments, isComposition));
			}
			foreach (var comp in compositions)
			{
				foreach (var prop in comp.PropertyTypes)
				{
					builder.Append(GetCMSPropertyMemo(prop, withComments, isComposition));
				}
			}
			if (contentType.AllowedContentTypes.Any())
			{
				builder.Append(GetChildrenMemo(contentType, withComments));
			}

			builder.Append("}", -1);
			builder.Append("}", -1);
			builder.Append("");

			return builder.ToString();
		}

		public string WriteModelClassDiff(IContentType contentType, AFLTypeInfo info, TypeDefinition cecilType, bool withComments)
		{
			List<IPropertyType> missingProps;
			List<PropertyDefinition> orphanedCecilProps;
			CalcModelClassDiff(contentType, info, cecilType, out missingProps, out orphanedCecilProps, out var missingInterfaces);
			var isComposition = IsComposition(contentType);
			var builder = new CodeBuilder();
			if (missingInterfaces.Any())
			{
				builder.Append("//Missing Interfaces:");
				foreach (var item in missingInterfaces)
				{
					builder.Append($"Missing Interface: {item}");
				}
			}
			if (missingProps.Any())
			{
				builder.Append($"//Missing properties:");
				foreach (var prop in missingProps)
				{
					builder.Append(GetCMSPropertyMemo(prop, withComments, isComposition));
				}
			}
			if (orphanedCecilProps.Any())
			{
				builder.Append($"//Orphaned properties:");
				foreach (var codeProp in orphanedCecilProps)
				{
					builder.Append($"No such CMS property: {codeProp.Name}");
				}
			}

			return builder.ToString();
		}

		public TypeDefinition GetCecilType(Type type)
		{
			return GetCecilAssembly(type).MainModule.Types.FirstOrDefault(tt => tt.Name == type.Name);
		}

		public string GetModelNameForAlias(string alias)
		{
			var typeName = registry.TryGetInfo(alias)?.ModelType.Name ?? alias;
			return char.ToUpperInvariant(typeName.First()) + typeName.Substring(1);
		}

		private void CalcModelClassDiff(IContentType contentType, AFLTypeInfo info, TypeDefinition cecilType, out List<IPropertyType> missingProps, out List<PropertyDefinition> orphanedCecilProps, out List<string> missingInterfaces)
		{
			var propMap = info.GetRegisteredCMSProperties();

			missingProps = contentType.PropertyTypes.ToList();
			var compositions = contentType.ContentTypeComposition.Where(x => !contentTypeService.GetChildren(x.Id).Contains(contentType));
			var compositionPropertyTypes = compositions.SelectMany(x => x.PropertyTypes);
			missingProps.AddRange(compositionPropertyTypes);
			missingProps = missingProps.Where(p => !propMap.ContainsKey(p.Alias)).ToList();

			var compsNotAsInterface = compositions.Select(x => x.Alias).Where(c => !cecilType.Interfaces.Select(x => x.InterfaceType.Name).DefaultIfEmpty().Any(i => c.Equals(i, StringComparison.OrdinalIgnoreCase)));
			missingInterfaces = compsNotAsInterface.Select(x => x.First().ToString().ToUpper() + x.Substring(1)).ToList();
			//cecilType.Interfaces.Where(x => !compositions.Select(c => c.Name).Any(c => x.Name.Equals(c, StringComparison.OrdinalIgnoreCase)));

			orphanedCecilProps = propMap.Keys
				.Where(n => !contentType.PropertyTypes.Any(p => p.Alias == n) && !compositionPropertyTypes.Any(p => p.Alias == n))
				.Select(n => cecilType.Properties.FirstOrDefault(tp => tp.Name == propMap[n].Name))
				.Where(cp => cp != null && IsMemoProperty(cp))
				.ToList();
		}

		AssemblyDefinition GetCecilAssembly(Type type)
		{
			var name = type.Assembly.FullName;
			if (!cecilAssemblies.ContainsKey(name))
			{
				var readerParameters = new ReaderParameters { ReadSymbols = registry.AFLDebugMode, SymbolReaderProvider = new PdbReaderProvider()};
				//var readerParameters = new ReaderParameters {};
				var localPath = type.Assembly.Location;//new Uri(type.Assembly.Location).LocalPath;
				var assemblyDefinition = AssemblyDefinition.ReadAssembly(localPath, readerParameters);
				cecilAssemblies[name] = assemblyDefinition;
			}
			return cecilAssemblies[name];
		}

		private string GetCodePropertyName(IPropertyType prop)
		{
			return new string(prop.Alias.Select((c, idx) => idx == 0 ? char.ToUpper(c) : c).ToArray());
		}

		private string GetClosestCommonBase(string model1, string model2)
		{
			return "";
		}

		private string GetCommonBaseTypeName(IEnumerable<string> aliases)
		{
			string closest = aliases.FirstOrDefault() ?? "AFLModelBase";

			foreach (var alias in aliases)
			{
				if (alias != closest)
				{
					//todo
				}
			}

			return GetModelNameForAlias(closest);
		}

		private string GetChildrenMemo(IContentType contentType, bool printComments)
		{
			var aliases = contentType.AllowedContentTypes.Select(d => d.Alias);
			var type = GetCommonBaseTypeName(aliases);
			return $"public {type} Children => Memo<{type}>();{(printComments ? "// " + JsonConvert.SerializeObject(aliases) : "")}";
		}

		private string GetCMSPropertyMemo(IPropertyType prop, bool printComments, bool isComposition)
		{
			//TODO: Fix this for Umbraco 8
			//var dts = Current.Services.DataTypeService;
			//var def = dts.GetDataType(prop.Id);
			//var pv = dts.GetPreValuesCollectionByDataTypeId(def.Id).FormatAsDictionary();
			//var comment = printComments ? $" // {prop.PropertyEditorAlias}/{def.Name}, {def.DatabaseType.ToString()}, " : "";//pv: {JsonConvert.SerializeObject(pv)}".Replace("\n", " ") : "";
			var datatype = dataTypeService.GetAll(prop.DataTypeId).FirstOrDefault();


			var comment = "";
			string retType = prop.PropertyEditorAlias switch
			{
				"Numeric" => "int",
				"Umbraco.MediaPicker" => GetModelNameForAlias("File"),
				"Umbraco.MultipleMediaPicker" => GetModelNameForAlias("File") + "[]",
				"Umbraco.TrueFalse" => "bool",
				"Umbraco.Slider" or "Umbraco.Integer" => "int",
				"Umbraco.MultiNodeTreePicker" => GetModelnameForMultinodeTreePicker(datatype),//"AFLModelBase[]",
				//case "Umbraco.MultiNodeTreePicker": retType = (pv.ContainsKey("filter") && !string.IsNullOrEmpty(pv["filter"].Value) ? GetModelNameForAlias(pv["filter"].Value) : "AFLModelBase") + "[]"; break;
				//case "Umbraco.ContentPickerAlias": retType = (pv.ContainsKey("filter") && !string.IsNullOrEmpty(pv["filter"].Value) ? GetModelNameForAlias(pv["filter"].Value) : "AFLModelBase"); break;
				"Umbraco.ContentPicker" => GetModelNameForContentPicker(datatype),
				"Umbraco.DropDown" => "string",
				"Umbraco.Tags" => "string[]",
				"Umbraco.RelatedLinks" => "RelatedLink[]",
				"Our.Umbraco.NestedContent" => "AFLModelBase[]",
				"Umbraco.MediaPicker3" => GetModelNameForMediaPicker(datatype),
				_ => null,
			};
			if (retType == null)
			{
				//TODO;
				//check the underlaying type...
				//switch (def?.DatabaseType)
				//{
				//	case ValueStorageType.Ntext: retType = "string"; break;
				//	case ValueStorageType.Nvarchar: retType = "string"; break;
				//	case ValueStorageType.Integer: retType = "int"; break;
				//	case ValueStorageType.Date: retType = "DateTime"; break;
				//	case ValueStorageType.Decimal: retType = "decimal"; break;
				//	default: retType = "string"; break;
				//}
				retType = "string";
			}

			var templateTag = retType == "string" ? "" : "<" + retType + ">";
			var rightSide = isComposition
				? "{ get; }"
				: $"=> Memo{templateTag}(); {comment}";

			return $"{(isComposition ? "" : "public")} {retType} {GetCodePropertyName(prop)} {rightSide}";
		}
		private Type GetCommonAncestor(Type current, Type[] types)
		{
			foreach (var type in types)
			{
				var isMatch = current.IsAssignableFrom(type);
				if (!isMatch)
				{
					return GetCommonAncestor(current.BaseType, types);
				}
			}
			return current;
		}

		private string GetModelnameForMultinodeTreePicker(IDataType datatype)
		{
			var configuration = datatype.Configuration as MultiNodePickerConfiguration;
			if (configuration is null)
			{
				logger.LogError("Unexpected configuration on Multinode Treepicker");
				return "AFLModelBase[]";
			}

			var arraySpecifier = configuration.MaxNumber > 1 || configuration.MaxNumber == 0 
				? "[]" 
				: "";
			var filters = configuration.Filter?.Split(",") ?? Array.Empty<string>();
			var noFiltersSpecified = !filters.Any();
			if (noFiltersSpecified)
			{
				//No filters specified, we cannot make assumptions on which models are allowed.
				return $"AFLModelBase{arraySpecifier}";
			}
			if (filters.Length == 1)
			{
				var modelName = registry.TryGetInfo(filters[0])?.ModelType.Name ?? "AFLModelBase";
				return $"{modelName}{arraySpecifier}"; // This solution doesnt handle the case where someone decorated the model with the Alias Attribute. Consider handling this.
			}

			var info = filters.Select(alias => registry.TryGetInfo(alias).ModelType).ToArray();

			var modelType = GetCommonAncestor(info.First(), info);
			return $"{modelType.Name}{arraySpecifier}";
		}

		private string GetModelNameForContentPicker(IDataType dataType)
		{
			if (dataType.Configuration is ContentPickerConfiguration contentPicker && contentPicker.StartNodeId != null)
			{
				// This might be a way to get a more specific model.
				// but it would entail getting all decendents of the start node, and finding the lowest common model.
				// probably quite an expensive operation so left out for now.
				//var content = contentService.GetById(Guid.Parse(contentPicker.StartNodeId.UriValue.LocalPath.Substring(1)));
			}
			return "AFLModelBase";
		}

		private string GetModelNameForMediaPicker(IDataType datatype)
		{
			string retType;
			if (datatype.Configuration is MediaPicker3Configuration mediaPicker3)
			{
				if(mediaPicker3.Filter.Equals("image", StringComparison.OrdinalIgnoreCase))
				{
					retType = $"UmbracoImage{(mediaPicker3.Multiple ? "[]" : "")}"; 
				}
				else
				{
					retType = $"UmbracoFile{(mediaPicker3.Multiple ? "[]" : "")}";
				}
				return retType;
			}
			logger.LogError($"Unexpected configuration for mediapicker 3 datatype: {datatype.Name}");
			return "object";
		}

		private bool IsMemoProperty(PropertyDefinition prop)
		{
			int stackHeight = 0;
			if (prop.GetMethod == null) { return false; }
			var instructions = prop.GetMethod.Body.Instructions;
			for (int i = 0; i < instructions.Count; ++i)
			{
				var ins = instructions[i];
				int newStackHeight = stackHeight + GetStackDelta(ins, stackHeight);

				if (stackHeight > 0 && newStackHeight == 0)
				{
					var call = i == 0 ? null : instructions[i - 1].Operand as MethodReference;
					if (call == null || call.Name != "Memo")
					{
						return false; //not memo!
					}

					var isBodyPropertyEnd = i == instructions.Count - 4 && ins.OpCode == OpCodes.Stloc_0 && instructions[i + 1].OpCode == OpCodes.Br_S && instructions[i + 2].OpCode == OpCodes.Ldloc_0 && instructions[i + 3].OpCode == OpCodes.Ret;
					var isInlinePropertyEnd = i == instructions.Count - 1 || ins.OpCode == OpCodes.Ret;
					if (!isBodyPropertyEnd && !isInlinePropertyEnd)
					{
						throw new InvalidOperationException("More than one statement!");
					}

					//yay memo call. check type of call (hacky)
					return call.Parameters.Count == 3 && call.Parameters[1].ParameterType.IsPrimitive;
				}

				stackHeight = newStackHeight;
			}
			return false;
		}

		static int GetPushDelta(Instruction instruction)
		{
			OpCode code = instruction.OpCode;
			switch (code.StackBehaviourPush)
			{
				case StackBehaviour.Push0:
					return 0;

				case StackBehaviour.Push1:
				case StackBehaviour.Pushi:
				case StackBehaviour.Pushi8:
				case StackBehaviour.Pushr4:
				case StackBehaviour.Pushr8:
				case StackBehaviour.Pushref:
					return 1;

				case StackBehaviour.Push1_push1:
					return 2;

				case StackBehaviour.Varpush:
					if (code.FlowControl == FlowControl.Call)
					{
						MethodReference method = (MethodReference)instruction.Operand;
						return method.ReturnType.MetadataType == MetadataType.Void ? 0 : 1;
					}

					break;
			}
			throw new ArgumentException("unknown instruction " + instruction.ToString());
		}

		int GetStackDelta(Instruction instruction, int oldStackHeight)
		{
			return GetPushDelta(instruction) - GetPopDelta(oldStackHeight, instruction);
		}

		int GetPopDelta(int stackHeight, Instruction instruction)
		{
			OpCode code = instruction.OpCode;
			switch (code.StackBehaviourPop)
			{
				case StackBehaviour.Pop0:
					return 0;
				case StackBehaviour.Popi:
				case StackBehaviour.Popref:
				case StackBehaviour.Pop1:
					return 1;

				case StackBehaviour.Pop1_pop1:
				case StackBehaviour.Popi_pop1:
				case StackBehaviour.Popi_popi:
				case StackBehaviour.Popi_popi8:
				case StackBehaviour.Popi_popr4:
				case StackBehaviour.Popi_popr8:
				case StackBehaviour.Popref_pop1:
				case StackBehaviour.Popref_popi:
					return 2;

				case StackBehaviour.Popi_popi_popi:
				case StackBehaviour.Popref_popi_popi:
				case StackBehaviour.Popref_popi_popi8:
				case StackBehaviour.Popref_popi_popr4:
				case StackBehaviour.Popref_popi_popr8:
				case StackBehaviour.Popref_popi_popref:
					return 3;

				case StackBehaviour.PopAll:
					return stackHeight;

				case StackBehaviour.Varpop:
					if (code.FlowControl == FlowControl.Call)
					{
						MethodReference method = (MethodReference)instruction.Operand;
						int count = method.Parameters.Count;
						if (method.HasThis && OpCodes.Newobj.Value != code.Value)
							++count;
						return count;
					}

					if (code.Value == OpCodes.Ret.Value)
						return 1; //assume non-void, since we are only interested in property getters.
					break;
			}
			throw new ArgumentException(instruction.ToString());
		}
	}
}
