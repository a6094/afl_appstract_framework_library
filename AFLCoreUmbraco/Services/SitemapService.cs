﻿using AFLCore;
using AFLCore.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Routing;
using Umbraco.Cms.Core.Services;
using Umbraco.Extensions;

namespace AFLCoreUmbraco
{
	public class SitemapService
	{
		private readonly UmbracoUtils umbracoUtils;
		private readonly Registry registry;
		private readonly IPublicAccessService publicAccessService;
		private readonly IPublishedUrlProvider publishedUrlProvider;

		public SitemapService(UmbracoUtils umbracoUtils, Registry registry, IPublicAccessService publicAccessService, IPublishedUrlProvider publishedUrlProvider)
		{
			this.umbracoUtils = umbracoUtils;
			this.registry = registry;
			this.publicAccessService = publicAccessService;
			this.publishedUrlProvider = publishedUrlProvider;
		}
		public XDocument GetSitemateXMLDocument()
		{
			var doc = new XDocument();
			var serializer = new XmlSerializer(typeof(XElement));

			XNamespace xhtmlNamespace = "http://www.w3.org/TR/xhtml11/xhtml11_schema.html";
			XNamespace sitemapNamespace = "http://www.sitemaps.org/schemas/sitemap/0.9";
			var map = new XElement(sitemapNamespace + "urlset",
				new XAttribute("xmlns", sitemapNamespace.NamespaceName),
				new XAttribute(XNamespace.Xmlns + "xhtml", xhtmlNamespace.NamespaceName));

			//TODO: Should this have its own virtual method, in case we have a project where the customer intendts to add other Xelements to the sitemap ?
			var nodes = GetSitemapNodes();
			foreach (var item in nodes)
			{
				var elem = CreateSingleNode(item.Url, item.LastModified, sitemapNamespace);
				if (item.CultureVariations != null)
				{
					foreach (var variation in item.CultureVariations)
					{
						elem.Add(CreateCultureSpecificAlt(variation.Url, variation.Culture, xhtmlNamespace));
					}
				}
				map.Add(elem);
			}

			using (var writer = doc.CreateWriter())
			{
				serializer.Serialize(writer, map);
			}

			return doc;
		}

		XElement CreateSingleNode(string url, DateTime lastModified, XNamespace xNamespace)
		{
			if (lastModified.Kind == DateTimeKind.Utc)
			{
				// convert to local time, the timezone will be stored in the zzz of the tostring
				// Without this we get a DateimTimeInvalidLocalFormat exception
				lastModified = lastModified.ToLocalTime();
			}
			return new XElement(xNamespace + "url",
				new XElement(xNamespace + "loc", url),
				new XElement(xNamespace + "lastmod", lastModified.ToString("yyyy-MM-ddTHH:mm:sszzz")));
		}

		XElement CreateCultureSpecificAlt(string url, string culture, XNamespace xNamespace)
		{
			return new XElement(xNamespace + "link",
				new XAttribute("rel", "alternate"),
				new XAttribute("hreflang", culture),
				new XAttribute("href", url)
				);
		}

		/// <summary>
		/// Populates a list of renderable non-protected nodes in umbraco. 
		/// it will take the latest UpdateDate from its non renderable children.
		/// if any cultural variations exist it will populate the xhtml:Link element with these.
		/// Override this to add your custom nodes to the sitemap.
		/// </summary>
		/// <returns></returns>
		public virtual List<ContentNode> GetSitemapNodes()
		{
			var content = umbracoUtils.UmbracoHelper
				.ContentAtRoot()
				.SelectMany(c => c.DescendantsOrSelf(umbracoUtils.VariationContextAccessor).Concat(c))
				.OrderBy(c => c.Path)
				//Only add rendereable and nonprotected pages
				.Where(c => registry.GetInfo(c).Renderable && !publicAccessService.IsProtected(c.Path));

			List<ContentNode> nodes = new List<ContentNode>();
			foreach (var node in content)
			{
				var lastModifiedDate = node.UpdateDate;
				//Get last modified in page content
				GetLastModifiedDate(node, ref lastModifiedDate);
				var toAdd = new ContentNode(node.Url(publishedUrlProvider, mode: UrlMode.Absolute), lastModifiedDate);

				var CultureSpecificUrls = node.Cultures
					.Select(x => new
					{
						Url = node.Url(publishedUrlProvider, node.Cultures[x.Key].Culture, UrlMode.Absolute),
						node.Cultures[x.Key].Culture
					})
					.ToArray();

				toAdd.CultureVariations = node.ContentType.Variations.VariesByCulture() ? CultureSpecificUrls.Select(x => new CultureSpecificURL(x.Url, x.Culture)).ToArray() : null;

				nodes.Add(toAdd);
			}
			return nodes;

		}
		void GetLastModifiedDate(IPublishedContent content, ref DateTime date)
		{
			var nonRenderableChildren = content.Children.Where(x => !registry.GetInfo(x).Renderable);
			foreach (var c in nonRenderableChildren)
			{
				var updateDateUTC = c.UpdateDate;
				if (updateDateUTC > date)
				{
					date = updateDateUTC;
				}
				GetLastModifiedDate(c, ref date);
			}
		}
		public class ContentNode
		{
			public string Url { get; set; }
			public DateTime LastModified { get; set; }

			public ContentNode(string url, DateTime lastmodified)
			{
				Url = url;
				LastModified = lastmodified;
			}
			public CultureSpecificURL[] CultureVariations { get; set; }
		}
		/// <summary>
		/// Used in ContentNode for alternative URLS
		/// </summary>
		public class CultureSpecificURL
		{
			public string Url { get; set; }
			public string Culture { get; set; }

			public CultureSpecificURL(string url, string culture)
			{
				Url = url;
				Culture = culture;
			}
		}
	}
}
