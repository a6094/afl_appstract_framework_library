﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Services;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common;
using Umbraco.Extensions;

namespace AFLCoreUmbraco
{
	public class UmbracoUtils
	{
		private readonly IHttpContextAccessor httpAccessor;
		private readonly IVariationContextAccessor variationContextAccessor;
		private readonly IUmbracoContextFactory umbracoContextFactory;

		public UmbracoUtils(IHttpContextAccessor httpContextAccessor, IVariationContextAccessor variationContextAccessor, IUmbracoContextFactory umbracoContextFactory)
		{
			this.httpAccessor = httpContextAccessor;
			this.variationContextAccessor = variationContextAccessor;
			this.umbracoContextFactory = umbracoContextFactory;
		}

		public IVariationContextAccessor VariationContextAccessor => variationContextAccessor;

		// TODO: Need inject due to https://our.umbraco.com/documentation/reference/querying/umbracohelper/
		// Do we need the custom implementation below ?
		// If we need it, can we inject and then apply the key ?
		public UmbracoHelper UmbracoHelper
		{
			get
			{
				const string key = "MemoSupport-umbracoHelper";
				UmbracoHelper instance = (UmbracoHelper)httpAccessor.HttpContext.Items[key];
				if (instance == null)
				{
					var contextacessor = httpAccessor.HttpContext?.RequestServices.GetRequiredService<IUmbracoContextAccessor>();
					if (!contextacessor.TryGetUmbracoContext(out _)) // We need this check when outside HttpContext. Like e.g. Umbraco Examine indexing.
					{
						umbracoContextFactory.EnsureUmbracoContext();
					}
					instance = httpAccessor.HttpContext?.RequestServices.GetRequiredService<UmbracoHelper>();
					httpAccessor.HttpContext.Items[key] = instance;
				}
				return instance;
			}
		}

		public void EnsureCultureStored()
		{
			httpAccessor.HttpContext.Session.SetString("__CultureName", System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
		}

		//call this from surface controllers and ajax stuff, to ensure that we don't loose the current users current culture :)
		public void FixCulture()
		{
			var culture = (string)httpAccessor.HttpContext.Session.GetString("__CultureName") ?? variationContextAccessor.VariationContext.Culture ?? "en-US"; //todo: does the culture through typedcontentatroot work? otherwise hardcode again as: "da-DK"
			System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(culture);
		}

		public string Translate(string key)
		{
			var val = UmbracoHelper.GetDictionaryValue(key);
			return string.IsNullOrEmpty(val) ? key : val;
		}
		/// <summary>
		/// Get's or creates a DocumentType
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="name"></param>
		/// <param name="docType"></param>
		/// <param name="cs"></param>
		/// <param name="cts"></param>
		/// <param name="publish"></param>
		/// <param name="defaultCulture">If creating and desired doctype varies by culture, this must be supplied</param>
		/// <returns></returns>
		public static IContent GetChildOrCreate(IContent parent, string name, string docType, IContentService cs, bool publish = true, string defaultCulture = "")
		{
			var child = cs.GetPagedChildren(parent.Id, 0, int.MaxValue, out long tot).FirstOrDefault(c => c.ContentType.Alias == docType && c.Name.Equals(name, StringComparison.OrdinalIgnoreCase));
			if (child != null)
			{
				return child;
			}
			var result = cs.Create(name, parent, docType);
			if (result.ContentType.VariesByCulture())
			{
				if (defaultCulture.IsNullOrWhiteSpace())
				{
					throw new ArgumentException("Desired conten type varies by culture, please supply a default culture on create");
				}
				result.SetCultureName(name, defaultCulture);
			}
			cs.Save(result);//, raiseEvents: false);
			if (publish) { cs.SaveAndPublish(result); }
			return result;
		}
		//public static bool HasChild(IContent parent, string docType)
		//{
		//	return parent.Children.Where(c => (c.ContentType.Alias == docType)).Any();
		//}

		public static string GetFileSize(double sizeInBytes)
		{
			string result = "0 KB";

			if (sizeInBytes > 0)
			{
				double sizeDouble = sizeInBytes;
				string[] units = new string[] { "B", "KB", "MB", "GB" };
				int i = 0;
				while (sizeDouble > 1024 && i < units.Count() - 1)
				{
					sizeDouble /= 1024;
					i++;
				}
				result = string.Format("{0} {1}", sizeDouble.ToString("F1"), units[i]);
			}
			return result;
		}

		//make sure the given url starts with http:// unless it's relative, and optionally append any additional path parts while ensuring that each part is separated by exactly one slash.
		public static string GetURLWithHttp(string url, params string[] joinWith)
		{
			url = url ?? ""; //url should never be null, but crashing if it is anyway is not good either....

			if (!string.IsNullOrWhiteSpace(url) && url.IndexOf("/") != 0 && url.IndexOf("http://") != 0 && url.IndexOf("https://") != 0) //if not a relative url, and it does not already have http://
			{
				url = "http://" + url;
			}

			//optionally join with additional parts, removing redundant slashes...
			if (joinWith != null && joinWith.Length > 0)
			{
				for (int i = 0; i < joinWith.Length; i++)
				{
					if (!string.IsNullOrEmpty(joinWith[i])) //if the param is empty, skip it (so we don't get consecutive slashes).
					{
						if (url.LastIndexOf('/') != url.Length - 1) { url = url + "/"; } //make sure the url ends with '/'
						url += joinWith[i].IndexOf('/') == 0 ? joinWith[i].Substring(1) : joinWith[i]; //make sure the additional part does not start with slash, and append it.
					}
				}
			}
			return url;
		}

		//public static List<string> GetSuggestionsFromIndexes(string searchString, string indexFolder, List<string> searchableFields, int numberOfItemsToRetrieve)
		//{
		//	Dictionary<string, float> result = new Dictionary<string,float>();

		//	if (string.IsNullOrWhiteSpace(searchString) || string.IsNullOrWhiteSpace(indexFolder) || searchableFields.Count == 0 || !(numberOfItemsToRetrieve > 0))
		//	{
		//		return new List<string>();
		//	}

		//	Analyzer std = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29);
		//	Lucene.Net.Store.Directory directory = Lucene.Net.Store.FSDirectory.Open(new System.IO.DirectoryInfo(indexFolder));

		//	foreach (string field in searchableFields)
		//	{
		//		foreach (KeyValuePair<string, float> keyValue in GetIndexedExpressions(searchString, indexFolder, field, numberOfItemsToRetrieve, std, directory))
		//		{
		//			if (result.ContainsKey(keyValue.Key.Trim()))
		//			{
		//				result[keyValue.Key.Trim()] += keyValue.Value;
		//			}
		//			else
		//			{
		//				result.Add(keyValue.Key.Trim(), keyValue.Value);
		//			}
		//		}
		//	}

		//	if (result.Count < numberOfItemsToRetrieve)
		//		numberOfItemsToRetrieve = result.Count;

		//	return result.OrderBy(p => p.Key.Length).Select(p => p.Key).ToList().GetRange(0, numberOfItemsToRetrieve);
		//}

		//private static Dictionary<string, float> GetIndexedExpressions(string searchString, string indexFolder, string fieldName, int numberOfItemsToRetrieve, Analyzer stdAnalyzer, Lucene.Net.Store.Directory directory)
		//{
		//	string[] stringsToReplace = new string[] { ".", ",", "\"", ":", ";", "*", "&", "-", "=", "?", "/", "\\", "(", ")", "{", "}", "§", "#229", ((char)10).ToString(), ((char)13).ToString(), ((char)160).ToString(), ((char)8211).ToString() };
		//	Dictionary<string, float> result = new Dictionary<string, float>();

		//	string firstWord = searchString;
		//	int indexOfSpace = 0;
		//	if ((indexOfSpace = searchString.IndexOf(' ')) > 0)
		//	{
		//		firstWord = searchString.Substring(0, indexOfSpace);
		//	}

		//	Lucene.Net.QueryParsers.QueryParser parser = new Lucene.Net.QueryParsers.QueryParser(Lucene.Net.Util.Version.LUCENE_29, fieldName, stdAnalyzer);
		//	Lucene.Net.Search.Query qry = parser.Parse(firstWord + "*");

		//	Lucene.Net.Search.Searcher srchr = new Lucene.Net.Search.IndexSearcher(Lucene.Net.Index.IndexReader.Open(directory, true));

		//	TopScoreDocCollector cllctr = TopScoreDocCollector.Create(1000, true);
		//	srchr.Search(qry, cllctr);

		//	ScoreDoc[] hits = cllctr.TopDocs().ScoreDocs;
		//	for (int i = 0; i < hits.Length; i++)
		//	{
		//		int docId = hits[i].Doc;
		//		Lucene.Net.Documents.Document doc = srchr.Doc(docId);
		//		//TODO: Get helper extension to work.
		//		//HtmlHelper.StripHtml("", new string[] { }));
		//		//string phrase = HtmlHelperRenderExtensions.StripHtml(HtmlHelper, doc.Get(fieldName), new string[] { });
		//		string phrase = "";
		//		throw new NotImplementedException();
		//		int index = FirstIndex(phrase, searchString);
		//		if (index == -1)
		//		{
		//			continue;
		//		}

		//		string backupPhrase = phrase;
		//		phrase = phrase.Substring(index, phrase.Length - index);

		//		if (phrase.IndexOf('.') > -1)
		//		{
		//			phrase = phrase.Substring(0, phrase.IndexOf('.'));
		//		}

		//		foreach (string s in stringsToReplace)
		//		{
		//			phrase = phrase.Replace(s, "");
		//		}
		//		phrase = phrase.Replace("#230", "æ");

		//		foreach (string term in GetExpressionsFromPhrase(phrase))
		//		{
		//			if (term.Length < searchString.Length)
		//				continue;

		//			string lowerCaseTerm = term.ToLower();
		//			if (result.ContainsKey(lowerCaseTerm))
		//			{
		//				result[lowerCaseTerm] += 1;
		//			}
		//			else
		//			{
		//				result.Add(lowerCaseTerm, 1);
		//			}
		//		}
		//	}

		//	return result;
		//}

		public static int FirstIndex(string str, string substr)
		{
			if (string.IsNullOrWhiteSpace(str) || string.IsNullOrWhiteSpace(substr))
			{
				throw new ArgumentException("String or substring is not specified.");
			}
			if (str.IndexOf(substr) == 0)
				return 0;

			return str.IndexOf(" " + substr);
		}

		private static List<string> GetExpressionsFromPhrase(string phrase)
		{
			List<string> result = new List<string>();

			string tempString = "";

			foreach (string word in phrase.Split(' '))
			{
				tempString += word + " ";
				result.Add(tempString.Trim());
			}

			return result;
		}
		/// <summary>
		/// Calculates the difference between now and date, returns it as a human readable string
		/// </summary>
		/// <param name="date"></param>
		/// <param name="number">The number of minutes, hours, days, weeks or months of difference</param>
		/// <returns>FewSecondsAgo, OneMinuteAgo, MinutesAgo, OneHourAgo, HourseAgo, ADayAgo, DaysAgo, AWeekAgo, WeeksAgo, AMonthAgo, MonthsAgo, AYearAgo, YearsAgo</returns>
		public static string DateDifferenceFuzzy(DateTime date, out double number)
		{
			number = 0;
			TimeSpan span = DateTime.UtcNow - date.ToUniversalTime();

			if (span.TotalSeconds < 60)
			{
				return "FewSecondsAgo";
			}

			if (span.TotalMinutes <= 60)
			{
				if (span.TotalMinutes == 1)
				{
					return "OneMinuteAgo";
				}
				else
				{
					number = span.TotalMinutes;
					return "MinutesAgo";
				}
			}

			if (span.TotalHours <= 24)
			{
				if (span.TotalHours == 1)
				{
					return "OneHourAgo";
				}
				else
				{
					number = span.TotalHours;
					return "HoursAgo";
				}
			}

			if (span.TotalDays <= 7) // days
			{
				if (span.TotalDays == 1)
				{
					return "ADayAgo";
				}
				else
				{
					number = span.TotalDays;
					return "DaysAgo";
				}
			}
			if (span.TotalDays <= 30) // weeks
			{
				var w = Math.Floor(span.TotalDays / 7);
				if (w == 1)
				{
					return "AWeekAgo";
				}
				else
				{
					number = w;
					return "WeeksAgo";
				}
			}
			if (span.TotalDays <= 365) // months
			{
				var m = Math.Floor(span.TotalDays / 30);
				if (m == 1)
				{
					return "AMonthAgo";
				}
				else
				{
					number = m;
					return "MonthsAgo";
				}
			}

			double years = Math.Floor(span.TotalDays / 365); // years
			if (Math.Floor(years) == 1)
				return "AYearAgo";

			number = years;
			return "YearsAgo";
		}

		///// <summary>
		///// deprecated. use BeginModelForm instead.
		///// </summary>
		//public static MvcForm BeginAppstractForm<TModel>(this HtmlHelper<TModel> html, string action, IAFLModel model, Type surfaceController, string classes = null, string method="post")
		//{
		//	var attributes = new Dictionary<string, object>();
		//	if(!classes.IsNullOrEmpty()) { attributes.Add("class", classes); }
		//	attributes.Add("method", method);

		//	var additionalRouteVals = new { ajaxPartialModelID = model.UID, ajaxPartialModelClass = model.GetType().FullName };
		//	return html.BeginUmbracoForm(action, surfaceController, additionalRouteVals, attributes);
		//}

		//public static Umbraco.Core.Models.PublishedContent.IPublishedContent GetNodeFromPostbackContext(Type expectedModelType)
		//{
		//	/*
		//	* * This is only applicable for models that represent pages, or that are otherwise created directly as a response to a clientside request (e.g. through ajax requests).
		//	*
		//	* In the normal case, the default controller will be fed with a RenderModel wich contains the correct IPublishedContent, and FrameworkMain.CreateModelInstance() is invoked to
		//	* create the correct model with the correct IPublishedContent.
		//	* However, we allow models to be created with a null IPublishedContent, if the HttpContext Request object contains information that allows us to fetch
		//	* the correct node automatically. This is needed in the case of postback to Surface Controllers, where the model will be constructed behind the scenes by MVC.
		//	*
		//	* If a model is created without a node, and without fitting data in "uformpostroutevals" (or "ufprt"), an exception will be thrown (below).
		//	* */
		//	var umbracoRoute = HttpContext.Current.Request["uformpostroutevals"].IsNullOrWhiteSpace() ? (HttpContext.Current.Request["ufprt"].IsNullOrWhiteSpace() ? null : HttpContext.Current.Request["ufprt"].DecryptWithMachineKey()) : HttpContext.Current.Request["uformpostroutevals"].DecryptWithMachineKey();
		//	var query = HttpUtility.ParseQueryString(umbracoRoute);
		//	var postBackNodeId = query["ajaxPartialModelID"];
		//	var postBackModelName = query["ajaxPartialModelClass"];

		//	var node = string.IsNullOrEmpty(postBackNodeId) ? null : UmbracoHelper.TypedContent(postBackNodeId);
		//	if (node == null || /*node.DocumentTypeAlias != ModelDocumentTypeAlias*/ expectedModelType.FullName != postBackModelName)
		//	{
		//		throw new InvalidOperationException("Model can not be created: no content node provided, and postback info is missing or invalid.");
		//	}
		//	return node;
		//}
	}

}