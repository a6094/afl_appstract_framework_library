﻿using AFLCore;

namespace AFLCoreUmbraco.Models
{
	/// <summary>
	/// Used with the block editor, to wrap the block editor and make models available in a strongly typed manner.
	/// </summary>
	/// <typeparam name="TBlockType"></typeparam>
	/// <typeparam name="TSettingsType"></typeparam>
	public class AFLBlockContent<TBlockType, TSettingsType>
		where TBlockType : IAFLModel
		where TSettingsType : IAFLModel
	{
		public AFLBlockContent(TBlockType blockModel, TSettingsType settingsModel)
		{
			BlockModel = blockModel;
			SettingsModel = settingsModel;
		}

		public TBlockType BlockModel { get; }
		public TSettingsType SettingsModel { get; }
	}


}
