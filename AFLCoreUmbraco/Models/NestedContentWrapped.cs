﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Cms.Core.Models.PublishedContent;

namespace AFLCoreUmbraco.Models
{
	public class NestedContentWrapped : IPublishedContent
	{
		private readonly IPublishedElement content;
		private readonly IPublishedContent parent; //TODO: Check if it works with event handlers. Also this is not really the parent, but the node which the nested content is a part of.
												   //todo2: CHECK how this behaves with double nested content!
		/// <summary>
		/// Wraps nested content, to mimic IPublishedContent behavior
		/// </summary>
		/// <param name="_content"></param>
		/// <param name="contextNode">If supplied, the parent, ie which node the nestedcontent is defined in, can be determined</param>
		public NestedContentWrapped(IPublishedElement _content, IPublishedContent contextNode)
		{
			content = _content;
			parent = contextNode;
		}
		public int Id => content.Key.GetHashCode();

		public string Name => "Nested Content Wrapped"; //TODO: No general name available atm, find other solution if needed

		public string UrlSegment => throw new NotImplementedException();

		public int SortOrder => throw new NotImplementedException();

		public int Level => throw new NotImplementedException();

		public string Path => throw new NotImplementedException();

		public int? TemplateId => throw new NotImplementedException();

		public int CreatorId => throw new NotImplementedException();

		public string CreatorName => throw new NotImplementedException();

		public DateTime CreateDate => throw new NotImplementedException();

		public int WriterId => throw new NotImplementedException();

		public string WriterName => throw new NotImplementedException();

		public DateTime UpdateDate => throw new NotImplementedException();

		public string Url => throw new NotImplementedException();

		public IReadOnlyDictionary<string, PublishedCultureInfo> Cultures => throw new NotImplementedException();

		public PublishedItemType ItemType => throw new NotImplementedException();

		public IPublishedContent Parent => parent ?? throw new NullReferenceException("parent is null, could be from multiple nested content");
		public IEnumerable<IPublishedContent> Children => throw new NotImplementedException("Children not supported for nested content");

		//public PublishedContentType ContentType => content.ContentType;

		public Guid Key => content.Key;

		public IEnumerable<IPublishedProperty> Properties => content.Properties;

		public IEnumerable<IPublishedContent> ChildrenForAllCultures => throw new NotImplementedException("Children not supported for nested content");

		public IPublishedContentType ContentType => content.ContentType;

		public PublishedCultureInfo GetCulture(string culture = null)
		{
			throw new NotImplementedException();
		}

		public IPublishedProperty GetProperty(string alias)
		{
			return Properties.FirstOrDefault(x => x.Alias == alias);
		}

		public string GetUrl(string culture = null)
		{
			throw new NotImplementedException();
		}

		public bool IsDraft(string culture = null)
		{
			throw new NotImplementedException();
		}

		public bool IsPublished(string culture = null)
		{
			throw new NotImplementedException();
		}
	}
}
