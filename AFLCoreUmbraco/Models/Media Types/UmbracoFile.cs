﻿using AFLCore.Attributes;
using Microsoft.AspNetCore.Http;
using Umbraco.Cms.Core.Models.PublishedContent;

namespace AFLCoreUmbraco.Models.MediaTypes
{
	[Alias("File")]
	public class UmbracoFile : MediaItem
	{
		public UmbracoFile(IPublishedContent node, IHttpContextAccessor httpContextAccessor) : base(node, httpContextAccessor)
		{
		}
	}
}
