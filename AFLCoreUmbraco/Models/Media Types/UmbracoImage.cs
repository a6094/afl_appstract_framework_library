﻿using System;
using AFLCore.Attributes;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Umbraco.Cms.Core.Models.PublishedContent;

namespace AFLCoreUmbraco.Models.MediaTypes
{
	[Alias("Image")]
	public class UmbracoImage : UmbracoFile
	{
		/// <summary>
		/// Optionally extend from this, in a project specific class, and create strongly typed crops like this:
		/// public string NewsImageUrl => Memo(() => node.GetCropUrl("NewsMedium"));
		/// </summary>
		/// <param name="node"></param>
		public UmbracoImage(IPublishedContent node, IHttpContextAccessor httpContextAccessor) : base(node, httpContextAccessor)
		{
		}
		[PropertyAlias("umbracoWidth")]
		public string Width => Memo();
		[PropertyAlias("umbracoHeight")]
		public string Height => Memo();

		public override string FilePath => Memo(() => {
			throw new NotImplementedException("Archaic implementation never worked. Implement");
			object cropperImage = "";//node.GetPropertyValue("umbracoFile", "");
		    if (cropperImage is string)
		    {
                if ("".Equals(cropperImage))
                {
                    return "";
                }

                try
                {
                    dynamic cropperJson = JsonConvert.DeserializeObject((string)cropperImage);
                    return cropperJson.src;
                }
                catch (Exception)
                {
                    return cropperImage;
                }
            }
            return "";
			
		});
	}
}
