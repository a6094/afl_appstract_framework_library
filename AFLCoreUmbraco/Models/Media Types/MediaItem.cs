﻿using AFLCore.Attributes;
using Microsoft.AspNetCore.Http;
using Umbraco.Cms.Core.Models.PublishedContent;

namespace AFLCoreUmbraco.Models.MediaTypes
{
	public abstract class MediaItem : AFLModelBase
	{
		public MediaItem(IPublishedContent node, IHttpContextAccessor httpContextAccessor) : base(node)
		{
			HttpContextAccessor = httpContextAccessor;
		}

		[PropertyAlias("umbracoFile")]
		public virtual string FilePath => Memo();
		//TODO: RIX THIS
		//public string FilePathWithDomain => Memo(() => HttpContextAccessor.HttpContext.Request.Url.Authority + FilePath);
		[PropertyAlias("umbracoBytes")]
		public string FileSize => Memo();
		[PropertyAlias("umbracoExtension")]
		public string FileExtension => Memo();

		public IHttpContextAccessor HttpContextAccessor { get; }
	}
}
