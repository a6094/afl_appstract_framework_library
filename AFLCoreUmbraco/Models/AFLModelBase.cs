﻿using AFLCore;
using AFLCore.Core;
using AFLCoreUmbraco.Models.DatabaseWrappers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Extensions;

namespace AFLCoreUmbraco.Models
{
	public abstract class AFLModelBase : IAFLModel
	{
		protected readonly IPublishedContent node;
		protected AFLModelBase(IPublishedContent node)
		{
			this.node = node ?? throw new InvalidOperationException("node can not be null!");
		}

		protected AFLModelBase()
		{
			//in this case the Registry will set node via reflection, when using CreateModelInstance.
		}

		protected T Memo<T>(Func<T> func, T fallback = default(T), [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, func, fallback, false, null, callerName, null);
		}

		protected T Memo<T>(Nav navType, Func<T, T> postProcess = null, T fallback = default(T), [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(postProcess, null, fallback, false, navType, callerName, null);
		}

		protected T Memo<T>(Nav<T> navType, T fallback = default(T), [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, null, fallback, false, navType, callerName, null);
		}

		protected TPost Memo<TPre, TPost>(Nav<TPre> navType, Func<TPre, TPost> postProcessFunc, TPost fallback = default(TPost), [CallerMemberName] string callerName = "")
		{
			return Memo(postProcessFunc, null, fallback, false, navType, callerName, null);
		}

		//for cases like: int ZipCode => Memo<int>();
		/// <summary>
		/// For cases like int ZipCode => Memo<int>();
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="fallback">Fallback incase of T being null</param>
		/// <param name="recurse">Value getter will recurse up the tree, untill it finds a non empty value</param>
		/// <param name="callerName"></param>
		/// <returns></returns>
		protected T Memo<T>(T fallback = default(T), bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, null, fallback, recurse, null, callerName, null);
		}
		//for cases like: string AddressLine1 => Memo();
		protected string Memo(string fallback = "", bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo<string, string>(null, null, fallback, recurse, null, callerName, null);
		}

		//for cases like: string SearchPageUrl => Memo<IPublishedContent[], string>(n => n.FirstOrDefault()?.Url);
		protected TPost Memo<TPre, TPost>(Func<TPre, TPost> postProcessFunc, TPost fallback = default(TPost), bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo(postProcessFunc, null, fallback, recurse, null, callerName, null);
		}
		//for cases like: EventPage[] RawEventPages => Memo<EventPage[]>(list => list.OrderByDescending(n => n.PublicationDate).ToArray());
		protected T Memo<T>(Func<T, T> postProcessFunc, T fallback = default(T), bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(postProcessFunc, null, fallback, recurse, null, callerName, null);
		}

		protected T Memo<T>(Injector injector, [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, null, default(T), false, null, callerName, injector);
		}

		protected TPost Memo<TPre, TPost>(Injector injector, Func<TPre, TPost> postProcessFunc, [CallerMemberName] string callerName = "")
		{
			return Memo(postProcessFunc, null, default(TPost), false, null, callerName, injector);
		}

		private TPost Memo<TPre, TPost>(Func<TPre, TPost> postProcessFunc, Func<TPre> valueGetterFunc, TPost fallback, bool recurse, Nav nav, string callerName, Injector injector)
		{
			var propertyInfo = Registry.GetInfo(this.GetType())?.GetProperty(callerName);

			if (valueGetterFunc == null)
			{
				var preType = typeof(TPre);
				var elementType = preType.IsArray ? preType.GetElementType() : preType;
				object value = null;
				valueGetterFunc = () =>
				{
					if (nav != null)
					{
						Func<object, bool> resultFilter = null;
						//todo: caching.
						if (nav.types is { Length: > 0 })
						{
							var aliases = nav.types.SelectMany(d => Registry.GetInfo(d).CompatibleAliases).ToArray();
							resultFilter = (n =>
								((nav.Visibility & Nav.VisibilityFlags.OnlyVisible) == 0 || (n is IPublishedContent node && !((bool)node.GetProperty("umbracoNaviHide").GetValue())) &&
								((nav.Visibility & Nav.VisibilityFlags.OnlyRenderable) == 0 || (Registry.GetInfo(n).Renderable)) &&
								aliases.Any(a => (Registry.GetAlias(n)) == a)
							));
						}
						else if (nav.Visibility != Nav.VisibilityFlags.All)
						{
							resultFilter = (n =>
								(n is IPublishedContent node && !((bool)node.GetProperty("umbracoNaviHide").GetValue()) &&
								((nav.Visibility & Nav.VisibilityFlags.OnlyRenderable) == 0 || (Registry.GetInfo(n).Renderable))));
						}

						bool entityConvert = typeof(AFLModelBase).IsAssignableFrom(elementType);
						if (!entityConvert && elementType != typeof(IPublishedContent))
						{
							throw new InvalidOperationException("Unsupported return type for memo property. Pre-type must be entity or IPublishedContent (or arrays of those, for list cases)");
						}
						bool expectNonListReturn = false;
						Func<object, object> conv = (n => entityConvert ? MemoSupport.GetModel(n) : n);

						if (nav.take == Nav.Take.Children)
						{
							var children = node.Children.Where(resultFilter ?? (p => true));
							value = MemoSupport.EnumerableToTypedArray(elementType, children.Select(n => conv(n)));
						}
						else if (nav.take == Nav.Take.FirstChild)
						{
							expectNonListReturn = true;
							var n = node.Children.Where(resultFilter ?? (p => true)).FirstOrDefault();
							value = n == null ? null : conv(n);
						}
						else if (nav.take == Nav.Take.AllSiblings)
						{
							value = MemoSupport.EnumerableToTypedArray(elementType, GetSiblings(node).Where(resultFilter ?? (p => true)).Select(n => conv(n)));
						}
						else if (nav.take == Nav.Take.FirstSibling)
						{
							expectNonListReturn = true;
							var n = GetSiblings(node).FirstOrDefault(resultFilter ?? (p => true));
							value = n == null ? null : conv(n);
						}
						else if (nav.take is Nav.Take.NextSibling or Nav.Take.PreviousSibling)
						{
							expectNonListReturn = true;
							//var n = nav.take == Nav.Take.NextSibling ? node.Next(resultFilter ?? (p => true)) : node.Previous(resultFilter ?? (p => true)); -- this was incorrect, since it navigates in the full content and not just siblings.
							var siblings = GetSiblings(node).ToArray();
							var selfIndex = siblings.IndexOf(node);
							var index = selfIndex + ((nav.take == Nav.Take.NextSibling) ? 1 : -1);
							var n = (selfIndex >= 0 && index >= 0) && index < siblings.Length ? siblings[index] : null;
							value = n == null ? null : conv(n);
						}
						else if (nav.take == Nav.Take.Descendants)
						{
							//TODO: TEST WITH NULL VARIATION CONTEXT.
							value = MemoSupport.EnumerableToTypedArray(elementType, node.Descendants(null).Where(resultFilter ?? (p => true)).Select(n => conv(n)));
						}
						else if (nav.take == Nav.Take.AllAncestors)
						{
							value = MemoSupport.EnumerableToTypedArray(elementType, node.AncestorsOrSelf().Where<IPublishedContent>(resultFilter ?? (p => true)).Select(n => conv(n)));
						}
						else if (nav.take is Nav.Take.ClosestAncestorExcludingSelf or Nav.Take.ClosestAncestorIncludingSelf)
						{
							expectNonListReturn = true;
							var n = (nav.take == Nav.Take.ClosestAncestorIncludingSelf ? node.AncestorsOrSelf() : node.Ancestors()).Where<IPublishedContent>(resultFilter ?? (p => true)).First();
							value = n == null ? null : conv(n);
						}
						else if (nav.take == Nav.Take.Parent)
						{
							expectNonListReturn = true;
							var n = new[] { node.Parent }.Where(resultFilter ?? (p => true)).FirstOrDefault();
							value = n == null ? null : conv(n);
						}
						else if (nav.take == Nav.Take.RootOrSelf)
						{
							// TODO: Can this be done nicer ?
							// Implemented to combat ClosestAncestorOrSelf not resulting in root for levels higher than 2.
							expectNonListReturn = true;
							var n = node.AncestorsOrSelf(1).FirstOrDefault();
							value = n == null ? null : conv(n);
						}
						if (expectNonListReturn && preType.IsArray)
						{
							throw new InvalidOperationException("Expected non array return type for this kind of memo selector");
						}
					}
					else
					{
						value = MemoSupport.GetValue<TPre>(this, propertyInfo, callerName, elementType, recurse, injector);
					}
					return (TPre)value;
				};
			}

			return MemoSupport.MemoImplementation(valueGetterFunc, postProcessFunc, fallback, Registry.GetID(node), callerName, propertyInfo); // use Registry.Current.GetID(node) to allow workaround for case of nested content which all have id zero.
		}

		/// <summary>
		/// umbracos built in PublishedContentExtensions.Siblings has a "bug", in that it directly uses UmbracoContext.Current when the current item has a null parent.
		/// for us, this means that DBContent reverts to regular IPublishedContent for that case (lookup of siblings on DBContent yields non-DBContent), which is not cool.
		/// therefore we re-implement our own version of the siblings-method here...
		/// </summary>
		private IEnumerable<IPublishedContent> GetSiblings(IPublishedContent content)
		{
			//TODO: Test if above comment still applies to Umbraco9
            if (content is IContentWithSiblings siblings)
            {
                return siblings.Siblings();
            }
            else
            {
				//TODO: Figure out a IPublishedSnapshot. Should we inject or should we provide our own ?
                return content.SiblingsAndSelf(null,null); // then use the ordinary PublishedContentExtensions.Siblings method.
            }
        }

		protected void FlushMemo([CallerMemberName] string callerName = "")
		{
			throw new NotImplementedException("Should be implemented via IAFLCache");
			//MemoSupport.Flush(Registry.GetID(node), callerName); // use Registry.Current.GetID(node) to allow workaround for case of nested content which all have id zero.
		}

		//public string ViewTypeName { get { return node.GetTemplateAlias(); } }


		public string UID => node.Id.ToString();

		public int ID => node.Id;

		public string Url => node.Url();

		public string AbsoluteUrl  => node.Url(mode: UrlMode.Absolute);
		 ///{ get { throw new NotImplementedException("TODO: Figureout strategy for IURLPROVIDER"); /*return node.Url(mode: UrlMode.Absolute); /*return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + node.Url;*/ } }

		public DateTime PublicationDate => node.UpdateDate;

		//protected UmbracoHelper UmbracoHelper => UmbracoUtils.UmbracoHelper; // See UmbracoUtils
		//protected MembershipHelper UmbracoMembershipHelper => UmbracoUtils.UmbracoMembershipHelper;


		public string RelativeUrl => node.Url(mode: UrlMode.Relative);
		//	Memo(() =>
		//{
		//	throw new NotImplementedException("TODO: Figureout strategy for IURLPROVIDER");
		//	return "";
		//	//string url = node.Url;
		//	//if (url.StartsWith("http://", StringComparison.Ordinal) || url.StartsWith("https://", StringComparison.Ordinal) || url.StartsWith("//", StringComparison.Ordinal))
		//	//{
		//	//	return new Uri(node.Url).PathAndQuery;
		//	//}
		//	//else
		//	//{
		//	//	return url;
		//	//}
		//});

		public string Name => node.Name;

		public bool IsRoot => node.Level == 1;

		public IPublishedContent Content => node;

		public string ViewTypeName
		{
			get
			{
				if (node is NestedContentWrapped)
				{
					return node.ContentType.Alias;
				}
				return node.GetTemplateAlias();
			}
		}

		public Registry Registry { get; set; }
		public IMemoSupport MemoSupport { get; set; }

		public string Translate(string key)
		{
			throw new NotImplementedException("Figure out IOC implementation");
			//UmbracoUtils.FixCulture();
			//var val = UmbracoUtils.Translate(key);
			//return string.IsNullOrEmpty(val) ? key : val;
		}

		public string Translate(string key, string defaultValue)
		{
			var result = Translate(key);
			return result == key ? defaultValue : result;
		}

		private AFLTypePropertyInfo GetPropertyInfo<TProperty>(Type type, Expression<Func<TProperty>> propertyLambda)
		{
			if (propertyLambda.Body is not MemberExpression member)
			{
				throw new ArgumentException(string.Format("Expression '{0}' refers to a method, not a property.", propertyLambda.ToString()));
			}
			var propInfo = member.Member as PropertyInfo;
			if (propInfo == null)
			{
				throw new ArgumentException(string.Format("Expression '{0}' refers to a field, not a property.", propertyLambda.ToString()));
			}

			if (propInfo.DeclaringType != type && !propInfo.DeclaringType.IsAssignableFrom(type))
			{
				throw new ArgumentException("Declaring type on the property is " + propInfo.DeclaringType.Name + ", expected " + type.Name);
			}

			var info = Registry.GetInfo(type)?.GetProperty(propInfo.Name);
			if (info == null || string.IsNullOrEmpty(info.CMSName))
			{
				throw new Exception("Could not resolve CMS property name");
			}
			return info;
		}

		public void SetProperty<TProperty>(Expression<Func<TProperty>> propertyLambda, object value)
		{
			if (node is not ISettableContent settableNode)
			{
				throw new Exception("This model is not settable!");
			}
			var info = GetPropertyInfo(GetType(), propertyLambda);

            if (value == null || value is AFLModelBase)
            {
                value = value == null ? null : GetDocumentRef((value as AFLModelBase).Content);
            }
            else if (value.GetType().IsArray && typeof(AFLModelBase).IsAssignableFrom(value.GetType().GetElementType()))
            {
                value = string.Join(",", ((AFLModelBase[])value).Select(v => GetDocumentRef(((AFLModelBase)v).Content)));
            }
            else if (value.GetType().IsGenericType && (value.GetType().GetGenericTypeDefinition() == typeof(List<>)))
            {
                var arr = (value as IEnumerable<AFLModelBase>)?.ToArray();
                if (arr != null)
                {
                    value = string.Join(",", arr.Select(v => GetDocumentRef(v.Content)));
                }
            }

            settableNode.SetProperty(info.CMSName, value);
			FlushMemo(info.PropertyInfo.Name);
		}

        private string GetDocumentRef(IPublishedContent content)
        {
            Guid key = content.Key;
            if (key == Guid.Empty)
            {
                throw new ArgumentException("The content does not have a guid. Make sure it has been added to umbraco.");
            }
            return "umb://document/" + key.ToString("N");
        }

        public void SetName(string value)
		{
			if (node is not ISettableContent settableNode)
			{
				throw new Exception("This model is not settable!");
			}
			settableNode.SetName(value);
		}


		public void SetNestedContentList(Expression<Func<object>> propertyLambda, object value)
		{
			if (node is not ISettableContent settableNode)
			{
				throw new Exception("This model is not settable!");
			}
			var info = GetPropertyInfo(GetType(), propertyLambda);

			if(value != null)
			{
				var modelList = (IEnumerable<AFLModelBase>)value;
				var dataList = new List<Dictionary<string, object>>();
				foreach (var v in modelList)
				{
					if(!(v.Content is DBDetachedContent))
					{
						throw new Exception("The nested content items must be memory backed items.");
					}
					dataList.Add(((DBDetachedContent)v.Content).GetData());
				}
				value = JsonConvert.SerializeObject(dataList);
			}

			settableNode.SetProperty(info.CMSName, value);
			FlushMemo(info.PropertyInfo.Name);
		}

		//public void SetNestedContentProperty(Expression<Func<object>> propertyLambda, AFLModelBase targetNestedContentNode, AFLModelBase updatedNestedContentNode)
		//{
		//	var settableNode = node as ISettableContent;
		//	if (settableNode == null)
		//	{
		//		throw new Exception("This model is not settable!");
		//	}
		//	if (updatedNestedContentNode == null && targetNestedContentNode == null)
		//	{
		//		throw new Exception("Either targetNestedContentNode or updatedNestedContentNode must be non-null.");
		//	}
		//	var info = GetPropertyInfo(GetType(), propertyLambda);

		//	var strData = settableNode.GetRawPropertyValue<string>(info.CMSName);
		//	var data = string.IsNullOrEmpty(strData) ? new List<Dictionary<string, object>>() : JsonConvert.DeserializeObject<Dictionary<string, object>[]>(strData).ToList();
		//	int index = -1;
		//	if (targetNestedContentNode != null)
		//	{
		//		var list = (IEnumerable<object>)propertyLambda.Compile().Invoke();
		//		if (list == null)
		//		{
		//			throw new ArgumentException("could not convert nested content property to enumerable");
		//		}
		//		index = list.IndexOf(targetNestedContentNode);
		//		if (index < 0)
		//		{
		//			throw new ArgumentException("target nested content node not found in list");
		//		}
		//	}
		//	if (updatedNestedContentNode == null) // delete the node
		//	{
		//		data.RemoveAt(index);
		//	}
		//	else
		//	{
		//		var type = updatedNestedContentNode.GetType();
		//		var newCopy = (AFLModelBase)Registry.CreateMemoryBackedModel(type, updatedNestedContentNode);

		//		if (targetNestedContentNode == null) // append the node
		//		{
		//			data.Add(((DBDetachedContent)newCopy.Content).GetData());
		//		}
		//		else  // replace
		//		{
		//			data[index] = ((DBDetachedContent)newCopy.Content).GetData();
		//		}
		//	}
		//	settableNode.SetProperty(info.CMSName, JsonConvert.SerializeObject(data));
		//}

		public void SaveContent(bool raiseEvents)
		{
			if (node is not ISettableContent settableNode)
			{
				throw new Exception("This model is not settable!");
			}

			settableNode.Save(raiseEvents);
		}

		public void PublishContent()
		{
			if (node is not ISettableContent settableNode)
			{
				throw new Exception("This model is not settable!");
			}

			settableNode.Publish();
		}
	}
}
