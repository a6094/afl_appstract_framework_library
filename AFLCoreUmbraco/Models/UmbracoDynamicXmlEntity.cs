﻿using System;
using AFLCore;
using AFLCore.Core;

namespace AFLCoreUmbraco.Models
{
	public abstract class UmbracoDynamicXmlEntity : IAFLModel
	{
		public string UID { get { throw new NotImplementedException(); } }

		public string ViewTypeName
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public Registry Registry { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
		public IMemoSupport MemoSupport { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

		public string Translate(string key)
		{
			throw new NotImplementedException();
		}
	}

	public abstract class UmbracoJArrayEntity : IAFLModel
	{
		protected dynamic item;
		public UmbracoJArrayEntity(dynamic item)
		{

		}

		public string UID { get { throw new NotImplementedException(); } }

		public string ViewTypeName
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public Registry Registry { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
		public IMemoSupport MemoSupport { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

		public string Translate(string key)
		{
			return "NOT IMPLEMENTED";// UmbracoUtils.Translate(key);
		}
	}
}
