﻿namespace AFLCoreUmbraco.Models.DatabaseWrappers
{
	public interface ISettableContent
	{
		void SetProperty(string property, object value);
		T GetRawPropertyValue<T>(string property);
		void SetName(string value);
		void Save(bool raiseEvents);
		void Publish();
	}
}
