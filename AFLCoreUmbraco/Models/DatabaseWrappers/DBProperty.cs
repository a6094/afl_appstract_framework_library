﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Models.PublishedContent;

namespace AFLCoreUmbraco.Models.DatabaseWrappers
{
	public class DBProperty : IPublishedProperty
	{
		string alias;
		private object value;

		IProperty prop;

		public DBProperty(string alias, object value)
		{
			this.alias = alias;
			this.value = value;
		}

		public DBProperty(IProperty _prop)
		{
			prop = _prop;
		}

		public object DataValue
		{
			get { return value; }
		}

		public bool HasValue
		{
			get { return prop?.Values?.Any() ?? value != null; }
		}

		public string PropertyTypeAlias => alias;

		public object Value => value;

		public object XPathValue
		{
			get { throw new NotImplementedException(); }
		}

		//TODO: Maybe we can wrap this aswell ???...
		public IPublishedPropertyType PropertyType => new DBPropertyType(prop.PropertyType);

		public string Alias => throw new NotImplementedException();


		public object GetSourceValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		public object GetValue(string culture = null, string segment = null)
		{
			culture = prop?.PropertyType?.Variations == ContentVariation.Culture ? culture : null; //set culture null if we're not varying by culture
			return prop?.GetValue(culture, segment) ?? value;
			//throw new NotImplementedException();
		}

		public object GetXPathValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		bool IPublishedProperty.HasValue(string culture, string segment)
		{
			return HasValue;
			//throw new NotImplementedException();
		}
	}
}
