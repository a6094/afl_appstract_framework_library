﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.PropertyEditors;

namespace AFLCoreUmbraco.Models.DatabaseWrappers
{
	public class DBPropertyType : IPublishedPropertyType
	{
		IPropertyType prop;

		public DBPropertyType(IPropertyType _prop)
		{
			prop = _prop;
		}

		public IPublishedContentType ContentType => throw new NotImplementedException();

		public PublishedDataType DataType => throw new NotImplementedException();

		public string Alias => prop.Alias;

		public string EditorAlias => prop.PropertyEditorAlias;

		public bool IsUserProperty => throw new NotImplementedException();

		public ContentVariation Variations => prop.Variations;

		public PropertyCacheLevel CacheLevel => throw new NotImplementedException();

		public Type ModelClrType => throw new NotImplementedException();

		public Type ClrType => throw new NotImplementedException();

		PropertyCacheLevel IPublishedPropertyType.CacheLevel => throw new NotImplementedException();

		public object ConvertInterToObject(IPublishedElement owner, PropertyCacheLevel referenceCacheLevel, object inter, bool preview)
		{
			throw new NotImplementedException();
		}

		public object ConvertInterToXPath(IPublishedElement owner, PropertyCacheLevel referenceCacheLevel, object inter, bool preview)
		{
			throw new NotImplementedException();
		}


		public object ConvertSourceToInter(IPublishedElement owner, object source, bool preview)
		{
			throw new NotImplementedException();
		}

		public bool? IsValue(object value, PropertyValueLevel level)
		{
			throw new NotImplementedException();
		}
	}
}
