﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.PropertyEditors;
using Umbraco.Cms.Core.Services;
using Umbraco.Extensions;

namespace AFLCoreUmbraco.Models.DatabaseWrappers
{
	public class DBContent : IPublishedContent, IContentWithSiblings, ISettableContent
	{
		private IContent content;
		private readonly IContentService contentService;
		private IReadOnlyDictionary<string, PublishedCultureInfo> _cultureInfos;
		public string savingCulture = null; // Intended to be non null in a save/publish event 

		public DBContent(IContent c, IContentService contentService)
		{
			var field = c.GetType().BaseType.GetField("_currentCultureChanges", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.FlattenHierarchy);
			if (field != null)
			{
				var currentCultureChanges = ((HashSet<string>, HashSet<string>, HashSet<string>))field.GetValue(c);
				savingCulture = currentCultureChanges.Item3?.FirstOrDefault(); //For now assume that only one culture is gonna be present in a regular save event
			}
			if (c == null) { throw new ArgumentException("DBContent instantiated with null backing-content"); }
			content = c;
			this.contentService = contentService;
		}

		//public DBContent(int id)
		//{
		//	content = Current.Services.ContentService.GetById(id);
		//}

		public IContent BackingContent => content;

		public IEnumerable<IPublishedContent> Children
		{
			get
			{
				var children = contentService.GetPagedChildren(content.Id, 0, int.MaxValue, out var x);
				//return content.Children().Select(c => new DBContent(c)).ToArray();
				return children.Select(c => new DBContent(c, contentService)).ToArray();
			}
		}

		public IEnumerable<IPublishedContent> Siblings()
		{
			var parent = Parent;
			var siblings = parent == null ? contentService.GetRootContent().Select(c => new DBContent(c, contentService)) : parent.Children;

			// make sure we run it once
			return siblings.ToArray();
		}

		public IEnumerable<IPublishedContent> ContentSet
		{
			get { throw new NotImplementedException(); }
		}

		public IPublishedContentType ContentType
		{
			get
			{
				return new PublishedContentType(content.Id, content.ContentType.Alias, PublishedItemType.Content,
					Enumerable.Empty<string>(),
					Enumerable.Empty<PublishedPropertyType>(),
					content.ContentType.Variations, content.ContentType.IsElement);
			}
		}

		public DateTime CreateDate => content.CreateDate;

		public int CreatorId => content.CreatorId;

		public string CreatorName
		{
			get { throw new NotImplementedException(); }
		}

		public string DocumentTypeAlias => content.ContentType.Alias;

		public int DocumentTypeId => content.ContentType.Id;

		public int GetIndex()
		{
			return 0;
		}

		public IPublishedProperty GetProperty(string alias, bool recurse)
		{
			try
			{
				var c = content;

				var prop = c.Properties[alias];
				var value = TransmogrifyValue(prop.GetValue(), prop.PropertyType);

				while (value == null && recurse)
				{
					c = contentService.GetById(c.Key);
					if (c == null) { break; }
					prop = c.Properties[alias];
					value = TransmogrifyValue(prop.GetValue(), prop.PropertyType);
				}

				return new DBProperty(alias, value);
			}
			catch (Exception e)
			{
				return null;
			}
		}

		public static object TransmogrifyValue(object value, IPropertyType type)
		{
			if (type?.PropertyEditorAlias == "Umbraco.DropDown")
			{
				//TODO: TEST ME OUT
				throw new Exception("IS THIS EVER CALLED?");
				//var preValues = Current.Services.DataTypeService.GetDataType(type.DataTypeKey);

				//return ((ValueListConfiguration)preValues.Configuration).Items.FirstOrDefault(x => x.Id.ToString().Equals(value.ToString(), StringComparison.Ordinal))?.Value;
			}
			else if (type?.PropertyEditorAlias == "Umbraco.DropDown.Flexible")
			{
				//TODO: TEST ME OUT
				throw new Exception("IS THIS EVER CALLED?");
				return Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(value as string).FirstOrDefault();
			}
			return value;
		}

		public IPublishedProperty GetProperty(string alias)
		{
			try
			{

				var property = content.Properties.FirstOrDefault(x => x.Alias == alias);

				if (property != null)
				{
					return new DBProperty(property);
				}


				//Fall back to culture inspecific.
				//TODO: Is there a way to verify if is culture specific ?
				var firstWithValue = content.GetValue(alias);
				if (firstWithValue == null)
				{
					//TODO: Might give wrong results on Save event, but no issues met yet, so keep like this for now
					firstWithValue = content.PublishedCultures.Select(x => content.GetValue(alias, x)).FirstOrDefault(v => v != null);
				}
				return new DBProperty(alias, firstWithValue);
			}
			catch (Exception ex)
			{
				Serilog.Log.Logger.Error<DBContent>(ex, "Error in DBContent get property", this);
				return null;
			}
		}

		public void SetProperty(string property, object value)
		{
			BackingContent.SetValue(property, value);
		}

		public void SetName(string value)
		{
			BackingContent.Name = value;
		}

		public void Save(bool raiseEvents)
		{
			contentService.Save(BackingContent);
		}

		public void Publish()
		{
			contentService.SaveAndPublish(BackingContent);
		}

		public T GetRawPropertyValue<T>(string property)
		{
			return BackingContent.GetValue<T>(property);
		}

		public string GetUrl(string culture = null)
		{
			throw new NotImplementedException();
		}

		public PublishedCultureInfo GetCulture(string culture = null)
		{
			throw new NotImplementedException();
			//return content.PublishedCultures.FirstOrDefault(x => x == culture); //TODO: FIND BETTER 
		}

		bool IPublishedContent.IsDraft(string culture)
		{
			throw new NotImplementedException();
		}

		public bool IsPublished(string culture = null)
		{
			if (culture is null)
			{
				return content.Published;
			}
			return content.IsCulturePublished(culture);
		}

		public int Id => content.Id;

		public bool IsDraft => false;

		public PublishedItemType ItemType => PublishedItemType.Content;

		public int Level => content.Level;

		public string Name => content.Name;

		public IPublishedContent Parent
		{
			get
			{
				var parentID = content.ParentId;
				if (parentID == -1) { return null; }
				//verify if this is proper solution
				return new DBContent(contentService.GetById(parentID), contentService);

			}
		}

		public string Path
		{
			get { throw new NotImplementedException(); }
		}

		public IEnumerable<IPublishedProperty> Properties
		{
			get { return content.Properties.Select(p => new DBProperty(p.Alias, p.GetValue())).ToArray(); }
		}

		public int SortOrder => content.SortOrder;

		//public int TemplateId => content.Template?.Id ?? -1;

		public DateTime UpdateDate => content.UpdateDate;

		public string Url => "";

		public string UrlName
		{
			get { throw new NotImplementedException(); }
		}

		//public Guid Version => content.Version;

		public int WriterId => content.WriterId;

		public string WriterName
		{
			get { throw new NotImplementedException(); }
		}

		public Guid Key => BackingContent.Key;

		public string UrlSegment => "";

		public int? TemplateId => throw new NotImplementedException();

		public IReadOnlyDictionary<string, PublishedCultureInfo> Cultures
		{

			get
			{
				if (!content.ContentType.VariesByCulture())
				{
					return new Dictionary<string, PublishedCultureInfo>();
				}
				if (_cultureInfos != null)
				{
					return _cultureInfos;
				}

				return _cultureInfos = content.PublishCultureInfos.Values.ToDictionary(x => x.Culture, x => new PublishedCultureInfo(x.Culture, x.Name, UrlSegment, x.Date));
			}
		}

		public IEnumerable<IPublishedContent> ChildrenForAllCultures => Children;

		public object this[string alias]
		{
			get { throw new NotImplementedException(); }
		}
	}
}
