﻿using System.Collections.Generic;
using Umbraco.Cms.Core.Models.PublishedContent;

namespace AFLCoreUmbraco.Models.DatabaseWrappers
{
	//used by AFL in preference to the PublishedContentExtensions.Siblings method, so that we an get around a problem for the case of root elements.
	public interface IContentWithSiblings
	{
		IEnumerable<IPublishedContent> Siblings();
	}
}
