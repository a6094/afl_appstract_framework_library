﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Core.Services;

namespace AFLCoreUmbraco.Models.DatabaseWrappers
{
	public class DBDetachedContent : IPublishedContent, ISettableContent
	{
		public const string ncContentTypeAlias = "ncContentTypeAlias";
		private Dictionary<string, object> data;
		private readonly IContentTypeService contentTypeService;

		public DBDetachedContent(Dictionary<string, object> data, IContentTypeService contentService)
		{
			this.data = data;
			this.contentTypeService = contentService;
			//required when adding nestedcontent
			if (!data.ContainsKey("key"))
			{
				data.Add("key", Guid.NewGuid());
			}
		}
		public DBDetachedContent(Dictionary<string, object> data, string alias, IContentTypeService contentService) : this(data, contentService)
		{
			data.Add(ncContentTypeAlias, alias);
		}

		public Dictionary<string, object> GetData() { return data; }

		public object this[string alias]
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public IEnumerable<IPublishedContent> Children => Enumerable.Empty<IPublishedContent>();

		public IEnumerable<IPublishedContent> ContentSet
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public IPublishedContentType ContentType
		{
			get
			{
				return new PublishedContentType(Guid.Parse(data["key"].ToString()), DocumentTypeId, DocumentTypeAlias, PublishedItemType.Content,
					Enumerable.Empty<string>(),
					Enumerable.Empty<PublishedPropertyType>(),
					ContentVariation.Culture);
			}
		}

		public DateTime CreateDate
		{
			get { return DateTime.MinValue; }
		}

		public int CreatorId
		{
			get
			{
				return 0;
			}
		}

		public string CreatorName
		{
			get
			{
				return null;
			}
		}

		public string DocumentTypeAlias => data.ContainsKey(ncContentTypeAlias) ? (data[ncContentTypeAlias] ?? "").ToString() : null;

		public int DocumentTypeId
		{
			get
			{
				return data.TryGetValue("id", out var id) ? int.Parse(id.ToString()) : -1;
			}
		}

		public int Id
		{
			get
			{
				return 0;
			}
		}

		public bool IsDraft
		{
			get
			{
				return false;
			}
		}

		public PublishedItemType ItemType => PublishedItemType.Content;

		public int Level
		{
			get
			{
				return 0;
			}
		}

		public string Name
		{
			get
			{
				return "TODO";//GetProperty("name").Value?.ToString();
			}
		}

		public IPublishedContent Parent
		{
			get { return null; }
		}

		public string Path
		{
			get { return null; }
		}

		public ICollection<IPublishedProperty> Properties
		{
			get { return data.Select(p => new DBProperty(p.Key, p.Value)).ToArray(); }
		}

		public int SortOrder
		{
			get
			{
				return 0;
			}
		}

		public int TemplateId
		{
			get
			{
				return 0;
			}
		}

		public DateTime UpdateDate
		{
			get
			{
				return DateTime.MinValue;
			}
		}

		public string Url
		{
			get
			{
				return "";
			}
		}

		public string UrlName
		{
			get
			{
				return "";
			}
		}

		public Guid Version
		{
			get
			{
				return Guid.Empty;
			}
		}

		public int WriterId
		{
			get
			{
				return 0;
			}
		}

		public string WriterName
		{
			get
			{
				return null;
			}
		}

		public string UrlSegment => throw new NotImplementedException();

		int? IPublishedContent.TemplateId => throw new NotImplementedException();

		public IReadOnlyDictionary<string, PublishedCultureInfo> Cultures => throw new NotImplementedException();

		public Guid Key => throw new NotImplementedException();

		IEnumerable<IPublishedProperty> IPublishedElement.Properties => throw new NotImplementedException();

		public IEnumerable<IPublishedContent> ChildrenForAllCultures => throw new NotImplementedException();

		public int GetIndex()
		{
			throw new NotImplementedException();
		}

		public IPublishedProperty GetProperty(string alias)
		{
			//todo - caching...

			object value = data.ContainsKey(alias) ? data[alias] : null;
			if (!string.IsNullOrEmpty(DocumentTypeAlias))
			{
				var doctype = contentTypeService.Get(DocumentTypeAlias);
				var proptype = doctype.PropertyTypes.FirstOrDefault(p => p.Alias == alias);
				value = DBContent.TransmogrifyValue(value, proptype);
				//value = DBContent.TransmogrifyValue(value, null);
			}
			return new DBProperty(alias, value);
		}

		public IPublishedProperty GetProperty(string alias, bool recurse)
		{
			return GetProperty(alias);
		}

		public void Save(bool raiseEvents)
		{
			throw new InvalidOperationException("Detached content can not be saved directly.");
		}

		public void Publish()
		{
			throw new InvalidOperationException("Detached content can not be published directly.");
		}

		public void SetName(string value)
		{
			data["name"] = value;
		}

		public void SetProperty(string property, object value)
		{
			data[property] = value;
		}

		public T GetRawPropertyValue<T>(string property)
		{
			return data.ContainsKey(property) ? (T)data[property] : default;
		}

		public string GetUrl(string culture = null)
		{
			return Url;
		}

		public PublishedCultureInfo GetCulture(string culture = null)
		{
			//TODO: Checkup
			return new PublishedCultureInfo(culture, culture, UrlSegment, DateTime.UtcNow);
		}

		bool IPublishedContent.IsDraft(string culture)
		{
			return false;
		}

		public bool IsPublished(string culture = null)
		{
			return true;
		}
	}}
