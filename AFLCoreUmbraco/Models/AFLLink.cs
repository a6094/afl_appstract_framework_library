﻿using Umbraco.Cms.Core.Models;

namespace AFLCoreUmbraco.Models
{
	/// <summary>
	/// Wraps Umbraco link, providing an internal link with a stronlgy typed reference
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class AFLLink<T> where T : AFLModelBase //todo: Martin says "Aleks, please put this in AFL1.4, don't just copy paste."
	{
		private Link link;
		public AFLLink(Link link, T backingModel)
		{
			this.link = link;
			Content = backingModel;
		}

		public T Content { get; private set; }

		public string ExternalLink => link.Type == LinkType.External ? link.Url : null;
		public string Name => link.Name;
		public string Target => link.Target;
		public string Url => link.Url;
	}
}