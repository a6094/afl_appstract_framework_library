﻿using System;
using System.Collections.Generic;
using AFLCore;
using AFLCore.Search;

namespace AFLCoreUmbraco.Search
{
	public abstract class SearchDefinitions : ISearchDefinitions
	{
		public const int AllIndexes = -1;

		public const string nodeType = "__NodeTypeAlias";
		public const string path = "__Path";

		public static Dictionary<SearchBoost, SearchBoostValue> Boosts => new Dictionary<SearchBoost, SearchBoostValue> {
			{ SearchBoost.Low, new SearchBoostValue("appstract_boost_low", 0.9) },
			{ SearchBoost.Normal, new SearchBoostValue("appstract_boost_normal", 1) },
			{ SearchBoost.High, new SearchBoostValue("appstract_boost_high", 1.1) },
			{ SearchBoost.VeryHigh, new SearchBoostValue("appstract_boost_veryhigh", 1.2) },
			{ SearchBoost.Special, new SearchBoostValue("appstract_boost_special", 0) }
		};

		public virtual IEnumerable<SearchIndex> SearchIndexes => new[] { new SearchIndex(1, "ExternalIndex") };

		public virtual void AddCustomFields(SearchIndex index, IAFLModel entity, IDictionary<string, List<object>> fields)
		{
		}
		public virtual bool PreventIndexing(IAFLModel model, SearchIndex index) { return false; }
	}
}
