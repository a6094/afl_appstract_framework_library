﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AFLCoreUmbraco.Search
{
	public class SearchResult<ResultT> { 
		public ResultT[] Results { get; set; } = Array.Empty<ResultT>();

		/// <summary>
		/// Will contain the last query passed to lucene after a search
		/// </summary>
		public string LastLuceneQuery { get; set; }

		public Lucene.Net.Documents.Document[] RawResults { get;  set; } = Array.Empty<Lucene.Net.Documents.Document>();

		public IEnumerable<Tuple<ResultT, Lucene.Net.Documents.Document>> ResultsAndRawResults => Results.Select((r, idx) => new Tuple<ResultT, Lucene.Net.Documents.Document>(r, RawResults[idx]));

		/// <summary>
		/// Total results, before any pagination
		/// </summary>
		public int TotalResults { get; set; } = 0;
	}

	//public class Autocompleter
	//{
	//	public Autocompleter(int searchIndexID, string query, int count = 10)
	//	{
	//		var searchDefinition = Registry.Current.SearchDefinitions.SearchIndexes.FirstOrDefault(d => d.ID == searchIndexID);
	//		//var searchProvider = ExamineManager.Instance.SearchProviderCollection[searchDefinition.Searcher];
	//		var searchProvider = ExamineManager.Instance.RegisteredSearchers.First(x => x == searchDefinition);
	//		//SearchProviderCollection[searchDefinition.Searcher];

	//		var safeUserQuery = Regex.Replace(query, IndexingHandler.NotAllowedChars, " ").ToLower();
	//		safeUserQuery = safeUserQuery.TrimEnd();
	//		IEnumerable<ISearchResult> res;
	//		if (!string.IsNullOrEmpty(safeUserQuery))
	//		{
	//			res = searchProvider.Search(safeUserQuery + "*").AsEnumerable();
	//			//res = searchProvider.Search(searchProvider.CreateSearchCriteria().RawQuery(safeUserQuery + "*")).AsEnumerable();
	//		}
	//		else
	//		{
	//			res = new SearchResult[0];
	//		}


	//		Results = res.SelectMany(r => r.Values.Values).SelectMany(v => v.ToLower().Split(' ').Where(word => word.StartsWith(safeUserQuery))).Distinct().Take(count).ToArray();
	//	}

	//	public string[] Results { get; private set; }
	//}
}
