﻿using System;
namespace AFLCoreUmbraco.Search
{
	/// <summary>
	/// Args for the searcher, pass a different <paramref name="SearchIndexID"/> if a different index than the default "external" is defined.
	/// </summary>
	/// <typeparam name="ResultT"></typeparam>
	/// <param name="query"></param>
	/// <param name="Page"></param>
	/// <param name="PageSize"></param>
	/// <param name="SearchIndexID"></param>
	/// <param name="UseResultAdaptor"></param>
	/// <param name="NoResultsOnEmptyQuery"></param>
	/// <param name="MaxResults"></param>
	/// <param name="SetHighlight"></param>
	/// <param name="ExtraRules">extra rules for the query. can be used to limit results to the children of a given page etc</param>
	public record SearcherArgs<ResultT>(string query, 
		int Page = 0,
		int PageSize = -1,
		int SearchIndexID = 1,
		bool UseResultAdaptor = false,
		bool NoResultsOnEmptyQuery = false,
		int MaxResults = 10000,
		Action<ResultT, Lazy<string>> SetHighlight = null,
		params ExtraRule[] ExtraRules)
	{
	}
}
