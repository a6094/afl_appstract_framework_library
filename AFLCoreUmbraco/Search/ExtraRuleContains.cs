﻿namespace AFLCoreUmbraco.Search
{
	public class ExtraRuleContains : ExtraRule
	{
		public readonly string strings;

		public ExtraRuleContains(string field, string strings = null) : base(field, null, null)
		{
			this.strings = strings;
		}
	}

	//public class Autocompleter
	//{
	//	public Autocompleter(int searchIndexID, string query, int count = 10)
	//	{
	//		var searchDefinition = Registry.Current.SearchDefinitions.SearchIndexes.FirstOrDefault(d => d.ID == searchIndexID);
	//		//var searchProvider = ExamineManager.Instance.SearchProviderCollection[searchDefinition.Searcher];
	//		var searchProvider = ExamineManager.Instance.RegisteredSearchers.First(x => x == searchDefinition);
	//		//SearchProviderCollection[searchDefinition.Searcher];

	//		var safeUserQuery = Regex.Replace(query, IndexingHandler.NotAllowedChars, " ").ToLower();
	//		safeUserQuery = safeUserQuery.TrimEnd();
	//		IEnumerable<ISearchResult> res;
	//		if (!string.IsNullOrEmpty(safeUserQuery))
	//		{
	//			res = searchProvider.Search(safeUserQuery + "*").AsEnumerable();
	//			//res = searchProvider.Search(searchProvider.CreateSearchCriteria().RawQuery(safeUserQuery + "*")).AsEnumerable();
	//		}
	//		else
	//		{
	//			res = new SearchResult[0];
	//		}


	//		Results = res.SelectMany(r => r.Values.Values).SelectMany(v => v.ToLower().Split(' ').Where(word => word.StartsWith(safeUserQuery))).Distinct().Take(count).ToArray();
	//	}

	//	public string[] Results { get; private set; }
	//}
}
