﻿using AFLCore;
using AFLCore.Extensions;
using AFLCore.Search;
using AFLCore.Utils;
using Examine;
using Examine.Lucene.Providers;
using Lucene.Net.QueryParsers.Classic;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Cms.Web.Common;

namespace AFLCoreUmbraco.Search
{
	public class Searcher<TResultType>
	{
		private readonly IExamineManager examineManager;
		private readonly IVariationContextAccessor variationContextAccessor;
		private readonly Registry registry;

		public Searcher(IExamineManager examineManager, IVariationContextAccessor variationContextAccessor, Registry registry)
		{
			this.examineManager = examineManager;
			this.variationContextAccessor = variationContextAccessor;
			this.registry = registry;
		}
		public SearchResult<TResultType> Search(SearcherArgs<TResultType> args)
		{
			//TODO: REDo this class into an injected class!?
			if (registry.SearchDefinitions is NoSearch)
			{
				throw new Exception("Indexing has not been configured. Search not possible.");
			}

			var safeUserQuery = Regex.Replace((args.query ?? "")
				.SearchFriendlyString(), IndexingHandler.NotAllowedChars, " ")
				.ToLower().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
				.Where(w => w.Length > 0).ToArray()
				;
			if (args.NoResultsOnEmptyQuery && safeUserQuery.Length == 0)
			{
				return new SearchResult<TResultType>();
			}
			var searchDefinition = registry.SearchDefinitions.SearchIndexes.FirstOrDefault(d => d.ID == args.SearchIndexID);
			if (searchDefinition == null)
			{
				throw new Exception("Search Definition not registered!");
			}

			var normalSearchFieldDefs = SearchDefinitions.Boosts.Where(kv => kv.Key != SearchBoost.Special).Select(kv => kv.Value).ToArray();

			if (!examineManager.TryGetIndex(searchDefinition.IndexName, out var index))
			{
				throw new Exception("Search provider not found!");
			}


			string luceneQuery = "";

			var culture = variationContextAccessor.VariationContext.Culture.ToString().ToLower();
			if (safeUserQuery.Length > 0)
			{
				luceneQuery = string.Join(" ",
					normalSearchFieldDefs.Select(fieldDef =>
					{
						var boostVal = $"*^{fieldDef.Boost.ToString(CultureInfo.InvariantCulture)}";
						var field = $"{fieldDef.Field}:";
						var fieldCultureSpecific = $"{fieldDef.Field}_{culture}:";
						return string.Join(" ", safeUserQuery.Select(s => $"{field}{s}{boostVal} {fieldCultureSpecific}{s}{boostVal}"));
					})
				);
			}
			luceneQuery = HandleExtraRules(args, luceneQuery);

			//lucene does not allow queries staring with *, but *:* is a special syntax that returns all documents.
			if (string.IsNullOrEmpty(luceneQuery)) { luceneQuery = "*:*"; }

			//perform search...
			var searchProvider = index.Searcher;
			var luceneSearch = ((LuceneSearcher)searchProvider).GetSearchContext().GetSearcher().IndexSearcher;
			var parser = new QueryParser(LuceneInfo.CurrentVersion, "", ((LuceneSearcher)searchProvider).LuceneAnalyzer);
			var result = luceneSearch.Search(parser.Parse(luceneQuery), args.MaxResults);
			var returnValue = new SearchResult<TResultType>();

			returnValue.LastLuceneQuery = luceneQuery;
			var resultDocs = result.ScoreDocs.Select(x => luceneSearch.Doc(x.Doc));
			//TODO: Culture is locked, need other solution for variation by culture"
			var publishKey = $"__Published_{culture}";
			resultDocs = resultDocs.Where(x => x.Get("__VariesByCulture") != "y" || x.Get(publishKey) == "y");
			//var res = searchProvider.CreateQuery().All().Execute().AsQueryable();
			//searchProvider.Search((luceneQuery), maxResults).AsQueryable();
			returnValue.TotalResults = resultDocs.Count();
			if (args.PageSize > -1)
			{
				resultDocs = resultDocs.Skip(args.Page * args.PageSize).Take(args.PageSize);
			}

			returnValue.RawResults = resultDocs.ToArray();
			returnValue.Results = resultDocs.Select(doc =>
			{
				var node = registry.GetNode(doc.Get("id"));
				TResultType entity = default;
				if (node != null)
				{
					if (args.UseResultAdaptor)
					{
						entity = (TResultType)Activator.CreateInstance(typeof(TResultType), registry.GetModel(node));
					}
					else if (typeof(TResultType).IsAssignableFrom(registry.TryGetInfo(node)?.ModelType))
					{
						entity = (TResultType)registry.GetModel(node);
					}
				}
				if (entity != null && args.SetHighlight != null)
				{
					var pieces = normalSearchFieldDefs
						.SelectMany(f => (doc.Get(f.Field) ?? "").Split('|'))
						.Where(s => safeUserQuery.Any(word => CultureInfo.CurrentCulture.CompareInfo.IndexOf(s, word, CompareOptions.IgnoreCase) >= 0));
					args.SetHighlight(entity, new Lazy<string>(() => MakeHighlights(pieces, safeUserQuery)));
				}
				return entity;
			}).Where(p => p != null).ToArray();

			return returnValue;
		}
		private string MakeHighlights(IEnumerable<string> pieces, string[] words)
		{
			const int maxPieces = 3;
			const int approxMaxTotalChars = 300;
			const int minCharsBeforeFirstAndAfterLastHighlight = 60;

			//no more than maxPieces...
			var shownPieces = pieces.Take(maxPieces).Select(p => new Piece { text = p, snipBefore = false, snipAfter = false }).ToList();

			//cut away beginning and end of each piece, if it doesn't contain highlights, and cut of the end if a piece is too long...
			for (int i = 0; i < shownPieces.Count; ++i)
			{
				var piece = shownPieces[i];
				int firstIndex = words.Select(w => piece.text.IndexOf(w, StringComparison.OrdinalIgnoreCase)).Where(idx => idx != -1).Min();
				int lastIndex = words.Select(w => piece.text.LastIndexOf(w, StringComparison.OrdinalIgnoreCase)).Where(idx => idx != -1).Max();
				if (firstIndex > minCharsBeforeFirstAndAfterLastHighlight * 2)
				{
					piece.snipBefore = DiscardBeginningAtWordBoundary(ref piece.text, firstIndex - minCharsBeforeFirstAndAfterLastHighlight);
				}
				if (piece.text.Length > approxMaxTotalChars || piece.text.Length - lastIndex > minCharsBeforeFirstAndAfterLastHighlight * 2)
				{
					piece.snipAfter = DiscardEndAtWordBoundary(ref piece.text, Math.Min(approxMaxTotalChars, lastIndex + minCharsBeforeFirstAndAfterLastHighlight));
				}
			}

			//try to limit to no more than ~ approxMaxTotalChars chars, by showing less pieces if neccesary...
			int totalChars = 0;
			for (int i = 0; i < shownPieces.Count; ++i)
			{
				totalChars += shownPieces[i].text.Length;
				if (totalChars > approxMaxTotalChars) { shownPieces = shownPieces.Take(i + 1).ToList(); break; }
			}

			//merge into one string (while removing multiple spaces and newlines etc.)...
			var result = Regex.Replace(string.Join(" ... ", shownPieces.Select(p => p.text)), "\\s{2,}", " ");

			//append and prepend elipsis if needed...
			if (shownPieces.FirstOrDefault()?.snipBefore ?? false) { result = "... " + result; }
			if (shownPieces.LastOrDefault()?.snipAfter ?? false) { result = result + " ..."; }

			//actual highlighting...
			foreach (var word in words)
			{
				result = Regex.Replace(result, word, "<strong><em>$&</strong></em>", RegexOptions.IgnoreCase); //$& substitutes the entire match.
			}

			return result;
		}
		//used by MakeHighlights()
		class Piece
		{
			public bool snipBefore;
			public bool snipAfter;
			public string text;
		}
		private bool DiscardBeginningAtWordBoundary(ref string str, int cutPoint)
		{
			while (cutPoint > 0 && str[cutPoint] != ' ')
			{
				--cutPoint;
			}
			str = str.Substring(cutPoint);
			return cutPoint > 0;
		}

		private bool DiscardEndAtWordBoundary(ref string str, int cutPoint)
		{
			while (cutPoint < str.Length - 1 && str[cutPoint] != ' ')
			{
				--cutPoint;
			}
			bool hasCut = cutPoint < str.Length - 1;
			str = str.Substring(0, cutPoint);
			return hasCut;
		}

		private static string HandleExtraRules(SearcherArgs<TResultType> args, string luceneQuery)
		{
			var noExtraRules = !(args.ExtraRules?.Any() ?? true);
			if (noExtraRules)
			{
				return luceneQuery;
			}
			var extraRulesString = string.Join(" AND ", args.ExtraRules.Select(r =>
			{
				if (r is ExtraRuleContains rc)
				{
					if (!string.IsNullOrEmpty(rc.strings))
					{
						var ids = rc.strings.Split(',');
						if (ids.Length == 1)
						{
							return " ( " + r.field + ":\\-1*" + ids[0] + "*) ";
						}
						else
						{
							return " ( " + string.Join(" OR ", ids.Select(id => r.field + ":\\-1*" + id + "*")) + ") ";
						}

					}
					return null;
				}
				else
				{
					//careful with the empty-string-case: it will cause lucene syntax errors.
					if (!string.IsNullOrEmpty(r.equals))
					{
						return " ( " + r.field + ":" + r.equals + " ) ";
					}
					else if (!string.IsNullOrEmpty(r.equalsAnySpaceSeparated))
					{
						return " ( " + string.Join(" ", r.equalsAnySpaceSeparated.Split(' ').Select(w => r.field + ":" + w)) + " ) ";
					}
					else
					{
						return null;
					}
				}
			}).Where(str => str != null));
			if (!string.IsNullOrEmpty(extraRulesString))
			{
				luceneQuery = string.IsNullOrEmpty(luceneQuery)
					? extraRulesString
					: $"( {luceneQuery} ) AND {extraRulesString}";
			}

			return luceneQuery;
		}
	}
}
