﻿using AFLCoreUmbraco.Models.MediaTypes;
using System.Globalization;
using System.Linq;
using Umbraco.Cms.Core.Models.PublishedContent;
using Umbraco.Extensions;

namespace AFLCoreUmbraco.Extensions
{
	public static class ImageExtensions
	{
		public static string GetImageUrl(this UmbracoImage item, string cropAlias, double percentageOfScale = 100)
		{
			return (item?.Content).GetImageUrl(cropAlias, percentageOfScale);
		}

		public static string GetImageUrl(this IPublishedContent item, string cropAlias, double percentageOfScale = 100)
		{
			if (item == null || string.IsNullOrEmpty(cropAlias) || percentageOfScale <= 0) { return null; }
			var cropUrl = item.GetCropUrl(cropAlias);
			return percentageOfScale == 100 ? cropUrl : GetScaledDownImageUrlFromUrl(cropUrl, percentageOfScale);
		}

		public static string GetScaledDownImageUrlFromUrl(string url, double percentageOfScale)
		{
			url = ModifyParameterValueByPercentageOfScale(url, "width", percentageOfScale);
			return ModifyParameterValueByPercentageOfScale(url, "height", percentageOfScale);
		}

		private static string ModifyParameterValueByPercentageOfScale(string url, string widthParameterKey, double percentageOfScale)
		{
			var value = GetQueryValue(url, widthParameterKey);
			double newValue = 0;
			if (!string.IsNullOrWhiteSpace(value) && double.TryParse(value, out newValue))
			{
				newValue *= percentageOfScale / 100;
				return ReplaceParameterValue(url, widthParameterKey, value, newValue.ToString(CultureInfo.InvariantCulture));
			}
			return url;
		}

		private static string GetQueryValue(string url, string parameterKey)
		{
			try
			{
				var parameters = url.Split('?')[1].Split('&').ToList();
				return parameters.FirstOrDefault(p => p.StartsWith(parameterKey))?.Split('=')?.LastOrDefault();

			}
			catch { return ""; }
		}

		private static string ReplaceParameterValue(string url, string parameterName, string oldValue, string newValue)
		{
			return url.Replace($"&{parameterName}={oldValue}", $"&{parameterName}={newValue}").Replace($"?{parameterName}={oldValue}", $"?{parameterName}={newValue}");
		}
	}
}
