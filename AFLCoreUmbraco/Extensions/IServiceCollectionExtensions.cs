﻿using AFLCore;
using AFLCore.Caching;
using AFLCore.Core;
using AFLCore.Options;
using AFLCore.Search;
using AFLCoreUmbraco.Search;
using AFLCoreUmbraco.Tools.ModelPlumber;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Umbraco.Cms.Core.DependencyInjection;
using Umbraco.Cms.Core.IO;
using Umbraco.Cms.Infrastructure.DependencyInjection;
using Umbraco.Cms.Web.Website.Controllers;
using Umbraco.Extensions;

namespace AFLCoreUmbraco.Extensions
{
	public static class IServiceCollectionExtensions
	{
		public static void InitializeAFL<TDefaultController, TAflImplementation>
			(
			this IServiceCollection services,
			IConfiguration configuration,
			ISearchDefinitions definitions = null
			)
			where TAflImplementation : IAFLImplementation
			where TDefaultController : UmbracoDefaultControllerBase
		{

			services.AddUnique<IDefaultViewContentProvider, AflViewContentProvider>();
			services.AddSingleton<IndexingHandler>()
				.AddSingleton<UmbracoUtils>()
				.Configure<UmbracoRenderingDefaultsOptions>(c =>
				{
					c.DefaultControllerType = typeof(TDefaultController);
				})
				.AddSingleton(typeof(IAFLImplementation), typeof(TAflImplementation))
				.AddSingleton(definitions ?? new UmbracoSearch())
				.AddSingleton<IAFLCache, AFLCache>()
				.AddScoped<AFLModelPlumber>()
				.AddSingleton<Registry>()
				.AddScoped<SitemapService>()
				.Configure<AFLOptions>(configuration.GetSection("AFL"))
				.AddSingleton<IMemoSupport, UmbracoMemoSupport>()
				.AddScoped(typeof(Searcher<>));
		}
		public static void InitializeAFL<TDefaultController>
			(
			this IServiceCollection services,
			IConfiguration configuration,
			ISearchDefinitions definitions = null
			)
			where TDefaultController : UmbracoDefaultControllerBase
		{
			InitializeAFL<TDefaultController, AFLCoreUmbraco>(services, configuration, definitions);
		}
	}
}
