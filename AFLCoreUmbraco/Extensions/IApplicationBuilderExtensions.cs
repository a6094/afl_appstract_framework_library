﻿using AFLCore;
using AFLCore.Core;
using AFLCoreUmbraco.Search;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Reflection;

namespace AFLCoreUmbraco.Extensions
{
	public static class IApplicationBuilderExtensions
	{
		public static void UseAFL(this IApplicationBuilder app, IEnumerable<Assembly> assemblies, string restBasePath = "ws")
		{
			var registry = app.ApplicationServices.GetRequiredService<Registry>();
            registry.Initialize(assemblies, restBasePath);

            app.UseSession();

			var implementation = app.ApplicationServices.GetRequiredService<IndexingHandler>();
			implementation.OnAFLStarted();

		}
	}
}
