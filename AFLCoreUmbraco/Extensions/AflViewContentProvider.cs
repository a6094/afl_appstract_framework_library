﻿using Umbraco.Cms.Core.IO;

namespace AFLCoreUmbraco.Extensions
{
	public class AflViewContentProvider : IDefaultViewContentProvider
	{
		public string GetDefaultFileContent(string layoutPageAlias = null, string modelClassName = null, string modelNamespace = null, string modelNamespaceAlias = null)
		{
			return $"layoutPageAlias: {layoutPageAlias},modelClassName={modelClassName},modelNamespace={modelNamespace},modelNamespaceAlias={modelNamespaceAlias}";
		}
	}
}
