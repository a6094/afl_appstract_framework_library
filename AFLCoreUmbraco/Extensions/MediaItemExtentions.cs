﻿using AFLCoreUmbraco.Models.MediaTypes;
using System;

namespace AFLCoreUmbraco.Extensions
{
	public static class MediaItemExtentions
	{
		public static string GetUrl(this MediaItem item)
		{
			return item?.FilePath ?? "";
		}

		public static string GetUrlWithDomain(this MediaItem item)
		{
			throw new NotImplementedException("Implement me");
			//return item?.FilePathWithDomain ?? "";
		}
	}
}
