﻿using AFLCore;
using AFLCore.Core;
using AFLCore.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.Logging;
using System;
using System.Xml.Linq;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Routing;
using Umbraco.Cms.Core.Web;
using Umbraco.Cms.Web.Common.Controllers;
using Umbraco.Cms.Web.Website.ActionResults;

namespace AFLCoreUmbraco
{
	public abstract class UmbracoDefaultControllerBase : 
		RenderController,
		ICMSDefaultController
	{
		private readonly UmbracoUtils umbracoUtils;
		private readonly IPublishedUrlProvider publishedUrlProvider;
		private readonly IUmbracoContextAccessor umbracoContextAccessor;
		private readonly SitemapService sitemapService;
		private readonly Registry registry;

		protected UmbracoDefaultControllerBase(UmbracoUtils umbracoUtils,
			IPublishedUrlProvider publishedUrlProvider,
			ILogger<RenderController> logger, 
			ICompositeViewEngine compositeViewEngine, 
			IUmbracoContextAccessor umbracoContextAccessor, 
			SitemapService sitemapService,
			Registry registry) : base(logger, compositeViewEngine, umbracoContextAccessor)
		{
			this.umbracoUtils = umbracoUtils;
			this.publishedUrlProvider = publishedUrlProvider;
			this.umbracoContextAccessor = umbracoContextAccessor;
			this.sitemapService = sitemapService;
			this.registry = registry;
		}

		[AcceptVerbs("POST", "GET")]
		public override IActionResult Index()
		{
			umbracoUtils.EnsureCultureStored();

			return new DefaultControllerImplementation(this, HttpContext, registry).IndexHelper(CurrentPage);
		}

		public IActionResult Rss(ContentModel model)
		{
			//throw new NotImplementedException();
			return new DefaultControllerImplementation(this, HttpContext, registry).RssHelper(model.Content);
		}

		public IActionResult RedirectToNode(string toNodeID, string queryString)
		{
			return new RedirectToUmbracoPageResult(Guid.Parse(toNodeID), new QueryString(queryString), publishedUrlProvider, umbracoContextAccessor);
		}

		public IActionResult RedirectToUrl(string url)
		{
			return new RedirectResult(url);
		}

		public IActionResult View(IAFLModel model)
		{
			return base.View(model.ViewTypeName, model);
		}

		public IActionResult Partial(string viewName, IAFLModel model)
		{
			return base.PartialView(viewName, model);
		}

		public IActionResult View(string viewName, IAFLModel model = null)
		{
			return base.View(viewName, model);
		}
		public XmlActionResult SiteMap()
		{
			XDocument doc = sitemapService.GetSitemateXMLDocument();
			return new XmlActionResult(doc);
		}

		[NonAction]
		public virtual IActionResult MakeActionResult(IAFLModel model) { return View(model); }
	}
}
