﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AFLCore;
using AFLCore.EventHandling;
using Umbraco.Cms.Core.Composing;
using Umbraco.Cms.Core.DependencyInjection;
using Umbraco.Cms.Core.Events;
using Umbraco.Cms.Core.Models;
using Umbraco.Cms.Core.Notifications;

namespace AFLCoreUmbraco.EventHandling
{
	public class AFLEventHandler : IComposer
	{

		public void Compose(IUmbracoBuilder builder)
		{
			AddNotificationHandler<ContentPublishedNotification>(builder);
			AddNotificationHandler<ContentPublishingNotification>(builder);
			AddNotificationHandler<ContentDeletedNotification>(builder);
			AddNotificationHandler<ContentDeletingNotification>(builder);
			AddNotificationHandler<ContentSavedNotification>(builder);
			AddNotificationHandler<ContentSavingNotification>(builder);
			AddNotificationHandler<ContentUnpublishedNotification>(builder);
			AddNotificationHandler<ContentUnpublishingNotification>(builder);

			//TODO: Handle moving slightly differntly
			//builder.AddNotificationHandler<ContentMovedNotification, ContentPublishing>();
			//builder.AddNotificationHandler<ContentMovingNotification, ContentPublishing>();
			//builder.AddNotificationHandler<ContentMovedToRecycleBinNotification, ContentPublishing>();
			//builder.AddNotificationHandler<ContentMovingToRecycleBinNotification, ContentPublishing>();
			//TODO: Add more once tested.
		}

		void AddNotificationHandler<TNoitification>(IUmbracoBuilder builder)
			where TNoitification : INotification
		{
			builder.AddNotificationHandler<TNoitification, HandleNotification<TNoitification>>();
		}
	}

	class HandleNotification<T> : INotificationHandler<T> where T : INotification
	{
		private readonly Registry registry;

		public HandleNotification(Registry registry)
		{
			this.registry = registry;
		}
		public void Handle(T notification)
		{
			switch (notification)
			{
				case ContentPublishedNotification published:
					EventDispatcher.OnChange(published.PublishedEntities, new CMSEventArgs<ContentPublishedNotification>(CMSEvent.Published, published), registry);
					break;
				case ContentPublishingNotification publishing:
					EventDispatcher.OnChange(publishing.PublishedEntities, new CMSEventArgs<ContentPublishingNotification>(CMSEvent.Publishing, publishing), registry);
					break;
				case ContentDeletedNotification deleted:
					EventDispatcher.OnChange(deleted.DeletedEntities, new CMSEventArgs<ContentDeletedNotification >(CMSEvent.Deleted, deleted), registry);
					break;
				case ContentDeletingNotification deleting:
					EventDispatcher.OnChange(deleting.DeletedEntities, new CMSEventArgs<ContentDeletingNotification >(CMSEvent.Deleting, deleting), registry);
					break;
				case ContentSavedNotification saved:
					EventDispatcher.OnChange(saved.SavedEntities, new CMSEventArgs<ContentSavedNotification >(CMSEvent.Saved, saved), registry);
					break;
				case ContentSavingNotification saving:
					EventDispatcher.OnChange(saving.SavedEntities, new CMSEventArgs<ContentSavingNotification >(CMSEvent.Saving, saving), registry);
					break;
				case ContentUnpublishedNotification unpublished:
					EventDispatcher.OnChange(unpublished.UnpublishedEntities, new CMSEventArgs<ContentUnpublishedNotification >(CMSEvent.Unpublished, unpublished), registry);
					break;
				case ContentUnpublishingNotification unpublishing:
					EventDispatcher.OnChange(unpublishing.UnpublishedEntities, new CMSEventArgs<ContentUnpublishingNotification >(CMSEvent.Unpublishing, unpublishing), registry);
					break;
				default:
					throw new NotImplementedException($"No AFL Support added for notification: {notification}");
			}
		}
	}
	class ContentPublishing : INotificationHandler<ContentPublishingNotification>
	{
		private readonly Registry registry;

		public ContentPublishing(Registry registry)
		{
			this.registry = registry;
		}
		public void Handle(ContentPublishingNotification notification)
		{
			EventDispatcher.OnChange(notification.PublishedEntities, new CMSEventArgs<ContentPublishingNotification>(CMSEvent.Publishing, notification), registry);
		}
	}

	class ContentPublished : INotificationHandler<ContentPublishedNotification>
	{
		private readonly Registry registry;

		public ContentPublished(Registry registry)
		{
			this.registry = registry;
		}
		public void Handle(ContentPublishedNotification notification)
		{
			EventDispatcher.OnChange(notification.PublishedEntities, new CMSEventArgs<ContentPublishedNotification>(CMSEvent.Published, notification), registry);
		}
	}

	class EventDispatcher 
	{
		public static void OnChange<T>(IEnumerable<IContent> contents, CMSEventArgs<T> arg, Registry registry)
			where T : class
		{
			if (contents == null) { return; }
			foreach (var content in contents.Where(c => c != null))
			{
				var model = registry.GetDBBackedModelFromNativeDBType(content);
				if (model != null)
				{
					var methods = model.GetType().GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
					foreach (var method in methods)
					{
						var attr = (CMSEventHandlerAttribute)method.GetCustomAttributes(typeof(CMSEventHandlerAttribute), false).FirstOrDefault();
						if (attr != null && attr.events.Contains(arg.EventType))
						{
							ParameterInfo[] parameters = method.GetParameters();
							if (parameters.Count() == 0)
							{
								method.Invoke(model, new object[] { });
							}
							else if (parameters.Count() == 1 && parameters.First().ParameterType == typeof(CMSEventArgs<T>))
							{
								method.Invoke(model, new object[] { arg });
							}
							else
							{
								throw new TargetParameterCountException("Event handler must have either 0 parameters, or one parameter of type CMSEventArgs. Content type: " + content.ContentType.Alias);
							}

							//if (arg.NativeEvent is CancellableEventArgs && arg.Canceled)
							//{
							//	var nativeArg = (CancellableEventArgs)arg.NativeEvent;
							//	nativeArg.CancelOperation(new EventMessage("Cancelled:", arg.CancelMessage, EventMessageType.Warning));
							//}
						}
					}
				}
			}
		}
	}
}
