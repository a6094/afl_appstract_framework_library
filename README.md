# AFL
[![pipeline status](https://gitlab.com/a6094/afl_appstract_framework_library/badges/master/pipeline.svg)](https://gitlab.com/a6094/afl_appstract_framework_library/-/commits/master)
[![Latest Release](https://gitlab.com/a6094/afl_appstract_framework_library/-/badges/release.svg)](https://gitlab.com/a6094/afl_appstract_framework_library/-/releases)

## Introduction

v10: https://www.nuget.org/packages/AFLCoreUmbraco/ 

## Older versions of Umbraco

- v9: https://www.nuget.org/packages/AFLCoreUmbraco/ 
	- Use alpha3.
- v8: https://www.nuget.org/packages/AFLUmbraco8/
- v7: https://www.nuget.org/packages/AFLUmbraco/

## Important Notice

As AFL progresses we will be making a radical change to the framework to be inline with the release philosophy of Umbraco. Sometime during this year we will merge AFLCore and AFLCoreUmbraco. 
When this happens, it will mark our new approach to primarily support the latest iteration of Umbraco. 
Previous major Umbraco (Legacy) versions will be kept, but will not have official support - nor receive updates.

## Getting Started

1. Provide a Default Controller Inheriting from UmbracoDefaultControllerbase.
2. In startup, while ConfiguringServices call the extension method ``services.InitializeAFL<TDefaultController>(IConfugration)`` where TDefaulcontroller is your implementation from step1.
3. In Configure call ``app.UseAFL(new[] { GetType().Assembly /*.. other assemblies */ })`` Its adviced to supply an assembly ending in .Core to store your businesslogic models for the models builder


## AFL Basics

Classes representing renderable nodes should be decorated with the ``[Renderable]`` Attribute. Every site should have atleast 1 renderable root.
TODO: Provide example



## Dependency injection.

Basic AFL models now support dependency injection in the constructor. The first argument to a given models constructor should always be ``IPublishedContent node``


### ModelBuilder notes

Currently the models builder will first look for a project ending in .Core and write the models there. If none is found it will take the first defined in step 3.

### Eventhandling notes

The base events are handled. We could add support for moving trashing and others in the future.

### Search

Custom indexes should in theory work, but only the default external index is currently tested.

To search decorate your models properties with the ``[Searchable]`` attribute. 

In your controller or your model you can now inject the ``Searcher<TResultType>`` class, which will return results of type ``TResultType``

## Improvement Proposals.

 - IOptions support.
	- After this JSON Schema could be nice, to improve discoverability
 - ModelBuilder, Define path of new models in AppSettings, before looking for a .Core Assembly, or simply taking the first assembly defined.
 - A Wrapper for Element types more logical than AFLModelBase - too many properties doesnt fit ElementTypes.



 ### Manually packing for nuspec
 ```dotnet pack -p:NuspecFile=AFLUmbracoWeb.nuspec```
