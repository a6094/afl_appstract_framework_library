﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using AFL;
using AFL.Core;
using AFLUmbraco8.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Composing;
using Umbraco.Core.Models;
using Umbraco.Core.Models.Blocks;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.PropertyEditors.ValueConverters;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace AFLUmbraco8
{
	class UmbracoMemoSupport : MemoSupportBase
	{
		const string umbDocumentPrefix = "umb://document/";
		const string umbMemberPrefix = "umb://member/";
		const string umbMediaPrefix = "umb://media/";

		public override object GetValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse, Injector injector)
		{
			if (injector != null)
			{
				return injector.GetInstance(elementType);
			}

			return GetPropertyValue<T>(model, propertyInfo, callerName, elementType, recurse);
		}

		//private object GetPropertyValueNested<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse)
		//{

		//}
		public string GetModelNameForAlias(string alias)
		{
			var typeName = Registry.Current.TryGetInfo(alias)?.ModelType.Name ?? alias;
			return char.ToUpperInvariant(typeName.First()) + typeName.Substring(1);
		}

		private object GetPropertyValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse)
		{
			var node = ((AFLModelBase)model).Content;
			var dbContentCulture = (node as DBContent)?.savingCulture;
			var type = typeof(T);
			object value;
			if (string.IsNullOrEmpty(propertyInfo.CMSName))
			{
				IContentTypeService service = Current.Services.ContentTypeService;
				var contentType = service.GetAll().Where(ct => ct.Alias == service.GetAllContentTypeAliases().Where(alias => alias == node.ContentType.Alias).First()).First();
				var descriptor = new MissingPropertyDescriptor(contentType.Alias, callerName, elementType, model.ToString());
				InMemoryMissingDocumentTypeProperties.GetInstance().RemoveAll(item => (item.TypeAlias == descriptor.TypeAlias) && (item.PropertyName == descriptor.PropertyName));
				InMemoryMissingDocumentTypeProperties.GetInstance().Add(descriptor);
				if (Registry.AFLDebugMode)
				{
					throw new InvalidOperationException(model.GetType().Name + "." + callerName + " has no corresponding CMS property. (If you just added it, remember to recycle the app pool).");
				}
				else
				{
					Serilog.Log.Logger.Error("{Name}.{CallerName} has no corresponding CMS property. (If you just added it, remember to recycle the app pool).", model.GetType().Name, callerName);
					return null;
				}
			}
			if (IsSimpleType<T>())
			{
				//TODO: Check if this new way is equivalent.
				//value = recurse ? node.Value<T>(propertyInfo.CMSName, fallback: Fallback.ToAncestors) : node.Value<T>(propertyInfo.CMSName, defaultValue : default(T));
				value = node.HasValue(propertyInfo.CMSName, culture: dbContentCulture, fallback: recurse ? Fallback.ToAncestors : default(Fallback)) ? 
					(object)node.Value<T>(propertyInfo.CMSName,culture: dbContentCulture, fallback: recurse ? Fallback.ToAncestors : default(Fallback)) : default(T);
				if (typeof(T) == typeof(string) && value == null)
				{
					value = string.Empty;	
				}
				//value = node.HasValue(propertyInfo.CMSName, recurse) ? (object)node.GetPropertyValue<T>(propertyInfo.CMSName, recurse) : default(T);
			}
			else if(typeof(Umbraco.Core.Models.Membership.IUser).IsAssignableFrom(type))
			{
				var userid = node.Value(propertyInfo.CMSName,culture: dbContentCulture, fallback: recurse ? Fallback.ToAncestors : default(Fallback));
				value = Current.Services.UserService.GetUserById(Convert.ToInt32(userid));
			}
			else if(typeof(Umbraco.Core.Models.Membership.IUser[]).IsAssignableFrom(type))
			{
				//what format?
				throw new NotImplementedException("TODO handle user list: " + type.FullName);
			}
			else if (typeof(IMember).IsAssignableFrom(type))
			{
				var memberid = node.Value<string>(propertyInfo.CMSName,culture: dbContentCulture, fallback: recurse ? Fallback.ToAncestors : default(Fallback));
				//var memberid = node.GetPropertyValue<string>(propertyInfo.CMSName, recurse);
				value = Current.Services.MemberService.GetByKey(UmbMemberGuidToGuid(memberid));
			}
			else if (typeof(IMember[]).IsAssignableFrom(type))
			{
				var strongValue = node.Value<IEnumerable<IPublishedContent>>(propertyInfo.CMSName, culture: dbContentCulture, fallback: recurse ? Fallback.ToAncestors : default(Fallback));
				if (strongValue == null)
				{
					var memberidList = node.Value<string>(propertyInfo.CMSName, culture: dbContentCulture, fallback: recurse ? Fallback.ToAncestors : default(Fallback));
					value = memberidList == null ? (new List<IMember>()).ToArray() : memberidList.Split(',').Select(umbguid => Current.Services.MemberService.GetByKey(UmbMemberGuidToGuid(umbguid))).Where(m => m != null).ToArray();
				}
				else
				{
					value = strongValue == null || strongValue.Count() == 0 ? (new List<IMember>()).ToArray() : strongValue.Select(mm => Current.Services.MemberService.GetByKey(mm.Key)).Where(m => m != null).ToArray();
				}
			}
			else if (typeof(UmbracoColor).IsAssignableFrom(type))
			{
				var strongValue = node.Value<ColorPickerValueConverter.PickedColor>(propertyInfo.CMSName, culture: dbContentCulture, fallback: recurse ? Fallback.ToAncestors : default(Fallback));
				if (strongValue == null || string.IsNullOrEmpty(strongValue.Color))
				{
					var str = node.Value<string>(propertyInfo.CMSName, culture: dbContentCulture, fallback: recurse ? Fallback.ToAncestors : default(Fallback));
					value = UmbracoColor.FromString(str);
				}
				else
				{
					value = new UmbracoColor { Label = strongValue.Label, Value = strongValue.Color };
				}
			}
			//else if (typeof(UmbracoGrid).IsAssignableFrom(type))
			//{
			//	//var str = node.GetPropertyValue<string>(propertyInfo.CMSName, recurse);
			//	//value = UmbracoGrid.Deserialize(str, model);
			//}
			else
			{
				object data;
				try
				{
					data = node.Value(propertyInfo.CMSName, culture: dbContentCulture, fallback: recurse ? Fallback.ToAncestors : default(Fallback));

				}
				catch (Exception)
				{
					//TODO: Find equivalent to this
					//data = node.GetProperty(propertyInfo.CMSName, recurse)?.DataValue;
					data = new object();
				}
				if (!GetComplexValueFromObject(data, type, out value, node))
				{
					throw new NotImplementedException("Unsupported return type: " + type.FullName);
				}
			}

			return value; 
		}

		public override bool GetComplexValueFromObject(object objectValue, Type resultType, out object value, object contextNode)
		{
			if (objectValue == null)
			{
				value = null;
				return true;
			}
			else if (resultType.IsArray)
			{
				var elementType = resultType.GetElementType();
				if (objectValue is int[] && typeof(AFLModelBase[]).IsAssignableFrom(resultType))
				{
					var list = ((int[])objectValue).Where(v => v != 0).Select(v => v.ToString()).ToArray();
					value = EnumerableToTypedArray(elementType, list.Select(id => GetModel(id, contextNode)).Where(n => n != null));
					return true;
				}
				else if (objectValue is string[] && typeof(AFLModelBase[]).IsAssignableFrom(resultType))
				{
					var list = ((string[])objectValue).Where(v => !string.IsNullOrEmpty(v)).ToArray();
					value = EnumerableToTypedArray(elementType, list.Select(id => GetModel(id, contextNode)).Where(n => n != null));
					return true;
				}
				else if (objectValue is int[] || objectValue is string[])
				{
					if (elementType == typeof(int))
					{
						value = ((object[])objectValue).Select(v => Convert.ToInt32(v)).ToArray();
						return true;
					}
					else if (elementType == typeof(string))
					{
						value = ((object[])objectValue).Select(v => v?.ToString() ?? "").ToArray();
						return true;
					}
				}
				else if ((objectValue is string || objectValue is JArray) && typeof(UmbracoJArrayEntity[]).IsAssignableFrom(resultType))
				{
					var jArray = objectValue is JArray ? (JArray)objectValue : Newtonsoft.Json.JsonConvert.DeserializeObject<JArray>((string)objectValue) ?? new JArray();
					value = this.EnumerableToTypedArray(elementType, jArray.Select(f => Activator.CreateInstance(elementType, f)));
					return true;
				}
				else if ((objectValue is string || objectValue.GetType().FullName == "nuPickers.Picker") && typeof(AFLModelBase[]).IsAssignableFrom(resultType))
				{
					value = EnumerableToTypedArray(elementType, ParseUmbracoNodeList(objectValue.ToString(), contextNode));
					return true;
				}
				else if (objectValue is IPublishedContent)
				{
					if (typeof(AFLModelBase).IsAssignableFrom(elementType))
					{
						value = new[] { Registry.Current.GetModel(objectValue) };
						return true;
					}
					else
					{
						throw new Exception("todo");
					}
				}
				else if (objectValue is IEnumerable<IPublishedContent>) //nested content (when used with normal AFLModelBase types, as opposed to NestedContentBase, and Multiple media picker in new umbraco versions
				{
					var nodes = ((IEnumerable<IPublishedContent>)objectValue);
					if (typeof(AFLModelBase).IsAssignableFrom(elementType))
					{
						value = EnumerableToTypedArray(elementType, nodes.Select(n => GetModel(n)).Where(v => v != null));
						return true;
					}
					else
					{
						throw new Exception("Unsupported Memo return type for NestedContent: " + resultType.FullName);
					}
				}
				else if (objectValue is BlockListModel blocklistWrapped && elementType.IsGenericType && typeof(AFLBlockContent<,>) == elementType.GetGenericTypeDefinition()) {
					value = EnumerableToTypedArray(elementType, blocklistWrapped
						.Select(x => ConstructAFLBlockContent(elementType, contextNode, x)));
					return true;
				}
				else if (objectValue is BlockListModel block)
				{
					value = EnumerableToTypedArray(elementType, block.Select(x => GetModelPublishedElementWrapped(x.Content, contextNode)));
					return true;
				}
				else if (objectValue is IEnumerable<IPublishedElement>) //nested content (when used with normal AFLModelBase types, as opposed to NestedContentBase, and Multiple media picker in new umbraco versions
				{
					var nodes = ((IEnumerable<IPublishedElement>)objectValue);
					if (typeof(AFLModelBase).IsAssignableFrom(elementType))
					{
						value = EnumerableToTypedArray(elementType, nodes.Select(n => GetModelPublishedElementWrapped(n, contextNode)).Where(v => v != null));
						return true;
					}
					else
					{
						throw new Exception("Unsupported Memo return type for NestedContent: " + resultType.FullName);
					}
				}
				else if (objectValue.GetType().Name == "Udi[]")
				{
					value = EnumerableToTypedArray(elementType, ParseUmbracoNodeList(string.Join(",", ((IEnumerable<object>)objectValue).Select(udi => udi.ToString())), contextNode));
					return true;
				}
				else if (objectValue is IEnumerable<Link> linkArray && elementType.IsGenericType && typeof(AFLLink<>) == elementType.GetGenericTypeDefinition())
				{
					//AFL link wraps umbraco LINK and provides a strongly typed model reference if its an internal link.
					value = EnumerableToTypedArray(elementType, linkArray.Select(x => ConstructAFLLink(x, elementType)));
					return true;
				}
				else if (objectValue is IEnumerable<object>)
				{
					//if there are any values in the result list, and the return type does not match, then we now have to re-deserialze the objectValue... hopefully we'll find a better solution in the future...
					//this was needed for the case of RelatedLinks on the Aktindsigtshaandbog project.
					var sampleValue = ((IEnumerable<object>)objectValue).FirstOrDefault();
					if (sampleValue != null && !(elementType.IsAssignableFrom(sampleValue.GetType())))
					{
						objectValue = SpecialHandlingForRelatedLinksAndSimilar(JsonConvert.SerializeObject(objectValue), resultType, elementType, contextNode);
					}
					//end silly hack :)

					value = EnumerableToTypedArray(elementType, (IEnumerable<object>)objectValue);
					return true;
				}
				else if(objectValue is string)
				{
					if(typeof(AFLModelBase).IsAssignableFrom(elementType))
					{
						value = EnumerableToTypedArray(elementType, ParseUmbracoNodeList((string)objectValue, contextNode));
					}
					else //e.g. related links - noramlly umbraco would deserialize these internally, but in at least one case it's been observed that a we end up with a string here, and in that case the below code handles it correctly...
					{
						value = EnumerableToTypedArray(elementType, SpecialHandlingForRelatedLinksAndSimilar((string)objectValue, resultType, elementType, contextNode));
					}
					return true;
				}
				
				throw new Exception("Type not supported: " + resultType?.FullName + ", value: " + objectValue?.GetType().FullName);
			}
			else if (objectValue is string)
			{
				value = ParseUmbracoNodeList((string)objectValue, contextNode).FirstOrDefault();
				return true;
			}
			else if (objectValue is int || objectValue.GetType().FullName == "nuPickers.Picker")
			{
				if (typeof(AFLModelBase).IsAssignableFrom(resultType))
				{
					value = IsEmptyValue(objectValue, true) ? null : GetModel(objectValue.ToString(), contextNode);
					return true;
				}
				else
				{
					throw new Exception("todo");
				}
			}
			else if (objectValue is IPublishedContent)
			{
				if (typeof(AFLModelBase).IsAssignableFrom(resultType))
				{
					value = Registry.Current.GetModel(objectValue);
					return true;
				}
				else
				{
					throw new Exception("todo");
				}
			}
			else if (objectValue is IEnumerable<IPublishedContent>)
			{
				objectValue = ((IEnumerable<IPublishedContent>)objectValue).FirstOrDefault();
				if(objectValue == null)
				{
					value = null;
					return true;
				}

				if (typeof(AFLModelBase).IsAssignableFrom(resultType))
				{
					value = Registry.Current.GetModel(objectValue);
					return true;
				}
				else
				{
					throw new Exception("todo");
				}
			}
			else if (objectValue.GetType().Name == "Udi")
			{
				value = UmbGuidToNode(objectValue.ToString(), contextNode);
				return true;
			}
			else if (objectValue.GetType().Name == "Udi[]")
			{
				value = UmbGuidToNode(((IEnumerable<object>)objectValue).FirstOrDefault()?.ToString(), contextNode);
				return true;
			}
			else if (objectValue is int[] && typeof(AFLModelBase).IsAssignableFrom(resultType))
			{
				var id = ((int[])objectValue).FirstOrDefault().ToString();
				value = GetModel(id, contextNode);
				return true;
			}
			else if (objectValue is string[] && typeof(AFLModelBase).IsAssignableFrom(resultType))
			{
				var id = ((string[])objectValue).FirstOrDefault();
				value = GetModel(id, contextNode);
				return true;
			}
			else if (objectValue.GetType().Name == "RelatedLinks") // special case of single result from array of related links
			{
				//value = ((IEnumerable<object>)objectValue).Select(x => FixRelatedLink(x, contextNode)).FirstOrDefault();
				value = new object();

				return true;
			}
			else if (objectValue is BlockListModel blocklistWrapped && resultType.IsGenericType && typeof(AFLBlockContent<,>) == resultType.GetGenericTypeDefinition())
			{
				value = blocklistWrapped
					.Select(x =>
						ConstructAFLBlockContent(resultType, contextNode, x))
					.FirstOrDefault();

				return true;
			}
			else if (objectValue is BlockListModel block)
			{
				value = block.Select(x => GetModelPublishedElementWrapped(x.Content, contextNode)).FirstOrDefault();
				return true;
			}
			else if (objectValue is IEnumerable<object>)
			{
				value = ((IEnumerable<object>)objectValue).FirstOrDefault();
				return true;
			}
            else if (objectValue is Link link && resultType.IsGenericType && typeof(AFLLink<>) == resultType.GetGenericTypeDefinition())
			{
				//AFL link wraps umbraco LINK and provides a strongly typed model reference if its an internal link.
				value = ConstructAFLLink(link, resultType);
				return true;
			}
            else if (objectValue is Link)
            {
                value = ((Link)objectValue);
                return true;
            }
			else if (objectValue is IPublishedElement element)
			{
				value = GetModelPublishedElementWrapped(element, contextNode as IPublishedContent);
				return true;
			}
			else
            {
				throw new Exception("todo");
			}
		}

		/// <summary>
		/// Activates an instace of AFL BlockContent
		/// </summary>
		/// <param name="resultType"></param>
		/// <param name="contextNode"></param>
		/// <param name="blockItem"></param>
		/// <returns></returns>
		private object ConstructAFLBlockContent(Type resultType, object contextNode, BlockListItem blockItem)
		{
			return Activator.CreateInstance(resultType, GetModelPublishedElementWrapped(blockItem.Content, contextNode), GetModelPublishedElementWrapped(blockItem.Settings, contextNode));
		}

		private object ConstructAFLLink(Link x, Type elementType)
		{
			return Activator.CreateInstance(elementType, x, x.Type == LinkType.Content
										? GetModel(UmbDocumentGuidToGuid(x.Udi.ToString()), x)
										: null);
		}

		private IEnumerable<object> SpecialHandlingForRelatedLinksAndSimilar(string objectValue, Type resultType, Type elementType, object contextNode)
		{
			if (!objectValue.Trim().StartsWith("[")) // might only happen for IContent and not IPublishedContent: case of Umbraco Multi Textstring
			{
				return objectValue.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
			}

			var objectList = (IEnumerable<object>)JsonConvert.DeserializeObject((string)objectValue, resultType) ?? new string[0];
			return objectList;
		}

		private dynamic FixRelatedLink(dynamic relatedLink, object contextNode)
		{
			var link = (string)relatedLink.Link?.ToString();
			if (link?.StartsWith(umbDocumentPrefix) ?? false) {
				relatedLink.Link = ((AFLModelBase)UmbGuidToNode(link, contextNode)).RelativeUrl;
			}
			else if (relatedLink.IsInternal && Regex.IsMatch(link ?? "", "^[0-9]+$"))
			{
				relatedLink.Link = ((AFLModelBase)GetModel(link, contextNode)).RelativeUrl;
			}
			return relatedLink;
		}

		//handles various kinds of string formats that resolve to node lists.
		private IEnumerable<IAFLModel> ParseUmbracoNodeList(string value, object contextNode)
		{
			if (value.StartsWith("[")) //relevant for IContent access to nested content...
			{
				var parsedList = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>[]>(value);
				var nodes = parsedList.Select(v => new DBDetachedContent(v));
				return nodes.Select(n => GetModel(n)).Where(v => v != null).ToArray();
			}
			else if (value.StartsWith(umbDocumentPrefix)) //relevant for the new umbraco ID replacement scheme...
			{
				return value.Split(',').Select(str => UmbGuidToNode(str, contextNode)).Where(n => n != null).ToArray();
			}
			else if (value.StartsWith(umbMediaPrefix)) //relevant for the new umbraco ID replacement scheme...
			{
				return value.Split(',').Select(str => UmbMediaGuidToNode(str, contextNode)).Where(n => n != null).ToArray();
			}
			else //relevant for normal comma seperated id list
			{
				var nodes = value.Trim().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
				return nodes.Select(n => GetModel(n, contextNode)).Where(v => v != null).ToArray();
			}
		}

		private Guid ParseGuidStringOrThrow(string strGuid)
		{
			Guid guid;
			var ok = Guid.TryParseExact(strGuid, "N", out guid);
			if (!ok)
			{
				throw new Exception("invalid guid: " + strGuid);
			}
			return guid;
		}

		private Guid UmbDocumentGuidToGuid(string umbGuid)
		{
			if (umbGuid == null) { return Guid.Empty; }
			var strGuid = umbGuid.Replace(umbDocumentPrefix, "");
			return ParseGuidStringOrThrow(strGuid);
		}

		private Guid UmbMediaGuidToGuid(string umbGuid)
		{
			if (umbGuid == null) { return Guid.Empty; }
			var strGuid = umbGuid.Replace(umbMediaPrefix, "");
			return ParseGuidStringOrThrow(strGuid);
		}

		private Guid UmbMemberGuidToGuid(string umbGuid)
		{
			if (umbGuid == null) { return Guid.Empty; }
			var strGuid = umbGuid.Replace(umbMemberPrefix, "");
			return ParseGuidStringOrThrow(strGuid);
		}

		private IAFLModel UmbGuidToNode(string umbGuid, object contextNode)
		{
			var guid = UmbDocumentGuidToGuid(umbGuid);
			if (guid == Guid.Empty)
			{
				return null;
			}
			return GetModel(guid, contextNode);
		}

		private IAFLModel UmbMediaGuidToNode(string umbGuid, object contextNode)
		{
			var guid = UmbMediaGuidToGuid(umbGuid);
			if (guid == Guid.Empty)
			{
				return null;
			}
			return GetModel(guid, contextNode, isMedia: true);
		}

		public override IAFLModel GetModel(Guid guid, object contextNode, bool isMedia = false)
		{
			IPublishedContent content;
			if (!isMedia && (contextNode is DBContent || contextNode is DBDetachedContent))
			{
                var icontent = Current.Services.ContentService.GetById(guid);
                if (icontent == null) { return null; }
                content = new DBContent(icontent);
            }
			else
			{
                content = isMedia ? UmbracoUtils.UmbracoHelper.Media(guid) : UmbracoUtils.UmbracoHelper.Content(guid);
            }
			return GetModel(content);
		}

		public override IAFLModel GetModel(string id, object contextNode)
		{
			return (contextNode is DBContent || contextNode is DBDetachedContent) ? Registry.Current.GetDBBackedModel(id) : Registry.Current.GetModel(id);
		}

		public override IAFLModel GetModel(object content)
		{
			if (content == null) { return null; }
			return (content is DBContent || content is DBDetachedContent) ? Registry.Current.GetDBBackedModelFromWrappedNativeDBType(content) : Registry.Current.GetModel(content);
		}
		public IAFLModel GetModelPublishedElementWrapped(IPublishedElement content, object contextNode)
		{
			if (content is null)
			{
				return null;
			}
			return Registry.Current.GetModel(new NestedContentWrapped(content, contextNode as IPublishedContent));
		}
	}
}