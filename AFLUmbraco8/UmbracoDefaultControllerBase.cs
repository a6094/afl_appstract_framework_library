﻿using AFL;
using AFL.Core;
using AFL.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.Serialization;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.Routing;

namespace AFLUmbraco8
{
    public abstract class UmbracoDefaultControllerBase :  RenderMvcController, ICMSDefaultController
	{
		//public ModelStateDictionary ModelState => throw new NotImplementedException();

		[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
		public override ActionResult Index(ContentModel model)
		{
			UmbracoUtils.EnsureCultureStored();

			return new DefaultControllerImplementation(this).IndexHelper(model.Content);
		}

		[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
		public ActionResult HandleWSCall()
		{
			if (Registry.Current.RestBasePath == null) { return null; }
			UmbracoUtils.FixCulture();
			//throw new NotImplementedException();
			return new DefaultControllerImplementation(this).WebserviceHelper();
		}

		public ActionResult Rss(ContentModel model)
		{
			//throw new NotImplementedException();
			return new DefaultControllerImplementation(this).RssHelper(model.Content);
		}

		public ActionResult RedirectToNode(string toNodeID, string queryString)
		{
			return new RedirectToUmbracoPageResult(Convert.ToInt32(toNodeID), queryString);
		}

		public ActionResult RedirectToUrl(string url)
		{
			return new RedirectResult(url);
		}

		public ActionResult View(IAFLModel model)
		{
			return base.View(model);
		}

		public ActionResult Partial(string viewName, IAFLModel model)
		{
			throw new NotImplementedException();
			//return base.PartialView(viewName, model);
		}

		public ActionResult View(string viewName, IAFLModel model = null)
		{
			return base.View(viewName, model);
		}
        public XmlActionResult SiteMap()
        {
            var doc = new XDocument();
            var serializer = new XmlSerializer(typeof(XElement));

            XNamespace xhtmlNamespace = "http://www.w3.org/TR/xhtml11/xhtml11_schema.html";
            XNamespace sitemapNamespace = "http://www.sitemaps.org/schemas/sitemap/0.9";
            var map = new XElement(sitemapNamespace + "urlset",
                new XAttribute("xmlns", sitemapNamespace.NamespaceName),
                new XAttribute(XNamespace.Xmlns + "xhtml", xhtmlNamespace.NamespaceName));

            //TODO: Should this have its own virtual method, in case we have a project where the customer intendts to add other Xelements to the sitemap ?
            var nodes = GetSitemapNodes();
            foreach (var item in nodes)
            {
                var elem = CreateSingleNode(item.Url, item.LastModified, sitemapNamespace);
                if (item.CultureVariations != null)
                {
                    foreach (var variation in item.CultureVariations)
                    {
                        elem.Add(CreateCultureSpecificAlt(variation.Url, variation.Culture, xhtmlNamespace));
                    }
                }
                map.Add(elem);
            }

            using (var writer = doc.CreateWriter())
            {
                serializer.Serialize(writer, map);
            }
            return new XmlActionResult(doc);
        }

        XElement CreateSingleNode(string url, DateTime lastModified, XNamespace xNamespace)
        {
			if (lastModified.Kind == DateTimeKind.Utc)
			{
                // convert to local time, the timezone will be stored in the zzz of the tostring
                // Without this we get a DateimTimeInvalidLocalFormat exception
                lastModified = lastModified.ToLocalTime(); 
			}
            return new XElement(xNamespace + "url",
                new XElement(xNamespace + "loc", url),
                new XElement(xNamespace + "lastmod", lastModified.ToString("yyyy-MM-ddTHH:mm:sszzz")));
        }

        XElement CreateCultureSpecificAlt(string url, string culture, XNamespace xNamespace)
        {
            return new XElement(xNamespace + "link",
                new XAttribute("rel", "alternate"),
                new XAttribute("hreflang", culture),
                new XAttribute("href", url)
                );
        }

        /// <summary>
        /// Populates a list of renderable non-protected nodes in umbraco. 
        /// it will take the latest UpdateDate from its non renderable children.
        /// if any cultural variations exist it will populate the xhtml:Link element with these.
        /// Override this to add your custom nodes to the sitemap.
        /// </summary>
        /// <returns></returns>
        public virtual List<ContentNode> GetSitemapNodes()
        {
            //Set the umbraco context so we can get absolute URLs
            var accessor = Current.Factory.GetInstance(typeof(IUmbracoContextAccessor));
            var umbracoContext = ((IUmbracoContextAccessor)accessor).UmbracoContext;


            var content = UmbracoUtils.UmbracoHelper
				.ContentAtRoot()
				.SelectMany(c => c.Descendants().Concat(c))
				.OrderBy(c => c.Path)
				//Only add rendereable and nonprotected pages
				.Where(c => Registry.Current.GetInfo(c).Renderable && !Current.Services.PublicAccessService.IsProtected(c.Path));

            List<ContentNode> nodes = new List<ContentNode>();
            foreach (var node in content)
            {
                var lastModifiedDate = node.UpdateDate;
                //Get last modified in page content
                GetLastModifiedDate(node, ref lastModifiedDate);
                var toAdd = new ContentNode(node.Url(mode: UrlMode.Absolute), lastModifiedDate);

                var CultureSpecificUrls = node.Cultures
                    .Select(x => new {
						Url = node.Url(node.Cultures[x.Key].Culture, UrlMode.Absolute), 
                        Culture = node.Cultures[x.Key].Culture})
                    .ToArray();

                toAdd.CultureVariations = node.ContentType.Variations.VariesByCulture() ? CultureSpecificUrls.Select(x => new CultureSpecificURL(x.Url, x.Culture)).ToArray() : null;
                
                nodes.Add(toAdd);
            }
            return nodes;

        }
        void GetLastModifiedDate(IPublishedContent content, ref DateTime date)
        {
            var nonRenderableChildren = content.Children.Where(x => !Registry.Current.GetInfo(x).Renderable);
            foreach (var c in nonRenderableChildren)
            {
                var updateDateUTC = c.UpdateDate;
                if (updateDateUTC > date)
                {
                    date = updateDateUTC;
                }
                GetLastModifiedDate(c, ref date);
            }
        }
        public class ContentNode
        {
            public string Url { get; set; }
            public DateTime LastModified { get; set; }

            public ContentNode(string url, DateTime lastmodified)
            {
                Url = url;
                LastModified = lastmodified;
            }
            public CultureSpecificURL[] CultureVariations { get; set; }
        }
        /// <summary>
        /// Used in ContentNode for alternative URLS
        /// </summary>
        public class CultureSpecificURL
        {
            public string Url { get; set; }
            public string Culture { get; set; }

            public CultureSpecificURL(string url, string culture)
            {
                Url = url;
                Culture = culture;
            }
        }

		[NonAction]
		public virtual ActionResult MakeActionResult(IAFLModel model) { return View(model); }
	}

	/// <summary>
	/// todo move to own file :)
	/// </summary>
	public class RestWithUmbracoRoutingContentFinder : IContentFinder
	{
		public bool TryFindContent(PublishedRequest request)
		{
			//Set the umbraco context so we can get absolute URLs
			var accessor = Current.Factory.GetInstance(typeof(IUmbracoContextAccessor));
			var umbracoContext = ((IUmbracoContextAccessor)accessor).UmbracoContext;

			if (request != null && umbracoContext.Content.GetByRoute(request.Uri.AbsolutePath) == null)
			{
				var lastPart = request.Uri.Segments.LastOrDefault();
				if (!string.IsNullOrEmpty(lastPart))
				{
					var parentPath = string.Join("/", request.Uri.Segments.Take(request.Uri.Segments.Length - 1));
					var content = umbracoContext.Content.GetByRoute(parentPath);

					if (content != null)
					{
						var info = Registry.Current.GetInfo(content);
						var handler = info.GetRestHandlerByUrlName(lastPart);
						if (handler != null && handler.Attribute.CMSRoutable)
						{
							request.PublishedContent = content;
							HttpContext.Current.Items["afl-rest-route-handler"] = handler;
							return true;
						}
					}
				}
			}

			return false;
		}
	}
}
