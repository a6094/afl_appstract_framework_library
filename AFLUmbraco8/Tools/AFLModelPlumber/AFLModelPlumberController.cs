﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AFL;
using Umbraco.Core.Composing;
using Umbraco.Core.Models;
using Umbraco.Web.Editors;

namespace AFLUmbraco8.Tools
{
    public class AFLModelPlumberController : UmbracoAuthorizedJsonController
	{
        List<IContentType> umbracoContentTypes;
        AFLModelPlumber plumber = new AFLModelPlumber();

        public AFLModelPlumberController()
        {
            var cts = Current.Services.ContentTypeService;
            umbracoContentTypes = cts.GetAll().ToList();//GetAllContentTypes().ToList();
        }

		[System.Web.Http.HttpGet]
		public IEnumerable<AFLModelDescriptor> GetModels()
        {
			var data = umbracoContentTypes.Select(contentType =>
			{
				var info = Registry.Current.TryGetInfo(contentType.Alias);
				var cecilType = info != null ? plumber.GetCecilType(info.ModelType) : null;
				var wasDefined = cecilType != null ? Registry.Current.TryGetInfo(contentType.Alias) != null : false;
				return new AFLModelDescriptor(contentType.Name, contentType.Alias, wasDefined, wasDefined ? !string.IsNullOrWhiteSpace(plumber.WriteModelClassDiff(contentType, info, cecilType, false)) : false);
			});
            return data;
        }

		[System.Web.Http.HttpGet]
		public string GetClassDiffCode(string alias, bool withComments = false)
        {
            IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);
            var info = Registry.Current.TryGetInfo(alias);
            var cecilType = plumber.GetCecilType(info.ModelType);
            var text = plumber.WriteModelClassDiff(contentType, info, cecilType, withComments);
            return text;
        }

		[System.Web.Http.HttpGet]
		public string GetClassCode(string alias, bool withComments = false)
        {
            IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);
            var text = plumber.WriteModelClassCode(contentType, withComments);
            return text;
        }

		[System.Web.Http.HttpGet]
		public bool WriteClassToFile(string alias, bool withComments = false)
        {
            plumber.SaveClassToFile(alias, withComments);
			return true;
        }

		[System.Web.Http.HttpGet]
		public bool WriteDiffToFile(string alias, bool withComments = false)
        {

            plumber.SaveDiffToFile(alias, withComments);
			return true;
        }
    }

	public class AFLModelDescriptor
	{
		public string name { get; }
		public string alias { get; }
		public bool defined { get; }
		public bool diff { get; }

		public AFLModelDescriptor(string name, string alias, bool defined, bool diff)
		{
			this.name = name;
			this.alias = alias;
			this.defined = defined;
			this.diff = diff;
		}
	}
}
