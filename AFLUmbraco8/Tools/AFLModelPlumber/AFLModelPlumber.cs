﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using AFL;
using AFL.Core;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Pdb;
using Newtonsoft.Json;
using Umbraco.Core.Composing;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace AFLUmbraco8.Tools
{
    public class AFLModelPlumber
    {
        Dictionary<string, AssemblyDefinition> cecilAssemblies = new Dictionary<string, AssemblyDefinition>();
        Dictionary<int, IContentType> contentTypeMap;
        List<IContentType> umbracoContentTypes;
		IContentTypeService cts;

        public AFLModelPlumber()
        {
            cts = Current.Services.ContentTypeService;
            umbracoContentTypes = cts.GetAll().ToList();//GetAllContentTypes().ToList();
            contentTypeMap = umbracoContentTypes.ToDictionary(t => t.Id);
        }


        public string GetSourcePath(TypeDefinition typeDefinition)
        {
            foreach (var prop in typeDefinition.Properties.Where(x => x.GetMethod?.HasBody ?? false))
            {
                foreach (var instruction in prop.GetMethod.Body.Instructions)
                {
                    if (instruction.SequencePoint != null)
                    {
                        return instruction.SequencePoint.Document.Url;
                    }
                }
            }
            foreach (var method in typeDefinition.Methods.Where(x => x.HasBody))
            {
                foreach (var instruction in method.Body.Instructions)
                {
                    if (instruction.SequencePoint != null)
                    {
                        return instruction.SequencePoint.Document.Url;
                    }
                }
            }
            return null;
        }

        public void SaveDiffToFile(string alias, bool withComments)
        {
            IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);

			var isComposition = IsComposition(contentType);
            var info = Registry.Current.TryGetInfo(alias);
            var cecilType = GetCecilType(info.ModelType);
            var diff = WriteModelClassDiff(contentType, info, cecilType, withComments);

            List<PropertyType> missingProps;
            List<PropertyDefinition> orphanedCecilProps;
            CalcModelClassDiff(contentType, info, cecilType, out missingProps, out orphanedCecilProps, out var missingInterfaces);

            var filePath = GetSourcePath(cecilType);
            var editor = new SimpleSourceCodeEditor(filePath);
			foreach (var item in missingInterfaces)
			{
				string class_definition_pattern = @"\b(public|private|internal|protected)\s*" + @"\b(class)?\s*[a-zA-Z]*(?<class>\s[a-zA-Z]+\s*)" + @": [a-zA-Z\<\>,\s]*\s*[a-zA-Z]*\s*[,]*?\s*]*";
				editor.AppendToLine(class_definition_pattern, $", {item}\n");
			}
            foreach (var prop in missingProps)
            {
                var regex = "^\\s*(public|protected) .+ [A-Za-z0-9_]+ \\=\\> Memo(\\<.+\\>)?\\(.*\\);";
                editor.InsertBefore(regex, editor.GetIndentAtLine(regex) + GetCMSPropertyMemo(prop, false, isComposition) + "\n");
            }
            foreach (var codeProp in orphanedCecilProps)
            {
                editor.Replace($"^\\s*(public|protected) .+ {codeProp.Name} \\=\\> Memo(\\<.+\\>)?\\(.*\\);\\s*$", "");
            }
            editor.Save();
        }

		private bool IsComposition(IContentType contentType)
		{
			//Compositions are parents and compositions themselves.
			//therefore if we are composition and not regular inheritance
			//we can find all nodes where the type is parent, and the children of current node. And if these two lists are not identical, we are composition.
			var nodesWhereContentTypeParent = umbracoContentTypes.Where(x => x.ContentTypeComposition.Contains(contentType));
			var children = cts.GetChildren(contentType.Id);

			return !nodesWhereContentTypeParent.All(x => children.Contains(x));
		}

        public void SaveClassToFile(string alias, bool withComments)
        {
            IContentType contentType = umbracoContentTypes.First(ct => ct.Alias == alias);
            var path = Path.GetFullPath(HttpRuntime.AppDomainAppPath + $"../{Registry.Current.BusinessLogicAssembly.GetName().Name}/{GetModelNameForAlias(contentType.Alias)}.cs");
            System.IO.File.WriteAllText(path, WriteModelClassCode(contentType, withComments));

            var projFilePath = Path.GetFullPath(HttpRuntime.AppDomainAppPath + $"../{Registry.Current.BusinessLogicAssembly.GetName().Name}/{Registry.Current.BusinessLogicAssembly.GetName().Name}.csproj");
            var editor = new SimpleSourceCodeEditor(projFilePath);
            editor.InsertBefore("\\<Compile Include\\=\\\"[A-Za-z0-9_]+\\.cs\" \\/\\>", $"<Compile Include=\"{GetModelNameForAlias(contentType.Alias)}.cs\" />\n");
            editor.Save();

        }

        public string WriteModelClassCode(IContentType contentType, bool withComments)
        {
            var builder = new CodeBuilder();

            builder.Append("using AFL;");
            builder.Append("using AFLUmbraco8.Models;");
            builder.Append("using Umbraco.Core.Models.PublishedContent;");
            builder.Append("");

            string baseType = "AFLModelBase";
            if (contentType.ParentId > 0 && contentTypeMap.ContainsKey(contentType.ParentId))
            {
                baseType = GetModelNameForAlias(contentTypeMap[contentType.ParentId].Alias);
            }
			var compositions = contentType.ContentTypeComposition.Where(x => !x.Alias.Equals(baseType, StringComparison.OrdinalIgnoreCase));
			if (compositions.Any())
			{
				baseType = baseType + ", " + string.Join(", ", compositions.Select(x => $"I{GetModelNameForAlias(x.Alias)}"));
			}
			var isComposition = IsComposition(contentType);

            //TODO: only works in umbraco 7.6 and up...
            var namespaceList = cts.GetContainers(contentType) ?? new EntityContainer[] { };
            //GetContentTypeContainers(contentType) ?? new EntityContainer[] { };
            //string namespaces = Registry.Current.BusinessLogicAssembly.GetName().Name + "." + string.Join(".", namespaceList.Select(ns => ns.Name));
            string namespaces = Registry.Current.BusinessLogicAssembly.GetName().Name;

            builder.Append($"namespace {namespaces}");
            builder.Append("{", 1);
            var comments = builder.AppendBuilder();
            builder.Append("");

			if (isComposition)
			{
				builder.Append("[Composition]");
			}
            builder.Append($"public {(isComposition ? "interface" : "class")} {GetModelNameForAlias(contentType.Alias)} {(!isComposition ? $": {baseType}" : "")}");
            builder.Append("{", 1);
			if (!isComposition)
			{
				builder.Append($"public {GetModelNameForAlias(contentType.Alias)}(IPublishedContent node) : base(node)");
				builder.Append("{", 1);
				builder.Append("}", -1);
			}
            builder.Append("");

            foreach (var prop in contentType.PropertyTypes.ToList())
            {
                builder.Append(GetCMSPropertyMemo(prop, withComments, isComposition));
            }
			foreach (var comp in compositions)
			{
				foreach (var prop in comp.PropertyTypes)
				{
					builder.Append(GetCMSPropertyMemo(prop, withComments, isComposition));
				}
			}
            if (contentType.AllowedContentTypes.Any())
            {
                builder.Append(GetChildrenMemo(contentType, withComments));
            }

            builder.Append("}", -1);
            builder.Append("}", -1);
            builder.Append("");

            return builder.ToString();
        }

        public string WriteModelClassDiff(IContentType contentType, AFLTypeInfo info, TypeDefinition cecilType, bool withComments)
        {
            List<PropertyType> missingProps;
            List<PropertyDefinition> orphanedCecilProps;
            CalcModelClassDiff(contentType, info, cecilType, out missingProps, out orphanedCecilProps, out var missingInterfaces);
			var isComposition = IsComposition(contentType);
            var builder = new CodeBuilder();
			if (missingInterfaces.Any())
			{
				builder.Append("//Missing Interfaces:");
				foreach (var item in missingInterfaces)
				{
					builder.Append($"Missing Interface: {item}");
				}
			}
            if (missingProps.Any())
            {
                builder.Append($"//Missing properties:");
                foreach (var prop in missingProps)
                {
                    builder.Append(GetCMSPropertyMemo(prop, withComments, isComposition));
                }
            }
            if (orphanedCecilProps.Any())
            {
                builder.Append($"//Orphaned properties:");
                foreach (var codeProp in orphanedCecilProps)
                {
                    builder.Append($"No such CMS property: {codeProp.Name}");
                }
            }

            return builder.ToString();
        }

        public TypeDefinition GetCecilType(Type type)
        {
            return GetCecilAssembly(type).MainModule.Types.FirstOrDefault(tt => tt.Name == type.Name);
        }

        public string GetModelNameForAlias(string alias)
        {
            var typeName = Registry.Current.TryGetInfo(alias)?.ModelType.Name ?? alias;
            return char.ToUpperInvariant(typeName.First()) + typeName.Substring(1);
        }

        private void CalcModelClassDiff(IContentType contentType, AFLTypeInfo info, TypeDefinition cecilType, out List<PropertyType> missingProps, out List<PropertyDefinition> orphanedCecilProps, out List<string> missingInterfaces)
		{
			var propMap = info.GetRegisteredCMSProperties();

			missingProps = contentType.PropertyTypes.ToList();
			var compositions = contentType.ContentTypeComposition.Where(x => !cts.GetChildren(x.Id).Contains(contentType));
			var compositionPropertyTypes = compositions.SelectMany(x => x.PropertyTypes);
			missingProps.AddRange(compositionPropertyTypes);
			missingProps = missingProps.Where(p => !propMap.ContainsKey(p.Alias)).ToList();

			var compsNotAsInterface =  compositions.Select(x => x.Alias).Where(c => !cecilType.Interfaces.Select(x => x.Name).DefaultIfEmpty().Any(i => c.Equals(i, StringComparison.OrdinalIgnoreCase)));
			missingInterfaces = compsNotAsInterface.Select(x => x.First().ToString().ToUpper() + x.Substring(1)).ToList();
			//cecilType.Interfaces.Where(x => !compositions.Select(c => c.Name).Any(c => x.Name.Equals(c, StringComparison.OrdinalIgnoreCase)));

			orphanedCecilProps = propMap.Keys
				.Where(n => !contentType.PropertyTypes.Any(p => p.Alias == n) && !compositionPropertyTypes.Any(p => p.Alias == n))
				.Select(n => cecilType.Properties.FirstOrDefault(tp => tp.Name == propMap[n].Name))
				.Where(cp => cp != null && IsMemoProperty(cp))
				.ToList();
		}

		AssemblyDefinition GetCecilAssembly(Type type)
		{
			var name = type.Assembly.FullName;
			if (!cecilAssemblies.ContainsKey(name))
			{
				var readerParameters = new ReaderParameters { ReadSymbols = Registry.AFLDebugMode, SymbolReaderProvider = new PdbReaderProvider() };
				cecilAssemblies[name] = AssemblyDefinition.ReadAssembly(new Uri(type.Assembly.CodeBase).LocalPath, readerParameters);
			}
			return cecilAssemblies[name];
		}

        private string GetCodePropertyName(PropertyType prop)
		{
			return new string(prop.Alias.Select((c, idx) => idx == 0 ? char.ToUpper(c) : c).ToArray());
		}

        private string GetClosestCommonBase(string model1, string model2)
		{
			return "";
		}

        private string GetCommonBaseTypeName(IEnumerable<string> aliases)
		{
			string closest = aliases.FirstOrDefault() ?? "AFLModelBase";

			foreach (var alias in aliases)
			{
				if (alias != closest)
				{
					//todo
				}
			}

			return GetModelNameForAlias(closest);
		}

        private string GetChildrenMemo(IContentType contentType, bool printComments)
		{
			var aliases = contentType.AllowedContentTypes.Select(d => d.Alias);
			var type = GetCommonBaseTypeName(aliases);
			return $"public {type} Children => Memo<{type}>();{(printComments ? "// " + JsonConvert.SerializeObject(aliases) : "")}";
		}

        private string GetCMSPropertyMemo(PropertyType prop, bool printComments, bool isComposition)
		{
			//TODO: Fix this for Umbraco 8
			var dts = Current.Services.DataTypeService;
			var def = dts.GetDataType(prop.Id);
			//var pv = dts.GetPreValuesCollectionByDataTypeId(def.Id).FormatAsDictionary();

			var comment = printComments ? $" // {prop.PropertyEditorAlias}/{def.Name}, {def.DatabaseType.ToString()}, " : "";//pv: {JsonConvert.SerializeObject(pv)}".Replace("\n", " ") : "";
			string retType;

			switch (prop.PropertyEditorAlias)
			{
				case "Numeric": retType = "int"; break;
				case "Umbraco.MediaPicker": retType = GetModelNameForAlias("File"); break;
				case "Umbraco.MultipleMediaPicker": retType = GetModelNameForAlias("File") + "[]"; break;
				case "Umbraco.TrueFalse": retType = "bool"; break;
				case "Umbraco.Slider": retType = "int"; break;
				case "Umbraco.MultiNodeTreePicker": retType = "AFLModelBase[]"; break;
				//case "Umbraco.MultiNodeTreePicker": retType = (pv.ContainsKey("filter") && !string.IsNullOrEmpty(pv["filter"].Value) ? GetModelNameForAlias(pv["filter"].Value) : "AFLModelBase") + "[]"; break;
				//case "Umbraco.ContentPickerAlias": retType = (pv.ContainsKey("filter") && !string.IsNullOrEmpty(pv["filter"].Value) ? GetModelNameForAlias(pv["filter"].Value) : "AFLModelBase"); break;
				case "Umbraco.ContentPickerAlias": retType = "AFLModelBase"; break;
				case "Umbraco.DropDown": retType = "string"; break;
				case "Umbraco.RelatedLinks": retType = "RelatedLink[]"; break;
				case "Our.Umbraco.NestedContent": retType = "AFLModelBase[]"; break;
				default: retType = null; break;
			}

			if (retType == null)
			{
				//check the underlaying type...
				switch (def?.DatabaseType)
				{
					case ValueStorageType.Ntext: retType = "string"; break;
					case ValueStorageType.Nvarchar: retType = "string"; break;
					case ValueStorageType.Integer: retType = "int"; break;
					case ValueStorageType.Date: retType = "DateTime"; break;
					case ValueStorageType.Decimal: retType = "decimal"; break;
					default: retType = "string"; break;
				}
			}

			var templateTag = retType == "string" ? "" : "<" + retType + ">";
			var rightSide = isComposition 
				? "{ get; }" 
				: $"=> Memo{templateTag}(); {comment}";

			return $"{(isComposition ? "" : "public")} {retType} {GetCodePropertyName(prop)} {rightSide}";
		}

        private bool IsMemoProperty(PropertyDefinition prop)
		{
			int stackHeight = 0;
			if (prop.GetMethod == null) { return false; }
			var instructions = prop.GetMethod.Body.Instructions;
			for (int i = 0; i < instructions.Count; ++i)
			{
				var ins = instructions[i];
				int newStackHeight = stackHeight + GetStackDelta(ins, stackHeight);

				if (stackHeight > 0 && newStackHeight == 0)
				{
					var call = i == 0 ? null : instructions[i - 1].Operand as MethodReference;
					if (call == null || call.Name != "Memo")
					{
						return false; //not memo!
					}

					var isBodyPropertyEnd = i == instructions.Count - 4 && ins.OpCode == OpCodes.Stloc_0 && instructions[i + 1].OpCode == OpCodes.Br_S && instructions[i + 2].OpCode == OpCodes.Ldloc_0 && instructions[i + 3].OpCode == OpCodes.Ret;
					var isInlinePropertyEnd = i == instructions.Count - 1 || ins.OpCode == OpCodes.Ret;
					if (!isBodyPropertyEnd && !isInlinePropertyEnd)
					{
						throw new InvalidOperationException("More than one statement!");
					}

					//yay memo call. check type of call (hacky)
					return call.Parameters.Count == 3 && call.Parameters[1].ParameterType.IsPrimitive;
				}

				stackHeight = newStackHeight;
			}
			return false;
		}

		static int GetPushDelta(Instruction instruction)
		{
			OpCode code = instruction.OpCode;
			switch (code.StackBehaviourPush)
			{
				case StackBehaviour.Push0:
					return 0;

				case StackBehaviour.Push1:
				case StackBehaviour.Pushi:
				case StackBehaviour.Pushi8:
				case StackBehaviour.Pushr4:
				case StackBehaviour.Pushr8:
				case StackBehaviour.Pushref:
					return 1;

				case StackBehaviour.Push1_push1:
					return 2;

				case StackBehaviour.Varpush:
					if (code.FlowControl == FlowControl.Call)
					{
						MethodReference method = (MethodReference)instruction.Operand;
						return method.ReturnType.MetadataType == MetadataType.Void ? 0 : 1;
					}

					break;
			}
			throw new ArgumentException("unknown instruction " + instruction.ToString());
		}

		int GetStackDelta(Instruction instruction, int oldStackHeight)
		{
			return GetPushDelta(instruction) - GetPopDelta(oldStackHeight, instruction);
		}

		int GetPopDelta(int stackHeight, Instruction instruction)
		{
			OpCode code = instruction.OpCode;
			switch (code.StackBehaviourPop)
			{
				case StackBehaviour.Pop0:
					return 0;
				case StackBehaviour.Popi:
				case StackBehaviour.Popref:
				case StackBehaviour.Pop1:
					return 1;

				case StackBehaviour.Pop1_pop1:
				case StackBehaviour.Popi_pop1:
				case StackBehaviour.Popi_popi:
				case StackBehaviour.Popi_popi8:
				case StackBehaviour.Popi_popr4:
				case StackBehaviour.Popi_popr8:
				case StackBehaviour.Popref_pop1:
				case StackBehaviour.Popref_popi:
					return 2;

				case StackBehaviour.Popi_popi_popi:
				case StackBehaviour.Popref_popi_popi:
				case StackBehaviour.Popref_popi_popi8:
				case StackBehaviour.Popref_popi_popr4:
				case StackBehaviour.Popref_popi_popr8:
				case StackBehaviour.Popref_popi_popref:
					return 3;

				case StackBehaviour.PopAll:
					return stackHeight;

				case StackBehaviour.Varpop:
					if (code.FlowControl == FlowControl.Call)
					{
						MethodReference method = (MethodReference)instruction.Operand;
						int count = method.Parameters.Count;
						if (method.HasThis && OpCodes.Newobj.Value != code.Value)
							++count;
						return count;
					}

					if (code.Value == OpCodes.Ret.Value)
						return 1; //assume non-void, since we are only interested in property getters.
					break;
			}
			throw new ArgumentException(instruction.ToString());
		}
    }
}
