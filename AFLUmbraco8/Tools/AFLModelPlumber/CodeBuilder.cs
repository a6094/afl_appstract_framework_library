﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AFLUmbraco8.Tools
{
    public class CodeBuilder
    {
        private StringBuilder builder = new StringBuilder();
        private int indent = 0;
        private List<CodeBuilder> nestedBuilders = new List<CodeBuilder>();

        public void Append(string val, int indentChange = 0)
        {
            if (indentChange < 0) //negative before.
            {
                ChangeIndent(indentChange);
            }

            builder.Append('\t', indent);
            builder.AppendLine(val);

            if (indentChange > 0) //positive after.
            {
                ChangeIndent(indentChange);
            }
        }

        public void ChangeIndent(int indentChange)
        {
            indent += indentChange;
            if (indent < 0) { throw new InvalidOperationException("negative indent"); }
        }

        public CodeBuilder AppendBuilder()
        {
            var nested = new CodeBuilder();
            nested.indent = indent;
            builder.Append($"$$CB:{nestedBuilders.Count}$$");
            nestedBuilders.Add(nested);
            return nested;
        }

        public new string ToString()
        {
            var result = builder.ToString();
            result = Regex.Replace(result, "\\$\\$CB:([0-9]+)\\$\\$", s => nestedBuilders[Convert.ToInt32(s.Groups[1].Value)].ToString());
            return result;
        }
    }

    public class SimpleSourceCodeEditor
    {
        string filePath;
        string content;
        public SimpleSourceCodeEditor(string filePath)
        {
            this.filePath = filePath;
            content = System.IO.File.ReadAllText(filePath);
        }

        public bool InsertBefore(string regex, string value)
        {
			var match = Regex.Match(content, regex, RegexOptions.Multiline);
			var constructorClosing = content.IndexOf("}");
            if (match != null)
            {
				if (match.Index == 0 && constructorClosing >= 0)
				{
					content = content.Insert(constructorClosing + 1, "\n\t\t" + value);
				}
				else
				{
					content = content.Insert(match.Index, value);
				}
				return true;
			}
			return false;
        }


        public bool AppendToLine(string regex, string value)
        {
			var match = Regex.Match(content, regex, RegexOptions.Multiline);
            if (match is null)
            {
                return false;
            }

            content = content.Insert(match.Index + match.Length, value);

            return true;
        }

        public bool Replace(string regex, string value)
        {
            var match = Regex.Match(content, regex, RegexOptions.Multiline);
            if (match != null)
            {
                content = content.Remove(match.Index, match.Length);
                content = content.Insert(match.Index, value);
                return true;
            }
            return false;
        }

        public string GetIndentAtLine(string regex)
        {

			var match = Regex.Match(content, regex, RegexOptions.Multiline);
            if (match != null)
            {
                return new string(content.Substring(content.LastIndexOf('\n', match.Index) + 1).TakeWhile(c => c == '\t' || c == ' ').ToArray());
            }
            return "";
        }

		public void Save()
        {
            System.IO.File.WriteAllText(filePath, content);
        }
    }
}
