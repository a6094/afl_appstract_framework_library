﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AFL;
using AFL.Core;
using AFL.Tools;
using Mono.Cecil;
using Umbraco.Core.Composing;
using Umbraco.Core.Models;

namespace AFLUmbraco8.Tools
{
    [Rest("modelplumber")]
    public class AFLModelPlumberWeb
    {
        List<IContentType> umbracoContentTypes;
        AFLModelPlumber plumber = new AFLModelPlumber();

        public AFLModelPlumberWeb()
        {
            var cts = Current.Services.ContentTypeService;
            umbracoContentTypes = cts.GetAll().ToList();//GetAllContentTypes().ToList();
        }

        [RestProperty]
        public bool IgnoreExisting { get; set; }
        [RestProperty]
        public bool WithComments { get; set; }

        [RestMethod(DebugOnly = true, Description = "Retrieves the list of all AFL models.")]
        public JsonResult GetModels(bool all = false)
        {
            var models = umbracoContentTypes.FindAll(contentType => Registry.Current.TryGetInfo(contentType.Alias) != null || all);
            var json = new JsonResult
            {
                Data = models.Select(contentType => new { name = contentType.Name, present = Registry.Current.TryGetInfo(contentType.Alias) != null })
            };
            return json;
        }

        [RestMethod(DebugOnly = true, Default = true, Description = "Helps creating the models needed for mapping the CMS types.")]
        public HTMLActionResult WriteModels(string rawUrl)
        {
            if (rawUrl == null)
            {
                rawUrl = HttpContext.Current.Request.Url.AbsolutePath;
            }
            var html = new HTMLBuilder(true);
            html.Body.AddChild(new Elem("h2", "AFL Model Plumber"));
            var commentSection = new Elem("section");
            html.Body.AddChild(commentSection);

            commentSection.AddParagraph("This is a utility to map all the document types that are defined in the current Umbraco database to C# code.");
            commentSection.AddChild(new Elem("h4", "Options:"));
            commentSection.AddChild(new Elem("p").AddChild(new Elem("ul")
                .AddChild(new Elem("li").AddChild(new Elem("a", $"{(IgnoreExisting ? "Don't" : "Always")} print all models.").AddAttr("href", RestUrl.Make(this, includeModelProps: true).Override("IgnoreExisting", !IgnoreExisting).ToString())))
                .AddChild(new Elem("li").AddChild(new Elem("a", $"{(WithComments ? "Disable" : "Enable")} comments with extra info from Umbraco.").AddAttr("href", RestUrl.Make(this, includeModelProps: true).Override("WithComments", !WithComments).ToString())))
            ));

            commentSection.AddParagraph("Note about Page Content: remember that no special handing is neccesary for custom page content types, since AFL will instantiate the correct type, all custom PageContents should derive from the same base, and the backoffice will limit which ones can be used. Just make a simple 'PageContent PageContent => Memo(Nav.FirstChild<PageContent>())' in you base Page");

            foreach (var contentType in umbracoContentTypes)
            {
                html.Body.AddChild(new Elem("h3", $"{contentType.Name} ({contentType.Alias})"));
                var info = Registry.Current.TryGetInfo(contentType.Alias);
                bool wasDefined = false;
                if (info != null)
                {
                    var cecilType = plumber.GetCecilType(info.ModelType);
                    html.Body.AddParagraph($"Defined in {plumber.GetSourcePath(cecilType)}.");
                    var diff = plumber.WriteModelClassDiff(contentType, info, cecilType, WithComments);
                    if (!string.IsNullOrWhiteSpace(diff))
                    {
                        html.Body.AddChild(new Elem("p").AddText("The following properties do not match:").AddChild(new Elem("a", "(fix it!)").AddAttr("href", RestUrl.Make(this, "WriteDiffToFile", queryString: $"alias={contentType.Alias}&redirect={HttpUtility.UrlEncode(rawUrl)}").ToString())));
                        html.Body.AddChild(new Elem("textarea", diff).AddAttr("rows", "6").AddAttr("cols", "100").AddAttr("readonly", "1"));
                    }
                    wasDefined = true;
                }
                else
                {
                    html.Body.AddParagraph($"{plumber.GetModelNameForAlias(contentType.Alias)} is not defined in code.");
                }
                if (!wasDefined || IgnoreExisting)
                {
                    html.Body.AddChild(new Elem("p").AddText("Suggested class definition:").AddChild(new Elem("a", "(write it!)").AddAttr("href", RestUrl.Make(this, "WriteClassToFile", queryString: $"alias={contentType.Alias}&redirect={HttpUtility.UrlEncode(rawUrl)}").ToString())));
                    string code = plumber.WriteModelClassCode(contentType, WithComments);
                    html.Body.AddChild(new Elem("textarea", code).AddAttr("rows", "20").AddAttr("cols", "100").AddAttr("readonly", "1"));
                }
            }
            return new HTMLActionResult(html.Result);
        }

        [RestMethod(HideFromHandlerList = true)]
        public RedirectToUrlActionResult WriteClassToFile(string alias, string redirect)
        {
            plumber.SaveClassToFile(alias, WithComments);
            return new RedirectToUrlActionResult(redirect);
        }

        [RestMethod(HideFromHandlerList = true)]
        public RedirectToUrlActionResult WriteDiffToFile(string alias, string redirect)
        {
            plumber.SaveDiffToFile(alias, WithComments);
            return new RedirectToUrlActionResult(redirect);
        }
    }
}
