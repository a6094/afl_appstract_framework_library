﻿using AFL;
using Umbraco.Web.Editors;

namespace AFLUmbraco8.Tools
{
	public class AFLDebugModeController : UmbracoAuthorizedJsonController
	{
		[System.Web.Http.HttpGet]
		public bool IsDebug()
		{
			return Registry.AFLDebugMode;
		}
	}
}
