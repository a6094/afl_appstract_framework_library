﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AFL;
using AFL.Search;
using Umbraco.Web.Editors;
using Umbraco.Web.WebApi;

namespace AFLUmbraco8.Tools
{
	public class AFLMemoSupportController: UmbracoAuthorizedJsonController
	{

		[System.Web.Http.HttpGet]
		public IEnumerable<AFLMemoSupportProperty> GetMissingProperties()
		{

			var json = InMemoryMissingDocumentTypeProperties.GetInstance().Select(typeProperty =>
			{
				return new AFLMemoSupportProperty(typeProperty.TypeAlias, typeProperty.ModelName, typeProperty.PropertyName, typeProperty.PropertyType.ToString());
			});
			return json;

			
		}

		[System.Web.Http.HttpGet]
		public bool ReregisterTypes()
		{
			Registry.Current.Reinitialize();
			return true;
		}

		[System.Web.Http.HttpGet]
		public bool ClearPropertyEntry(string alias, string propertyName)
		{
			InMemoryMissingDocumentTypeProperties.GetInstance().RemoveAll(item => (item.TypeAlias == alias) && (item.PropertyName == propertyName));
			return true;
		}

		[System.Web.Http.HttpGet]
		public bool ClearAllProperties()
		{
			InMemoryMissingDocumentTypeProperties.GetInstance().Clear();
			return true;
		}

	}

	public class AFLMemoSupportProperty
	{
		public string alias { get; }
		public string modelName { get; }
		public string name { get; }
		public string type { get; }
		public AFLMemoSupportProperty(string alias, string modelName, string name, string type)
		{
			this.alias = alias;
			this.modelName = modelName;
			this.name = name;
			this.type = type;
		}
	}
}
