﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Umbraco.Core;
using Umbraco.Web.Composing;
using System.Text.RegularExpressions;
using Umbraco.Web.Editors;
using System.Web.Http;
using Umbraco.Web.Mvc;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace AFLUmbraco8.Tools
{
	[PluginController("URH")]
	public class UmbracoRedirectHelperController : UmbracoAuthorizedJsonController
	{
		public class RedirectItem
		{
			public RedirectItem(string id, string url, string culture)
			{
				this.Id = id;
				this.Url = url;
				this.Culture = culture;
			}

			public string Id { get; private set; }
			public string Url { get; private set; }
			public string Culture { get; private set; }
		}

		[HttpGet]
		public IEnumerable<RedirectItem> GetExistingRedirects(int nodeId)
		{
			var findUrlsSql = @"select [id],[url],[culture] from umbracoRedirectUrl where contentKey = @ContentKey";
			var contentKey = Umbraco.Content(nodeId)?.Key;
			if (contentKey == null) { return null; }

			try
			{
				List<(string id, string url, string culture)> data = new List<(string, string, string)>();

				using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
				using (var command = new SqlCommand(findUrlsSql, conn))
				{
					command.Parameters.AddWithValue("@ContentKey", contentKey);
					conn.Open();
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							data.Add((id: reader["id"].ToString(), url: reader["url"].ToString(), culture: reader["culture"].ToString()));
						}
					}
				}

				return data.Select(x => new RedirectItem(x.id, TranslateUmbracoUrl(x.url, x.culture), x.culture)).ToList();
			}
			catch
			{
				return null;
			}
		}

		[HttpGet]
		public bool DeleteRedirect(string id)
		{
			var deleteSql = @"DELETE FROM umbracoRedirectUrl WHERE id = @Id;";
			try
			{
				using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
				using (var command = new SqlCommand(deleteSql, conn))
				{
					command.Parameters.AddWithValue("@Id", id);
					conn.Open();
					command.ExecuteNonQuery();
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		public string TranslateUmbracoUrl(string url, string culture)
		{
			if (url[0] == '/') return url;

			ChangeCulture(culture);
			var nodeId = url.Split('/').First();
			var startUrl = Umbraco.Content(nodeId).Url();
			return $"{startUrl}{url.Substring(nodeId.Length + 1)}";
		}

		private void ChangeCulture(string languageISOCode)
		{
			Current.VariationContextAccessor.VariationContext = new VariationContext(languageISOCode);
		}

		[HttpGet]
		public bool AddAlternativeRedirect(int nodeId, string base64Url, string languageISOCode)
		{
			ChangeCulture(languageISOCode);
			var node = Umbraco.Content(nodeId);
			var contentKey = node.Key;
			var root = node.AncestorOrSelf(1);
			var url = FormatUrl(Base64Decode(base64Url), root.Id);
			var urlHash = GetSHA1(url);

			if (!IsValidUrl(url)) { return false; }
			var insertSql = @"INSERT INTO umbracoRedirectUrl(id, contentKey, createDateUtc, url, urlHash, culture) Values(@ID, @ContentKey, @Date, @Url, @UrlHash, @Culture)";
			var findExisting = @"select count(*) from umbracoRedirectUrl where urlHash = @UrlHash";

			var alreadyExists = Current.UmbracoContext.Content.GetByRoute(true, url) != null;
			if (alreadyExists) { return false; }

			try
			{
				using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
				{
					conn.Open();
					using (var command = new SqlCommand(findExisting, conn))
					{
						command.Parameters.AddWithValue("@UrlHash", urlHash);
						var count = (int)command.ExecuteScalar();
						if (count > 0)
						{
							return false;
						}
					}

					using (var command = new SqlCommand(insertSql, conn))
					{
						command.Parameters.AddWithValue("@ID", Guid.NewGuid());
						command.Parameters.AddWithValue("@ContentKey", contentKey);
						command.Parameters.AddWithValue("@Date", DateTime.UtcNow);
						command.Parameters.AddWithValue("@Url", url);
						command.Parameters.AddWithValue("@UrlHash", urlHash);
						command.Parameters.AddWithValue("@Culture", languageISOCode);
						command.ExecuteNonQuery();
					}
				}

				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		private static string Base64Decode(string base64EncodedData)
		{
			var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
			return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
		}

		private static string GetSHA1(string s)
		{
			HashAlgorithm algo = SHA1.Create();
			var rawHash = algo.ComputeHash(Encoding.UTF8.GetBytes(s));

			StringBuilder sb = new StringBuilder();
			foreach (byte b in rawHash)
			{
				sb.Append(b.ToString("X2"));
			}
			return sb.ToString();
		}

		private string FormatUrl(string url, int basenodeId)
		{
			return $"{basenodeId}/{url.ToLower().Trim(' ', '/')}";
		}

		private static bool IsValidUrl(string url)
		{
			var rgx = new Regex(@"^[\/]?(?:[a-zA-Z0-9\/\-]+\/?)*$");
			return rgx.IsMatch(url);
		}
	}
}
