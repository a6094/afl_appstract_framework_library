﻿using AFL;
using AFL.Core;
using AFL.Tools;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace AFLUmbraco8.Tools
{

	class AFLDictionaryTool
	{

		public AFLDictionaryTool(bool isBackOffice)
		{
			this.isBackOffice = isBackOffice;
		}

		public void SetupMissingDictionaryItemsNoTrans()
		{
			SetupMissingDictionaryItems(false);
		}

		private HashSet<string> alreadyModified;
		private readonly bool isBackOffice;

		public void SetupMissingDictionaryItems(bool autoTranslate = true)
		{
			alreadyModified = new HashSet<string>();
			var localizationService = Current.Services.LocalizationService;
			var currentTranslations = localizationService.GetDictionaryItemKeyMap();
			CreateDictionaryItem("Umbraco", null, true);
			var umbracoNode = localizationService.GetDictionaryItemByKey("Umbraco");
			var contentTypeService = Current.Services.ContentTypeService;
			foreach (var contentType in contentTypeService.GetAll())
			{
				if (!contentType.Name.StartsWith("#Umbraco.")) { continue; }

				CreateDictionaryItemIfNeeded(contentType.Name, umbracoNode, true, autoTranslate: autoTranslate);
				var contentTypeDictionary = localizationService.GetDictionaryItemByKey(contentType.Name.Substring(1));
				CreateDictionaryItemForDescription(() => contentType.Description, x => contentType.Description = x, contentType.Name, contentType, contentTypeDictionary);

				foreach (var group in contentType.PropertyGroups)
				{
					CreateDictionaryItemIfNeeded(group.Name, contentTypeDictionary, autoTranslate: autoTranslate);
				}

				foreach (var property in contentType.PropertyTypes)
				{
					CreateDictionaryItemIfNeeded(property.Name, contentTypeDictionary, autoTranslate: autoTranslate);
					CreateDictionaryItemForDescription(() => property.Description, x => property.Description = x, property.Name, contentType, contentTypeDictionary);
				}
			}
		}

		public void FillOutEmptyDictionaryItems()
		{
			alreadyModified = new HashSet<string>();

			var localizationService = Current.Services.LocalizationService;
			var languages = localizationService.GetAllLanguages();

			var umbracoNodeId = localizationService.GetDictionaryItemByKey("Umbraco").Key;
			var allRelevantDictionaries = localizationService.GetDictionaryItemDescendants(umbracoNodeId);

			foreach (var dictItem in allRelevantDictionaries)
			{
				var autoTranslation = CreateTranslation(dictItem.ItemKey);
				var debug = new List<string>();

				var translations = dictItem.Translations.ToList();
				foreach (var lang in languages.Where(lan => lan.CultureInfo.TwoLetterISOLanguageName == "en"))
				{
					IDictionaryTranslation temp = translations.FirstOrDefault(t => t.LanguageId == lang.Id);
					if (temp == null)
					{
						translations.Add(new DictionaryTranslation(lang, autoTranslation));
						debug.Add(lang.CultureName);
					}
					else if (temp.Value.IsNullOrWhiteSpace())
					{
						temp.Value = autoTranslation;
						debug.Add(lang.CultureName);
					}
				}
				if (debug.Count > 0)
				{
					dictItem.Translations = translations;
					localizationService.Save(dictItem);
					if (!isBackOffice)
					{
						Write("Translated", $"{dictItem.ItemKey} to {autoTranslation} in | {string.Join(" | ", debug)} |");
					}
				}
			}
		}

		public void MakeAllContentTypesIntoDictionaryItems()
		{
			alreadyModified = new HashSet<string>();

			var localizationService = Current.Services.LocalizationService;
			var currentTranslations = localizationService.GetDictionaryItemKeyMap();
			CreateDictionaryItem("Umbraco", null);
			var umbracoNode = localizationService.GetDictionaryItemByKey("Umbraco");

			var contentTypeService = Current.Services.ContentTypeService;
			foreach (var contentType in contentTypeService.GetAll())
			{
				void save() => contentTypeService.Save(contentType);
				ConvertToDictionaryItemIfNeeded(() => contentType.Name, () => contentType.Alias, x => contentType.Name = x, save, umbracoNode, true);

				var contentTypeDict = localizationService.GetDictionaryItemByKey(contentType.Name.Substring(1));
				if (contentTypeDict == null)
				{
					if (!isBackOffice)
					{
						Write("error", $"Cannot work on {contentType.Name} since it have no dictionary item");
					}
					continue;
				}
				CreateDictionaryItemForDescription(() => contentType.Description, x => contentType.Description = x, contentType.Name, contentType, contentTypeDict);

				foreach (var group in contentType.PropertyGroups)
				{
					ConvertToDictionaryItemIfNeeded(() => group.Name, () => CreateAliasFromName(group.Name), x => group.Name = x, save, contentTypeDict);
				}

				foreach (var property in contentType.PropertyTypes)
				{
					ConvertToDictionaryItemIfNeeded(() => property.Name, () => property.Alias, x => property.Name = x, save, contentTypeDict);
					CreateDictionaryItemForDescription(() => property.Description, x => property.Description = x, property.Name, contentType, contentTypeDict, true);
				}
			}
		}

		private string CreateAliasFromName(string name)
		{
			return Regex.Replace(name, @"\s", ""); //TODO, could improve this. e.g. "Make alias" -> "makeAlias"
		}

		private void ConvertToDictionaryItemIfNeeded(Func<string> currentName, Func<string> newName, Action<string> setName, Action save, IDictionaryItem parrent, bool isPage = false)
		{
			if (!currentName().StartsWith("#Umbraco."))
			{
				string fullNewName = "#" + parrent.ItemKey + "." + newName().ToFirstUpper();
				if (CreateDictionaryItemIfNeeded(fullNewName, parrent, isPage, translation: CreateTranslation(currentName())))
				{
					setName(fullNewName);
					save();
				}
			}
		}

		private string CreateTranslation(string original)
		{
			var translation = new LinkedList<char>(original.Split('.').Last());
			var current = translation.First;

			while (current.Next != null)
			{
				current = current.Next;
				if (current.Value.IsUpperCase() &&
					current.Previous.Value != ' ' &&
					(current.Previous.Value.IsLowerCase() ||
					(current.Next?.Value.IsLowerCase() ?? false))) //If next char is upper, we assume the current char is part of an abbrivation. e.g. HTML
				{
					translation.AddBefore(current, ' ');
				}
			}

			return new string(translation.ToArray());
		}

		private void CreateDictionaryItemForDescription(Func<string> getDescription, Action<string> setDescription, string nameOfItem, IContentType contentType, IDictionaryItem parrent, bool createFromComment = false)
		{
			if (!string.IsNullOrEmpty(getDescription()))
			{
				string translation = null;
				if (getDescription() == "#?" && nameOfItem.StartsWith("#Umbraco."))
				{
					setDescription(nameOfItem + "Desc");
					Current.Services.ContentTypeService.Save(contentType);
					if (!isBackOffice)
					{
						Write("Rename", $"#? --> {getDescription()}");
					}
				}
				else if (createFromComment && nameOfItem.StartsWith("#Umbraco.") && !getDescription().StartsWith("#Umbraco."))
				{
					translation = getDescription();
					setDescription(nameOfItem + "Desc");
					Current.Services.ContentTypeService.Save(contentType);
					if (!isBackOffice)
					{
						Write("Generated", $"{getDescription()} <-- \"{translation}\"");
					}
				}
				if (getDescription().StartsWith("#Umbraco."))
				{
					CreateDictionaryItem(getDescription()?.Substring(1), parrent, translation: translation);
				}
			}
		}

		private bool CreateDictionaryItemIfNeeded(string name, IDictionaryItem parrent, bool isPage = false, string translation = null, bool autoTranslate = false)
		{
			if (!alreadyModified.Contains(name))
			{
				alreadyModified.Add(name);
				if (name.StartsWith("#Umbraco."))
				{
					var dictKey = name.Substring(1);
					return CreateDictionaryItem(dictKey, parrent, isPage, translation, autoTranslate);
				}
			}
			return false;
		}

		private bool CreateDictionaryItem(string name, IDictionaryItem parrent, bool isPage = false, string translation = null, bool autoTranslate = false)
		{
			var localizationService = Current.Services.LocalizationService;
			var languages = localizationService.GetAllLanguages();
			if (!localizationService.DictionaryItemExists(name))
			{
				var trans = autoTranslate ? CreateTranslation(name) : translation;
				var dictItem = localizationService.CreateDictionaryItemWithIdentity(name, parrent?.Key);
				if (!trans.IsNullOrWhiteSpace())
				{
					dictItem.Translations = languages.Where(lan => lan.CultureInfo.TwoLetterISOLanguageName == "en").Select(lan => new DictionaryTranslation(lan, trans)).ToArray();
					localizationService.Save(dictItem);
				}
				if (!isBackOffice)
				{
					Write(isPage ? "Page Added" : "Added", name);
				}
				return true;
			}
			else
			{
				if (isPage && !isBackOffice)
				{
					Write("Page Existed", name);
				}
				RelocateIfNeeded(localizationService.GetDictionaryItemByKey(name), parrent, isPage);
				return false;
			}
		}

		private bool RelocateIfNeeded(IDictionaryItem node, IDictionaryItem expectedParrent, bool isPage = false)
		{
			bool relocate = node.ParentId != expectedParrent?.Key;
			if (relocate)
			{
				node.ParentId = expectedParrent?.Key;
				Current.Services.LocalizationService.Save(node);
				if (!isBackOffice)
				{
					Write(isPage ? "Page Relocate" : "Relocate", $"{node.ItemKey} --> {expectedParrent?.ItemKey}");
				}
			}
			return relocate;
		}
		private void Write(string action, string details)
		{
			if (isBackOffice)
			{
				throw new InvalidOperationException("Cannot write to HTTP response directly when in backoffice context");
			}
			string msg = String.Format("{0,-20}{1}\n", action, details);
			HttpContext.Current.Response.Write(msg);
			HttpContext.Current.Response.Flush();
		}
	}
}
