﻿using Umbraco.Web.Editors;

namespace AFLUmbraco8.Tools
{
	public class AFLDictionaryToolController: UmbracoAuthorizedJsonController
	{
		private AFLDictionaryTool tool = new AFLDictionaryTool(false);

		[System.Web.Http.HttpGet]
		public bool SetupMissingDictionaryItemsNoTrans()
		{
			tool.SetupMissingDictionaryItemsNoTrans();
			return true;
		}

		[System.Web.Http.HttpGet]
		public bool SetupMissingDictionaryItems(bool autoTranslate = true)
		{
			tool.SetupMissingDictionaryItems(autoTranslate);
			return true;
		}

		[System.Web.Http.HttpGet]
		public bool FillOutEmptyDictionaryItems()
		{
			tool.FillOutEmptyDictionaryItems();
			return true;
		}

		[System.Web.Http.HttpGet]
		public bool MakeAllContentTypesIntoDictionaryItems()
		{
			tool.MakeAllContentTypesIntoDictionaryItems();
			return true;
		}

	}
}
