﻿using AFL;
using AFL.Core;
using AFL.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace AFLUmbraco8.Tools
{

	[Rest("DictionaryTool")]
	class AFLDictionaryToolWeb
	{

		private AFLDictionaryTool tool = new AFLDictionaryTool(false);

		[RestMethod(DebugOnly = true, Default = true, Description = "Create dictionary items for items in the backoffice")]
		public HTMLActionResult DictionaryToolMenu()
		{
			var html = new HTMLBuilder(true);
			html.Body.AddChild(new Elem("h2", "Dictionary tool"));

			var description = "This tool will add empty dictionary elements for Document types(and properties) which name starts with \"#Umbraco.\" but for which doesn't exist a dictionary item.\nIt will also move dictionary items which are not placed correctly in the structure.";
			AddMenuItem(html.Body, "Setup Missing Dictionary Items", description, "Run it", "SetupMissingDictionaryItemsNoTrans");

			description = "Same as above, it will however also generate an automatic translation to the dictionary items based on the last part of their name.";
			AddMenuItem(html.Body, "Setup Missing Dictionary Items with translation", description, "Run it", "SetupMissingDictionaryItems");

			description = "A tool which for empty dictionary items fill out an automatic translation based on the last part of their name. \nE.g. \nUmbraco.Fronpage.BodyText -> Body Text. \n\nIt only translate dictionaries for the things in the backoffice (In practice this is all the descendatns of the Umbraco node in the dictionary)";
			AddMenuItem(html.Body, "Fill Empty Dictionaries", description, "Run it", "FillOutEmptyDictionaryItems");

			description = "This tool will rename the Documents types(and properties) which currently don't start with #Umbraco, and then they will add a dictionary item which is translated to what the document type was called before it was renamed.\nE.g.\nA Document Type called \"Page Content\" would be renamed to \"#Umbraco.PageContent\" and in the dictionary it would be translated to \"Page Content\"";
			AddMenuItem(html.Body, "Make Document types use dictionary items", description, "Run it", "MakeAllContentTypesIntoDictionaryItems");


			return new HTMLActionResult(html.Result);
		}

		private void AddMenuItem(Elem position, string toolTitle, string toolDescription, string linkText, string methodName)
		{
			//TODO Could probably set this up more advanced to it would become a little pretier.
			position.AddChild(new Elem("h3", toolTitle));
			foreach (var desc in toolDescription.Split('\n'))
			{
				position.AddChild(new Elem("p", desc));
			}
			position.AddChild(new Elem("a", linkText).AddAttr("href", RestUrl.Make(this, methodName).ToString()));
			position.AddChild(new Elem("br"));
			position.AddChild(new Elem("br"));
		}

		[RestMethod(HideFromHandlerList = true)]
		public void SetupMissingDictionaryItemsNoTrans()
		{
			tool.SetupMissingDictionaryItemsNoTrans();
		}

		[RestMethod(HideFromHandlerList = true)]
		public void SetupMissingDictionaryItems(bool autoTranslate = true)
		{
			HttpContext.Current.Response.ContentType = "text/event-stream";
			tool.SetupMissingDictionaryItems(autoTranslate);
			HttpContext.Current.Response.Write("DONE!!!");
			HttpContext.Current.Response.End();
		}

		[RestMethod(HideFromHandlerList = true)]
		public void FillOutEmptyDictionaryItems()
		{
			HttpContext.Current.Response.ContentType = "text/event-stream";
			tool.FillOutEmptyDictionaryItems();
			HttpContext.Current.Response.Write("DONE!!!");
			HttpContext.Current.Response.End();
		}

		[RestMethod(HideFromHandlerList = true)]
		public void MakeAllContentTypesIntoDictionaryItems()
		{
			HttpContext.Current.Response.ContentType = "text/event-stream";
			tool.MakeAllContentTypesIntoDictionaryItems();
			HttpContext.Current.Response.Write("DONE!!!");
			HttpContext.Current.Response.End();
		}


	}
}
