﻿

using System.Net.Http.Formatting;
using System.Web.Http.ModelBinding;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Trees;
using Umbraco.Web.WebApi.Filters;

namespace AFLUmbraco8Web
{
	[Tree("aflsection", "afltree", TreeTitle = "AFL", TreeGroup = "aflGroup", SortOrder = 5)]
	public class AFLTreeController : TreeController
	{
		protected override MenuItemCollection GetMenuForNode(string id, [ModelBinder(typeof(HttpQueryStringModelBinder))] FormDataCollection queryStrings)
		{
			return new MenuItemCollection();
		}

		protected override TreeNodeCollection GetTreeNodes(string id, [ModelBinder(typeof(HttpQueryStringModelBinder))] FormDataCollection queryStrings)
		{
			return new TreeNodeCollection();
		}
	}
}
