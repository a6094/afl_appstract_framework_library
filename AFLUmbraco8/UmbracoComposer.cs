﻿using AFLUmbraco8.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Composing;

namespace AFLUmbraco8
{
    [RuntimeLevel(MinLevel = RuntimeLevel.Run, MaxLevel =RuntimeLevel.Run)]
    public class UmbracoComposer : ICoreComposer
    {
        public void Compose(Composition composition)
        {
            composition.RegisterUnique(typeof(CustomIndexCreator));
            composition.Components().Append<CustomExamineComponent>();
        }
    }
}
