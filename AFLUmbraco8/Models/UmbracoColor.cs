﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFLUmbraco8.Models
{
	public class UmbracoColor
	{
		[JsonProperty("label")]
		public string Label { get; set; }
		[JsonProperty("value")]
		public string Value { get; set; }

		public static UmbracoColor FromString(string str)
		{
			if (string.IsNullOrEmpty(str))
			{
				return new UmbracoColor();
			}
			else if (str.Trim().StartsWith("{"))
			{
				return JsonConvert.DeserializeObject<UmbracoColor>(str);
			}
			else
			{
				return new UmbracoColor { Label = str, Value = str };
			}
		}
	}
}
