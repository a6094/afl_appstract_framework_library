﻿using System.Web;
using AFL;
using Umbraco.Core.Models;

namespace AFLUmbraco8.Models
{
	public abstract class MediaItem : AFLModelBase
	{
		public MediaItem(Umbraco.Core.Models.PublishedContent.IPublishedContent node) : base(node)
		{
		}

		[PropertyAlias("umbracoFile")]
		public virtual string FilePath => Memo();
		public string FilePathWithDomain => Memo(() => HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + FilePath);
		[PropertyAlias("umbracoBytes")]
		public string FileSize => Memo();
		[PropertyAlias("umbracoExtension")]
		public string FileExtension => Memo();
	}

	public static class MediaItemExtentions
	{
		public static string GetUrl(this MediaItem item)
		{
			return item?.FilePath ?? "";
		}

		public static string GetUrlWithDomain(this MediaItem item)
		{
			return item?.FilePathWithDomain ?? "";
		}
	}
}
