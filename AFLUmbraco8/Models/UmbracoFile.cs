﻿using AFL;
using Umbraco.Core.Models;

namespace AFLUmbraco8.Models
{
	[Alias("File")]
	public class UmbracoFile : MediaItem
	{
		public UmbracoFile(Umbraco.Core.Models.PublishedContent.IPublishedContent node) : base(node)
		{
		}
	}
}
