﻿using AFL;
using AFL.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Core.Models.PublishedContent;

namespace AFLUmbraco8.Models
{
	public abstract class AFLModelBase : IAFLModel
	{
		protected readonly IPublishedContent node;
		protected AFLModelBase(IPublishedContent node)
		{
			if (node == null) { throw new InvalidOperationException("node can not be null!"); }
			this.node = node;
		}

		protected AFLModelBase()
		{
			//in this case the Registry will set node via reflection, when using CreateModelInstance.
		}

		protected T Memo<T>(Func<T> func, T fallback = default(T), [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, func, fallback, false, null, callerName, null);
		}

		protected T Memo<T>(Nav navType, Func<T, T> postProcess = null, T fallback = default(T), [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(postProcess, null, fallback, false, navType, callerName, null);
		}

		protected T Memo<T>(Nav<T> navType, T fallback = default(T), [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, null, fallback, false, navType, callerName, null);
		}

		protected TPost Memo<TPre, TPost>(Nav<TPre> navType, Func<TPre, TPost> postProcessFunc, TPost fallback = default(TPost), [CallerMemberName] string callerName = "")
		{
			return Memo(postProcessFunc, null, fallback, false, navType, callerName, null);
		}

		//for cases like: int ZipCode => Memo<int>();
		/// <summary>
		/// For cases like int ZipCode => Memo<int>();
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="fallback">Fallback incase of T being null</param>
		/// <param name="recurse">Value getter will recurse up the tree, untill it finds a non empty value</param>
		/// <param name="callerName"></param>
		/// <returns></returns>
		protected T Memo<T>(T fallback = default(T), bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, null, fallback, recurse, null, callerName, null);
		}
		//for cases like: string AddressLine1 => Memo();
		protected string Memo(string fallback = null, bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo<string, string>(null, null, fallback, recurse, null, callerName, null);
		}

		//for cases like: string SearchPageUrl => Memo<IPublishedContent[], string>(n => n.FirstOrDefault()?.Url);
		protected TPost Memo<TPre, TPost>(Func<TPre, TPost> postProcessFunc, TPost fallback = default(TPost), bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo(postProcessFunc, null, fallback, recurse, null, callerName, null);
		}
		//for cases like: EventPage[] RawEventPages => Memo<EventPage[]>(list => list.OrderByDescending(n => n.PublicationDate).ToArray());
		protected T Memo<T>(Func<T, T> postProcessFunc, T fallback = default(T), bool recurse = false, [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(postProcessFunc, null, fallback, recurse, null, callerName, null);
		}

		protected T Memo<T>(Injector injector, [CallerMemberName] string callerName = "")
		{
			return Memo<T, T>(null, null, default(T), false, null, callerName, injector);
		}

		protected TPost Memo<TPre, TPost>(Injector injector, Func<TPre, TPost> postProcessFunc, [CallerMemberName] string callerName = "")
		{
			return Memo(postProcessFunc, null, default(TPost), false, null, callerName, injector);
		}

		private TPost Memo<TPre, TPost>(Func<TPre, TPost> postProcessFunc, Func<TPre> valueGetterFunc, TPost fallback, bool recurse, Nav nav, string callerName, Injector injector)
		{
			var propertyInfo = Registry.Current.GetInfo(this.GetType())?.GetProperty(callerName);

			if (valueGetterFunc == null)
			{
				var preType = typeof(TPre);
				var elementType = preType.IsArray ? preType.GetElementType() : preType;
				object value = null;
				valueGetterFunc = () =>
				{
					if (nav != null)
					{
						Func<object, bool> resultFilter = null;
						//todo: caching.
						if (nav.types != null && nav.types.Length > 0)
						{
							var aliases = nav.types.SelectMany(d => Registry.Current.GetInfo(d).CompatibleAliases).ToArray();
							resultFilter = (n =>
								((nav.Visibility & Nav.VisibilityFlags.OnlyVisible) == 0 || (n is IPublishedContent && !((IPublishedContent)n).Value<bool>("umbracoNaviHide"))) &&
								((nav.Visibility & Nav.VisibilityFlags.OnlyRenderable) == 0 || (Registry.Current.GetInfo(n).Renderable)) &&
								aliases.Any(a => (Registry.Current.GetAlias(n)) == a)
							);
						}
						else if (nav.Visibility != Nav.VisibilityFlags.All)
						{
							resultFilter = (n =>
								(n is IPublishedContent && !((IPublishedContent)n).Value<bool>("umbracoNaviHide")) &&
								((nav.Visibility & Nav.VisibilityFlags.OnlyRenderable) == 0 || (Registry.Current.GetInfo(n).Renderable))
							);
						}

						bool entityConvert = typeof(AFLModelBase).IsAssignableFrom(elementType);
						if (!entityConvert && elementType != typeof(IPublishedContent))
						{
							throw new InvalidOperationException("Unsupported return type for memo property. Pre-type must be entity or IPublishedContent (or arrays of those, for list cases)");
						}
						bool expectNonListReturn = false;
						Func<object, object> conv = (n => entityConvert ? Registry.Current.MemoSupport.GetModel(n) : n);

						if (nav.take == Nav.Take.Children)
						{
							var children = node.Children.Where(resultFilter ?? (p => true));
							value = Registry.Current.MemoSupport.EnumerableToTypedArray(elementType, children.Select(n => conv(n)));
						}
						else if (nav.take == Nav.Take.FirstChild)
						{
							expectNonListReturn = true;
							var n = node.Children.Where(resultFilter ?? (p => true)).FirstOrDefault();
							value = n == null ? null : conv(n);
						}
						else if (nav.take == Nav.Take.AllSiblings)
						{
							value = Registry.Current.MemoSupport.EnumerableToTypedArray(elementType, GetSiblings(node).Where(resultFilter ?? (p => true)).Select(n => conv(n)));
						}
						else if (nav.take == Nav.Take.FirstSibling)
						{
							expectNonListReturn = true;
							var n = GetSiblings(node).FirstOrDefault(resultFilter ?? (p => true));
							value = n == null ? null : conv(n);
						}
						else if (nav.take == Nav.Take.NextSibling || nav.take == Nav.Take.PreviousSibling)
						{
							expectNonListReturn = true;
							//var n = nav.take == Nav.Take.NextSibling ? node.Next(resultFilter ?? (p => true)) : node.Previous(resultFilter ?? (p => true)); -- this was incorrect, since it navigates in the full content and not just siblings.
							var siblings = GetSiblings(node).ToArray();
							var selfIndex = siblings.IndexOf(node);
							var index = selfIndex + ((nav.take == Nav.Take.NextSibling) ? 1 : -1);
							var n = (selfIndex >= 0 && index >= 0) && index < siblings.Length ? siblings[index] : null;
							value = n == null ? null : conv(n);
						}
						else if (nav.take == Nav.Take.Descendants)
						{
							value = Registry.Current.MemoSupport.EnumerableToTypedArray(elementType, node.Descendants().Where(resultFilter ?? (p => true)).Select(n => conv(n)));
						}
						else if (nav.take == Nav.Take.AllAncestors)
						{
							value = Registry.Current.MemoSupport.EnumerableToTypedArray(elementType, node.AncestorsOrSelf().Where<IPublishedContent>(resultFilter ?? (p => true)).Select(n => conv(n)));
						}
						else if (nav.take == Nav.Take.ClosestAncestorExcludingSelf || nav.take == Nav.Take.ClosestAncestorIncludingSelf)
						{
							expectNonListReturn = true;
							var n = (nav.take == Nav.Take.ClosestAncestorIncludingSelf ? node.AncestorsOrSelf() : node.Ancestors()).Where<IPublishedContent>(resultFilter ?? (p => true)).FirstOrDefault();
							value = n == null ? null : conv(n);
						}
						else if (nav.take == Nav.Take.Parent)
						{
							expectNonListReturn = true;
							var n = new[] { node.Parent }.Where(resultFilter ?? (p => true)).FirstOrDefault();
							value = n == null ? null : conv(n);
						}
						if (expectNonListReturn && preType.IsArray)
						{
							throw new InvalidOperationException("Expected non array return type for this kind of memo selector");
						}
					}
					else
					{
						value = Registry.Current.MemoSupport.GetValue<TPre>(this, propertyInfo, callerName, elementType, recurse, injector);
					}
					return (TPre)value;
				};
			}

			return Registry.Current.MemoSupport.MemoImplementation(valueGetterFunc, postProcessFunc, fallback, Registry.Current.GetID(node), callerName, propertyInfo); // use Registry.Current.GetID(node) to allow workaround for case of nested content which all have id zero.
		}

		/// <summary>
		/// umbracos built in PublishedContentExtensions.Siblings has a "bug", in that it directly uses UmbracoContext.Current when the current item has a null parent.
		/// for us, this means that DBContent reverts to regular IPublishedContent for that case (lookup of siblings on DBContent yields non-DBContent), which is not cool.
		/// therefore we re-implement our own version of the siblings-method here...
		/// </summary>
		private IEnumerable<IPublishedContent> GetSiblings(IPublishedContent content)
		{
            if (content is IContentWithSiblings)
            {
                return ((IContentWithSiblings)content).Siblings();
            }
            else
            {
                return content.SiblingsAndSelf(); // then use the ordinary PublishedContentExtensions.Siblings method.
            }
        }

		protected void FlushMemo([CallerMemberName] string callerName = "")
		{
			Registry.Current.MemoSupport.Flush(Registry.Current.GetID(node), callerName); // use Registry.Current.GetID(node) to allow workaround for case of nested content which all have id zero.
		}

		//public string ViewTypeName { get { return node.GetTemplateAlias(); } }


		public string UID { get { return node.Id.ToString(); } }

		public int ID { get { return node.Id; } }

		public string Url { get { return node.Url; } }

		public string AbsoluteUrl { get { return node.Url(mode: UrlMode.Absolute); /*return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + node.Url;*/ } }

		public DateTime PublicationDate => node.UpdateDate;

		//protected UmbracoHelper UmbracoHelper => UmbracoUtils.UmbracoHelper;
		//protected MembershipHelper UmbracoMembershipHelper => UmbracoUtils.UmbracoMembershipHelper;


		public string RelativeUrl => Memo(() =>
		{
			string url = node.Url;
			if (url.StartsWith("http://", StringComparison.Ordinal) || url.StartsWith("https://", StringComparison.Ordinal) || url.StartsWith("//", StringComparison.Ordinal))
			{
				return new Uri(node.Url).PathAndQuery;
			}
			else
			{
				return url;
			}
		});

		public string Name => node.Name;

		public bool IsRoot => node.Level == 1;

		public IPublishedContent Content { get { return node; } }

		public string ViewTypeName
		{
			get
			{
				if (node is NestedContentWrapped)
				{
					return node.ContentType.Alias;
				}
				return node.GetTemplateAlias();
			}
		}
		public string Translate(string key)
		{
			if (node is MockPublishedContent) { return ((MockPublishedContent)node).MockTranslate(key); }
			UmbracoUtils.FixCulture();
			var val = UmbracoUtils.Translate(key);
			return string.IsNullOrEmpty(val) ? key : val;
		}

		public string Translate(string key, string defaultValue)
		{
			var result = Translate(key);
			return result == key ? defaultValue : result;
		}

		private AFLTypePropertyInfo GetPropertyInfo<TProperty>(Type type, Expression<Func<TProperty>> propertyLambda)
		{
			MemberExpression member = propertyLambda.Body as MemberExpression;
			if (member == null)
			{
				throw new ArgumentException(string.Format("Expression '{0}' refers to a method, not a property.", propertyLambda.ToString()));
			}
			var propInfo = member.Member as PropertyInfo;
			if (propInfo == null)
			{
				throw new ArgumentException(string.Format("Expression '{0}' refers to a field, not a property.", propertyLambda.ToString()));
			}

			if (propInfo.DeclaringType != type && !propInfo.DeclaringType.IsAssignableFrom(type))
			{
				throw new ArgumentException("Declaring type on the property is " + propInfo.DeclaringType.Name + ", expected " + type.Name);
			}

			var info = Registry.Current.GetInfo(type)?.GetProperty(propInfo.Name);
			if (info == null || string.IsNullOrEmpty(info.CMSName))
			{
				throw new Exception("Could not resolve CMS property name");
			}
			return info;
		}

		public void SetProperty<TProperty>(Expression<Func<TProperty>> propertyLambda, object value)
		{

			var settableNode = node as ISettableContent;
			if (settableNode == null)
			{
				throw new Exception("This model is not settable!");
			}
			var info = GetPropertyInfo(GetType(), propertyLambda);

            if (value == null || value is AFLModelBase)
            {
                value = value == null ? null : GetDocumentRef((value as AFLModelBase).Content);
            }
            else if (value.GetType().IsArray && typeof(AFLModelBase).IsAssignableFrom(value.GetType().GetElementType()))
            {
                value = string.Join(",", ((AFLModelBase[])value).Select(v => GetDocumentRef(((AFLModelBase)v).Content)));
            }
            else if (value.GetType().IsGenericType && (value.GetType().GetGenericTypeDefinition() == typeof(List<>)))
            {
                var arr = (value as IEnumerable<AFLModelBase>)?.ToArray();
                if (arr != null)
                {
                    value = string.Join(",", arr.Select(v => GetDocumentRef(v.Content)));
                }
            }

            settableNode.SetProperty(info.CMSName, value);
			FlushMemo(info.PropertyInfo.Name);
		}

        private string GetDocumentRef(IPublishedContent content)
        {
            Guid key = content.Key;
            if (key == Guid.Empty)
            {
                throw new ArgumentException("The content does not have a guid. Make sure it has been added to umbraco.");
            }
            return "umb://document/" + key.ToString("N");
        }

        public void SetName(string value)
		{
			var settableNode = node as ISettableContent;
			if (settableNode == null)
			{
				throw new Exception("This model is not settable!");
			}
			settableNode.SetName(value);
		}


		public void SetNestedContentList(Expression<Func<object>> propertyLambda, object value)
		{
			var settableNode = node as ISettableContent;
			if (settableNode == null)
			{
				throw new Exception("This model is not settable!");
			}
			var info = GetPropertyInfo(GetType(), propertyLambda);

			if(value != null)
			{
				var modelList = (IEnumerable<AFLModelBase>)value;
				var dataList = new List<Dictionary<string, object>>();
				foreach (var v in modelList)
				{
					if(!(v.Content is DBDetachedContent))
					{
						throw new Exception("The nested content items must be memory backed items.");
					}
					dataList.Add((v.Content as DBDetachedContent).GetData());
				}
				value = JsonConvert.SerializeObject(dataList);
			}

			settableNode.SetProperty(info.CMSName, value);
			FlushMemo(info.PropertyInfo.Name);
		}

		public void SetNestedContentProperty(Expression<Func<object>> propertyLambda, AFLModelBase targetNestedContentNode, AFLModelBase updatedNestedContentNode)
		{
			var settableNode = node as ISettableContent;
			if (settableNode == null)
			{
				throw new Exception("This model is not settable!");
			}
			if (updatedNestedContentNode == null && targetNestedContentNode == null)
			{
				throw new Exception("Either targetNestedContentNode or updatedNestedContentNode must be non-null.");
			}
			var info = GetPropertyInfo(GetType(), propertyLambda);

			var strData = settableNode.GetRawPropertyValue<string>(info.CMSName);
			var data = string.IsNullOrEmpty(strData) ? new List<Dictionary<string, object>>() : JsonConvert.DeserializeObject<Dictionary<string, object>[]>(strData).ToList();
			int index = -1;
			if (targetNestedContentNode != null)
			{
				var list = (IEnumerable<object>)propertyLambda.Compile().Invoke();
				if (list == null)
				{
					throw new ArgumentException("could not convert nested content property to enumerable");
				}
				index = list.IndexOf(targetNestedContentNode);
				if (index < 0)
				{
					throw new ArgumentException("target nested content node not found in list");
				}
			}
			if (updatedNestedContentNode == null) // delete the node
			{
				data.RemoveAt(index);
			}
			else
			{
				var type = updatedNestedContentNode.GetType();
				var newCopy = (AFLModelBase)Registry.Current.CreateMemoryBackedModel(type, updatedNestedContentNode);

				if (targetNestedContentNode == null) // append the node
				{
					data.Add(((DBDetachedContent)newCopy.Content).GetData());
				}
				else  // replace
				{
					data[index] = ((DBDetachedContent)newCopy.Content).GetData();
				}
			}
			settableNode.SetProperty(info.CMSName, JsonConvert.SerializeObject(data));
		}

		public void SaveContent(bool raiseEvents)
		{
			var settableNode = node as ISettableContent;
			if (settableNode == null)
			{
				throw new Exception("This model is not settable!");
			}

			settableNode.Save(raiseEvents);
		}

		public void PublishContent()
		{
			var settableNode = node as ISettableContent;
			if (settableNode == null)
			{
				throw new Exception("This model is not settable!");
			}

			settableNode.Publish();
		}
	}
}
