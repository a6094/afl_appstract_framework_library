﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Web.Composing;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using System.Web;
using Umbraco.Web;
using Umbraco.Core;
using Umbraco.Core.PropertyEditors;

namespace AFLUmbraco8.Models
{
	//used by AFL in preference to the PublishedContentExtensions.Siblings method, so that we an get around a problem for the case of root elements.
	public interface IContentWithSiblings
	{
		IEnumerable<IPublishedContent> Siblings();
	}

	public interface ISettableContent
	{
		void SetProperty(string property, object value);
		T GetRawPropertyValue<T>(string property);
		void SetName(string value);
		void Save(bool raiseEvents);
		void Publish();
	}

	//public class DBContent : IPublishedContentWithKey, IContentWithSiblings, ISettableContent
	public class DBContent : IPublishedContent, IContentWithSiblings, ISettableContent
	{
		private IContent content;
        private IReadOnlyDictionary<string, PublishedCultureInfo> _cultureInfos;
		public string savingCulture = null; // Intended to be non null in a save/publish event 

        public DBContent(IContent c)
		{
			var field = c.GetType().BaseType.GetField("_currentCultureChanges", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.FlattenHierarchy);
			if (field != null)
			{
				var currentCultureChanges = ( (HashSet<string>, HashSet<string>, HashSet<string>))field.GetValue(c);
				savingCulture = currentCultureChanges.Item3?.FirstOrDefault(); //For now assume that only one culture is gonna be present in a regular save event
			}
			if (c == null) { throw new ArgumentException("DBContent instantiated with null backing-content"); }
			content = c;
		}

		public DBContent(int id)
		{
			content = Current.Services.ContentService.GetById(id);
		}

		public IContent BackingContent => content;

		public IEnumerable<IPublishedContent> Children
		{
			get
			{
				var children = Current.Services.ContentService.GetPagedChildren(content.Id, 0, int.MaxValue, out var x);
                //return content.Children().Select(c => new DBContent(c)).ToArray();
                return children.Select(c => new DBContent(c)).ToArray();
			}
		}

		public IEnumerable<IPublishedContent> Siblings()
		{
			var parent = Parent;
			var siblings = parent == null ? Current.Services.ContentService.GetRootContent().Select(c => new DBContent(c)) : parent.Children;

			// make sure we run it once
			return siblings.ToArray();
		}

		public IEnumerable<IPublishedContent> ContentSet
		{
			get { throw new NotImplementedException(); }
		}

		public IPublishedContentType ContentType
		{
			get {
				return new PublishedContentType(content.Id, content.ContentType.Alias, PublishedItemType.Content, 
					Enumerable.Empty<string>(), 
					Enumerable.Empty<PublishedPropertyType>(), 
					content.ContentType.Variations, content.ContentType.IsElement);
			} 
		}

		public DateTime CreateDate => content.CreateDate;

		public int CreatorId => content.CreatorId;

		public string CreatorName
		{
			get { throw new NotImplementedException(); }
		}

		public string DocumentTypeAlias => content.ContentType.Alias;

		public int DocumentTypeId => content.ContentType.Id;

		public int GetIndex()
		{
			return 0;
		}

		public IPublishedProperty GetProperty(string alias, bool recurse)
		{
			try
			{
				var c = content;

				var prop = c.Properties[alias];
                var value = TransmogrifyValue(prop.GetValue(), prop.PropertyType);

                while (value == null && recurse)
                {
                    c = Current.Services.ContentService.GetById(c.Key);
                    if (c == null) { break; }
                    prop = c.Properties[alias];
                    value = TransmogrifyValue(prop.GetValue(), prop.PropertyType);
                }

                return new DBProperty(alias, value);
			}
			catch (Exception e)
			{
				return null;
			}
		}

		public static object TransmogrifyValue(object value, PropertyType type)
		{
			if (type?.PropertyEditorAlias == "Umbraco.DropDown")
			{
                var preValues = Current.Services.DataTypeService.GetDataType(type.DataTypeKey);

                return ((Umbraco.Core.PropertyEditors.ValueListConfiguration)preValues.Configuration).Items.FirstOrDefault(x => x.Id.ToString().Equals(value.ToString(), StringComparison.Ordinal))?.Value;

                //var preValue = preValues.IsDictionaryBased ? (preValues.PreValuesAsDictionary.ContainsKey(value.ToString()) ? preValues.PreValuesAsDictionary[value.ToString()] : null) : preValues.PreValuesAsArray.FirstOrDefault(a => a.Id.ToString() == value.ToString());
                //return preValue.Value;

                //if the above fails, then use: ApplicationContext.Current.Services.DataTypeService.GetPreValueAsString(Convert.ToInt32(value));
            } else if (type?.PropertyEditorAlias == "Umbraco.DropDown.Flexible")
			{
				return Newtonsoft.Json.JsonConvert.DeserializeObject<string[]>(value as string).FirstOrDefault();

			}
			return value;
		}

		public IPublishedProperty GetProperty(string alias)
		{
			try
			{

				var property = content.Properties.FirstOrDefault(x => x.Alias == alias);

				if (property != null)
				{
					return new DBProperty(property);
				}


				//Fall back to culture inspecific.
				//TODO: Is there a way to verify if is culture specific ?
				var firstWithValue = content.GetValue(alias);
				if (firstWithValue == null)
				{
					//TODO: Might give wrong results on Save event, but no issues met yet, so keep like this for now
					firstWithValue = content.PublishedCultures.Select(x => content.GetValue(alias, x)).FirstOrDefault(v => v != null);
				}
				return new DBProperty(alias, firstWithValue);
			}
			catch (Exception ex) 
			{
				Serilog.Log.Logger.Error<DBContent>(ex, "Error in DBContent get property", this);
				return null;
			}
		}

		public void SetProperty(string property, object value)
		{
			BackingContent.SetValue(property, value);
		}

		public void SetName(string value)
		{
			BackingContent.Name = value;
		}

		public void Save(bool raiseEvents)
		{
			Current.Services.ContentService.Save(BackingContent, raiseEvents: raiseEvents);
		}

		public void Publish()
		{
			Current.Services.ContentService.SaveAndPublish(BackingContent);
		}

		public T GetRawPropertyValue<T>(string property)
		{
			return BackingContent.GetValue<T>(property);
		}

		public string GetUrl(string culture = null)
		{
			throw new NotImplementedException();
		}

		public PublishedCultureInfo GetCulture(string culture = null)
		{
            throw new NotImplementedException();
            //return content.PublishedCultures.FirstOrDefault(x => x == culture); //TODO: FIND BETTER 
		}

		bool IPublishedContent.IsDraft(string culture)
		{
			throw new NotImplementedException();
		}

		public bool IsPublished(string culture = null)
		{
			if (culture is null)
			{
				return content.Published;
			}
			return content.IsCulturePublished(culture);
		}

		public int Id => content.Id;

		public bool IsDraft => false;

		public PublishedItemType ItemType => PublishedItemType.Content;

		public int Level => content.Level;

		public string Name => content.Name;

		public IPublishedContent Parent
		{
			get
			{
				var parentID = content.ParentId;
				if(parentID == -1) { return null; }
				//verify if this is proper solution
				return new DBContent(Current.Services.ContentService.GetById(parentID));
					
			}
		}

		public string Path
		{
			get { throw new NotImplementedException(); }
		}

        public IEnumerable<IPublishedProperty> Properties
        {
            get { return content.Properties.Select(p => new DBProperty(p.Alias, p.GetValue())).ToArray(); }
        }

        public int SortOrder => content.SortOrder;

		//public int TemplateId => content.Template?.Id ?? -1;

		public DateTime UpdateDate => content.UpdateDate;

		public string Url => "";

		public string UrlName
		{
			get { throw new NotImplementedException(); }
		}

		//public Guid Version => content.Version;

		public int WriterId => content.WriterId;

		public string WriterName
		{
			get { throw new NotImplementedException(); }
		}

		public Guid Key => BackingContent.Key;

		public string UrlSegment => "";

		public int? TemplateId => throw new NotImplementedException();

		public IReadOnlyDictionary<string, PublishedCultureInfo> Cultures {

            get
            {
                if (!content.ContentType.VariesByCulture())
                {
                    return new Dictionary<string, PublishedCultureInfo>();
                }
                if (_cultureInfos != null)
                {
                    return _cultureInfos;
                }

                return _cultureInfos = content.PublishCultureInfos.Values.ToDictionary(x => x.Culture, x => new PublishedCultureInfo(x.Culture, x.Name, UrlSegment, x.Date));
            }
        }

		public IEnumerable<IPublishedContent> ChildrenForAllCultures => Children;

		public object this[string alias]
		{
			get { throw new NotImplementedException(); }
		}
	}

	public class DBDetachedContent : IPublishedContent, ISettableContent
	{
		public const string ncContentTypeAlias = "ncContentTypeAlias";
		private Dictionary<string, object> data;
		public DBDetachedContent(Dictionary<string, object> data)
		{
			this.data = data;
			//required when adding nestedcontent
			if (!data.ContainsKey("key"))
			{
				data.Add("key", Guid.NewGuid());
			}
		}
		public DBDetachedContent(Dictionary<string, object> data, string alias) : this(data)
		{
			data.Add(ncContentTypeAlias, alias);
		}

		public Dictionary<string, object> GetData() { return data; }

		public object this[string alias]
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public IEnumerable<IPublishedContent> Children => Enumerable.Empty<IPublishedContent>();

		public IEnumerable<IPublishedContent> ContentSet
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		public IPublishedContentType ContentType
		{
			get
			{
				return new PublishedContentType(DocumentTypeId, DocumentTypeAlias, PublishedItemType.Content,
					Enumerable.Empty<string>(),
					Enumerable.Empty<PublishedPropertyType>(),
					ContentVariation.Culture);
			}
		}

		public DateTime CreateDate
		{
			get { return DateTime.MinValue; }
		}

		public int CreatorId
		{
			get
			{
				return 0;
			}
		}

		public string CreatorName
		{
			get
			{
				return null;
			}
		}

		public string DocumentTypeAlias => data.ContainsKey(ncContentTypeAlias) ? (data[ncContentTypeAlias] ?? "").ToString() : null;

		public int DocumentTypeId
		{
			get
			{
				return data.TryGetValue("id", out var id) ? Int32.Parse(id.ToString()) : -1;
			}
		}

		public int Id
		{
			get
			{
				return 0;
			}
		}

		public bool IsDraft
		{
			get
			{
				return false;
			}
		}

		public PublishedItemType ItemType => PublishedItemType.Content;

		public int Level
		{
			get
			{
				return 0;
			}
		}

		public string Name
		{
			get
			{
				return "TODO";//GetProperty("name").Value?.ToString();
			}
		}

		public IPublishedContent Parent
		{
			get { return null; }
		}

		public string Path
		{
			get { return null; }
		}

		public ICollection<IPublishedProperty> Properties
		{
			get { return data.Select(p => new DBProperty(p.Key, p.Value)).ToArray(); }
		}

		public int SortOrder
		{
			get
			{
				return 0;
			}
		}

		public int TemplateId
		{
			get
			{
				return 0;
			}
		}

		public DateTime UpdateDate
		{
			get
			{
				return DateTime.MinValue;
			}
		}

		public string Url
		{
			get
			{
				return "";
			}
		}

		public string UrlName
		{
			get
			{
				return "";
			}
		}

		public Guid Version
		{
			get
			{
				return Guid.Empty;
			}
		}

		public int WriterId
		{
			get
			{
				return 0;
			}
		}

		public string WriterName
		{
			get
			{
				return null;
			}
		}

		public string UrlSegment => throw new NotImplementedException();

		int? IPublishedContent.TemplateId => throw new NotImplementedException();

		public IReadOnlyDictionary<string, PublishedCultureInfo> Cultures => throw new NotImplementedException();

		public Guid Key => throw new NotImplementedException();

		IEnumerable<IPublishedProperty> IPublishedElement.Properties => throw new NotImplementedException();

		public IEnumerable<IPublishedContent> ChildrenForAllCultures => throw new NotImplementedException();

		public int GetIndex()
		{
			throw new NotImplementedException();
		}

		public IPublishedProperty GetProperty(string alias)
		{
			//todo - caching...

			object value = data.ContainsKey(alias) ? data[alias] : null;
			if(!string.IsNullOrEmpty(DocumentTypeAlias))
			{
                var doctype = Current.Services.ContentTypeService.Get(DocumentTypeAlias);
                var proptype = doctype.PropertyTypes.FirstOrDefault(p => p.Alias == alias);
				value = DBContent.TransmogrifyValue(value, proptype);
				//value = DBContent.TransmogrifyValue(value, null);
			}
			return new DBProperty(alias, value);
		}

		public IPublishedProperty GetProperty(string alias, bool recurse)
		{
			return GetProperty(alias);
		}

		public void Save(bool raiseEvents)
		{
			throw new InvalidOperationException("Detached content can not be saved directly.");
		}

		public void Publish()
		{
			throw new InvalidOperationException("Detached content can not be published directly.");
		}

		public void SetName(string value)
		{
			data["name"] = value;
		}

		public void SetProperty(string property, object value)
		{
			data[property] = value;
		}

		public T GetRawPropertyValue<T>(string property)
		{
			return data.ContainsKey(property) ? ((T)data[property]) : default(T);
		}

		public string GetUrl(string culture = null)
		{
			return Url;
		}

		public PublishedCultureInfo GetCulture(string culture = null)
		{
			//TODO: Checkup
			return new PublishedCultureInfo(culture, culture, UrlSegment, DateTime.UtcNow);
		}

		bool IPublishedContent.IsDraft(string culture)
		{
			return false;
		}

		public bool IsPublished(string culture = null)
		{
			return true;
		}
	}
	public class DBPropertyType : IPublishedPropertyType
	{
		PropertyType prop;

		public DBPropertyType(PropertyType _prop)
		{
			prop = _prop;
		}

		public IPublishedContentType ContentType => throw new NotImplementedException();

		public PublishedDataType DataType => throw new NotImplementedException();

		public string Alias => prop.Alias;

		public string EditorAlias => prop.PropertyEditorAlias;

		public bool IsUserProperty => throw new NotImplementedException();

		public ContentVariation Variations => prop.Variations;

		public PropertyCacheLevel CacheLevel => throw new NotImplementedException();

		public Type ModelClrType => throw new NotImplementedException();

		public Type ClrType => throw new NotImplementedException();

		public object ConvertInterToObject(IPublishedElement owner, PropertyCacheLevel referenceCacheLevel, object inter, bool preview)
		{
			throw new NotImplementedException();
		}

		public object ConvertInterToXPath(IPublishedElement owner, PropertyCacheLevel referenceCacheLevel, object inter, bool preview)
		{
			throw new NotImplementedException();
		}

		public object ConvertSourceToInter(IPublishedElement owner, object source, bool preview)
		{
			throw new NotImplementedException();
		}

		public bool? IsValue(object value, PropertyValueLevel level)
		{
			throw new NotImplementedException();
		}
	}

	public class DBProperty : IPublishedProperty
	{
		string alias;
		private object value;

		Property prop;

		public DBProperty(string alias, object value)
		{
			this.alias = alias;
			this.value = value;
		}

		public DBProperty(Property _prop)
		{
			prop = _prop;
		}

		public object DataValue
		{
			get { return value; }
		}

		public bool HasValue
		{
			get { return prop?.Values?.Any() ?? value != null; }
		}

		public string PropertyTypeAlias => alias;

		public object Value => value;

		public object XPathValue
		{
			get { throw new NotImplementedException(); }
		}

		//TODO: Maybe we can wrap this aswell ???...
		public IPublishedPropertyType PropertyType => new DBPropertyType(prop.PropertyType);

		public string Alias => throw new NotImplementedException();


		public object GetSourceValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		public object GetValue(string culture = null, string segment = null)
		{
			culture = prop?.PropertyType?.Variations == ContentVariation.Culture ? culture : null; //set culture null if we're not varying by culture
			return prop?.GetValue(culture, segment) ?? value;
			//throw new NotImplementedException();
		}

		public object GetXPathValue(string culture = null, string segment = null)
		{
			throw new NotImplementedException();
		}

		bool IPublishedProperty.HasValue(string culture, string segment)
		{
			return HasValue;
			//throw new NotImplementedException();
		}
	}
}
