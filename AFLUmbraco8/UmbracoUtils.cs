﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using AFL;
using AFL.Utils;
using Lucene.Net.Analysis;
using Lucene.Net.Search;
using Umbraco.Web;
using Umbraco.Web.Security;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core;
using Umbraco.Web.Composing;
using Umbraco.Core.Models;
using Umbraco.Core.Services.Implement;
using Umbraco.Core.Services;

namespace AFLUmbraco8
{
	public static class UmbracoUtils
	{
		public static UmbracoHelper UmbracoHelper
		{
			get
			{
				const string key = "MemoSupport-umbracoHelper";
				UmbracoHelper instance = (UmbracoHelper)HttpContext.Current?.Items[key];
				if(instance == null)
				{
					//Not sure if this is still needed after umbraco8
					//if (UmbracoContext.Current == null) // We need this null check when outside HttpContext. Like e.g. Umbraco Examine indexing.
					//{
					//	var dummyContext = new HttpContextWrapper(new HttpContext(new SimpleWorkerRequest("/", string.Empty, new StringWriter())));
					//	//UmbracoContext.EnsureContext(dummyContext, ApplicationContext.Current, new WebSecurity(dummyContext, ApplicationContext.Current), false);
					//}

					instance = Current.UmbracoHelper;
					if(HttpContext.Current != null)
					{
						HttpContext.Current.Items[key] = instance;
					}
				}
				return instance;
			}
		}

		public static MembershipHelper UmbracoMembershipHelper
		{
			get
			{
				const string key = "MemoSupport-umbracoMembershipHelper";
				MembershipHelper instance = (MembershipHelper)HttpContext.Current?.Items[key];
				if (instance == null)
				{
					// Not sure if still needed after Umbraco 8
					//if (UmbracoContext.Current == null) // We need this null check when outside HttpContext. Like e.g. Umbraco Examine indexing.
					//{
					//	var dummyContext = new HttpContextWrapper(new HttpContext(new SimpleWorkerRequest("/", string.Empty, new StringWriter())));
					//	//UmbracoContext.EnsureContext(dummyContext, ApplicationContext.Current, new WebSecurity(dummyContext, ApplicationContext.Current), false);
					//}

					instance = Current.Factory.GetInstance<MembershipHelper>();// new MembershipHelper(UmbracoContext.Current);
					if (HttpContext.Current != null)
					{
						HttpContext.Current.Items[key] = instance;
					}
				}
				return instance;
			}
		}

		public static void EnsureCultureStored()
		{
			if (HttpContext.Current != null)
			{
				HttpContext.Current.Session["__CultureName"] = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
			}
		}

		//call this from surface controllers and ajax stuff, to ensure that we don't loose the current users current culture :)
		public static void FixCulture()
		{
			//var culture = (string)HttpContext.Current?.Session?["__CultureName"] ?? UmbracoHelper.TypedContentAtRoot()?.FirstOrDefault()?.GetCulture()?.Name ?? "en-US"; //todo: does the culture through typedcontentatroot work? otherwise hardcode again as: "da-DK"
			var culture = (string)HttpContext.Current?.Session?["__CultureName"] ?? Current.VariationContextAccessor.VariationContext.Culture ?? "en-US"; //todo: does the culture through typedcontentatroot work? otherwise hardcode again as: "da-DK"
			System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(culture);
		}

		public static string Translate(string key)
		{
			var val = UmbracoHelper.GetDictionaryValue(key);
			return string.IsNullOrEmpty(val) ? key : val;
		}
		/// <summary>
		/// Get's or creates a DocumentType
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="name"></param>
		/// <param name="docType"></param>
		/// <param name="cs"></param>
		/// <param name="cts"></param>
		/// <param name="publish"></param>
		/// <param name="defaultCulture">If creating and desired doctype varies by culture, this must be supplied</param>
		/// <returns></returns>
		public static IContent GetChildOrCreate(IContent parent, string name, string docType, IContentService cs, bool publish = true, string defaultCulture = "")
		{
			var child = cs.GetPagedChildren(parent.Id, 0, int.MaxValue, out long tot).FirstOrDefault(c => (c.ContentType.Alias == docType) && (c.Name.Equals(name, StringComparison.OrdinalIgnoreCase)));
			if (child != null)
			{
				return child;
			}
			var result = cs.Create(name, parent, docType);
			if (result.ContentType.VariesByCulture())
			{
				if (defaultCulture.IsNullOrWhiteSpace())
				{
					throw new ArgumentException("Desired conten type varies by culture, please supply a default culture on create");
				}
				result.SetCultureName(name, defaultCulture);
			}
			//var result = cs.CreateAndSave(name,parent,docType);
			cs.Save(result, raiseEvents: false);
			if (publish) { cs.SaveAndPublish(result);}
			return result;
		}
		//public static bool HasChild(IContent parent, string docType)
		//{
		//	return parent.Children.Where(c => (c.ContentType.Alias == docType)).Any();
		//}

		public static string GetFileSize(double sizeInBytes)
		{
			string result = "0 KB";

			if (sizeInBytes > 0)
			{
				double sizeDouble = sizeInBytes;
				string[] units = new string[] { "B", "KB", "MB", "GB" };
				int i = 0;
				while ((sizeDouble > 1024) && (i < units.Count() - 1))
				{
					sizeDouble /= 1024;
					i++;
				}
				result = string.Format("{0} {1}", sizeDouble.ToString("F1"), units[i]);
			}
			return result;
		}
		
		//make sure the given url starts with http:// unless it's relative, and optionally append any additional path parts while ensuring that each part is separated by exactly one slash.
		public static string GetURLWithHttp(string url, params string[] joinWith)
		{
			url = url ?? ""; //url should never be null, but crashing if it is anyway is not good either....

			if (!string.IsNullOrWhiteSpace(url) && url.IndexOf("/") != 0 && url.IndexOf("http://") != 0 && url.IndexOf("https://") != 0) //if not a relative url, and it does not already have http://
			{
				url = "http://" + url;
			}

			//optionally join with additional parts, removing redundant slashes...
			if (joinWith != null && joinWith.Length > 0)
			{
				for (int i = 0; i < joinWith.Length; i++)
				{
					if (!string.IsNullOrEmpty(joinWith[i])) //if the param is empty, skip it (so we don't get consecutive slashes).
					{
						if (url.LastIndexOf('/') != url.Length - 1) { url = url + "/"; } //make sure the url ends with '/'
						url += joinWith[i].IndexOf('/') == 0 ? joinWith[i].Substring(1) : joinWith[i]; //make sure the additional part does not start with slash, and append it.
					}
				}
			}
			return url;
		}

		//public static List<string> GetSuggestionsFromIndexes(string searchString, string indexFolder, List<string> searchableFields, int numberOfItemsToRetrieve)
		//{
		//	Dictionary<string, float> result = new Dictionary<string,float>();

		//	if (string.IsNullOrWhiteSpace(searchString) || string.IsNullOrWhiteSpace(indexFolder) || searchableFields.Count == 0 || !(numberOfItemsToRetrieve > 0))
		//	{
		//		return new List<string>();
		//	}

		//	Analyzer std = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29);
		//	Lucene.Net.Store.Directory directory = Lucene.Net.Store.FSDirectory.Open(new System.IO.DirectoryInfo(indexFolder));

		//	foreach (string field in searchableFields)
		//	{
		//		foreach (KeyValuePair<string, float> keyValue in GetIndexedExpressions(searchString, indexFolder, field, numberOfItemsToRetrieve, std, directory))
		//		{
		//			if (result.ContainsKey(keyValue.Key.Trim()))
		//			{
		//				result[keyValue.Key.Trim()] += keyValue.Value;
		//			}
		//			else
		//			{
		//				result.Add(keyValue.Key.Trim(), keyValue.Value);
		//			}
		//		}
		//	}

		//	if (result.Count < numberOfItemsToRetrieve)
		//		numberOfItemsToRetrieve = result.Count;

		//	return result.OrderBy(p => p.Key.Length).Select(p => p.Key).ToList().GetRange(0, numberOfItemsToRetrieve);
		//}

		private static Dictionary<string, float> GetIndexedExpressions(string searchString, string indexFolder, string fieldName, int numberOfItemsToRetrieve, Analyzer stdAnalyzer, Lucene.Net.Store.Directory directory)
		{
			string[] stringsToReplace = new string[] { ".", ",", "\"", ":", ";", "*", "&", "-", "=", "?", "/", "\\", "(", ")", "{", "}", "§", "#229", ((char)10).ToString(), ((char)13).ToString(), ((char)160).ToString(), ((char)8211).ToString() };
			Dictionary<string, float> result = new Dictionary<string, float>();

			string firstWord = searchString;
			int indexOfSpace = 0;
			if ((indexOfSpace = searchString.IndexOf(' ')) > 0)
			{
				firstWord = searchString.Substring(0, indexOfSpace);
			}

			Lucene.Net.QueryParsers.QueryParser parser = new Lucene.Net.QueryParsers.QueryParser(Lucene.Net.Util.Version.LUCENE_29, fieldName, stdAnalyzer);
			Lucene.Net.Search.Query qry = parser.Parse(firstWord + "*");

			Lucene.Net.Search.Searcher srchr = new Lucene.Net.Search.IndexSearcher(Lucene.Net.Index.IndexReader.Open(directory, true));

			TopScoreDocCollector cllctr = TopScoreDocCollector.Create(1000, true);
			srchr.Search(qry, cllctr);

			ScoreDoc[] hits = cllctr.TopDocs().ScoreDocs;
			for (int i = 0; i < hits.Length; i++)
			{
				int docId = hits[i].Doc;
				Lucene.Net.Documents.Document doc = srchr.Doc(docId);
				//TODO: Get helper extension to work.
				//HtmlHelper.StripHtml("", new string[] { }));
			//string phrase = HtmlHelperRenderExtensions.StripHtml(HtmlHelper, doc.Get(fieldName), new string[] { });
				string phrase = "";
				throw new NotImplementedException();
				int index = FirstIndex(phrase, searchString);
				if (index == -1)
				{
					continue;
				}

				string backupPhrase = phrase;
				phrase = phrase.Substring(index, phrase.Length - index);

				if (phrase.IndexOf('.') > -1)
				{
					phrase = phrase.Substring(0, phrase.IndexOf('.'));
				}

				foreach (string s in stringsToReplace)
				{
					phrase = phrase.Replace(s, "");
				}
				phrase = phrase.Replace("#230", "æ");

				foreach (string term in GetExpressionsFromPhrase(phrase))
				{
					if (term.Length < searchString.Length)
						continue;

					string lowerCaseTerm = term.ToLower();
					if (result.ContainsKey(lowerCaseTerm))
					{
						result[lowerCaseTerm] += 1;
					}
					else
					{
						result.Add(lowerCaseTerm, 1);
					}
				}
			}

			return result;
		}

		public static int FirstIndex(string str, string substr)
		{
			if (string.IsNullOrWhiteSpace(str) || string.IsNullOrWhiteSpace(substr))
			{
				throw new ArgumentException("String or substring is not specified.");
			}
			if (str.IndexOf(substr) == 0)
				return 0;

			return str.IndexOf(" " + substr);
		}

		private static List<string> GetExpressionsFromPhrase(string phrase)
		{
			List<string> result = new List<string>();

			string tempString = "";

			foreach (string word in phrase.Split(' '))
			{
				tempString += word + " ";
				result.Add(tempString.Trim());
			}

			return result;
		}
		/// <summary>
		/// Calculates the difference between now and date, returns it as a human readable string
		/// </summary>
		/// <param name="date"></param>
		/// <param name="number">The number of minutes, hours, days, weeks or months of difference</param>
		/// <returns>FewSecondsAgo, OneMinuteAgo, MinutesAgo, OneHourAgo, HourseAgo, ADayAgo, DaysAgo, AWeekAgo, WeeksAgo, AMonthAgo, MonthsAgo, AYearAgo, YearsAgo</returns>
		public static string DateDifferenceFuzzy(DateTime date, out double number)
		{
			number = 0;
			var diff = (DateTime.UtcNow - date.ToUniversalTime()).TotalSeconds;

			if (diff < 60)
			{
				return "FewSecondsAgo";
			}

			diff = Math.Floor(diff / 60); // minutes
			if (diff <= 60)
			{
				if (diff == 1)
				{
					return "OneMinuteAgo";
				}
				else
				{
					number = diff;
					return "MinutesAgo";
				}
			}

			diff = Math.Floor(diff / 60); // hours
			if (diff <= 24)
			{
				if (diff == 1)
				{
					return "OneHourAgo";
				}
				else
				{
					number = diff;
					return "HoursAgo";
				}
			}

			diff = Math.Floor(diff / 24);
			if (diff <= 7) // days
			{
				if (diff == 1)
				{
					return "ADayAgo";
				}
				else
				{
					number = diff;
					return "DaysAgo";
				}
			}
			if (diff <= 30) // weeks
			{
				var w = Math.Floor(diff / 7);
				if (w == 1)
				{
					return "AWeekAgo";
				}
				else
				{
					number = w;
					return "WeeksAgo";
				}
			}
			if (diff <= 365) // months
			{
				var m = Math.Floor(diff / 30);
				if (m == 1)
				{
					return "AMonthAgo";
				}
				else
				{
					number = m;
					return "MonthsAgo";
				}
			}

			diff = Math.Floor(diff / 365); // years
			if (Math.Floor(diff) == 1)
				return "AYearAgo";

			number = diff;
			return "YearsAgo";
		}

		/// <summary>
		/// deprecated. use BeginModelForm instead.
		/// </summary>
		//public static MvcForm BeginAppstractForm<TModel>(this HtmlHelper<TModel> html, string action, IAFLModel model, Type surfaceController, string classes = null, string method="post")
		//{
		//	var attributes = new Dictionary<string, object>();
		//	if(!classes.IsNullOrEmpty()) { attributes.Add("class", classes); }
		//	attributes.Add("method", method);

		//	var additionalRouteVals = new { ajaxPartialModelID = model.UID, ajaxPartialModelClass = model.GetType().FullName };
		//	return html.BeginUmbracoForm(action, surfaceController, additionalRouteVals, attributes);
		//}

		//public static Umbraco.Core.Models.PublishedContent.IPublishedContent GetNodeFromPostbackContext(Type expectedModelType)
		//{
		//	/*
		//	* * This is only applicable for models that represent pages, or that are otherwise created directly as a response to a clientside request (e.g. through ajax requests).
		//	*
		//	* In the normal case, the default controller will be fed with a RenderModel wich contains the correct IPublishedContent, and FrameworkMain.CreateModelInstance() is invoked to
		//	* create the correct model with the correct IPublishedContent.
		//	* However, we allow models to be created with a null IPublishedContent, if the HttpContext Request object contains information that allows us to fetch
		//	* the correct node automatically. This is needed in the case of postback to Surface Controllers, where the model will be constructed behind the scenes by MVC.
		//	*
		//	* If a model is created without a node, and without fitting data in "uformpostroutevals" (or "ufprt"), an exception will be thrown (below).
		//	* */
		//	var umbracoRoute = HttpContext.Current.Request["uformpostroutevals"].IsNullOrWhiteSpace() ? (HttpContext.Current.Request["ufprt"].IsNullOrWhiteSpace() ? null : HttpContext.Current.Request["ufprt"].DecryptWithMachineKey()) : HttpContext.Current.Request["uformpostroutevals"].DecryptWithMachineKey();
		//	var query = HttpUtility.ParseQueryString(umbracoRoute);
		//	var postBackNodeId = query["ajaxPartialModelID"];
		//	var postBackModelName = query["ajaxPartialModelClass"];
				
		//	var node = string.IsNullOrEmpty(postBackNodeId) ? null : UmbracoHelper.TypedContent(postBackNodeId);
		//	if (node == null || /*node.DocumentTypeAlias != ModelDocumentTypeAlias*/ expectedModelType.FullName != postBackModelName)
		//	{
		//		throw new InvalidOperationException("Model can not be created: no content node provided, and postback info is missing or invalid.");
		//	}
		//	return node;
		//}
	}

	[Flags]
	public enum TranslationFlag { Default, EmptyWhenNotFound };

	public static class Translation
	{


		public static string Get(string key, TranslationFlag flags = TranslationFlag.Default)
		{
			var val = UmbracoUtils.UmbracoHelper.GetDictionaryValue(key) ?? "";

			if (string.IsNullOrWhiteSpace(val) && !flags.HasFlag(TranslationFlag.EmptyWhenNotFound))
			{
				return "¤" + key + "¤";
			}
			return val;
		}
	}

}