﻿using AFL;
using AFL.Core;
using AFLUmbraco8.Models;
using AFLUmbraco8.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Composing;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace AFLUmbraco8
{
	public class AFLUmbraco : IAFLImplementation
	{
		public void OnStartup()
		{
			var handler = new IndexingHandler();
			handler.OnAFLStarted();
		}

		public string GetAlias(object node)
		{
			//TODO: Check up
			//return Current.Services.ContentService.GetById(((IPublishedContent)node).Id).ContentType.Alias;
			return (node as IPublishedContent)?.ContentType?.Alias;
		}

		public string GetID(object node)
		{
			//CHECK NESTED CONTENT
			//todo: FIX THIS
			int id = (node as IPublishedContent).Id;
			if (id == 0 && (node.GetType().Name.Equals("DetachedPublishedContent", StringComparison.Ordinal) || node.GetType().Name.Equals("DBDetachedContent", StringComparison.Ordinal))) // todo - more robust identification. the latter one is for the AFL IContent support.
			{
				return Guid.NewGuid().ToString(); // give it a unique id, to prevent caching collisions. as of now, there is no way to distinguish nested content nodes, so this is the best we can do.
			}
			return id.ToString();
		}

		public object GetNode(string nodeID)
		{
			return UmbracoUtils.UmbracoHelper.Content(nodeID);
		}
		public object GetNode(Guid nodeID)
		{
			return UmbracoUtils.UmbracoHelper.Content(nodeID);
		}

		public IEnumerable<object> GetTopLevelNodes(AFLTypeInfo nodeType = null)
		{
			var cts = Current.Services.ContentTypeService;
			return UmbracoUtils.UmbracoHelper.ContentAtRoot().Where(n => nodeType == null || cts.Get(n.Id).Alias.Equals(nodeType.Alias,StringComparison.Ordinal));
		}

		public object GetDBNode(string nodeID)
		{
            IContent content = null;
            try
            {
                var id = Int32.Parse(nodeID);
                content = Current.Services.ContentService.GetById(id);

            }
            catch (Exception)
            {
            }
            if (content == null) { return null; }
			return new DBContent(content); 
		}
		public object NativeDBTypeToDBNode(object dbType)
		{
			if (dbType == null) { return null; }
			return new DBContent((IContent)dbType);
		}

		public object CreateMemoryBackedNode(IAFLModel copyFrom = null)
		{
			var node = new DBDetachedContent(new Dictionary<string, object>());

			var copyFromModel = copyFrom as AFLModelBase;
			if (copyFromModel != null)
			{
				var aflType = Registry.Current.GetInfo(copyFromModel.GetType());
				node.SetName(copyFromModel.Name);
				node.SetProperty("ncContentTypeAlias", aflType.Alias);
				var properties = aflType.AllPropertyNames;
				foreach (var property in properties)
				{
					var cmsName = aflType.GetProperty(property).CMSName;
					if (!string.IsNullOrEmpty(cmsName))
					{
						//node.SetProperty(cmsName, copyFromModel.Content.GetProperty(cmsName).DataValue);
					}
				}
			}

			return node;
		}

		public object GetParentNode(object node)
		{
			return ((IPublishedContent)node).Parent;
		}

		public IEnumerable<PropertyTypeData> GetPropertiesForAlias(string alias)
		{
			if (string.Equals(alias, "File", StringComparison.Ordinal) || string.Equals(alias, "Image", StringComparison.Ordinal))//these don't exist in ContentTypeService, but should still be mapped.
			{
				return Current.Services.MediaTypeService.Get(alias)?.CompositionPropertyTypes.Select(cpt => new PropertyTypeData(cpt.Alias, cpt.Name));
			}

			return Current.Services.ContentTypeService.Get(alias)?.CompositionPropertyTypes.Select(cpt => new PropertyTypeData(cpt.Alias, cpt.Name));
		}

		public string InferAliasFromTypeName(string name)
		{
			var type = Current.Services.ContentTypeService.Get(name);
			if (type != null)
			{
				return type.Alias; //return correctly capitalized version.
			}
			return null;
		}

		public string InferViewName(IAFLModel model)
		{
			if (model is AFLModelBase)
			{
				//return ((AFLModelBase)model).Content.GetTemplateAlias();
			}
			return null;
		}

		public IMemoSupport MakeMemoSupport()
		{
			return new UmbracoMemoSupport();
		}



		public Type NodeType => typeof(IPublishedContent);
	}
}
