﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFLUmbraco8
{
	public class InMemoryMissingDocumentTypeProperties
	{
		private static List<MissingPropertyDescriptor> missingDocumentTypeProperties = new List<MissingPropertyDescriptor>();
		public static List<MissingPropertyDescriptor> GetInstance()
		{
			return missingDocumentTypeProperties;
		}
	}
	public class MissingPropertyDescriptor
	{
		public MissingPropertyDescriptor(string typeAlias, string propertyName, Type propertyType, string modelName)
		{
			TypeAlias = typeAlias;
			ModelName = modelName;
			PropertyName = propertyName;
			PropertyType = propertyType;
		}
		public string TypeAlias { get; }
		public string ModelName { get; }
		public string PropertyName { get; }
		public Type PropertyType { get; }

	}
}
