﻿using AFL;
using Examine;
using Examine.LuceneEngine.Directories;
using Examine.LuceneEngine.Providers;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Composing;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using Umbraco.Examine;
using Umbraco.Web.Search;

namespace AFLUmbraco8.Search
{
    public class CustomIndexCreator : LuceneIndexCreator, IUmbracoIndexesCreator
    {
        protected IProfilingLogger ProfilingLogger { get; }
        protected ILocalizationService LanguageService { get; }
        public CustomIndexCreator(IProfilingLogger profilingLogger, ILocalizationService languageService)
        {
            ProfilingLogger = profilingLogger ?? throw new System.ArgumentNullException(nameof(profilingLogger));
            LanguageService = languageService ?? throw new System.ArgumentNullException(nameof(languageService));
        }
        public override IEnumerable<IIndex> Create()
        {
            foreach (var item in Registry.Current.SearchDefinitions.SearchIndexes)
            {
                yield return CreateCustomIndex(item.IndexName, item.PublishedValuesOnly, item.ParentID, item.IncludeItemTypes, item.ExcludeItemTypes);
            }
        }

        IIndex CreateCustomIndex(string indexName, bool publishedValuesOnly, int? parentID, string[] includeItemTypes, string[] excludeItemTypes)
        {
            return new UmbracoContentIndex(
                indexName,
                CreateFileSystemLuceneDirectory(indexName),
                new UmbracoFieldDefinitionCollection(), //TODO: figure out if we need our own field collection
                new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30),
                ProfilingLogger,
                LanguageService,
                new ContentValueSetValidator(
                    publishedValuesOnly,
                    parentID,
                    includeItemTypes,
                    excludeItemTypes
                ));
        }


    }
    [RuntimeLevel(MinLevel = Umbraco.Core.RuntimeLevel.Run)]
    public class CustomExamineComponent : IComponent
    {
        private readonly IExamineManager _examineManager;
        private readonly CustomIndexCreator _indexCreator;
        private readonly ILogger _logger;

        public CustomExamineComponent(IExamineManager examineManager, CustomIndexCreator indexCreator, ILogger logger)
        {
            _examineManager = examineManager;
            _indexCreator = indexCreator;
            _logger = logger;
        }


        public void Initialize()
        {
            //foreach (var index in _indexCreator.Create())
            //{
            //    if(_examineManager.TryGetIndex(index.Name, out var x)){
            //        continue;
            //    }
            //    _examineManager.AddIndex(index);
            //}
            //DirectoryFactory.DefaultLockFactory = d => new NoPrefixSimpleFsLockFactory(d);
        }

        public void InitIndexes()
        {
            DirectoryFactory.DefaultLockFactory = d => new NoPrefixSimpleFsLockFactory(d);
            foreach (var index in _indexCreator.Create())
            {
                //not such a cool way to skip if Someone adds ExternalIndex or other already existsing indexes. 
                //TODO: Find better solution
                if (_examineManager.TryGetIndex(index.Name, out var x))
                {
                    continue;
                }
                _examineManager.AddIndex(index);
            }
            UnlockLuceneIndexes(_examineManager, _logger);
        }
        internal void UnlockLuceneIndexes(IExamineManager examineManager, ILogger logger)
        {
            foreach (var luceneIndexer in examineManager.Indexes.OfType<LuceneIndex>())
            {
                //We now need to disable waiting for indexing for Examine so that the appdomain is shutdown immediately and doesn't wait for pending
                //indexing operations. We used to wait for indexing operations to complete but this can cause more problems than that is worth because
                //that could end up halting shutdown for a very long time causing overlapping appdomains and many other problems.
                luceneIndexer.WaitForIndexQueueOnShutdown = false;

                //we should check if the index is locked ... it shouldn't be! We are using simple fs lock now and we are also ensuring that
                //the indexes are not operational unless MainDom is true
                var dir = luceneIndexer.GetLuceneDirectory();
                if (IndexWriter.IsLocked(dir))
                {
                    logger.Info(typeof(CustomExamineComponent), "Forcing index {IndexerName} to be unlocked since it was left in a locked state", luceneIndexer.Name);
                    IndexWriter.Unlock(dir);
                }
            }
        }


        public void Terminate()
        {

        }

    }
}

