﻿using AFL;
using AFL.Core;
using AFL.Search;
using AFL.Utils;
using AFLUmbraco8.Models;
using Examine;
using Examine.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Web;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Umbraco.Core.Composing;
using Umbraco.Core;

namespace AFLUmbraco8.Search
{
	class IndexingHandler
	{
		public const string NotAllowedChars = "[^a-zA-Z0-9_æøåÆØÅ]+";

		public void OnAFLStarted()
		{
			//Initialize the Cusom added components.
			((CustomExamineComponent)Current.Factory.GetInstance(typeof(CustomExamineComponent))).InitIndexes();

			foreach (var provider in ExamineManager.Instance.Indexes)
			{
				var searchIndex = Registry.Current.SearchDefinitions.SearchIndexes.FirstOrDefault(index => index.IndexName == provider.Name);
				if (searchIndex != null)
				{
					((BaseIndexProvider)provider).TransformingIndexValues += (sender, e) => AddPageToIndex(sender, e, searchIndex);
				}
			}
		}

		private bool IndexAsRenderablePage(AFLTypeInfo info)
		{
			return info.Renderable || info.HasAttribute<IndexedAsRenderablePage>();
		}

		private bool ShouldBeIndexed(AFLModelBase model, SearchIndex index)
		{
			var info = model == null ? null : Registry.Current.GetInfo(model);
			if (info == null || Registry.Current.SearchDefinitions.PreventIndexing(model, index) || Registry.Current.SearchDefinitions.PreventIndexing(model.Content, index) || !IndexAsRenderablePage(info) || info.HasAttribute<DisableIndexingAttribute>())
			{
				return false;
			}
			var onlyInIndexes = info.GetAttribute<LimitIndexingAttribute>()?.SearchIndexIDs;
			return onlyInIndexes == null || onlyInIndexes.Contains(index.ID);
		}

		public void AddPageToIndex(object sender, IndexingItemEventArgs e, SearchIndex index)
		{


			var pageEntity = Registry.Current.GetDBBackedModel<AFLModelBase>(e.ValueSet.Id.ToString());

			bool shouldBeIndexed = ShouldBeIndexed(pageEntity, index);
			if (!shouldBeIndexed)
			{
				return;
			}
			//SearchDefinitions.Boosts.Keys
			var languages = Current.Services.LocalizationService.GetAllLanguages().Select(x => x.CultureInfo.Name);
			var builders = SearchDefinitions.Boosts.Keys.ToDictionary(k => k.ToString(), k => new StringBuilder());
			foreach (var lang in languages)
			{
				foreach (var boost in SearchDefinitions.Boosts.Keys)
				{
					builders.Add($"{boost.ToString()}_{lang}", new StringBuilder());
				}
			}


			FetchSearchableEntityPropertyText(pageEntity, e.ValueSet.Values, builders, index);
			foreach (var boost in SearchDefinitions.Boosts)
			{
				e.ValueSet.Add(boost.Value.Field, builders[boost.Key.ToString()].ToString());
			}
			foreach (var culture in languages)
			{
				foreach (var boost in SearchDefinitions.Boosts)
				{
					e.ValueSet.Add($"{boost.Value.Field}_{culture}", builders[$"{boost.Key.ToString()}_{culture}"].ToString());
				}
			}

			Registry.Current.SearchDefinitions.AddCustomFields(index, pageEntity, e.ValueSet.Values);
		}

		public void FetchSearchableEntityPropertyText(AFLModelBase entity, IDictionary<string, List<object>> fields, Dictionary<string, StringBuilder> builders, SearchIndex index)
		{
			try
			{
				var info = Registry.Current.GetInfo(entity);
				var nodeNameSearchable = info.GetAttribute<NodeNameSearchableAttribute>();
				if (nodeNameSearchable != null)
				{
					var val = entity.Content.ContentType.VariesByCulture()
						? string.Join(" | ", entity.Content.Cultures.Keys.Select(x => (entity.Content as DBContent).BackingContent.GetPublishName(x)).Where(x => !string.IsNullOrEmpty(x)))
						: entity.Name;
					val = Regex.Replace(val, NotAllowedChars, " ");
					builders[nodeNameSearchable.Boost.ToString()].Append($"{val} | ");
				}
				var PropertiesAndInfo = entity.GetType().GetProperties().Select(p => new { prop = p, propInfo = info.GetProperty(p.Name) });
				foreach (var g in PropertiesAndInfo)
				{
					var searchable = g.propInfo.GetAttributes<SearchableAttribute>().FirstOrDefault(s => s.SearchIndexID == SearchDefinitions.AllIndexes || s.SearchIndexID == index.ID);
					if (searchable != null && searchable.Boost != SearchBoost.NotIndexed && (searchable.Boost == SearchBoost.CustomField || builders.ContainsKey(searchable.Boost.ToString())))
					{
						if (!entity.Content.ContentType.VariesByCulture())
						{
							string val = "";
							if (searchable.RawCMSProperty || !g.prop.PropertyType.IsSimpleType())
							{
								val = StringUtils.StripHtml((string)entity.Content.GetProperty(g.propInfo.CMSName).GetValue()).ToLower(); //TODO: Verify that this is a good solution. Don't like casting object to string..
							}
							else
							{
								//No CMS name, Get value from property.
								val = StringUtils.StripHtml(Convert.ToString(g.prop.GetValue(entity))).ToLower();
							}
							if (!string.IsNullOrEmpty(val))
							{
								//replacing, with space is needed when looking for comma separated numbers, otherwise lucene doesn't find anything.
								val = Regex.Replace(val, NotAllowedChars, " ");

								if (searchable.Boost == SearchBoost.CustomField) //custom fields (special use case)
								{
									AddCustomField(fields, searchable, val);
								}
								else
								{
									// normal stuff goes into the builders...
									AddToBuilders(builders, searchable.Boost.ToString(), val);
								}
							}
						} else
						{
							foreach (var culture in entity.Content.Cultures.Keys)
							{
								string val = "";
								if (searchable.RawCMSProperty || !g.prop.PropertyType.IsSimpleType())
								{
									val = StringUtils.StripHtml((string)entity.Content.GetProperty(g.propInfo.CMSName).GetValue(culture)).ToLower(); //TODO: Verify that this is a good solution. Don't like casting object to string..
								}
								else if (g.propInfo.CMSName != null)
								{
									// If we're indexing a property that has a Field in the CMS, get it with a regular getter.
									val = entity.Content.GetProperty(g.propInfo.CMSName).Value<string>(culture);
								}
								else
								{
									val = StringUtils.StripHtml(Convert.ToString(g.prop.GetValue(entity))).ToLower();
								}
								if (!string.IsNullOrEmpty(val))
								{
									//replacing, with space is needed when looking for comma separated numbers, otherwise lucene doesn't find anything.
									val = Regex.Replace(val, NotAllowedChars, " ");

									if (searchable.Boost == SearchBoost.CustomField) //custom fields (special use case)
									{
										AddCustomField(fields, searchable, val);
									}
									else
									{
										AddToBuilders(builders, $"{searchable.Boost}_{culture}", val);
									}

								}
							}
						}
					}
				}
				//entity.Content.Children.Where(c => Registry.Current.GetInfo(c) != null && !IndexAsRenderablePage(Registry.Current.GetInfo(c))).Select(c => Registry.Current.GetModel(c)).ForEach(m => FetchSearchableEntityPropertyText((AFLModelBase)m, fields, builders, index));
				foreach (var m in entity.Content.Children.Where(c => Registry.Current.GetInfo(c) != null && !IndexAsRenderablePage(Registry.Current.GetInfo(c))).Select(c => Registry.Current.GetModel(c)))
				{
					FetchSearchableEntityPropertyText((AFLModelBase)m, fields, builders, index);
				}
			}
			catch (Exception e) when (!System.Diagnostics.Debugger.IsAttached)
			{
				Serilog.Log.Logger.Error(e, $"Exception caught while rebuilding index '{index.IndexName}'", this);
			}
		}

		private void AddToBuilders(Dictionary<string, StringBuilder> builders, string key, string val)
		{
			// make the value search friendly. that is flatten ie æøå to aoa.
			builders[key].Append(val.SearchFriendlyString() + " | "); //seperating the actual fields with '|', so we can split them in the highlighter.
		}

		private static void AddCustomField(IDictionary<string, List<object>> fields, SearchableAttribute searchable, string val)
		{
			if (fields.ContainsKey(searchable.CustomFieldName))
			{
				fields[searchable.CustomFieldName].Add($"{fields[searchable.CustomFieldName]}  |  {val}");
			}
			else
			{
				fields.Add(searchable.CustomFieldName, new List<object>() { $"{fields[searchable.CustomFieldName]}  |  {val}" });
			}
		}
	}

	//public class CustomAnalyzer : StandardAnalyzer
	//{
	//	//public CustomAnalyzer() : base(Lucene.Net.Util.Version.LUCENE_29, new Hashtable()) { }
	//}}
}
