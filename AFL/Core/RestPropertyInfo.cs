﻿using System.Reflection;

namespace AFL.Core
{
	public class RestPropertyInfo
	{
		public RestPropertyInfo(PropertyInfo prop, RestPropertyAttribute attr)
		{
			Property = prop;
			Attribute = attr;
		}

		public PropertyInfo Property { get; private set; }
		public RestPropertyAttribute Attribute { get; private set; }
	}
}