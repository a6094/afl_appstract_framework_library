﻿using System;
using System.Reflection;

namespace AFL.Core
{
	public class RestHandlerInfo
	{
		public RestHandlerInfo(Type type, MethodInfo method, RestAttribute classAttr, RestMethodAttribute attr, string path)
		{
			Type = type;
			Method = method;
			Attribute = attr ?? new RestMethodAttribute(); //create default attribute in case of null method (view rendering of class). this avoids some special cases.
			ClassAttribute = classAttr;
			Path = path;
		}
		public string Path { get; protected set; }
		public Type Type { get; protected set; }
		public MethodInfo Method { get; protected set; }
		public RestMethodAttribute Attribute { get; protected set; }
		public RestAttribute ClassAttribute { get; protected set; }
	}
}