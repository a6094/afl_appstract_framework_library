﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AFL.Core
{
	// This class encapsulates the idea of a property of a class.
	public class AFLTypePropertyInfo
	{
		Attribute[] attributes;

		public AFLTypePropertyInfo(PropertyInfo me, bool isDeclaring, PropertyTypeData[] candidateCMSProperties)
		{
			PropertyInfo = me;
			DeclaredAtThisLevel = isDeclaring;
			CMSName = GetAttribute<PropertyAliasAttribute>()?.Alias ?? char.ToLowerInvariant(me.Name[0]) + me.Name.Substring(1);
			var propertyTypeData = candidateCMSProperties?.FirstOrDefault(ptd => ptd.Alias == CMSName);

			Title = propertyTypeData?.Name;
			if (candidateCMSProperties != null && propertyTypeData  == null)
			{
				CMSName = null;
			}
		}

		public PropertyInfo PropertyInfo { get; private set; }

		public string CMSName { get; private set; }

		public string Title { get; set; }

		public bool DeclaredAtThisLevel { get; private set; }

		public bool IsPrivate => PropertyInfo.GetMethod?.IsPrivate ?? true;

		public AttribT GetAttribute<AttribT>() where AttribT : Attribute
		{
			return GetAttributes<AttribT>().FirstOrDefault();
		}

		public IEnumerable<AttribT> GetAttributes<AttribT>() where AttribT : Attribute
		{
			if(attributes == null)
			{
				var attrs = PropertyInfo.GetCustomAttributes(true).Select(a => a as Attribute).Where(a => a!=null).ToList();
				
				attributes = attrs.ToArray();
			}

			return attributes.OfType<AttribT>();
		}
	}
}