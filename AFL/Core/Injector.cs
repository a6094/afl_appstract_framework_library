﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace AFL.Core
{
	public class RequestInjector :Injector
	{
		//protected readonly IDictionary<Type, object> IsntancicedType = new ConcurrentDictionary<Type, object>();

		public override object GetInstance(Type elementType)
		{
			Func<object> funcCreateInstance;

			if (dictionnary.TryGetValue(elementType, out funcCreateInstance))
			{
				return funcCreateInstance();
			}

			// Probably Throw around here or fetch the type from somewhere in something is defined?
			return null;
		}

	}

	public class PoolInjector : Injector
	{
		public override object GetInstance(Type elementType)
		{
			Func<object> funcCreateInstance;

			if (dictionnary.TryGetValue(elementType, out funcCreateInstance))
			{
				return funcCreateInstance();
			}

			// Probably Throw around here or fetch the type from somewhere in something is defined?
			return null;
		}
	}


	public abstract class Injector
	{
		protected readonly IDictionary<Type, Func<object>> dictionnary = new ConcurrentDictionary<Type, Func<object>>();

		private readonly static PoolInjector _instancePoolInjector      = new PoolInjector();

		public static Injector FromPool()
		{
			return _instancePoolInjector;
		}

		public void RegisterType<TInterface, TImplementation>()
		{
			this.RegisterType(typeof(TInterface), typeof(TImplementation));
		}

		public void RegisterType(Type interfaceType, Type implementationType)
		{
			var constructor = implementationType.GetConstructors()
												.FirstOrDefault(c => c.GetParameters().Count() == 0);

			if (constructor != null)
			{
				this.dictionnary.Add(interfaceType, () => constructor.Invoke(new object[0]));
			}
		}

		public void RegisterType<TInterface>(Func<object> implementationInstance)
		{
			var typeInterface = typeof(TInterface);

			this.dictionnary.Add(typeInterface, implementationInstance);
		}

		public abstract object GetInstance(Type elementType);
	}
}
