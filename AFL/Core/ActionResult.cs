﻿using System.Text;
using System.Web;
using System.Web.Mvc;

namespace AFL.Core
{
	public interface IAFLActionResult
	{
		ActionResult GetMVCAction(ICMSDefaultController controller);
	}

	public class ViewActionResult : IAFLActionResult
	{
		public ViewActionResult(string name, IAFLModel model)
		{
			ViewName = name;
			Model = model;
		}
		/// <summary>
		/// View can be null, in which case the CMS decides.
		/// </summary>
		public string ViewName { get; protected set; }
		public IAFLModel Model { get; protected set; }

		public ActionResult GetMVCAction(ICMSDefaultController controller)
		{
			return ViewName == null ? controller.View(Model) : controller.View("~/Views/" + ViewName + ".cshtml", Model); // hardcoded view path, because otherwise only /views/shared/ etc are searched!?!
		}
	}

	public class PartialActionResult : IAFLActionResult
	{
		public PartialActionResult(string name, IAFLModel model)
		{
			ViewName = name;
			Model = model;
		}
		/// <summary>
		/// View can be null, in which case the CMS decides.
		/// </summary>
		public string ViewName { get; protected set; }
		public IAFLModel Model { get; protected set; }

		public ActionResult GetMVCAction(ICMSDefaultController controller)
		{
			return controller.Partial(ViewName, Model);
		}
	}

	public class RedirectToNodeActionResult : IAFLActionResult
	{
		public RedirectToNodeActionResult(string nodeID, string queryString = null)
		{
			RedirectToNodeID = nodeID;
			QueryString = queryString;
		}

		public string RedirectToNodeID { get; protected set; }
		public string QueryString { get; protected set; }

		public ActionResult GetMVCAction(ICMSDefaultController controller)
		{
			return controller.RedirectToNode(RedirectToNodeID, QueryString);
		}
	}

	public class RedirectToUrlActionResult : IAFLActionResult
	{
		public RedirectToUrlActionResult(string url = null)
		{
			RedirectToUrl = url ?? HttpContext.Current.Request.RawUrl;
		}

		public string RedirectToUrl { get; protected set; }

		public ActionResult GetMVCAction(ICMSDefaultController controller)
		{
			return controller.RedirectToUrl(RedirectToUrl);
		}
	}

	public class HTMLActionResult : IAFLActionResult
	{
		public HTMLActionResult(string value = "")
		{
			Buffer.Append(value);
		}

		public StringBuilder Buffer { get; private set; } = new StringBuilder();

		public ActionResult GetMVCAction(ICMSDefaultController controller)
		{
			var result = new ContentResult();
			result.Content = Buffer.ToString();
			result.ContentType = "text/html";
			return result;
		}
	}
}
