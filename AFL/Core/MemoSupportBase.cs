﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AFL.Core
{
	public interface IMemoSupport
	{
		TPost MemoImplementation<TPre, TPost>(Func<TPre> func, Func<TPre, TPost> postProcessFunc, TPost fallback, string uid, string callerName, AFLTypePropertyInfo propertyInfo = null);
		void NewRequest();
		bool Load(string cacheKey, out object value);
		void Store(string cacheKey, object value, CacheAttribute attr = null);
		void Flush(string uid, string callerName);
		bool IsSimpleType<T>();
		bool IsEmptyValue(object v, bool considerZeroInt);
		object GetValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse, Injector injector);
		//object GetPropertyValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse);
		bool GetComplexValueFromObject(object objectValue, Type resultType, out object value, object contextNode);
		object EnumerableToTypedArray(Type elementType, IEnumerable<object> list);

		//helper method for making DB models / regular models easier
		IAFLModel GetModel(string id, object contextNode);
		IAFLModel GetModel(Guid guid, object contextNode, bool isMedia = false);
		//helper method for making DB models / regular models easier
		IAFLModel GetModel(object content);

		Injector Injector { get; }
	}

	public abstract class MemoSupportBase : IMemoSupport
	{
		protected static readonly object nullPlaceholder = new object();
		protected const string cachePrefix = "attr-cache";
		private ConcurrentDictionary<string, object> fallbackCache = null;

		public Injector Injector => Injector.FromPool();

		public TPost MemoImplementation<TPre, TPost>(Func<TPre> func, Func<TPre, TPost> postProcessFunc, TPost fallback, string uid, string callerName, AFLTypePropertyInfo propertyInfo = null)
		{
			var cacheKey = cachePrefix + uid + callerName;
			object value = null;

			if (Load(cacheKey, out value))
			{
				return (TPost)value;
			}

			value = func();

			if (postProcessFunc != null)
			{
				if (IsEmptyValue(value, false) && typeof(TPre).IsArray)
				{
					value = Array.CreateInstance(typeof(TPre).GetElementType(), 0);
				}
				value = postProcessFunc((TPre)value);
				if (IsEmptyValue(value, false))
				{
					value = fallback;
				}
			}
			else if (IsEmptyValue(value, false))
			{
				//if there is no specified fallback and TPost is an array type, then use an empty array of TPosts element type as fallabck, so we don't have a special case of null returns.
				value = fallback;
				if (typeof(TPost).IsArray && fallback == null)
				{
					value = Array.CreateInstance(typeof(TPost).GetElementType(), 0);
				}
			}

			Store(cacheKey, value, propertyInfo.GetAttribute<CacheAttribute>());
			return (TPost)value;
		}

		public void NewRequest()
		{
			fallbackCache = new ConcurrentDictionary<string, object>();
		}

		public bool Load(string cacheKey, out object value)
		{
			value = (HttpContext.Current?.Items ?? fallbackCache)?[cacheKey] ?? HttpContext.Current?.Session?[cacheKey] ?? HttpRuntime.Cache[cacheKey];
			if (value == nullPlaceholder)
			{
				value = null;
				return true;
			}

			return value != null;
		}

		public void Flush(string uid, string callerName)
		{
			var cacheKey = cachePrefix + uid + callerName;

			HttpContext.Current?.Items.Remove(cacheKey);
			HttpContext.Current?.Session?.Remove(cacheKey);
			HttpRuntime.Cache?.Remove(cacheKey);
		}

		public void Store(string cacheKey, object value, CacheAttribute attr = null)
		{
			try
			{
				switch (attr?.Caching ?? CacheScope.Request)
				{
					case CacheScope.Request:
						if ((attr?.CacheDurationMinutes ?? 0) != 0) { throw new InvalidOperationException("cache duration not supported for request caching"); }
						(HttpContext.Current?.Items ?? fallbackCache)?.Add(cacheKey, value ?? nullPlaceholder);
						break;
					case CacheScope.Session:
						if (attr.CacheDurationMinutes != 0) { throw new InvalidOperationException("cache duration not supported for session caching"); }
						if (HttpContext.Current != null)
						{
							HttpContext.Current.Session[cacheKey] = value ?? nullPlaceholder;
						}
						else
						{
							fallbackCache?.TryAdd(cacheKey, value ?? nullPlaceholder);
						}
						break;
					case CacheScope.Global:
						var duration = attr.CacheDurationMinutes == 0 ? System.Web.Caching.Cache.NoAbsoluteExpiration : DateTime.UtcNow + TimeSpan.FromMinutes(attr.CacheDurationMinutes);
						HttpRuntime.Cache.Insert(cacheKey, value ?? nullPlaceholder, null, duration, System.Web.Caching.Cache.NoSlidingExpiration);
						break;
					case CacheScope.NotCached:
						if (attr.CacheDurationMinutes != 0) { throw new InvalidOperationException("cache duration not supported when not caching"); }
						break;
				}
			}
			catch (ArgumentException) { /* will happen if the key has already been added. this can happen in reentrant cases when two models use the same node, which is uncool but should probably be allowed */ }
		}

		public bool IsSimpleType<T>()
		{
			return typeof(T) == typeof(string) || typeof(T) == typeof(int) || typeof(T) == typeof(double) || typeof(T) == typeof(bool) || typeof(T) == typeof(DateTime);
		}

		//todo: and then move the DateTime check out to where it needs to be treated as a simple type?
		bool IsSimpleType2<T>()
		{
			var type = typeof(T);
			return type.IsPrimitive
				|| type.IsEnum
				|| type.Equals(typeof(string))
				|| type.Equals(typeof(decimal));
		}

		public bool IsEmptyValue(object v, bool considerZeroInt)
		{
			if (v == null) { return true; }
			if (v is string) { return (string)v == ""; }
			if (v is DateTime) { return (DateTime)v == DateTime.MinValue; }
			if (considerZeroInt && v is int) { return (int)v == 0; }
			if (considerZeroInt && v is double) { return (double)v == 0; }
			return false;
		}

		public abstract bool GetComplexValueFromObject(object objectValue, Type resultType, out object value, object contextNode);
		public abstract object GetValue<T>(IAFLModel model, AFLTypePropertyInfo propertyInfo, string callerName, Type elementType, bool recurse, Injector injector);

		public object EnumerableToTypedArray(Type elementType, IEnumerable<object> list)
		{
			var arr = list.ToArray();
			var typedArray = Array.CreateInstance(elementType, arr.Length);
			if (arr.Any(e => e == null || !elementType.IsAssignableFrom(e.GetType()))) //sanity check, for better error output.
			{
				throw new InvalidOperationException("Error creating list: " + arr.First(e => e == null || !elementType.IsAssignableFrom(e.GetType()))?.GetType().FullName + " is not of type " + elementType.FullName);
			}
			Array.Copy(arr, typedArray, arr.Length);
			return typedArray;
		}

		public abstract IAFLModel GetModel(string id, object contextNode);
		public abstract IAFLModel GetModel(Guid guid, object contextNode, bool isMedia = false);

		public abstract IAFLModel GetModel(object content);
	}
}
