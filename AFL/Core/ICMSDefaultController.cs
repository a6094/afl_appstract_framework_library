﻿using System.Web.Mvc;

namespace AFL.Core
{
	public interface ICMSDefaultController
	{
		ActionResult RedirectToNode(string toNodeID, string queryString = null);
		ActionResult RedirectToUrl(string url);
		ActionResult View(IAFLModel model);
		ActionResult Partial(string viewName, IAFLModel model);
		ActionResult View(string viewName, IAFLModel model = null);

		ActionResult MakeActionResult(IAFLModel model);

		ModelStateDictionary ModelState { get; }
	}
}
