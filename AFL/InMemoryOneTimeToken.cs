﻿using System;
using System.Collections.Concurrent;

namespace AFL
{
	public class InMemoryOneTimeToken
	{
		private static readonly ConcurrentDictionary<string, TokenValue> OneTimeTokens = new ConcurrentDictionary<string, TokenValue>();
		public InMemoryOneTimeToken() 
		{
		}
		public string GenerateOneTimeToken(string id, int validityInSeconds, bool IsRandomNumber = false)
		{
			id = id.ToLowerInvariant();
			string oneTimeToken;
			if (OneTimeTokens.ContainsKey(id))
			{
				OneTimeTokens.TryRemove(id, out TokenValue tokenObject);
				oneTimeToken = tokenObject.GetToken();
			}
			else
			{
				if (IsRandomNumber)
				{
					Random generator = new Random();
					oneTimeToken = generator.Next(0, 999999).ToString("D6");
				}
				else
				{
					oneTimeToken = Guid.NewGuid().ToString().ToLowerInvariant();
				}
			}
			OneTimeTokens.TryAdd(id, new TokenValue(oneTimeToken, validityInSeconds));
			return oneTimeToken;
		}
		public bool ConsumeOneTimeTokenAndValidate(string token, string id)
		{
			var isValid = ValidateOneTimeToken(token, id);
			ConsumeOneTimeToken(id);
			return isValid;
			
		}
		public bool ValidateOneTimeToken(string token, string id)
		{
			id = id.ToLowerInvariant();
			if (OneTimeTokens.TryGetValue(id, out TokenValue value))
			{
				return value.Valid(token);
			}
			return false;
		}
		public void ConsumeOneTimeToken(string id)
		{
			id = id.ToLowerInvariant();
			OneTimeTokens.TryRemove(id, out TokenValue discard);
		}
	}
	public class TokenValue
	{
		public TokenValue(string oneTimeToken, int vaildityInSeconds)
		{
			this.oneTimeToken = oneTimeToken;
			validUntil = DateTime.UtcNow.AddSeconds(vaildityInSeconds);
		}
		private readonly string oneTimeToken;
		private readonly DateTime validUntil;

		internal bool Valid(string oneTimeToken)
		{
			return this.oneTimeToken.ToLowerInvariant() == oneTimeToken.ToLowerInvariant() && DateTime.UtcNow <= validUntil;
		}

		internal string GetToken()
		{
			return oneTimeToken;
		}
	}
}