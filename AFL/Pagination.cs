﻿using System.Collections.Generic;

namespace AFL
{
	public static class Pagination
	{
		/// <summary>
		/// returns a list of page number to show in a pagination link bar. BEWARE: returns null for sugested "..." entries.
		///	Doesnt count first, last and dots - so worst case it returns numDisplayedPages + 4 
		/// </summary>
		/// <returns></returns>
		public static IEnumerable<int?> Render(int currentPage, int pageCount, int numDisplayedPages = 7)
		{
			List<int> visibleSequence = new List<int>();
			int start = currentPage - numDisplayedPages / 2;
			if (currentPage + numDisplayedPages / 2 > pageCount)
			{
				start = start - (currentPage + numDisplayedPages / 2 - pageCount);
			}

			if (start > 1)
			{
				yield return 1;
				if (start > 2)
				{
					yield return null; //indicate sugested '...' entry.
				}
			}
			int index = start;
			while (numDisplayedPages > 0)
			{
				if (index > 0)
				{
					visibleSequence.Add(index);
					numDisplayedPages--;
				}
				index++;
			}

			foreach (int n in visibleSequence)
			{
				if (n <= pageCount)
				{
					yield return n;
				}
			}

			if (index < pageCount + 1)
			{
				if (index < pageCount)
				{
					yield return null;  //indicate sugested '...' entry.
				}
				yield return pageCount;
			}
		}

		/// <summary>
		/// Returns exactly the amount of pages requested in numDisplayedPages
        /// BEWARE: returns null for sugested "..." entries which is counted as page
		/// </summary>
		/// <returns></returns>
		public static IEnumerable<int?> RenderExact(int currentPage, int pageCount, int numDisplayedPages =7)
		{
			List<int> visibleSequence = new List<int>();
			int origRequestedpages = numDisplayedPages;
			bool dotsShown = false;
			numDisplayedPages -= 2;
			int offSet = numDisplayedPages / 2;
			int start = currentPage - offSet;
			if (currentPage + offSet > pageCount)
			{
				start = start - (currentPage + offSet - pageCount);
			}

			if (start > 1)
			{
				numDisplayedPages--;
				yield return 1;
				if (start > 2)
				{
					dotsShown = true;
					numDisplayedPages--;
					yield return origRequestedpages >= pageCount ? (int?)start - 1 : null; //indicate sugested '...' entry.
				}
			}

			int index = dotsShown && pageCount - currentPage > offSet
				? start + 1
				: start;
			while (numDisplayedPages > 0)
			{
				if (index > 0)
				{
					visibleSequence.Add(index);
					numDisplayedPages--;
				}
				index++;
			}

			foreach (int n in visibleSequence)
			{
				if (n <= pageCount)
				{
					yield return n;
				}
			}

			if (index < pageCount + 1)
			{
				if (index < pageCount)
				{
					// Ternary to fix edge case where index is second last entry
					yield return index == pageCount - 1 ? (int?)pageCount - 1 : null;
				}
				yield return pageCount;
			}
		}
	}
}
