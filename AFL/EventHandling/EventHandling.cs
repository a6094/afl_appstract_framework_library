﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFL.EventHandling
{
	public enum CMSEvent { /*Created,*/ Published, Unpublished, Saved, Trashed, Deleted, Moved, /*Creating,*/ Publishing, Unpublishing, Saving, Trashing, Deleting, Moving };

	/// <summary>
	/// Usage:
	/// Specify the event type(s) that should trigger the method.
	/// The method must take excactly one parameter of type CMSEventArgs.
	/// Method will be invoked on a DB model instance.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class CMSEventHandlerAttribute : Attribute
	{
		public readonly CMSEvent[] events;
		public CMSEventHandlerAttribute(params CMSEvent[] events)
		{
			this.events = events;
			if (events == null || events.Length == 0)
			{
				throw new Exception("CMSEventHandlerAttribute requires at least one event type parameter.");
			}
		}
	}

	public class CMSEventArgs
	{
		public CMSEventArgs(CMSEvent eventType, EventArgs nativeEvent, bool canCancel)
		{
			EventType = eventType;
			NativeEvent = nativeEvent;
			CanCancel = canCancel;
		}
		public CMSEvent EventType { get; protected set; }
		public EventArgs NativeEvent { get; protected set; }
		public bool CanCancel { get; protected set; }
		public void Cancel(string message) { 
			Canceled = true; 
			CancelMessage = message;
		}

		//public void CancelSilently()
		//{
		//	Canceled = true;
		//	CancelMessage = $"{EventType.ToString()} has been cancelled by code";
		//}
		public bool Canceled { get; protected set; }
		public string CancelMessage { get; protected set; }
	}
}
