﻿using System;
using System.Text.RegularExpressions;

namespace AFL
{
	public enum CacheScope { Request, Session, Global, NotCached };

	[AttributeUsage (AttributeTargets.Property, Inherited = true)]
	public class CacheAttribute : Attribute
	{
		public CacheScope Caching { get; set; }

		public int CacheDurationMinutes { get; set; }
	}
	[AttributeUsage (AttributeTargets.Class, Inherited = false)]
	public class NodeNameSearchableAttribute : Attribute
	{
		public NodeNameSearchableAttribute(SearchBoost boost = SearchBoost.Normal)
		{
			Boost = boost;
		}
		public SearchBoost Boost { get; set; }
	}
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public class NodeNameSearchableInherited : NodeNameSearchableAttribute
	{

		public NodeNameSearchableInherited(SearchBoost boost = SearchBoost.Normal) : base(boost)
		{

		}
	}

	public enum SearchBoost { NotIndexed, Low, Normal, High, VeryHigh, Special, CustomField };
	[AttributeUsage (AttributeTargets.Property, Inherited = true)]
	public class SearchableAttribute : Attribute
	{
		public SearchableAttribute(int indexID = -1, SearchBoost boost = SearchBoost.Normal, string customFieldName = null, bool rawCMSProperty = false)
		{
			if((customFieldName!=null) != (boost==SearchBoost.CustomField)) { throw new InvalidOperationException("custom field name should (and should only) be set if boost is set to CustomField"); }
			SearchIndexID = indexID;
			Boost = boost;
			RawCMSProperty = rawCMSProperty;
			CustomFieldName = customFieldName;
		}

		public SearchableAttribute(SearchBoost boost = SearchBoost.Normal, string customFieldName = null, bool rawCMSProperty = false) : this(-1, boost, customFieldName, rawCMSProperty)
		{
		}

		public SearchableAttribute() : this(-1)
		{
		}

		public int SearchIndexID { get; private set; }

		public SearchBoost Boost { get; private set; }

		public string CustomFieldName { get; private set; }

		/// <summary>
		/// If this is set to true, or the return-type is not a simple type (TypeUtils.IsSimpleType),
		/// then the umbraco property will be evaluated directly by the search indexer,
		/// without invoking the model property.
		/// </summary>
		public bool RawCMSProperty { get; set; }
	}

	
	[AttributeUsage (AttributeTargets.Class, Inherited = false)]
	public class DisableBreadcrumbAttribute : Attribute
	{
	}

	[AttributeUsage (AttributeTargets.Class, Inherited = false)]
	public class IndexedAsRenderablePage : Attribute
	{
	}

	[AttributeUsage (AttributeTargets.Class, Inherited = false)]
	public class DisableIndexingAttribute : Attribute
	{
	}

	[AttributeUsage (AttributeTargets.Class, Inherited = false)]
	public class LimitIndexingAttribute : Attribute
	{
		public LimitIndexingAttribute(params int[] searchIndexIDs)
		{
			SearchIndexIDs = searchIndexIDs;
		}

		public int[] SearchIndexIDs { get; private set; }
	}

	[AttributeUsage (AttributeTargets.Class, Inherited = false)]
	public class AliasAttribute : Attribute
	{
		private string alias;

		public AliasAttribute(string alias = null)
		{
			this.alias = alias;
		}

		public string Alias => alias;

		/// <summary>
		/// Use this to indicate that the class is meant to override another (specified class) in the alias -> entity type map.
		/// </summary>
		public Type Override { get; set; }
	}

	[AttributeUsage (AttributeTargets.Property, Inherited = false)]
	public class PropertyAliasAttribute : Attribute
	{
		private string alias;

		public PropertyAliasAttribute(string alias = null)
		{
			this.alias = alias;
		}

		public string Alias => alias;
	}

	[AttributeUsage (AttributeTargets.Class, Inherited = false)]
	public class RenderableAttribute : Attribute
	{
	}

	/// <summary>
	/// Specifies that the decorated class can be used with Rest.
	/// The optional name will be used as path for the rest url. falls back to the class name.
	/// Note that the name can contain slashes, so that the resulting path can be any arbitrary depth.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public class RestAttribute : Attribute
	{
		public RestAttribute(string name = null)
		{
			if(name != null) //allow null, since it will fall back to method name, but otherwise, check the format for illegal chars.
			{
				name = name.Trim('/');
				if(!Regex.IsMatch(name, "^[a-zA-Z0-9_\\-\\./]+$"))
				{
					throw new InvalidOperationException("Invalid rest path name: " + name);
				}
			}
			Name = name;
		}

		public string Name { get; protected set; }

		/// <summary>
		/// If true, this class can be instantiated and used for rendering with a view, as specified in the route parameter of the rest request.
		/// For this use-case, no RestMethod is needed.
		/// </summary>
		public bool RestRenderable { get; set; }

		/// <summary>
		/// Mark this model as submittable.
		/// This can be used together with a rest handler method which is marked as ModelFormSubmitHandler, or it can be used
		/// without a method. If using without a handler method, you should normally combine with RestRenderable, except if it's a GET submit and
		/// the model is just some component on the rendered page. In that case you don't want a redirect, and you just want the regular rendering to proceed
		/// after the component model has been filled with request data, and in that case ModelFormSubmittable makes sense without a handler method and without RestRenderable.
		/// </summary>
		public bool ModelFormSubmittable { get; set; }
	}

	/// <summary>
	/// Specifies that the decorated method can be used with Rest.
	/// The optional name will be used as path for the rest url. falls back to the method name.
	/// Note that the name can contain slashes, so that the resulting path can be any arbitrary depth.
	/// </summary>
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class RestMethodAttribute : Attribute
	{
		public RestMethodAttribute(string name = null)
		{
			if(name != null) //allow null, since it will fall back to method name, but otherwise, check the format for illegal chars.
			{
				name = name.Trim('/');
				if(!Regex.IsMatch(name, "^[a-zA-Z0-9_\\-\\./]+$"))
				{
					throw new InvalidOperationException("Invalid rest path name: " + name);
				}
			}
			Name = name;
		}

		public string Name { get; protected set; }

		/// <summary>
		/// Indicates that the handler path will not include the class part.
		/// </summary>
		public bool Global { get; set; }

		/// <summary>
		/// Default handler in class. This means its path does not include the method name.
		/// </summary>
		public bool Default { get; set; }

		public bool AllowPost { get; set; } = true;
		public bool AllowGet { get; set; } = true;

		/// <summary>
		/// This should mainly be used for backoffice handlers, where the node might be dynamically obtained clientside.
		/// </summary>
		public bool UnencryptedRoute { get; set; }

		/// <summary>
		/// The model is not created through CreateModelInstance by default, since it should normally instantiate the class containing this method.
		/// This can be inconvenient when, eg. using inheritance, in which case CreateModelDynamically should be set.
		/// This property is ignored if the handler method is static.
		/// </summary>
		public bool CreateModelDynamically { get; set; }

		/// <summary>
		/// If true, the return value of this method will be used for rendering a razor view, as specified by the route argument.
		/// Make sure the return value is void or derived from IAFLModel, or this will have no effect.
		/// </summary>
		public bool UseRouteView { get; set; }

		/// <summary>
		/// ...
		/// </summary>
		public bool CMSRoutable { get; set; }

		/// <summary>
		/// Marks this rest handler method as a model form submit handler.
		/// If the return value is void or derived from IAFLModel, this will render a view, just like if using UseRouteView - alternatively, returning e.g. a redirect will also work.
		/// </summary>
		public bool ModelFormSubmitHandler { get; set; }

		public bool DebugOnly { get; set; }
		public bool HideFromHandlerList { get; set; }

		/// <summary>
		/// Descriptions are shown on the rest root url, in debug mode.
		/// </summary>
		public string Description { get; set; }
	}

	/// <summary>
	/// This attribute must be present, for a property to be used by the rest system.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class RestPropertyAttribute : Attribute
	{
		public RestPropertyAttribute(string shortName)
		{
			ShortName = shortName;
		}

		public RestPropertyAttribute() : this(null)
		{
		}

		/// <summary>
		/// Shorter alias to use in query arguments.
		/// Note that this is not used for forms.
		/// </summary>
		public string ShortName { get; private set; }

		/// <summary>
		/// Note that this is not respected for forms.
		/// </summary>
		public bool Encrypted { get; set; }
	}

	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
	public class RestEncryptedAttribute : Attribute
	{
		
	}

	/// <summary>
	/// Attribute to signify that the interface is an umbraco composition
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface, AllowMultiple = false)]
	public class CompositionAttribute : Attribute
	{

	}
}
