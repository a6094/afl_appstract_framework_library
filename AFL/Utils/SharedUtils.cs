﻿using Newtonsoft.Json;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace AFL.Utils
{
	public class SharedUtils
	{
		public static string CleanPhone(string phone)
		{
			Regex digitsOnly = new Regex(@"[^\d]");
			return digitsOnly.Replace(phone, "");
		}
		public static byte[] UploadFile(string address, string name, string filename, string contentType, Stream fileStream)
		{
			var request = WebRequest.Create(address);
			request.Method = "POST";
			var boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x", NumberFormatInfo.InvariantInfo);
			request.ContentType = "multipart/form-data; boundary=" + boundary;
			boundary = "--" + boundary;

			using (var requestStream = request.GetRequestStream())
			{
				var buffer = Encoding.ASCII.GetBytes(boundary + Environment.NewLine);
				requestStream.Write(buffer, 0, buffer.Length);
				buffer = Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"{2}", name, filename, Environment.NewLine));
				requestStream.Write(buffer, 0, buffer.Length);
				buffer = Encoding.ASCII.GetBytes(string.Format("Content-Type: {0}{1}{1}", contentType, Environment.NewLine));
				requestStream.Write(buffer, 0, buffer.Length);
				fileStream.CopyTo(requestStream);
				buffer = Encoding.ASCII.GetBytes(Environment.NewLine);
				requestStream.Write(buffer, 0, buffer.Length);

				var boundaryBuffer = Encoding.ASCII.GetBytes(boundary + "--");
				requestStream.Write(boundaryBuffer, 0, boundaryBuffer.Length);
			}

			request.Timeout = 1000 * 60 * 2;

			using (var response = request.GetResponse())
			using (var responseStream = response.GetResponseStream())
			using (var stream = new MemoryStream())
			{
				responseStream.CopyTo(stream);
				return stream.ToArray();
			}
		}

		public static double ParseNumberField(string str)
		{
			if (string.IsNullOrEmpty(str))
			{
				return 0;
			}
			return Convert.ToDouble(str.Replace(".", "").Replace(",", "."));
		}

		public static string FormatNumberField(double number, int numDecimals = 0)
		{
			return string.Format(new CultureInfo("da-DK"), "{0:n" + numDecimals + "}", number);
		}

		public static string FormatNumberField(decimal number, int numDecimals = 0)
		{
			return string.Format(new CultureInfo("da-DK"), "{0:n" + numDecimals + "}", number);
		}

		public static T TryDeserializeObject<T>(string json)
		{
			return JsonConvert.DeserializeObject<T>(json, new JsonSerializerSettings
			{
				Error = (object sender, Newtonsoft.Json.Serialization.ErrorEventArgs errorArgs) => {
					errorArgs.ErrorContext.Handled = true;
				}
			});
		}

		public static string Base64Encode(string plainText)
		{
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
			return System.Convert.ToBase64String(plainTextBytes);
		}

		public static string GetUserIP()
		{
			string ipAdd = GetRawIP();
			if (string.IsNullOrEmpty(ipAdd) || IsLocalIP(ipAdd) || ipAdd.StartsWith("172.") || ipAdd.StartsWith("192.") || ipAdd.StartsWith("10."))
			{
				ipAdd = "93.176.67.106"; // appstract public ip for local development
			}
			return ipAdd;
		}
		public static string GetUserAgentString()
		{
			return HttpContext.Current.Request.UserAgent;
		}

		public static string GetRawIP()
		{
			string ipAdd = "";
			try
			{
				ipAdd = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
				if (string.IsNullOrEmpty(ipAdd))
				{
					ipAdd = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
				}
				if (string.IsNullOrEmpty(ipAdd) && HttpContext.Current.Request.UserHostAddress.Length != 0)
				{
					ipAdd = HttpContext.Current.Request.UserHostAddress;
				}
			}
			catch { }
			return ipAdd?.Split(',')?.FirstOrDefault(); // IP addresses from Africa sometimes come in groups of public IP addresses seprated by ',' & the first one is relevant one, Bug fixed: thanks to RelyOnNutec
		}
		public static bool IsLocalIP(string ipAdd = "")
		{
			if (string.IsNullOrEmpty(ipAdd))
			{
				ipAdd = GetRawIP();
			}
			return (ipAdd == "::1" || ipAdd == "127.0.0.1");
		}
	}
}
