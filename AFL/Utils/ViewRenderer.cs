﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AFL.Utils
{
	public static class ViewRenderer
	{
		internal class FakeController : Controller
		{
		}

		/// <summary>
		///     Gets the razor view as string.
		/// </summary>
		/// <param name="model">The filtered values.</param>
		/// <param name="filePath">The file path.</param>
		/// <returns></returns>
		public static string GetRazorViewAsString(object model, string filePath)

		{
			var st = new StringWriter();
			var context = new HttpContextWrapper(HttpContext.Current);
			var routeData = new RouteData();
			var controllerContext = new ControllerContext(new RequestContext(context, routeData), new FakeController());

			routeData.Values.Add("controller", "null");
			controllerContext.RouteData = routeData;

			var razor = new RazorView(controllerContext, filePath, null, false, null);
			razor.Render(new ViewContext(controllerContext, razor, new ViewDataDictionary(model), new TempDataDictionary(), st),st);

			return st.ToString();
		}

	}
}
