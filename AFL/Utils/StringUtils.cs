﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Hosting;

namespace AFL.Utils
{
	public static class StringUtils
	{
		public static bool IsNullOrWhitespace(this string str)
		{
			return string.IsNullOrWhiteSpace(str);
		}

		public static bool IsNullOrEmpty(this string str)
		{
			return string.IsNullOrEmpty(str);
		}
		public static string ReplaceLastOccurence(this string str, string find, string replace)
		{
			var index = str.LastIndexOf(find);
			if (index > -1)
			{
				str = str.Substring(0, index) + str.Substring(index).Replace(find, replace);
			}
			return str;
		}

		public static string GetFileContent(string fileName)
		{
			try
			{
				var stream = new StreamReader(HostingEnvironment.MapPath("~") + fileName);
				return stream.ReadToEnd();
			}
			catch(Exception e)
			{
				return e.Message;
			}
		}
		/// <summary>
		/// Flattens UTF-8 to ASCII so we can search it out again. æ becomes a, ø becomes o, å becomes a
		/// </summary>
		/// <param name="q"></param>
		/// <returns></returns>
		public static string SearchFriendlyString(this string q)
		{
			byte[] tempBytes;
			tempBytes = Encoding.GetEncoding("ISO-8859-8").GetBytes(q);
			return Encoding.UTF8.GetString(tempBytes);
		}

		public static string StripHtml(string str)
		{
			if (string.IsNullOrEmpty(str)) { return ""; }
			HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
			doc.LoadHtml(str);
			StringBuilder sb = new StringBuilder();
			foreach (var node in doc.DocumentNode.ChildNodes)
			{
				sb.Append(node.InnerText);
			}
			return sb.ToString();
		}

		public static string CombineUri(params string[] uriParts)
		{
			string uri = string.Empty;
			if (uriParts != null && uriParts.Count() > 0)
			{
				char[] trims = new char[] { '\\', '/' };
				uri = (uriParts[0] ?? string.Empty).TrimEnd(trims);
				for (int i = 1; i < uriParts.Count(); i++)
				{
					uri = string.Format("{0}/{1}", uri.TrimEnd(trims), (uriParts[i] ?? string.Empty).TrimStart(trims));
				}
			}
			return uri;
		}
	}
}
