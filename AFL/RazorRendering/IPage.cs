﻿namespace AFL.RazorRendering
{
	public interface IPage
	{
		dynamic Model
		{
			get;
			set;
		}

		string ExecuteAndGetResult();
	}
}
