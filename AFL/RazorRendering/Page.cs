﻿using System.IO;
using System.Web;
using System.Web.WebPages;
using System.Web.WebPages.Instrumentation;

namespace AFL.RazorRendering
{
	public class Page : WebPageBase, IPage
	{
		private readonly StringWriter writer = new StringWriter();

		public dynamic Model
		{
			get;
			set;
		}

		public override void Execute()
		{
			// do nothing
			// TODO explain why
		}

		public override void WriteLiteral(object value)
		{
			this.writer.Write(value);
		}

		public override void Write(object value)
		{
			this.writer.Write(value);
		}

		public override void Write(HelperResult result)
		{
			if (result != null)
			{
				result.WriteTo(writer);
			}
		}

		protected override TextWriter GetOutputWriter()
		{
			return this.writer;
		}

		public override void WriteAttribute(string name, PositionTagged<string> prefix, PositionTagged<string> suffix, params AttributeValue[] values)
		{
			if (prefix != null)
			{
				this.writer.Write(prefix.Value);
			}

			foreach (var value in values)
			{
				this.writer.Write(value.Value);
			}

			if (suffix != null)
			{
				this.writer.Write(suffix);
			}
		}

		public string ExecuteAndGetResult()
		{
			this.Execute();
			var result = this.writer.ToString();
			return result;
		}

		public IHtmlString Raw(string value)
		{
			return new HtmlString(value);
		}
	}
}
