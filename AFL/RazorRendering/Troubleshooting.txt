﻿:: ISSUE :: Exception is thrown when calling CompileAssemblyFromDom

This is the exception 
System.IO.DirectoryNotFoundException: 'Could not find a part of the path '<path to your project>\bin\Debug\bin\roslyn\csc.exe'.'

Fix:
1) Uninstall NuGet package Microsoft.CodeDom.Providers.DotNetCompilerPlatform 2.0.0
2) Install NuGet package Microsoft.CodeDom.Providers.DotNetCompilerPlatform 2.0.0
3) This will update your config file and now it should work


:: OTHER ISSUES ::

Ensure all required assemblies are loaded before invoking Razor.Evaluate method
Ensure that required namespaces are added to either the view or using the engineHost.NamespaceImports.Add() method in the Razor.GenerateType method
