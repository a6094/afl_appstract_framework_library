﻿namespace AFL.Search
{
	public class SearchBoostValue
	{
		public SearchBoostValue(string field, double boost)
		{
			Field = field;
			Boost = boost;
		}

		public string Field { get; private set; }
		public double Boost { get; private set; }
	}
}
