﻿using System.Collections.Generic;

namespace AFL.Search
{
	public interface ISearchDefinitions
	{
		IEnumerable<SearchIndex> SearchIndexes { get; }

		void AddCustomFields(SearchIndex index, IAFLModel entity, IDictionary<string, List<object>> fields);
		
		bool PreventIndexing(object node, SearchIndex index);
		bool PreventIndexing(IAFLModel model, SearchIndex index);
	}
}
