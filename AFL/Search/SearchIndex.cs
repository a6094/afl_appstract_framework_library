﻿using System;
using System.Linq;

namespace AFL.Search
{
	/// <summary>
	/// initialize some of these in a project-specific static class, into readonly static properties with useful names,
	/// one for each index, so the different indexes can be conveniently accessed.
	/// </summary>
	public class SearchIndex
	{
        public SearchIndex(int id, string indexName) 
        {
            ID = id;
            IndexName = indexName;
            PublishedValuesOnly = true;
            ParentID = null;
            IncludeItemTypes = Array.Empty<string>();
            ExcludeItemTypes = Array.Empty<string>();
        }
        public SearchIndex(int id, string indexName, bool publishedValuesOnly, int? parentID) : this(id, indexName)
        {
            PublishedValuesOnly = publishedValuesOnly;
            ParentID = parentID;
        }
        public SearchIndex(int id, string indexName, bool publishedValuesOnly = true, int? parentID = null, string[] includeItemTypes = null, string[] excludeItemTypes = null) : this(id, indexName, publishedValuesOnly, parentID)
        {
            IncludeItemTypes = includeItemTypes ?? Array.Empty<string>();
            ExcludeItemTypes = excludeItemTypes ?? Array.Empty<string>();
		}

        /// <summary>
        /// Custom Search Index for umbraco
        /// </summary>
        /// <param name="id">Unique ID for the index</param>
        /// <param name="indexName">Name of the index</param>
        /// <param name="publishedValuesOnly"></param>
        /// <param name="parentID"></param>
        /// <param name="includeItemTypes">AFL types to include</param>
        /// <param name="excludeItemTypes">AFL types to exclude</param>
        public SearchIndex(int id, string indexName, bool publishedValuesOnly = true, int? parentID = null, Type[] includeItemTypes = null, Type[] excludeItemTypes = null) : this(id, indexName, publishedValuesOnly, parentID)
        {
            throw new NotImplementedException("This doesn't work in the current state - it will cause a race between AFL initialized and this");
            IncludeItemTypes = includeItemTypes.Select(x => Registry.Current.GetInfo(x.GetType()).Alias).ToArray();
            ExcludeItemTypes = excludeItemTypes.Select(x => Registry.Current.GetInfo(x.GetType()).Alias).ToArray();
        }
		public int ID { get; protected set; }
		public string Searcher { get; protected set; }
		public string IndexName { get; protected set; }

        public bool PublishedValuesOnly { get; protected set; }
        public int? ParentID { get; protected set; }
        public string[] IncludeItemTypes { get; protected set; }
        public string[] ExcludeItemTypes { get; protected set; }
	}
}
