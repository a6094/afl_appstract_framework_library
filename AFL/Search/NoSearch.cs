﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AFL.Search
{
	public sealed class NoSearch : ISearchDefinitions
	{
		public IEnumerable<SearchIndex> SearchIndexes => Enumerable.Empty<SearchIndex>();

		public void AddCustomFields(SearchIndex index, IAFLModel entity, IDictionary<string, List<object>> fields)
		{
			throw new NotImplementedException();
		}

		public bool PreventIndexing(object node, SearchIndex index) { return false; }

		public bool PreventIndexing(IAFLModel model, SearchIndex index) { return false; }
	}
}
